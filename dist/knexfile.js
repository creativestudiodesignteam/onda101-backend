"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const dotenv = require("dotenv");
dotenv.config();
const dev_1 = require("./database/connections/dev");
const production_1 = require("./database/connections/production");
const migrationsFolder = "./database/migrations";
const seedsFolder = "./database/seeds";
const migrationsConfig = {
    tableName: "knex_migrations",
    directory: migrationsFolder,
};
const seedsConfig = {
    directory: seedsFolder,
};
module.exports = {
    development: {
        client: process.env.DB_TYPE,
        connection: dev_1.default,
        migrations: migrationsConfig,
        seeds: seedsConfig,
    },
    production: {
        client: process.env.DB_TYPE,
        connection: production_1.default,
        pool: {
            min: 2,
            max: 10,
        },
        migrations: migrationsConfig,
    },
};
//# sourceMappingURL=knexfile.js.map