export declare class EncryptedService {
    encrypt(data: string): Promise<any>;
    compare(data: string, encrypted: string): Promise<any>;
}
