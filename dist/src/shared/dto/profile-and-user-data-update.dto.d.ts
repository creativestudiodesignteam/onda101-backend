import { ProfileOnlyEditDto } from "./profile-only-edit.dto";
import { UserOnlyEditDto } from "./user-only-edit.dto";
export declare class ProfileAndUserDataUpdateDto {
    user: UserOnlyEditDto;
    profile: ProfileOnlyEditDto;
    password: string;
}
