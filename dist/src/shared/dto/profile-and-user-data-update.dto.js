"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProfileAndUserDataUpdateDto = void 0;
const class_transformer_1 = require("class-transformer");
const class_validator_1 = require("class-validator");
const profile_only_edit_dto_1 = require("./profile-only-edit.dto");
const user_only_edit_dto_1 = require("./user-only-edit.dto");
class ProfileAndUserDataUpdateDto {
}
__decorate([
    class_validator_1.ValidateNested(),
    class_validator_1.IsOptional(),
    __metadata("design:type", user_only_edit_dto_1.UserOnlyEditDto)
], ProfileAndUserDataUpdateDto.prototype, "user", void 0);
__decorate([
    class_validator_1.ValidateNested(),
    class_transformer_1.Type(() => profile_only_edit_dto_1.ProfileOnlyEditDto),
    class_validator_1.IsOptional(),
    __metadata("design:type", profile_only_edit_dto_1.ProfileOnlyEditDto)
], ProfileAndUserDataUpdateDto.prototype, "profile", void 0);
__decorate([
    class_validator_1.IsString(),
    __metadata("design:type", String)
], ProfileAndUserDataUpdateDto.prototype, "password", void 0);
exports.ProfileAndUserDataUpdateDto = ProfileAndUserDataUpdateDto;
//# sourceMappingURL=profile-and-user-data-update.dto.js.map