export declare class ProfileOnlyEditDto {
    first_name: string;
    last_name: string;
    constructor(first_name: string, last_name: string);
}
