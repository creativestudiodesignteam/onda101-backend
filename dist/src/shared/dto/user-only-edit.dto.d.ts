export declare class UserOnlyEditDto {
    username: string;
    email: string;
    constructor(username: string, email: string);
}
