"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SharedModule = void 0;
const validation_pipe_1 = require("./pipes/validation.pipe");
const common_1 = require("@nestjs/common");
const encrypted_service_1 = require("./encrypted/encrypted.service");
const upload_files_service_1 = require("./upload-files/upload-files.service");
const mail_sender_service_1 = require("./mail-sender/mail-sender.service");
let SharedModule = class SharedModule {
};
SharedModule = __decorate([
    common_1.Module({
        providers: [
            validation_pipe_1.ValidationPipe,
            encrypted_service_1.EncryptedService,
            upload_files_service_1.UploadFilesService,
            mail_sender_service_1.MailSenderService,
        ],
        exports: [validation_pipe_1.ValidationPipe, encrypted_service_1.EncryptedService, mail_sender_service_1.MailSenderService],
        imports: [encrypted_service_1.EncryptedService],
    })
], SharedModule);
exports.SharedModule = SharedModule;
//# sourceMappingURL=shared.module.js.map