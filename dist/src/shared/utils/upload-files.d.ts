export declare const imageFileFilter: (req: any, file: any, callback: any) => any;
export declare const editFileName: (req: any, file: any, callback: any) => void;
export declare const deleteFile: (path: any, filename: any, callback: any) => Promise<void>;
