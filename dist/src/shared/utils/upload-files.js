"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteFile = exports.editFileName = exports.imageFileFilter = void 0;
const path_1 = require("path");
const fs_1 = require("fs");
exports.imageFileFilter = (req, file, callback) => {
    if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
        return callback(null, false);
    }
    callback(null, true);
};
exports.editFileName = (req, file, callback) => {
    const name = file.originalname.split('.')[0];
    const fileExtName = path_1.extname(file.originalname);
    const randomName = Array(4)
        .fill(null)
        .map(() => Math.round(Math.random() * 16).toString(16))
        .join('');
    callback(null, `${Date.now()}-${randomName}${fileExtName}`);
};
exports.deleteFile = async (path, filename, callback) => {
    const userAvatarFilePath = path_1.join(path, filename);
    const userAvatarFileExists = await fs_1.promises.stat(userAvatarFilePath);
    if (userAvatarFileExists) {
        await fs_1.promises.unlink(userAvatarFilePath);
    }
    callback(null, true);
};
//# sourceMappingURL=upload-files.js.map