"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MailSenderService = void 0;
const common_1 = require("@nestjs/common");
const nodemailer = require("nodemailer");
const mail_config_1 = require("../../../mail.config");
let MailSenderService = class MailSenderService {
    async sendEmail(to, message) {
        try {
            const transporter = nodemailer.createTransport({
                host: mail_config_1.config.host,
                secure: true,
                port: mail_config_1.config.port,
                auth: {
                    user: mail_config_1.config.user,
                    pass: mail_config_1.config.password,
                },
            });
            const info = await transporter.sendMail({
                from: `Equipe Onda101 <${mail_config_1.config.user}>`,
                to,
                subject: "Onda 101 - Códigos de acesso ao App",
                text: message,
            });
        }
        catch (error) {
            console.log(error);
            throw error;
        }
    }
    sendEmails(emails) {
        return;
    }
};
MailSenderService = __decorate([
    common_1.Injectable()
], MailSenderService);
exports.MailSenderService = MailSenderService;
//# sourceMappingURL=mail-sender.service.js.map