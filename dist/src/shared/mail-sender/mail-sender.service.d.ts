import { MailSender } from "./models/mail-sender";
export declare class MailSenderService implements MailSender {
    sendEmail(to: string, message: string): Promise<void>;
    sendEmails(emails: string[]): void;
}
