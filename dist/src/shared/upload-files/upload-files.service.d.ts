import { CompressDto } from "./dto/compress.dto";
export declare class UploadFilesService {
    delete(path: any, filename: any): Promise<void>;
    compress(file: any, quality: number, size: number): Promise<CompressDto>;
}
