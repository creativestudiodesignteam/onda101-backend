"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UnexpectedErrorToSendImage = void 0;
class UnexpectedErrorToSendImage extends Error {
    constructor() {
        super(...arguments);
        this.message = "Erro inesperado ao enviar uma imagem!";
    }
}
exports.UnexpectedErrorToSendImage = UnexpectedErrorToSendImage;
//# sourceMappingURL=UnexpectedError.js.map