"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FileIsNotExists = void 0;
class FileIsNotExists extends Error {
    constructor() {
        super(...arguments);
        this.message = "Arquivo inexistente";
    }
}
exports.FileIsNotExists = FileIsNotExists;
//# sourceMappingURL=FileIsNotExists.js.map