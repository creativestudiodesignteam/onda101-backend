export declare class UnexpectedErrorToSendImage extends Error {
    readonly message = "Erro inesperado ao enviar uma imagem!";
}
