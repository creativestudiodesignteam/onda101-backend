export declare class FileIsNotExists extends Error {
    readonly message = "Arquivo inexistente";
}
