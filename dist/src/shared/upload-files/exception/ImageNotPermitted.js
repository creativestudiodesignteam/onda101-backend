"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ImageNotPermited = void 0;
class ImageNotPermited extends Error {
    constructor() {
        super(...arguments);
        this.message = "Somente arquivos de imagem são permitidos!";
    }
}
exports.ImageNotPermited = ImageNotPermited;
//# sourceMappingURL=ImageNotPermitted.js.map