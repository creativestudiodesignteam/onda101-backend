"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UploadFilesService = void 0;
const path_1 = require("path");
const fs_1 = require("fs");
const common_1 = require("@nestjs/common");
const FileIsNotExists_1 = require("./exception/FileIsNotExists");
const sharp = require("sharp");
const compress_dto_1 = require("./dto/compress.dto");
const UnexpectedError_1 = require("./exception/UnexpectedError");
let UploadFilesService = class UploadFilesService {
    async delete(path, filename) {
        const filePath = path_1.join(path, filename);
        const fileExists = await fs_1.promises.stat(filePath);
        if (!fileExists) {
            throw new FileIsNotExists_1.FileIsNotExists();
        }
        await fs_1.promises.unlink(filePath);
    }
    async compress(file, quality, size) {
        const newPath = file.path.split(".")[0] + ".webp";
        const compressed = await sharp(file.path)
            .resize(size !== null && size !== void 0 ? size : 80)
            .toFormat("webp")
            .webp({
            quality: quality !== null && quality !== void 0 ? quality : 80,
        })
            .toBuffer()
            .then(async (data) => {
            const fileExists = await fs_1.promises.stat(file.path);
            if (!fileExists) {
                throw new FileIsNotExists_1.FileIsNotExists();
            }
            try {
                await fs_1.promises.writeFile(newPath, data);
            }
            catch (err) {
                if (err) {
                    console.log("erro send image line 46-->", err);
                    await fs_1.promises.unlink(newPath);
                    throw new UnexpectedError_1.UnexpectedErrorToSendImage();
                }
            }
            await fs_1.promises.unlink(file.path);
            return newPath;
        });
        const brokenToString = compressed.split("/");
        let pathString = "";
        for (let cont = 0; cont < brokenToString.length - 1; cont++) {
            pathString = pathString + "/" + brokenToString[cont];
        }
        console.log(brokenToString);
        return new compress_dto_1.CompressDto(pathString, brokenToString[brokenToString.length - 1]);
    }
};
UploadFilesService = __decorate([
    common_1.Injectable()
], UploadFilesService);
exports.UploadFilesService = UploadFilesService;
//# sourceMappingURL=upload-files.service.js.map