export declare class CompressDto {
    path: string;
    filename: string;
    constructor(path: string, filename: string);
}
