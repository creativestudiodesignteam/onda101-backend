"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CompressDto = void 0;
class CompressDto {
    constructor(path, filename) {
        this.path = path;
        this.filename = filename;
    }
}
exports.CompressDto = CompressDto;
//# sourceMappingURL=compress.dto.js.map