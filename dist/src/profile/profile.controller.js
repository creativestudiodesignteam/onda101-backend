"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProfileController = void 0;
const common_1 = require("@nestjs/common");
const profile_service_1 = require("./profile.service");
const profile_not_exists_1 = require("./exception/profile-not-exists");
const custom_success_response_dto_1 = require("./dto/custom-success-response.dto");
const profile_query_params_dto_1 = require("./dto/profile-query-params.dto");
const profile_list_user_dto_1 = require("./dto/profile-list-user.dto");
const profile_realtions_dto_1 = require("./dto/profile-realtions.dto");
const jwt_auth_guard_1 = require("../auth/jwt-auth.guard");
const platform_express_1 = require("@nestjs/platform-express");
const multer_1 = require("multer");
const upload_files_1 = require("../shared/utils/upload-files");
const file_not_found_1 = require("../exception/file-not-found");
const rxjs_1 = require("rxjs");
const path_1 = require("path");
const fs_1 = require("fs");
const upload_files_service_1 = require("../shared/upload-files/upload-files.service");
let ProfileController = class ProfileController {
    constructor(profileService, uploadFilesService) {
        this.profileService = profileService;
        this.uploadFilesService = uploadFilesService;
    }
    async findBySearch(queryParams) {
        try {
            const findedUsers = await this.profileService.findUser(queryParams);
            const paginatedList = new profile_list_user_dto_1.ProfileListDto(findedUsers, queryParams.page, queryParams.perPage);
            return paginatedList.getPaginatedData();
        }
        catch (error) {
            throw new common_1.InternalServerErrorException({
                message: "Alguma coisa deu errado. Tente novamente em alguns instantes...",
            });
        }
    }
    async findById(req) {
        try {
            const findProfileByUser = await this.profileService.findByUser(req.user.id);
            const findedProfile = await this.profileService.findById(findProfileByUser.id);
            return new profile_realtions_dto_1.ProfileRelationsDto(findedProfile.id, findedProfile.user, findedProfile.first_name, findedProfile.last_name, findedProfile.birth_date, findedProfile.total_score, findedProfile.challengeLevel, findedProfile.group, findedProfile.avatar);
        }
        catch (error) {
            if (error instanceof profile_not_exists_1.ProfileNotExists)
                throw new common_1.NotFoundException(error.message);
            throw new common_1.InternalServerErrorException({
                error,
                message: "Alguma coisa deu errado. Tente novamente em alguns instantes...",
            });
        }
    }
    async delete(req) {
        try {
            const findProfileByUser = await this.profileService.findByUser(req.user.id);
            await this.profileService.delete(findProfileByUser.id);
            return new custom_success_response_dto_1.CustomSuccessResponse("Perfil excluído com sucesso");
        }
        catch (error) {
            if (error instanceof profile_not_exists_1.ProfileNotExists)
                throw new common_1.NotFoundException(error.message);
            throw new common_1.InternalServerErrorException({
                message: "Alguma coisa deu errado. Tente novamente em alguns instantes...",
            });
        }
    }
    async setProfileAvatar(req, file) {
        const profile = await this.profileService.findByUser(req.user.id);
        const compressFile = await this.uploadFilesService.compress(file, 80, 500);
        profile.avatar = compressFile.filename;
        await this.profileService.updateKnownProfile(profile);
        return {
            message: "avatar atualizado com sucesso!",
        };
    }
    async getProfileAvatar(req, res) {
        var _a;
        try {
            const profile = await this.profileService.findByUser(req.user.id);
            const fileName = (_a = profile.avatar) !== null && _a !== void 0 ? _a : "";
            if (fileName !== "") {
                const filePath = await this.getPathToAvatar(fileName, "upload/profile-avatar");
                return rxjs_1.of(res.sendFile(path_1.join(process.cwd(), filePath)));
            }
            else {
                throw new common_1.NotFoundException("Perfil ainda não tem um avatar");
            }
        }
        catch (error) {
            if (error instanceof file_not_found_1.FileNotFound || error instanceof common_1.NotFoundException) {
                throw new common_1.NotFoundException(error.message);
            }
            throw new common_1.InternalServerErrorException({
                message: "Alguma coisa deu errado. Tente novamente em alguns instantes...",
            });
        }
    }
    async getPathToAvatar(filename, path) {
        const filePath = path_1.join(path, filename);
        const checkFileExists = fs_1.existsSync(filePath);
        if (!checkFileExists) {
            throw new file_not_found_1.FileNotFound();
        }
        return filePath;
    }
};
__decorate([
    common_1.Get(),
    __param(0, common_1.Query(new common_1.ValidationPipe())),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [profile_query_params_dto_1.ProfileQueryParams]),
    __metadata("design:returntype", Promise)
], ProfileController.prototype, "findBySearch", null);
__decorate([
    common_1.Get("/find"),
    common_1.UseGuards(jwt_auth_guard_1.JwtAuthGuard),
    common_1.HttpCode(200),
    __param(0, common_1.Request()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ProfileController.prototype, "findById", null);
__decorate([
    common_1.Delete(),
    common_1.UseGuards(jwt_auth_guard_1.JwtAuthGuard),
    common_1.HttpCode(200),
    __param(0, common_1.Request()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ProfileController.prototype, "delete", null);
__decorate([
    common_1.Post("set-avatar"),
    common_1.HttpCode(200),
    common_1.UseGuards(jwt_auth_guard_1.JwtAuthGuard),
    common_1.UseInterceptors(platform_express_1.FileInterceptor("file", {
        fileFilter: (req, file, cb) => {
            if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
                req.fileErrors = {
                    error: true,
                };
                return cb(null, false);
            }
            return cb(null, true);
        },
        storage: multer_1.diskStorage({
            destination: "./upload/profile-avatar",
            filename: upload_files_1.editFileName,
        }),
    })),
    __param(0, common_1.Req()), __param(1, common_1.UploadedFile()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], ProfileController.prototype, "setProfileAvatar", null);
__decorate([
    common_1.Get("avatar"),
    common_1.HttpCode(200),
    common_1.UseGuards(jwt_auth_guard_1.JwtAuthGuard),
    __param(0, common_1.Req()), __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], ProfileController.prototype, "getProfileAvatar", null);
ProfileController = __decorate([
    common_1.Controller("profile"),
    __metadata("design:paramtypes", [profile_service_1.ProfileService,
        upload_files_service_1.UploadFilesService])
], ProfileController);
exports.ProfileController = ProfileController;
//# sourceMappingURL=profile.controller.js.map