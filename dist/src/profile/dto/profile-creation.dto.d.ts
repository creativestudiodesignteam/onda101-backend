export declare class ProfileCreationDto {
    firstName: string;
    lastName: string;
    birthDate: string;
    constructor(firstName: string, lastName: string, birthDate: string);
    getFirstName(): string;
    setFirstName(firstName: string): void;
    getLastName(): string;
    setLastName(lastName: string): void;
}
