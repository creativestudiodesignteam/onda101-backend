"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProfileDto = void 0;
class ProfileDto {
    constructor(id, first_name, last_name, birth_date, group_name, user_id, created_at, updated_at) {
        this.id = id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.birth_date = birth_date;
        this.group_name = group_name;
        this.user_id = user_id;
        this.created_at = created_at !== null && created_at !== void 0 ? created_at : undefined;
        this.updated_at = updated_at !== null && updated_at !== void 0 ? updated_at : undefined;
    }
}
exports.ProfileDto = ProfileDto;
//# sourceMappingURL=profile.dto.js.map