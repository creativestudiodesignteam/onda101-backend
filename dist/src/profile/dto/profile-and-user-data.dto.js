"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProfileAndUserDataDto = void 0;
class ProfileAndUserDataDto {
    constructor(username, email, firstName, lastName) {
        this.username = username;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
    }
}
exports.ProfileAndUserDataDto = ProfileAndUserDataDto;
//# sourceMappingURL=profile-and-user-data.dto.js.map