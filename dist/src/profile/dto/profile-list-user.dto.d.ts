import { PaginatedList } from "./../paginated-list";
import { Profile } from "../profile.entity";
export declare class ProfileListDto implements PaginatedList {
    profiles: Profile[];
    page: number;
    perPage: number;
    constructor(profiles: Profile[], page: number, perPage: number);
    getPaginatedData(): {
        pagination: {
            page: number;
            perPage: number;
        };
        profiles: Profile[];
    };
}
