import { ProfileAndUserData } from "../models/profile-and-user-data";
export declare class ProfileAndUserDataDto implements ProfileAndUserData {
    username: string;
    email: string;
    firstName: string;
    lastName: string;
    constructor(username: string, email: string, firstName: string, lastName: string);
}
