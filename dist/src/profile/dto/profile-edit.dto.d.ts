export declare class ProfileEditDto {
    id: number;
    first_name: string;
    last_name: string;
    birth_date: Date;
    group_name: string;
    user_id: number;
    created_at?: Date;
    updated_at?: Date;
    constructor(id: number, first_name: string, last_name: string, birth_date: Date, group_name: string, user_id: number, created_at?: Date, updated_at?: Date);
}
