"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProfileEditDto = void 0;
const class_validator_1 = require("class-validator");
class ProfileEditDto {
    constructor(id, first_name, last_name, birth_date, group_name, user_id, created_at, updated_at) {
        this.id = id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.birth_date = birth_date;
        this.group_name = group_name;
        this.user_id = user_id;
        this.created_at = created_at !== null && created_at !== void 0 ? created_at : undefined;
        this.updated_at = updated_at !== null && updated_at !== void 0 ? updated_at : undefined;
    }
}
__decorate([
    class_validator_1.IsOptional(),
    __metadata("design:type", String)
], ProfileEditDto.prototype, "first_name", void 0);
__decorate([
    class_validator_1.IsOptional(),
    __metadata("design:type", String)
], ProfileEditDto.prototype, "last_name", void 0);
__decorate([
    class_validator_1.IsOptional(),
    __metadata("design:type", Date)
], ProfileEditDto.prototype, "birth_date", void 0);
__decorate([
    class_validator_1.IsOptional(),
    __metadata("design:type", String)
], ProfileEditDto.prototype, "group_name", void 0);
__decorate([
    class_validator_1.IsOptional(),
    __metadata("design:type", Number)
], ProfileEditDto.prototype, "user_id", void 0);
exports.ProfileEditDto = ProfileEditDto;
//# sourceMappingURL=profile-edit.dto.js.map