"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProfileListDto = void 0;
class ProfileListDto {
    constructor(profiles, page, perPage) {
        this.profiles = profiles;
        this.page = page;
        this.perPage = perPage;
    }
    getPaginatedData() {
        return {
            pagination: {
                page: this.page,
                perPage: this.perPage,
            },
            profiles: this.profiles,
        };
    }
}
exports.ProfileListDto = ProfileListDto;
//# sourceMappingURL=profile-list-user.dto.js.map