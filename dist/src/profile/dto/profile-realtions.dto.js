"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProfileRelationsDto = void 0;
const challenge_level_entity_1 = require("../../challenge/challenge-level/challenge-level.entity");
const user_entity_1 = require("../../user/user.entity");
class ProfileRelationsDto {
    constructor(id, user, firstName, lastName, birth_date, total_score, challengeLevel, group, avatar) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birth_date = birth_date;
        this.total_score = total_score;
        this.user = user;
        this.challengeLevel = challengeLevel;
        this.group = group;
        this.avatar = avatar ? this.toDownloadUrl() : null;
    }
    toDownloadUrl() {
        return `${process.env.SERVER_DOMAIN}/profile/avatar`;
    }
}
exports.ProfileRelationsDto = ProfileRelationsDto;
//# sourceMappingURL=profile-realtions.dto.js.map