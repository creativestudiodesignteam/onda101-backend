import { ChallengeLevel } from "src/challenge/challenge-level/challenge-level.entity";
import { User } from "src/user/user.entity";
import { Groups } from "../groups/groups.entity";
import { ProfileRelations } from "../models/profile-relations";
export declare class ProfileRelationsDto implements ProfileRelations {
    id: number;
    firstName: string;
    lastName: string;
    birth_date: Date;
    avatar: string | null;
    total_score: number;
    challengeLevel: ChallengeLevel;
    user: User;
    group: Groups;
    constructor(id: number, user: User, firstName: string, lastName: string, birth_date: Date, total_score: number, challengeLevel: ChallengeLevel, group: Groups, avatar: string);
    private toDownloadUrl;
}
