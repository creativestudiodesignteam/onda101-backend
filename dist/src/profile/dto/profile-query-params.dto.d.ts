export declare class ProfileQueryParams {
    user: string;
    perPage: number;
    page: number;
}
