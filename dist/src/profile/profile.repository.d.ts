import { Profile } from "./profile.entity";
import { Repository } from "typeorm";
import { ProfileQueryParams } from "./dto/profile-query-params.dto";
import { AllowedProfilerelation } from "./models/allowed-profile-relation";
export declare class ProfileRepository extends Repository<Profile> {
    findById(id: number): Promise<Profile>;
    findWithRelation(id: number, relation: AllowedProfilerelation[]): Promise<Profile>;
    findUser(queryParams: ProfileQueryParams): Promise<Profile[]>;
    findBy(paramToSearch: any): Promise<Profile>;
    findByWithRelations(paramToSearch: any, relations: AllowedProfilerelation[]): Promise<Profile>;
    destroy(profile: Profile): Promise<void>;
    getLast(): Promise<Profile>;
}
