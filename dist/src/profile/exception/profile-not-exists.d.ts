export declare class ProfileNotExists extends Error {
    readonly message = "Perfil inexistente";
}
