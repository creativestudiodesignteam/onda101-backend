"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProfileNotExists = void 0;
class ProfileNotExists extends Error {
    constructor() {
        super(...arguments);
        this.message = "Perfil inexistente";
    }
}
exports.ProfileNotExists = ProfileNotExists;
//# sourceMappingURL=profile-not-exists.js.map