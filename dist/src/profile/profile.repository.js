"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProfileRepository = void 0;
const profile_entity_1 = require("./profile.entity");
const typeorm_1 = require("typeorm");
let ProfileRepository = class ProfileRepository extends typeorm_1.Repository {
    async findById(id) {
        return await this.findOne(id);
    }
    findWithRelation(id, relation) {
        return this.findOne({ relations: relation, where: { id } });
    }
    async findUser(queryParams) {
        return this.find({
            first_name: typeorm_1.Like(`%${queryParams.user}%`),
        });
    }
    async findBy(paramToSearch) {
        return await this.findOne({ where: paramToSearch });
    }
    async findByWithRelations(paramToSearch, relations) {
        return await this.findOne({ where: paramToSearch, relations: relations });
    }
    async destroy(profile) {
        await this.remove(profile);
    }
    getLast() {
        return super
            .createQueryBuilder()
            .orderBy("id", "DESC")
            .getOne();
    }
};
ProfileRepository = __decorate([
    typeorm_1.EntityRepository(profile_entity_1.Profile)
], ProfileRepository);
exports.ProfileRepository = ProfileRepository;
//# sourceMappingURL=profile.repository.js.map