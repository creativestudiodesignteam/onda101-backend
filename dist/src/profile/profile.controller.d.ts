import { ProfileService } from "./profile.service";
import { CustomSuccessResponse } from "./dto/custom-success-response.dto";
import { ProfileQueryParams } from "./dto/profile-query-params.dto";
import { ProfileRelationsDto } from "./dto/profile-realtions.dto";
import { UploadFilesService } from "src/shared/upload-files/upload-files.service";
export declare class ProfileController {
    private profileService;
    private uploadFilesService;
    constructor(profileService: ProfileService, uploadFilesService: UploadFilesService);
    findBySearch(queryParams: ProfileQueryParams): Promise<{
        pagination: {
            page: number;
            perPage: number;
        };
        profiles: import("./profile.entity").Profile[];
    }>;
    findById(req: any): Promise<ProfileRelationsDto>;
    delete(req: any): Promise<CustomSuccessResponse>;
    setProfileAvatar(req: any, file: any): Promise<{
        message: string;
    }>;
    getProfileAvatar(req: any, res: any): Promise<import("rxjs").Observable<any>>;
    private getPathToAvatar;
}
