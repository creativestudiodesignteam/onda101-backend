import { ChallengeLevel } from "src/challenge/challenge-level/challenge-level.entity";
import { CompletedChallenge } from "src/challenge/completed-challenge/completed-challenge.entity";
import { CurrentChallengeStatus } from "src/challenge/current-challenge-status/current-challenge-status.entity";
import { ChallengeRating } from "src/challenge/challenge-rating/challenge-rating.entity";
import { RedoneTimes } from "src/challenge/redone-times/redone-times.entity";
import { User } from "../user/user.entity";
import { Groups } from "./groups/groups.entity";
export declare class Profile {
    id: number;
    first_name: string;
    last_name: string;
    birth_date: Date;
    avatar?: string;
    user: User;
    group: Groups;
    completedChallenges: CompletedChallenge;
    total_score: number;
    challengesInProgress: CurrentChallengeStatus[];
    challengeLevel: ChallengeLevel;
    redoneTimes: RedoneTimes[];
    challengeRatings: ChallengeRating[];
    created_at: Date;
    updated_at: Date;
}
