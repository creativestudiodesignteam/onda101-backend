import { Groups } from "./groups.entity";
import { DeepPartial, Repository } from "typeorm";
import { GroupsQueryParams } from "./dto/groups-query-params.dto";
import { GroupsEditDTO } from "./dto/groups-edit.dto";
export declare class GroupsRepository extends Repository<Groups> {
    findAll(queryParams: GroupsQueryParams): Promise<Groups[]>;
    findById(id: number): Promise<Groups>;
    findBy(paramToSearch: any): Promise<Groups>;
    findByAndOrder(): Promise<Groups>;
    store(groupsToBeCreated: Groups): Promise<import("typeorm").InsertResult>;
    updateGroups(groups: DeepPartial<GroupsEditDTO>): Promise<import("typeorm").UpdateResult>;
    destroy(profile: Groups): Promise<void>;
}
