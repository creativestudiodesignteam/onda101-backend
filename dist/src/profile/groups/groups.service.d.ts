import { DeepPartial } from 'typeorm';
import { GroupsCreateDto } from './dto/groups-create.dto';
import { GroupsEditDTO } from './dto/groups-edit.dto';
import { GroupsQueryParams } from './dto/groups-query-params.dto';
import { Groups } from './groups.entity';
import { GroupsRepository } from './groups.repository';
export declare class GroupsService {
    private groupsRepository;
    constructor(groupsRepository: GroupsRepository);
    findAll({ perPage, page }: GroupsQueryParams): Promise<Groups[]>;
    findById(id: number): Promise<Groups>;
    selectGroups(): Promise<Groups>;
    create(groupsToBeCreated: GroupsCreateDto): Promise<Groups>;
    update(id: number, groupsChanges: DeepPartial<GroupsEditDTO>): Promise<Groups>;
    delete(id: number): Promise<void>;
}
