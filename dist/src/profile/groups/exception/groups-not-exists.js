"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GroupsNotExists = void 0;
class GroupsNotExists extends Error {
    constructor() {
        super(...arguments);
        this.message = "Grupo inexistente";
    }
}
exports.GroupsNotExists = GroupsNotExists;
//# sourceMappingURL=groups-not-exists.js.map