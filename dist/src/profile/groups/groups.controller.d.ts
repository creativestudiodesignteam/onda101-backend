import { GroupsDto } from './dto/groups.dto';
import { GroupsService } from './groups.service';
import { GroupsQueryParams } from './dto/groups-query-params.dto';
import { GroupsCreateDto } from './dto/groups-create.dto';
import { GroupsEditDTO } from './dto/groups-edit.dto';
import { CustomSuccessResponse } from './dto/custom-success-response.dto';
export declare class GroupsController {
    private groupsRepository;
    constructor(groupsRepository: GroupsService);
    findAll(queryParams: GroupsQueryParams): Promise<{
        pagination: {
            page: number;
            perPage: number;
        };
        groups: import("./groups.entity").Groups[];
    }>;
    findById(param: any): Promise<GroupsDto>;
    create(groups: GroupsCreateDto): Promise<GroupsDto>;
    update(param: any, groups: GroupsEditDTO): Promise<any>;
    delete(param: any): Promise<CustomSuccessResponse>;
}
