"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GroupsRepository = void 0;
const groups_entity_1 = require("./groups.entity");
const typeorm_1 = require("typeorm");
let GroupsRepository = class GroupsRepository extends typeorm_1.Repository {
    async findAll(queryParams) {
        const groups = await this.find({
            take: queryParams.perPage,
            skip: queryParams.perPage * (queryParams.page - 1),
            order: { id: "ASC" },
        });
        return groups;
    }
    findById(id) {
        return this.findOne(id);
    }
    async findBy(paramToSearch) {
        return await this.findOne({ where: paramToSearch });
    }
    async findByAndOrder() {
        return await this.findOne({ order: { id: "ASC" } });
    }
    async store(groupsToBeCreated) {
        const groups = await this.insert(groupsToBeCreated);
        return groups;
    }
    async updateGroups(groups) {
        return await this.update(groups.id, groups);
    }
    async destroy(profile) {
        await this.remove(profile);
    }
};
GroupsRepository = __decorate([
    typeorm_1.EntityRepository(groups_entity_1.Groups)
], GroupsRepository);
exports.GroupsRepository = GroupsRepository;
//# sourceMappingURL=groups.repository.js.map