"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GroupsListsDto = void 0;
class GroupsListsDto {
    constructor(groups, page, perPage) {
        this.groups = groups;
        this.page = page;
        this.perPage = perPage;
    }
    getPaginatedData() {
        return {
            pagination: {
                page: this.page,
                perPage: this.perPage,
            },
            groups: this.groups,
        };
    }
}
exports.GroupsListsDto = GroupsListsDto;
//# sourceMappingURL=groups-list.dto.js.map