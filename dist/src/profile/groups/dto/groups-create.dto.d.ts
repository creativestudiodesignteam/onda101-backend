export declare class GroupsCreateDto {
    id: number;
    name: string;
    wave_number: number;
    created_at?: Date;
    updated_at?: Date;
    constructor(id: number, name: string, wave_number: number, created_at?: Date, updated_at?: Date);
}
