import { PaginatedList } from "./../paginated-list";
import { Groups } from "./../groups.entity";
export declare class GroupsListsDto implements PaginatedList {
    groups: Groups[];
    page: number;
    perPage: number;
    constructor(groups: Groups[], page: number, perPage: number);
    getPaginatedData(): {
        pagination: {
            page: number;
            perPage: number;
        };
        groups: Groups[];
    };
}
