export declare class GroupsQueryParams {
    perPage: number;
    page: number;
}
