export declare class GroupsEditDTO {
    id: number;
    name: string;
    wave_number: number;
}
