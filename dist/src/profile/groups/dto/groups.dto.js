"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GroupsDto = void 0;
class GroupsDto {
    constructor(id, name, wave_number, created_at, updated_at) {
        this.id = id;
        this.name = name;
        this.wave_number = wave_number;
        this.created_at = created_at !== null && created_at !== void 0 ? created_at : undefined;
        this.updated_at = updated_at !== null && updated_at !== void 0 ? updated_at : undefined;
    }
}
exports.GroupsDto = GroupsDto;
//# sourceMappingURL=groups.dto.js.map