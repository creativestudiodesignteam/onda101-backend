"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GroupsController = void 0;
const common_1 = require("@nestjs/common");
const groups_dto_1 = require("./dto/groups.dto");
const groups_not_exists_1 = require("./exception/groups-not-exists");
const groups_service_1 = require("./groups.service");
const validation_pipe_1 = require("../../shared/pipes/validation.pipe");
const groups_query_params_dto_1 = require("./dto/groups-query-params.dto");
const groups_list_dto_1 = require("./dto/groups-list.dto");
const groups_create_dto_1 = require("./dto/groups-create.dto");
const groups_edit_dto_1 = require("./dto/groups-edit.dto");
const custom_success_response_dto_1 = require("./dto/custom-success-response.dto");
let GroupsController = class GroupsController {
    constructor(groupsRepository) {
        this.groupsRepository = groupsRepository;
    }
    async findAll(queryParams) {
        try {
            const findedGroups = await this.groupsRepository.findAll(queryParams);
            const paginatedList = new groups_list_dto_1.GroupsListsDto(findedGroups, queryParams.page, queryParams.perPage);
            return paginatedList.getPaginatedData();
        }
        catch (error) {
            throw new common_1.InternalServerErrorException({
                message: "Alguma coisa deu errado. Tente novamente em alguns instantes...",
            });
        }
    }
    async findById(param) {
        try {
            const findedGroup = await this.groupsRepository.findById(param.id);
            return new groups_dto_1.GroupsDto(findedGroup.id, findedGroup.name, findedGroup.wave_number);
        }
        catch (error) {
            if (error instanceof groups_not_exists_1.GroupsNotExists)
                throw new common_1.NotFoundException(error.message);
            throw new common_1.InternalServerErrorException({
                message: "Alguma coisa deu errado. Tente novamente em alguns instantes...",
            });
        }
    }
    async create(groups) {
        try {
            const createdGroups = await this.groupsRepository.create(groups);
            return new groups_dto_1.GroupsDto(createdGroups.id, createdGroups.name, createdGroups.wave_number);
        }
        catch (error) {
            throw new common_1.InternalServerErrorException({
                message: "Alguma coisa deu errado. Tente novamente em alguns instantes...",
            });
        }
    }
    async update(param, groups) {
        try {
            const updatedUser = await this.groupsRepository.update(param.id, groups);
            return updatedUser;
        }
        catch (error) {
            if (error instanceof groups_not_exists_1.GroupsNotExists)
                throw new common_1.NotFoundException(error.message);
            throw new common_1.InternalServerErrorException({
                message: "Alguma coisa deu errado. Tente novamente em alguns instantes...",
            });
        }
    }
    async delete(param) {
        try {
            await this.groupsRepository.delete(param.id);
            return new custom_success_response_dto_1.CustomSuccessResponse("Usuário excluído com sucesso");
        }
        catch (error) {
            if (error instanceof groups_not_exists_1.GroupsNotExists) {
                throw new common_1.NotFoundException(error.message);
            }
            throw new common_1.InternalServerErrorException({
                message: "Alguma coisa deu errado. Tente novamente em alguns instantes...",
            });
        }
    }
};
__decorate([
    common_1.UseInterceptors(common_1.ClassSerializerInterceptor),
    common_1.Get(),
    __param(0, common_1.Query(new validation_pipe_1.ValidationPipe())),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [groups_query_params_dto_1.GroupsQueryParams]),
    __metadata("design:returntype", Promise)
], GroupsController.prototype, "findAll", null);
__decorate([
    common_1.Get(':id'),
    common_1.HttpCode(200),
    __param(0, common_1.Param()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], GroupsController.prototype, "findById", null);
__decorate([
    common_1.Post(),
    __param(0, common_1.Body(new validation_pipe_1.ValidationPipe())),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [groups_create_dto_1.GroupsCreateDto]),
    __metadata("design:returntype", Promise)
], GroupsController.prototype, "create", null);
__decorate([
    common_1.Put(":id"),
    __param(0, common_1.Param()),
    __param(1, common_1.Body(new validation_pipe_1.ValidationPipe())),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, groups_edit_dto_1.GroupsEditDTO]),
    __metadata("design:returntype", Promise)
], GroupsController.prototype, "update", null);
__decorate([
    common_1.Delete(":id"),
    common_1.HttpCode(200),
    __param(0, common_1.Param()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], GroupsController.prototype, "delete", null);
GroupsController = __decorate([
    common_1.Controller('groups'),
    __metadata("design:paramtypes", [groups_service_1.GroupsService])
], GroupsController);
exports.GroupsController = GroupsController;
//# sourceMappingURL=groups.controller.js.map