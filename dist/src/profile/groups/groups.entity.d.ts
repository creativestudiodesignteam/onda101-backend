import { Profile } from "../profile.entity";
export declare class Groups {
    id: number;
    name: string;
    wave_number: number;
    profiles: Profile[];
    created_at: Date;
    updated_at: Date;
}
