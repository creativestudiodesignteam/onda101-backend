"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GroupsService = void 0;
const common_1 = require("@nestjs/common");
const groups_not_exists_1 = require("./exception/groups-not-exists");
const groups_entity_1 = require("./groups.entity");
const groups_repository_1 = require("./groups.repository");
let GroupsService = class GroupsService {
    constructor(groupsRepository) {
        this.groupsRepository = groupsRepository;
    }
    async findAll({ perPage = 10, page = 1 }) {
        const findedGroups = await this.groupsRepository.findAll({
            perPage,
            page
        });
        return findedGroups;
    }
    async findById(id) {
        try {
            const findedProfile = await this.groupsRepository.findById(id);
            if (!findedProfile)
                throw new groups_not_exists_1.GroupsNotExists();
            return findedProfile;
        }
        catch (error) {
            throw error;
        }
    }
    async selectGroups() {
        const lastGroup = await this.groupsRepository.findByAndOrder();
        return lastGroup;
    }
    async create(groupsToBeCreated) {
        try {
            const groups = new groups_entity_1.Groups();
            groups.name = groupsToBeCreated.name;
            groups.wave_number = groupsToBeCreated.wave_number;
            await this.groupsRepository.store(groups);
            return groups;
        }
        catch (error) {
            throw error;
        }
    }
    async update(id, groupsChanges) {
        try {
            const findedGroups = await this.groupsRepository.findById(id);
            if (!findedGroups)
                throw new groups_not_exists_1.GroupsNotExists;
            const groups = this.groupsRepository.create(Object.assign({ id }, groupsChanges));
            await this.groupsRepository.updateGroups(groups);
            return groups;
        }
        catch (error) {
            throw error;
        }
    }
    async delete(id) {
        try {
            const groups = await this.groupsRepository.findById(id);
            if (!groups)
                throw new groups_not_exists_1.GroupsNotExists();
            this.groupsRepository.destroy(groups);
        }
        catch (error) {
            throw error;
        }
    }
};
GroupsService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [groups_repository_1.GroupsRepository])
], GroupsService);
exports.GroupsService = GroupsService;
//# sourceMappingURL=groups.service.js.map