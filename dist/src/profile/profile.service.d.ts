import { DeepPartial } from "typeorm";
import { ProfileQueryParams } from "./dto/profile-query-params.dto";
import { AllowedProfilerelation } from "./models/allowed-profile-relation";
import { Profile } from "./profile.entity";
import { ProfileRepository } from "./profile.repository";
export declare class ProfileService {
    private profileRepository;
    constructor(profileRepository: ProfileRepository);
    findById(id: number, withRelations?: AllowedProfilerelation[]): Promise<Profile>;
    findUser({ user, page, perPage }: ProfileQueryParams): Promise<Profile[]>;
    findByUser(user: any): Promise<Profile>;
    findByUserWithRelations(user: any): Promise<Profile>;
    delete(id: number): Promise<any>;
    getLastProfileRegistered(): Promise<Profile>;
    addChallengeScore(score: number, profile: Profile): Promise<void>;
    updateKnownProfile(profile: DeepPartial<Profile>): Promise<DeepPartial<Profile> & Profile>;
}
