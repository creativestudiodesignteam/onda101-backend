"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProfileService = void 0;
const common_1 = require("@nestjs/common");
const profile_not_exists_1 = require("./exception/profile-not-exists");
const profile_repository_1 = require("./profile.repository");
let ProfileService = class ProfileService {
    constructor(profileRepository) {
        this.profileRepository = profileRepository;
    }
    async findById(id, withRelations) {
        try {
            const findedProfile = await this.profileRepository.findWithRelation(id, withRelations !== null && withRelations !== void 0 ? withRelations : ["user", "group"]);
            if (!findedProfile)
                throw new profile_not_exists_1.ProfileNotExists();
            delete findedProfile.user.password;
            return findedProfile;
        }
        catch (error) {
            throw error;
        }
    }
    async findUser({ user, page = 1, perPage = 10 }) {
        try {
            const findedProfile = await this.profileRepository.findUser({
                user,
                page,
                perPage,
            });
            return findedProfile;
        }
        catch (error) {
            throw error;
        }
    }
    async findByUser(user) {
        try {
            const findedProfileByUser = await this.profileRepository.findBy({
                user,
            });
            return findedProfileByUser;
        }
        catch (error) {
            throw error;
        }
    }
    async findByUserWithRelations(user) {
        try {
            const findedProfileByUser = await this.profileRepository.findByWithRelations({
                user,
            }, ["group"]);
            return findedProfileByUser;
        }
        catch (error) {
            throw error;
        }
    }
    async delete(id) {
        const profile = await this.profileRepository.findById(id);
        if (!profile)
            throw new profile_not_exists_1.ProfileNotExists();
        this.profileRepository.destroy(profile);
    }
    async getLastProfileRegistered() {
        const profile = await this.profileRepository.getLast();
        if (!profile) {
            return null;
        }
        else {
            return profile;
        }
    }
    async addChallengeScore(score, profile) {
        const updatedProfile = profile;
        updatedProfile.total_score = updatedProfile.total_score + score;
        await this.profileRepository.save(profile);
    }
    updateKnownProfile(profile) {
        return this.profileRepository.save(profile);
    }
};
ProfileService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [profile_repository_1.ProfileRepository])
], ProfileService);
exports.ProfileService = ProfileService;
//# sourceMappingURL=profile.service.js.map