"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Profile = void 0;
const challenge_level_entity_1 = require("../challenge/challenge-level/challenge-level.entity");
const completed_challenge_entity_1 = require("../challenge/completed-challenge/completed-challenge.entity");
const current_challenge_status_entity_1 = require("../challenge/current-challenge-status/current-challenge-status.entity");
const challenge_rating_entity_1 = require("../challenge/challenge-rating/challenge-rating.entity");
const redone_times_entity_1 = require("../challenge/redone-times/redone-times.entity");
const typeorm_1 = require("typeorm");
const user_entity_1 = require("../user/user.entity");
const groups_entity_1 = require("./groups/groups.entity");
let Profile = class Profile {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], Profile.prototype, "id", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], Profile.prototype, "first_name", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], Profile.prototype, "last_name", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", Date)
], Profile.prototype, "birth_date", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], Profile.prototype, "avatar", void 0);
__decorate([
    typeorm_1.OneToOne(() => user_entity_1.User),
    typeorm_1.JoinColumn({ name: "user_id" }),
    __metadata("design:type", user_entity_1.User)
], Profile.prototype, "user", void 0);
__decorate([
    typeorm_1.ManyToOne(() => groups_entity_1.Groups, group => group.profiles),
    typeorm_1.JoinColumn({ name: "group_id" }),
    __metadata("design:type", groups_entity_1.Groups)
], Profile.prototype, "group", void 0);
__decorate([
    typeorm_1.OneToMany(() => completed_challenge_entity_1.CompletedChallenge, completedChallenge => completedChallenge.profile),
    __metadata("design:type", completed_challenge_entity_1.CompletedChallenge)
], Profile.prototype, "completedChallenges", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", Number)
], Profile.prototype, "total_score", void 0);
__decorate([
    typeorm_1.OneToMany(() => current_challenge_status_entity_1.CurrentChallengeStatus, currentChallengeStatus => currentChallengeStatus.challengeInProgress),
    __metadata("design:type", Array)
], Profile.prototype, "challengesInProgress", void 0);
__decorate([
    typeorm_1.ManyToOne(() => challenge_level_entity_1.ChallengeLevel, challengeLevel => challengeLevel.profiles, { eager: true }),
    typeorm_1.JoinColumn({ name: "challenge_level_id" }),
    __metadata("design:type", challenge_level_entity_1.ChallengeLevel)
], Profile.prototype, "challengeLevel", void 0);
__decorate([
    typeorm_1.OneToMany(() => redone_times_entity_1.RedoneTimes, redoneTimes => redoneTimes.profile),
    __metadata("design:type", Array)
], Profile.prototype, "redoneTimes", void 0);
__decorate([
    typeorm_1.OneToMany(() => challenge_rating_entity_1.ChallengeRating, challengeRating => challengeRating.profile),
    __metadata("design:type", Array)
], Profile.prototype, "challengeRatings", void 0);
__decorate([
    typeorm_1.CreateDateColumn(),
    __metadata("design:type", Date)
], Profile.prototype, "created_at", void 0);
__decorate([
    typeorm_1.UpdateDateColumn(),
    __metadata("design:type", Date)
], Profile.prototype, "updated_at", void 0);
Profile = __decorate([
    typeorm_1.Entity({ name: "profiles" })
], Profile);
exports.Profile = Profile;
//# sourceMappingURL=profile.entity.js.map