import { User } from "src/user/user.entity";
import { Groups } from "../groups/groups.entity";
export interface ProfileRelations {
    user: User;
    firstName: string;
    lastName: string;
    birth_date: Date;
    group: Groups;
}
