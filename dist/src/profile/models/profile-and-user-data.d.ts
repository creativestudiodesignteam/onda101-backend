export interface ProfileAndUserData {
    username: string;
    email: string;
    firstName: string;
    lastName: string;
}
