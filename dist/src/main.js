"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const validation_pipe_1 = require("./shared/pipes/validation.pipe");
const core_1 = require("@nestjs/core");
const app_module_1 = require("./app.module");
const morgan = require("morgan");
async function bootstrap() {
    const app = await core_1.NestFactory.create(app_module_1.AppModule);
    app.use(morgan("tiny"));
    app.useGlobalPipes(new validation_pipe_1.ValidationPipe());
    app.enableCors({
        origin: "*",
        methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
        preflightContinue: false,
    });
    await app.listen(3000);
}
bootstrap();
//# sourceMappingURL=main.js.map