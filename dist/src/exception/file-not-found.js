"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FileNotFound = void 0;
class FileNotFound extends Error {
    constructor() {
        super(...arguments);
        this.message = "Arquivo não encontrado";
    }
}
exports.FileNotFound = FileNotFound;
//# sourceMappingURL=file-not-found.js.map