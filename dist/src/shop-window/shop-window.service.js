"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ShopWindowService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const settings_service_1 = require("./settings/settings.service");
const shop_window_repository_1 = require("./shop-window.repository");
let ShopWindowService = class ShopWindowService {
    constructor(repository, settingsService) {
        this.repository = repository;
        this.settingsService = settingsService;
    }
    async findAll(queryParams) {
        const allProducts = await this.repository.findAll(queryParams);
        const installment = await this.settingsService.findInstallment();
        return { allProducts, installment };
    }
};
ShopWindowService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(shop_window_repository_1.ShopWindowRepository, "ecommerceConnection")),
    __metadata("design:paramtypes", [shop_window_repository_1.ShopWindowRepository,
        settings_service_1.SettingsService])
], ShopWindowService);
exports.ShopWindowService = ShopWindowService;
//# sourceMappingURL=shop-window.service.js.map