import { ShopWindowQueryParams } from "./dto/shop-window-query-params.dto";
import { SettingsService } from "./settings/settings.service";
import { ShopWindowRepository } from "./shop-window.repository";
export declare class ShopWindowService {
    private repository;
    private settingsService;
    constructor(repository: ShopWindowRepository, settingsService: SettingsService);
    findAll(queryParams: ShopWindowQueryParams): Promise<{
        allProducts: import("./shop-window.entity").ShopWindow[];
        installment: import("./settings/settings.entity").Settings;
    }>;
}
