import { Settings } from "../settings/settings.entity";
import { PaginatedList } from "./../paginated-list";
import { ShopWindow } from "./../shop-window.entity";
import { ShopWindowLinks } from "./shop-window-links";
export declare class ShopWindowListDto implements PaginatedList {
    products: ShopWindowLinks[];
    page: number;
    perPage: number;
    constructor(products: ShopWindow[], page: number, perPage: number, installment?: Settings);
    getPaginatedData(): {
        pagination: {
            page: number;
            perPage: number;
        };
        products: ShopWindowLinks[];
    };
}
