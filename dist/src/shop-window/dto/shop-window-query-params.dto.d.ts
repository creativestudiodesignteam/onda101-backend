export declare class ShopWindowQueryParams {
    perPage: number;
    page: number;
}
