"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ShopWindowListDto = void 0;
class ShopWindowListDto {
    constructor(products, page, perPage, installment) {
        const ShopWindowLinks = products.map(result => {
            const transformWithLinks = {
                id: result.product_id,
                name: result.name,
                price: result.price,
                url_image: `${process.env.URL_ECOMMERCE}/image/${result.image}`,
                url_product: `${process.env.URL_ECOMMERCE}/index.php?route=product/product&product_id=${result.product_id}`,
            };
            if (installment) {
                transformWithLinks.installment = installment.value;
            }
            return transformWithLinks;
        });
        this.products = ShopWindowLinks;
        this.page = page;
        this.perPage = perPage;
    }
    getPaginatedData() {
        return {
            pagination: {
                page: this.page,
                perPage: this.perPage,
            },
            products: this.products,
        };
    }
}
exports.ShopWindowListDto = ShopWindowListDto;
//# sourceMappingURL=shop-window-list.js.map