export declare class ShopWindowLinks {
    id: number;
    name: string;
    price: number;
    url_image: string;
    url_product: string;
}
