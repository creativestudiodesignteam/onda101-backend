"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ShopWindowController = void 0;
const common_1 = require("@nestjs/common");
const validation_pipe_1 = require("../shared/pipes/validation.pipe");
const shop_window_list_1 = require("./dto/shop-window-list");
const shop_window_query_params_dto_1 = require("./dto/shop-window-query-params.dto");
const shop_window_service_1 = require("./shop-window.service");
let ShopWindowController = class ShopWindowController {
    constructor(service) {
        this.service = service;
    }
    async findAll({ page = 1, perPage = 10 }) {
        var _a;
        const products = await this.service.findAll({ page, perPage });
        const paginatedList = new shop_window_list_1.ShopWindowListDto(products.allProducts, page, perPage, (_a = products.installment) !== null && _a !== void 0 ? _a : undefined);
        return paginatedList.getPaginatedData();
    }
};
__decorate([
    common_1.Get(),
    __param(0, common_1.Query(new validation_pipe_1.ValidationPipe())),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [shop_window_query_params_dto_1.ShopWindowQueryParams]),
    __metadata("design:returntype", Promise)
], ShopWindowController.prototype, "findAll", null);
ShopWindowController = __decorate([
    common_1.Controller("shop-window"),
    __metadata("design:paramtypes", [shop_window_service_1.ShopWindowService])
], ShopWindowController);
exports.ShopWindowController = ShopWindowController;
//# sourceMappingURL=shop-window.controller.js.map