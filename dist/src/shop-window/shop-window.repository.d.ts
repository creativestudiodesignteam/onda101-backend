import { Repository } from "typeorm";
import { ShopWindowQueryParams } from "./dto/shop-window-query-params.dto";
import { ShopWindow } from "./shop-window.entity";
export declare class ShopWindowRepository extends Repository<ShopWindow> {
    findAll(queryParams: ShopWindowQueryParams): Promise<ShopWindow[]>;
}
