import { ShopWindowQueryParams } from "./dto/shop-window-query-params.dto";
import { ShopWindowService } from "./shop-window.service";
export declare class ShopWindowController {
    private service;
    constructor(service: ShopWindowService);
    findAll({ page, perPage }: ShopWindowQueryParams): Promise<{
        pagination: {
            page: number;
            perPage: number;
        };
        products: import("./dto/shop-window-links").ShopWindowLinks[];
    }>;
}
