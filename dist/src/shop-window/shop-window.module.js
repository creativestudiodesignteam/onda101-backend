"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ShopWindowModule = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const shop_window_repository_1 = require("./shop-window.repository");
const shop_window_controller_1 = require("./shop-window.controller");
const shop_window_service_1 = require("./shop-window.service");
const settings_service_1 = require("./settings/settings.service");
const settings_repository_1 = require("./settings/settings.repository");
let ShopWindowModule = class ShopWindowModule {
};
ShopWindowModule = __decorate([
    common_1.Module({
        imports: [
            typeorm_1.TypeOrmModule.forFeature([shop_window_repository_1.ShopWindowRepository, settings_repository_1.SettingsRepository], "ecommerceConnection"),
        ],
        controllers: [shop_window_controller_1.ShopWindowController],
        providers: [shop_window_service_1.ShopWindowService, settings_service_1.SettingsService],
        exports: [shop_window_service_1.ShopWindowService],
    })
], ShopWindowModule);
exports.ShopWindowModule = ShopWindowModule;
//# sourceMappingURL=shop-window.module.js.map