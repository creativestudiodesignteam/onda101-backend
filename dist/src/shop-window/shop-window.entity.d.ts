export declare class ShopWindow {
    product_id: number;
    name: string;
    quantity: number;
    image: string;
    price: number;
}
