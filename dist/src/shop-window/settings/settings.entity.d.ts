export declare class Settings {
    setting_id: number;
    store_id: string;
    code: number;
    key: string;
    value: number;
    serialized: number;
}
