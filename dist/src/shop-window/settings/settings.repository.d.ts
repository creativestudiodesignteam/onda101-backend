import { Repository } from "typeorm";
import { Settings } from "./settings.entity";
export declare class SettingsRepository extends Repository<Settings> {
    findInstallment(): Promise<Settings>;
}
