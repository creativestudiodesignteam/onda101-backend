import { SettingsRepository } from "./settings.repository";
export declare class SettingsService {
    private repository;
    constructor(repository: SettingsRepository);
    findInstallment(): Promise<import("./settings.entity").Settings>;
}
