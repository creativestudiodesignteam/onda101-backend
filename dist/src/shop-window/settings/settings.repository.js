"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SettingsRepository = void 0;
const typeorm_1 = require("typeorm");
const settings_entity_1 = require("./settings.entity");
let SettingsRepository = class SettingsRepository extends typeorm_1.Repository {
    async findInstallment() {
        const installment = this.findOne({
            where: { setting_id: 1292, code: "payment_mp_standard" },
        });
        return installment;
    }
};
SettingsRepository = __decorate([
    typeorm_1.EntityRepository(settings_entity_1.Settings)
], SettingsRepository);
exports.SettingsRepository = SettingsRepository;
//# sourceMappingURL=settings.repository.js.map