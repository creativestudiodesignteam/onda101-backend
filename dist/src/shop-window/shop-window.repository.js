"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ShopWindowRepository = void 0;
const typeorm_1 = require("typeorm");
const shop_window_entity_1 = require("./shop-window.entity");
let ShopWindowRepository = class ShopWindowRepository extends typeorm_1.Repository {
    async findAll(queryParams) {
        const products = this.find({
            take: queryParams.perPage,
            skip: queryParams.perPage * (queryParams.page - 1),
        });
        return products;
    }
};
ShopWindowRepository = __decorate([
    typeorm_1.EntityRepository(shop_window_entity_1.ShopWindow)
], ShopWindowRepository);
exports.ShopWindowRepository = ShopWindowRepository;
//# sourceMappingURL=shop-window.repository.js.map