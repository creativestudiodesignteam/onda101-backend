"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UnauthorizedAuth = void 0;
class UnauthorizedAuth extends Error {
    constructor() {
        super(...arguments);
        this.message = "E-mail ou senha inválida";
    }
}
exports.UnauthorizedAuth = UnauthorizedAuth;
//# sourceMappingURL=unauthorized-auth.js.map