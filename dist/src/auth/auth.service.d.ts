import { EncryptedService } from "src/shared/encrypted/encrypted.service";
import { UserService } from "src/user/user.service";
import { JwtService } from "@nestjs/jwt";
export declare class AuthService {
    private userService;
    private encryptService;
    private jwtService;
    constructor(userService: UserService, encryptService: EncryptedService, jwtService: JwtService);
    private apiKeys;
    validateUser(username: string, password: string): Promise<any>;
    login(user: any): Promise<{
        user: {
            id: any;
            username: any;
            email: any;
        };
        access_token: string;
    }>;
    validateApiKey(apiKey: string): string;
}
