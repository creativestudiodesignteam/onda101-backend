"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UnauthorizedExceptionAcessRoute = void 0;
class UnauthorizedExceptionAcessRoute extends Error {
    constructor() {
        super(...arguments);
        this.message = "Acesso não autorizado";
    }
}
exports.UnauthorizedExceptionAcessRoute = UnauthorizedExceptionAcessRoute;
//# sourceMappingURL=UnauthorizedExceptionAcessRoute.js.map