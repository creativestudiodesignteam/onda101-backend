import { AuthService } from "./auth.service";
export declare class AuthController {
    private authService;
    constructor(authService: AuthService);
    login(req: any): Promise<{
        user: {
            id: any;
            username: any;
            email: any;
        };
        access_token: string;
    }>;
}
