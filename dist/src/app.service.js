"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppService = void 0;
const common_1 = require("@nestjs/common");
const path_1 = require("path");
const fs_1 = require("fs");
const file_not_found_1 = require("./exception/file-not-found");
const typeorm_1 = require("typeorm");
const user_entity_1 = require("./user/user.entity");
const profile_entity_1 = require("./profile/profile.entity");
let AppService = class AppService {
    constructor(connection) {
        this.connection = connection;
    }
    getHello() {
        return "Hello World!";
    }
    async getImage(filename, path) {
        const filePath = path_1.join(path, filename);
        const checkFileExists = fs_1.existsSync(filePath);
        if (!checkFileExists) {
            throw new file_not_found_1.FileNotFound();
        }
        return filename;
    }
    async profileAndUserDataUpdate(userId, profileChanges, userChanges) {
        const queryRunner = this.connection.createQueryRunner();
        await queryRunner.connect();
        await queryRunner.startTransaction();
        try {
            const user = await queryRunner.manager.findOne(user_entity_1.User, userId);
            const profile = await queryRunner.manager.findOne(profile_entity_1.Profile, {
                where: { user },
            });
            const partialProfile = queryRunner.manager.create(profile_entity_1.Profile, Object.assign({ id: profile.id }, profileChanges));
            await queryRunner.manager.update(profile_entity_1.Profile, { id: profile.id }, partialProfile);
            const partialuser = queryRunner.manager.create(user_entity_1.User, Object.assign({ id: userId }, userChanges));
            await queryRunner.manager.update(user_entity_1.User, { id: userId }, partialuser);
            await queryRunner.commitTransaction();
        }
        catch (error) {
            await queryRunner.rollbackTransaction();
            throw error;
        }
        finally {
            await queryRunner.release();
        }
    }
};
AppService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [typeorm_1.Connection])
], AppService);
exports.AppService = AppService;
//# sourceMappingURL=app.service.js.map