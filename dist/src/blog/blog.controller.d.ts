import { BlogService } from "./blog.service";
import { BlogQueryParams } from "./dto/blog-query-params.dto";
export declare class BlogController {
    private service;
    constructor(service: BlogService);
    findAll(queryParams: BlogQueryParams): Promise<{
        pagination: {
            page: number;
            perPage: number;
        };
        articles: import("./dto/blog-image-links").BlogLinks[];
    }>;
}
