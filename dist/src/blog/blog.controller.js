"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BlogController = void 0;
const common_1 = require("@nestjs/common");
const validation_pipe_1 = require("../shared/pipes/validation.pipe");
const blog_service_1 = require("./blog.service");
const blog_list_1 = require("./dto/blog-list");
const blog_query_params_dto_1 = require("./dto/blog-query-params.dto");
let BlogController = class BlogController {
    constructor(service) {
        this.service = service;
    }
    async findAll(queryParams) {
        try {
            const allBlogsPosts = await this.service.findWithRelations(queryParams);
            const paginatedList = new blog_list_1.BlogListDto(allBlogsPosts, queryParams.page, queryParams.perPage);
            return paginatedList.getPaginatedData();
        }
        catch (error) {
            throw new common_1.InternalServerErrorException({
                error,
                message: "Alguma coisa deu errado. Tente novamente em alguns instantes...",
            });
        }
    }
};
__decorate([
    common_1.Get(),
    __param(0, common_1.Query(new validation_pipe_1.ValidationPipe())),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [blog_query_params_dto_1.BlogQueryParams]),
    __metadata("design:returntype", Promise)
], BlogController.prototype, "findAll", null);
BlogController = __decorate([
    common_1.Controller("blog"),
    __metadata("design:paramtypes", [blog_service_1.BlogService])
], BlogController);
exports.BlogController = BlogController;
//# sourceMappingURL=blog.controller.js.map