import { Repository } from "typeorm";
import { BlogQueryParams } from "./dto/blog-query-params.dto";
import { Blog } from "./blog.entity";
import { BlogWithImage } from "./dto/blog-with-image";
export declare class BlogRepository extends Repository<Blog> {
    findWithRelations(queryParams: BlogQueryParams): Promise<BlogWithImage[]>;
}
