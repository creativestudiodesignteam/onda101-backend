import { BlogImage } from "./blog-image/blog-image.entity";
export declare class Blog {
    article_id: number;
    title: string;
    articleImages: BlogImage[];
}
