import { BlogQueryParams } from "./dto/blog-query-params.dto";
import { BlogRepository } from "./blog.repository";
export declare class BlogService {
    private repository;
    constructor(repository: BlogRepository);
    findWithRelations(queryParams: BlogQueryParams): Promise<import("./dto/blog-with-image").BlogWithImage[]>;
}
