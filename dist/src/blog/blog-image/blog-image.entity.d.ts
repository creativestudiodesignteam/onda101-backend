import { Blog } from "../blog.entity";
export declare class BlogImage {
    article_item_id: number;
    article_id: number;
    path: string;
    type: string;
    article: Blog;
}
