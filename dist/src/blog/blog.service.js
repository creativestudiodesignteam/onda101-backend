"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BlogService = void 0;
const common_1 = require("@nestjs/common");
const blog_repository_1 = require("./blog.repository");
const typeorm_1 = require("@nestjs/typeorm");
let BlogService = class BlogService {
    constructor(repository) {
        this.repository = repository;
    }
    async findWithRelations(queryParams) {
        const blogPosts = await this.repository.findWithRelations(queryParams);
        return blogPosts;
    }
};
BlogService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(blog_repository_1.BlogRepository, "ecommerceConnection")),
    __metadata("design:paramtypes", [blog_repository_1.BlogRepository])
], BlogService);
exports.BlogService = BlogService;
//# sourceMappingURL=blog.service.js.map