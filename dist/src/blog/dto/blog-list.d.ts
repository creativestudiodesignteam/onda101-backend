import { PaginatedList } from "./../paginated-list";
import { BlogLinks } from "./blog-image-links";
import { BlogWithImage } from "./blog-with-image";
export declare class BlogListDto implements PaginatedList {
    articles: BlogLinks[];
    page: number;
    perPage: number;
    constructor(article: BlogWithImage[], page: number, perPage: number);
    getPaginatedData(): {
        pagination: {
            page: number;
            perPage: number;
        };
        articles: BlogLinks[];
    };
}
