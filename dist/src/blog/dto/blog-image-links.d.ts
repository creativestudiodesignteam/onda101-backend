import { ArticleImagesLinks } from "../blog-image/models/article-images-links";
export declare class BlogLinks {
    id: number;
    title: string;
    article_images: ArticleImagesLinks[];
    url_article: string;
}
