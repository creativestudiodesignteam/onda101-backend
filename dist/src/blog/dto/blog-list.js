"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BlogListDto = void 0;
class BlogListDto {
    constructor(article, page, perPage) {
        const BlogLinks = article.map(result => {
            const transformWithLinks = {
                id: result.article_id,
                title: result.title,
                url_article: `${process.env.URL_ECOMMERCE}/index.php?route=blog/article&article_id=${result.article_id}`,
                article_images: result.articleImages.map(articleImage => {
                    return {
                        id: articleImage.article_id,
                        url: articleImage.path
                            ? `${process.env.URL_ECOMMERCE}/image/${articleImage.path}`
                            : null,
                    };
                }),
            };
            return transformWithLinks;
        });
        this.articles = BlogLinks;
        this.page = page;
        this.perPage = perPage;
    }
    getPaginatedData() {
        return {
            pagination: {
                page: this.page,
                perPage: this.perPage,
            },
            articles: this.articles,
        };
    }
}
exports.BlogListDto = BlogListDto;
//# sourceMappingURL=blog-list.js.map