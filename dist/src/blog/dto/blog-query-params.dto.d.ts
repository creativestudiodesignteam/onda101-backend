export declare class BlogQueryParams {
    perPage: number;
    page: number;
}
