"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BlogRepository = void 0;
const typeorm_1 = require("typeorm");
const blog_entity_1 = require("./blog.entity");
let BlogRepository = class BlogRepository extends typeorm_1.Repository {
    async findWithRelations(queryParams) {
        return await this.find({
            relations: ["articleImages"],
            take: queryParams.perPage ? queryParams.perPage : 10,
            skip: queryParams.page && queryParams.perPage
                ? queryParams.perPage * (queryParams.page - 1)
                : 0,
            order: { article_id: "DESC" },
        });
    }
};
BlogRepository = __decorate([
    typeorm_1.EntityRepository(blog_entity_1.Blog)
], BlogRepository);
exports.BlogRepository = BlogRepository;
//# sourceMappingURL=blog.repository.js.map