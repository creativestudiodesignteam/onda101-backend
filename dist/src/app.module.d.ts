import { MiddlewareConsumer } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { Connection } from "typeorm";
export declare class AppModule {
    private connection;
    private configService;
    constructor(connection: Connection, configService: ConfigService);
    configure(consumer: MiddlewareConsumer): void;
}
