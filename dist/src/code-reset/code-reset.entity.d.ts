import { User } from "src/user/user.entity";
import { CodeResetStatus } from "../code-reset-status/code-reset-status.entity";
export declare class CodeReset {
    id: number;
    code: string;
    created_at: Date;
    update_at: Date;
    due_date: Date;
    codeStatus: CodeResetStatus;
    user: User;
}
