"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CodeResetRepository = void 0;
const code_reset_entity_1 = require("./code-reset.entity");
const typeorm_1 = require("typeorm");
let CodeResetRepository = class CodeResetRepository extends typeorm_1.Repository {
    async findById(id) {
        const codeReset = await this.findOne(id);
        return codeReset;
    }
    async findBy(paramToSearch) {
        return await this.findOne({ where: paramToSearch });
    }
    async store(codeReset) {
        return await this.insert(codeReset);
    }
    async updateCodeReset(codeReset) {
        return await this.update(codeReset.id, codeReset);
    }
    async destroy(codeReset) {
        await this.remove(codeReset);
    }
};
CodeResetRepository = __decorate([
    typeorm_1.EntityRepository(code_reset_entity_1.CodeReset)
], CodeResetRepository);
exports.CodeResetRepository = CodeResetRepository;
//# sourceMappingURL=code-reset.repository.js.map