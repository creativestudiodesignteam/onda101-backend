"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CodeResetController = void 0;
const common_1 = require("@nestjs/common");
const code_reset_service_1 = require("./code-reset.service");
let CodeResetController = class CodeResetController {
    constructor(codeResetService) {
        this.codeResetService = codeResetService;
    }
};
CodeResetController = __decorate([
    common_1.Controller('code-reset'),
    __metadata("design:paramtypes", [code_reset_service_1.CodeResetService])
], CodeResetController);
exports.CodeResetController = CodeResetController;
//# sourceMappingURL=code-reset.controller.js.map