"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CodeResetNotExists = void 0;
class CodeResetNotExists extends Error {
    constructor() {
        super(...arguments);
        this.message = "Codigo informado não existe";
    }
}
exports.CodeResetNotExists = CodeResetNotExists;
//# sourceMappingURL=codereset-not-exists.dto.js.map