"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CodeResetModule = void 0;
const common_1 = require("@nestjs/common");
const code_reset_service_1 = require("./code-reset.service");
const code_reset_controller_1 = require("./code-reset.controller");
const code_reset_repository_1 = require("./code-reset.repository");
const typeorm_1 = require("@nestjs/typeorm");
let CodeResetModule = class CodeResetModule {
};
CodeResetModule = __decorate([
    common_1.Module({
        controllers: [code_reset_controller_1.CodeResetController],
        providers: [code_reset_service_1.CodeResetService],
        imports: [
            typeorm_1.TypeOrmModule.forFeature([code_reset_repository_1.CodeResetRepository]),
        ],
        exports: [
            code_reset_service_1.CodeResetService,
            typeorm_1.TypeOrmModule.forFeature([code_reset_repository_1.CodeResetRepository]),
        ]
    })
], CodeResetModule);
exports.CodeResetModule = CodeResetModule;
//# sourceMappingURL=code-reset.module.js.map