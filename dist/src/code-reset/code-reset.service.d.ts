import { CodeReset } from './code-reset.entity';
import { CodeResetRepository } from './code-reset.repository';
import { CreateCodeResetDto } from './dto/create-code-reset.dto';
export declare class CodeResetService {
    private codeResetRepository;
    constructor(codeResetRepository: CodeResetRepository);
    create(codeResetDTO: CreateCodeResetDto): Promise<import("typeorm").InsertResult>;
    findAll(): string;
    findOne(code: string): Promise<CodeReset>;
    update(id: number, updateCodeResetDto: CreateCodeResetDto): string;
    delete(id: number): Promise<CodeReset>;
}
