"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CodeResetService = void 0;
const common_1 = require("@nestjs/common");
const user_entity_1 = require("../user/user.entity");
const code_reset_entity_1 = require("./code-reset.entity");
const code_reset_repository_1 = require("./code-reset.repository");
const code_reset_status_entity_1 = require("../code-reset-status/code-reset-status.entity");
const codereset_not_exists_dto_1 = require("./exception/codereset-not-exists.dto");
var CodeResetStatusValue;
(function (CodeResetStatusValue) {
    CodeResetStatusValue[CodeResetStatusValue["active"] = 1] = "active";
    CodeResetStatusValue[CodeResetStatusValue["used"] = 2] = "used";
    CodeResetStatusValue[CodeResetStatusValue["inative"] = 3] = "inative";
})(CodeResetStatusValue || (CodeResetStatusValue = {}));
let CodeResetService = class CodeResetService {
    constructor(codeResetRepository) {
        this.codeResetRepository = codeResetRepository;
    }
    async create(codeResetDTO) {
        const user = new user_entity_1.User();
        const resetStatus = new code_reset_status_entity_1.CodeResetStatus();
        resetStatus.id = CodeResetStatusValue.active;
        user.id = codeResetDTO.user.id;
        const newObj = new code_reset_entity_1.CodeReset();
        newObj.code = codeResetDTO.codeRandom;
        newObj.user = user;
        newObj.codeStatus = resetStatus;
        user.id = codeResetDTO.user.id;
        let obj = await this.codeResetRepository.store(newObj);
        return obj;
    }
    findAll() {
        return `This action returns all codeReset`;
    }
    async findOne(code) {
        try {
            const codeExists = await this.codeResetRepository.findBy({
                code,
            });
            return codeExists;
        }
        catch (error) {
            throw error;
        }
    }
    update(id, updateCodeResetDto) {
        return `This action updates a #${id} codeReset`;
    }
    async delete(id) {
        try {
            const obj = await this.codeResetRepository.findById(id);
            if (!obj)
                throw new codereset_not_exists_dto_1.CodeResetNotExists();
            this.codeResetRepository.destroy(obj);
            return obj;
        }
        catch (error) {
            throw error;
        }
    }
};
CodeResetService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [code_reset_repository_1.CodeResetRepository])
], CodeResetService);
exports.CodeResetService = CodeResetService;
//# sourceMappingURL=code-reset.service.js.map