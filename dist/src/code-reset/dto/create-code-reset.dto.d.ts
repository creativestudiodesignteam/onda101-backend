import { UserDto } from "src/user/dto/user.dto";
export declare class CreateCodeResetDto {
    email: string;
    user: UserDto;
    codeRandom: string;
    constructor(email: string, user: UserDto, codeRandom: any);
}
