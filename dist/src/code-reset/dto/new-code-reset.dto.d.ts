export declare class NewCodeResetDTO {
    id: number;
    code: string;
    created_at?: Date;
    updated_at?: Date;
    constructor(id: number, code: string, created_at?: Date, updated_at?: Date);
}
