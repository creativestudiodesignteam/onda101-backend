"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.NewCodeResetDTO = void 0;
const class_validator_1 = require("class-validator");
class NewCodeResetDTO {
    constructor(id, code, created_at, updated_at) {
        this.id = id;
        this.code = code;
        this.created_at = created_at !== null && created_at !== void 0 ? created_at : undefined;
        this.updated_at = updated_at !== null && updated_at !== void 0 ? updated_at : undefined;
    }
}
__decorate([
    class_validator_1.IsString(),
    __metadata("design:type", String)
], NewCodeResetDTO.prototype, "code", void 0);
exports.NewCodeResetDTO = NewCodeResetDTO;
//# sourceMappingURL=new-code-reset.dto.js.map