import { CodeReset } from "./code-reset.entity";
import { DeepPartial, Repository } from "typeorm";
import { UpdateCodeResetDto } from "./dto/update-code-reset.dto";
export declare class CodeResetRepository extends Repository<CodeReset> {
    findById(id: number): Promise<CodeReset>;
    findBy(paramToSearch: any): Promise<CodeReset>;
    store(codeReset: CodeReset): Promise<import("typeorm").InsertResult>;
    updateCodeReset(codeReset: DeepPartial<UpdateCodeResetDto>): Promise<import("typeorm").UpdateResult>;
    destroy(codeReset: CodeReset): Promise<void>;
}
