"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppModule = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const config_1 = require("@nestjs/config");
const app_controller_1 = require("./app.controller");
const typeorm_2 = require("typeorm");
const app_service_1 = require("./app.service");
const shared_module_1 = require("./shared/shared.module");
const challenge_module_1 = require("./challenge/challenge.module");
const user_module_1 = require("./user/user.module");
const profile_module_1 = require("./profile/profile.module");
const auth_module_1 = require("./auth/auth.module");
const access_code_module_1 = require("./access-code/access-code.module");
const code_receiver_data_module_1 = require("./code-receiver-data/code-receiver-data.module");
const shop_window_module_1 = require("./shop-window/shop-window.module");
const dbConnections_1 = require("../dbConnections");
const blog_module_1 = require("./blog/blog.module");
const auth_middleware_1 = require("./auth/middleware/auth.middleware");
const code_reset_status_module_1 = require("./code-reset-status/code-reset-status.module");
const code_reset_module_1 = require("./code-reset/code-reset.module");
const mail_sender_service_1 = require("./shared/mail-sender/mail-sender.service");
let AppModule = class AppModule {
    constructor(connection, configService) {
        this.connection = connection;
        this.configService = configService;
    }
    configure(consumer) {
        consumer.apply(auth_middleware_1.AuthMiddleware).forRoutes("access-code/generate");
    }
};
AppModule = __decorate([
    common_1.Module({
        imports: [
            config_1.ConfigModule.forRoot({
                isGlobal: true,
            }),
            typeorm_1.TypeOrmModule.forRoot({
                name: dbConnections_1.dbConnections[0].name,
                type: "mysql",
                port: parseInt(dbConnections_1.dbConnections[0].port, 10),
                username: dbConnections_1.dbConnections[0].username,
                password: dbConnections_1.dbConnections[0].password,
                database: dbConnections_1.dbConnections[0].database,
                host: dbConnections_1.dbConnections[0].host,
                entities: dbConnections_1.dbConnections[0].entities,
            }),
            typeorm_1.TypeOrmModule.forRoot({
                name: dbConnections_1.dbConnections[1].name,
                type: "postgres",
                port: parseInt(dbConnections_1.dbConnections[1].port, 10),
                username: dbConnections_1.dbConnections[1].username,
                password: dbConnections_1.dbConnections[1].password,
                database: dbConnections_1.dbConnections[1].database,
                host: dbConnections_1.dbConnections[1].host,
                entities: dbConnections_1.dbConnections[1].entities,
            }),
            shared_module_1.SharedModule,
            challenge_module_1.ChallengeModule,
            user_module_1.UserModule,
            profile_module_1.ProfileModule,
            access_code_module_1.AccessCodeModule,
            code_receiver_data_module_1.CodeReceiverDataModule,
            auth_module_1.AuthModule,
            shop_window_module_1.ShopWindowModule,
            blog_module_1.BlogModule,
            code_reset_status_module_1.CodeResetStatusModule,
            code_reset_module_1.CodeResetModule,
            mail_sender_service_1.MailSenderService,
        ],
        controllers: [app_controller_1.AppController],
        providers: [app_service_1.AppService],
    }),
    __metadata("design:paramtypes", [typeorm_2.Connection,
        config_1.ConfigService])
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map