import { CodeReset } from "../code-reset/code-reset.entity";
export declare class CodeResetStatus {
    id: number;
    description: string;
    codes: CodeReset;
}
