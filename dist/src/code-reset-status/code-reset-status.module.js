"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CodeResetStatusModule = void 0;
const common_1 = require("@nestjs/common");
const code_reset_status_service_1 = require("./code-reset-status.service");
const code_reset_status_controller_1 = require("./code-reset-status.controller");
let CodeResetStatusModule = class CodeResetStatusModule {
};
CodeResetStatusModule = __decorate([
    common_1.Module({
        controllers: [code_reset_status_controller_1.CodeResetStatusController],
        providers: [code_reset_status_service_1.CodeResetStatusService]
    })
], CodeResetStatusModule);
exports.CodeResetStatusModule = CodeResetStatusModule;
//# sourceMappingURL=code-reset-status.module.js.map