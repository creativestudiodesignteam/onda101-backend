"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CodeResetStatus = void 0;
const typeorm_1 = require("typeorm");
const code_reset_entity_1 = require("../code-reset/code-reset.entity");
let CodeResetStatus = class CodeResetStatus {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], CodeResetStatus.prototype, "id", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], CodeResetStatus.prototype, "description", void 0);
__decorate([
    typeorm_1.OneToMany(() => code_reset_entity_1.CodeReset, code => code.codeStatus),
    __metadata("design:type", code_reset_entity_1.CodeReset)
], CodeResetStatus.prototype, "codes", void 0);
CodeResetStatus = __decorate([
    typeorm_1.Entity({ name: "code_reset_status" })
], CodeResetStatus);
exports.CodeResetStatus = CodeResetStatus;
//# sourceMappingURL=code-reset-status.entity.js.map