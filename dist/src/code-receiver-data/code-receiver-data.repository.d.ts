import { Repository } from "typeorm";
import { CodeReceiverData } from "./code-receiver-data.entity";
export declare class CodeReceiverDataRepository extends Repository<CodeReceiverData> {
    store(codeReceiver: CodeReceiverData): Promise<import("typeorm").InsertResult>;
}
