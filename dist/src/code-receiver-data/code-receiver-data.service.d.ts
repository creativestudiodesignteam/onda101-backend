import { CodeReceiverData } from "./code-receiver-data.entity";
import { CodeReceiverDataRepository } from "./code-receiver-data.repository";
export declare class CodeReceiverDataService {
    private repository;
    constructor(repository: CodeReceiverDataRepository);
    store(codeReceiver: CodeReceiverData): Promise<void>;
}
