"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CodeReceiverData = void 0;
const access_code_entity_1 = require("../access-code/access-code.entity");
const typeorm_1 = require("typeorm");
let CodeReceiverData = class CodeReceiverData {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], CodeReceiverData.prototype, "id", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], CodeReceiverData.prototype, "known_receiver_name", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], CodeReceiverData.prototype, "code_requester_email", void 0);
__decorate([
    typeorm_1.OneToOne(() => access_code_entity_1.AccessCode),
    typeorm_1.JoinColumn({ name: "app_access_code_id" }),
    __metadata("design:type", access_code_entity_1.AccessCode)
], CodeReceiverData.prototype, "accessCode", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", Boolean)
], CodeReceiverData.prototype, "has_welcome_message", void 0);
CodeReceiverData = __decorate([
    typeorm_1.Entity({ name: "code_receivers_data" })
], CodeReceiverData);
exports.CodeReceiverData = CodeReceiverData;
//# sourceMappingURL=code-receiver-data.entity.js.map