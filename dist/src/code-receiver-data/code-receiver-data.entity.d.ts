import { AccessCode } from "src/access-code/access-code.entity";
export declare class CodeReceiverData {
    id: number;
    known_receiver_name: string;
    code_requester_email: string;
    accessCode: AccessCode;
    has_welcome_message: boolean;
}
