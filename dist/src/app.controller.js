"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppController = void 0;
const common_1 = require("@nestjs/common");
const path_1 = require("path");
const rxjs_1 = require("rxjs");
const app_service_1 = require("./app.service");
const file_not_found_1 = require("./exception/file-not-found");
const code_receiver_data_entity_1 = require("./code-receiver-data/code-receiver-data.entity");
const generated_codes_response_dto_1 = require("./access-code/dto/generated-codes-response.dto");
const access_code_service_1 = require("./access-code/access-code.service");
const code_reset_service_1 = require("./code-reset/code-reset.service");
const generate_code_request_dto_1 = require("./access-code/dto/generate-code-request-dto");
const code_receiver_data_service_1 = require("./code-receiver-data/code-receiver-data.service");
const mail_sender_service_1 = require("./shared/mail-sender/mail-sender.service");
const jwt_auth_guard_1 = require("./auth/jwt-auth.guard");
const profile_and_user_data_update_dto_1 = require("./shared/dto/profile-and-user-data-update.dto");
const encrypted_service_1 = require("./shared/encrypted/encrypted.service");
const typeorm_1 = require("typeorm");
const user_entity_1 = require("./user/user.entity");
const user_password_not_match_1 = require("./user/exception/user-password-not-match");
const UnauthorizedExceptionAcessRoute_1 = require("./auth/middleware/exception/UnauthorizedExceptionAcessRoute");
const axios_1 = require("axios");
let AppController = class AppController {
    constructor(appService, accessCodeservice, codeResetService, codeReceiverService, mailSenderService, encryptService, entityManager) {
        this.appService = appService;
        this.accessCodeservice = accessCodeservice;
        this.codeResetService = codeResetService;
        this.codeReceiverService = codeReceiverService;
        this.mailSenderService = mailSenderService;
        this.encryptService = encryptService;
        this.entityManager = entityManager;
    }
    getHello() {
        return "Mudou o commit deploy automático";
    }
    async getFiles(param, res) {
        try {
            const filePath = await this.appService.getImage(param.filename, "upload");
            return rxjs_1.of(res.sendFile(path_1.join(process.cwd(), `upload/${filePath}`)));
        }
        catch (error) {
            if (error instanceof file_not_found_1.FileNotFound)
                throw new common_1.NotFoundException(error.message);
            throw new common_1.InternalServerErrorException({
                message: "Alguma coisa deu errado. Tente novamente em alguns instantes... fd",
            });
        }
    }
    async getposts(param, res) {
        try {
            const filePath = await this.appService.getImage(param.filename, "upload");
            return rxjs_1.of(res.sendFile(path_1.join(process.cwd(), `upload/${filePath}`)));
        }
        catch (error) {
            if (error instanceof file_not_found_1.FileNotFound)
                throw new common_1.NotFoundException(error.message);
            throw new common_1.InternalServerErrorException({
                message: "Alguma coisa deu errado. Tente novamente em alguns instantes...",
            });
        }
    }
    async receiveNewUsers(bodyRequest) {
        try {
            const verifyPayment = await axios_1.default.get(`https://api.mercadopago.com/v1/payments/${bodyRequest.paymentId}`, {
                headers: {
                    Authorization: `Bearer ${process.env.MP_ACESS_TOKEN}`,
                },
            });
            if (verifyPayment.data.status !== "rejected") {
                return new common_1.BadRequestException("Desculpe, o seu pagamento não foi aprovado :D!");
            }
        }
        catch (error) {
            return new common_1.BadRequestException("Desculpe, não foi encontrado :D!");
        }
        try {
            const totalCodesNeeded = bodyRequest.names.length;
            const codes = await this.accessCodeservice.provideAccessCodesToNewUsers(totalCodesNeeded);
            const storedAcccessCodeAsPromises = codes.map(code => this.accessCodeservice.findByCodeValue(code));
            const resolvedStoredAccessCode = await Promise.all(storedAcccessCodeAsPromises);
            resolvedStoredAccessCode.forEach(async (accessCode, index) => {
                const codeReceiverData = new code_receiver_data_entity_1.CodeReceiverData();
                codeReceiverData.accessCode = accessCode;
                codeReceiverData.code_requester_email = bodyRequest.email;
                codeReceiverData.has_welcome_message =
                    bodyRequest.names[index].hasWelcomeMessage;
                const knownName = bodyRequest.names[index].name;
                if (knownName) {
                    codeReceiverData.known_receiver_name = knownName;
                }
                await this.codeReceiverService.store(codeReceiverData);
            });
            const allCodesInSingleLine = codes.reduce((singleString, email) => singleString.concat(`${email}; `), "");
            const mailMessage = `Aqui estão seus códigos: ${allCodesInSingleLine}`;
            let rsp = [];
            for (let codeU in codes) {
                const code = codes[codeU];
                const name = bodyRequest.names[codeU];
                rsp.push({ name: name.name, code });
            }
            try {
                axios_1.default
                    .get("https://onda101.com.br/api-codes?data=" +
                    JSON.stringify({
                        order: bodyRequest.order,
                        codes: rsp,
                    }))
                    .then(resd => console.log(resd.data, resd.config.url));
            }
            catch (error) {
                console.log("error", error);
            }
            return new generated_codes_response_dto_1.GeneratedCodesResponseDto({
                order: bodyRequest.order,
                codes: rsp,
            });
        }
        catch (error) {
            if (error instanceof UnauthorizedExceptionAcessRoute_1.UnauthorizedExceptionAcessRoute)
                throw new common_1.BadRequestException(error.message);
            return new common_1.InternalServerErrorException();
        }
    }
    async updateProfileAndUserData(req, bodyRequest) {
        try {
            const user = await this.entityManager.findOne(user_entity_1.User, req.user.id);
            const samePassword = await this.encryptService.compare(bodyRequest.password, user.password);
            if (!samePassword)
                throw new user_password_not_match_1.UserPasswordNotMatch();
            await this.appService.profileAndUserDataUpdate(req.user.id, bodyRequest.profile, bodyRequest.user);
            return { message: "Informações atualizadas com sucesso" };
        }
        catch (error) {
            if (error instanceof user_password_not_match_1.UserPasswordNotMatch)
                throw new common_1.BadRequestException("Senha informada está incorreta");
            throw new common_1.InternalServerErrorException({
                message: "Alguma coisa deu errado. Tente novamente em alguns instantes...",
            });
        }
    }
};
__decorate([
    common_1.Get(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", String)
], AppController.prototype, "getHello", null);
__decorate([
    common_1.Get("files/:filename"),
    common_1.HttpCode(200),
    __param(0, common_1.Param()), __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], AppController.prototype, "getFiles", null);
__decorate([
    common_1.Get("posts/:filename"),
    common_1.HttpCode(200),
    __param(0, common_1.Param()), __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], AppController.prototype, "getposts", null);
__decorate([
    common_1.Post("access-code/generate"),
    __param(0, common_1.Body(new common_1.ValidationPipe())),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [generate_code_request_dto_1.GenerateCodeRequestDto]),
    __metadata("design:returntype", Promise)
], AppController.prototype, "receiveNewUsers", null);
__decorate([
    common_1.Put("profile-data"),
    common_1.UseGuards(jwt_auth_guard_1.JwtAuthGuard),
    __param(0, common_1.Req()),
    __param(1, common_1.Body(new common_1.ValidationPipe())),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, profile_and_user_data_update_dto_1.ProfileAndUserDataUpdateDto]),
    __metadata("design:returntype", Promise)
], AppController.prototype, "updateProfileAndUserData", null);
AppController = __decorate([
    common_1.Controller(),
    __metadata("design:paramtypes", [app_service_1.AppService,
        access_code_service_1.AccessCodeService,
        code_reset_service_1.CodeResetService,
        code_receiver_data_service_1.CodeReceiverDataService,
        mail_sender_service_1.MailSenderService,
        encrypted_service_1.EncryptedService,
        typeorm_1.EntityManager])
], AppController);
exports.AppController = AppController;
//# sourceMappingURL=app.controller.js.map