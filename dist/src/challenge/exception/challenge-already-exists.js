"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChallengeAlreadyExists = void 0;
class ChallengeAlreadyExists extends Error {
    constructor() {
        super(...arguments);
        this.message = "O Desafio já existe";
    }
}
exports.ChallengeAlreadyExists = ChallengeAlreadyExists;
//# sourceMappingURL=challenge-already-exists.js.map