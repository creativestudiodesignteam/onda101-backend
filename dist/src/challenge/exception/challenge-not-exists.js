"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChallengeNotExists = void 0;
class ChallengeNotExists extends Error {
    constructor() {
        super(...arguments);
        this.message = "Desafio inexistente";
    }
}
exports.ChallengeNotExists = ChallengeNotExists;
//# sourceMappingURL=challenge-not-exists.js.map