"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChallengeNumberAlreadyExists = exports.ChallengeNotExists = void 0;
const challenge_not_exists_1 = require("./challenge-not-exists");
Object.defineProperty(exports, "ChallengeNotExists", { enumerable: true, get: function () { return challenge_not_exists_1.ChallengeNotExists; } });
const challenge_number_already_exists_1 = require("./challenge-number-already-exists");
Object.defineProperty(exports, "ChallengeNumberAlreadyExists", { enumerable: true, get: function () { return challenge_number_already_exists_1.ChallengeNumberAlreadyExists; } });
//# sourceMappingURL=index.js.map