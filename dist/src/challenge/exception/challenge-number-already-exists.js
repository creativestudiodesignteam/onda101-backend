"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChallengeNumberAlreadyExists = void 0;
class ChallengeNumberAlreadyExists extends Error {
    constructor() {
        super(...arguments);
        this.message = "Número do desafio já existe";
    }
}
exports.ChallengeNumberAlreadyExists = ChallengeNumberAlreadyExists;
//# sourceMappingURL=challenge-number-already-exists.js.map