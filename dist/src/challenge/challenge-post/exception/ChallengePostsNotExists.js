"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChallengePostNotExists = void 0;
class ChallengePostNotExists extends Error {
    constructor() {
        super(...arguments);
        this.message = "Postagem não existe";
    }
}
exports.ChallengePostNotExists = ChallengePostNotExists;
//# sourceMappingURL=ChallengePostsNotExists.js.map