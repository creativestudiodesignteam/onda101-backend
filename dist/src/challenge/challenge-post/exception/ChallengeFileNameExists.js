"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChallengeFileExists = void 0;
class ChallengeFileExists extends Error {
    constructor() {
        super(...arguments);
        this.message = "Arquivo já existe";
    }
}
exports.ChallengeFileExists = ChallengeFileExists;
//# sourceMappingURL=ChallengeFileNameExists.js.map