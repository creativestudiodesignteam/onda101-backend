"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChallengePostFileNotFound = void 0;
class ChallengePostFileNotFound extends Error {
    constructor() {
        super(...arguments);
        this.message = "Arquivo inválido, verifique se o arquivo segue os padrões estabelecidos";
    }
}
exports.ChallengePostFileNotFound = ChallengePostFileNotFound;
//# sourceMappingURL=ChallengePostFileNotFound.js.map