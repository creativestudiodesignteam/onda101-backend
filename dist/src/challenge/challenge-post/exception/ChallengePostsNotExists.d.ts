export declare class ChallengePostNotExists extends Error {
    readonly message = "Postagem n\u00E3o existe";
}
