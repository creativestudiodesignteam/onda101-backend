"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChallengesPosts = void 0;
const profile_entity_1 = require("../../profile/profile.entity");
const typeorm_1 = require("typeorm");
const challenge_entity_1 = require("../challenge.entity");
let ChallengesPosts = class ChallengesPosts {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], ChallengesPosts.prototype, "id", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], ChallengesPosts.prototype, "name_file", void 0);
__decorate([
    typeorm_1.ManyToOne(() => profile_entity_1.Profile, profile => profile.completedChallenges),
    typeorm_1.JoinColumn({ name: "profile_id" }),
    __metadata("design:type", profile_entity_1.Profile)
], ChallengesPosts.prototype, "profile", void 0);
__decorate([
    typeorm_1.ManyToOne(() => challenge_entity_1.Challenge, challenge => challenge.completedChallenges),
    typeorm_1.JoinColumn({ name: "challenge_id" }),
    __metadata("design:type", challenge_entity_1.Challenge)
], ChallengesPosts.prototype, "challenge", void 0);
__decorate([
    typeorm_1.CreateDateColumn(),
    __metadata("design:type", Date)
], ChallengesPosts.prototype, "created_at", void 0);
ChallengesPosts = __decorate([
    typeorm_1.Entity({ name: "challenges_posts" })
], ChallengesPosts);
exports.ChallengesPosts = ChallengesPosts;
//# sourceMappingURL=challenge-post.entity.js.map