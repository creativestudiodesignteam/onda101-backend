interface File {
    filename: string;
    originalname: string;
    encoding: string;
    mimetype: string;
    destination: string;
    path: string;
    size: number;
}
export declare class CreateChallengePost {
    file: File;
    user_id: number;
    challenge_id: number;
}
export {};
