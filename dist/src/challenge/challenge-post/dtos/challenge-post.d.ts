import { Challenge } from "src/challenge/challenge.entity";
import { Profile } from "src/profile/profile.entity";
export declare class ChallengePostDto {
    id: number;
    name_file: string;
    challenge: Challenge;
    profile: Profile;
    created_at?: Date;
    constructor(id: number, name_file: string, profile: Profile, challenge: Challenge, created_at?: Date);
}
