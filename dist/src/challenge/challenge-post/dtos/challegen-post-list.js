"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChallengesPostsListDto = void 0;
class ChallengesPostsListDto {
    constructor(challengePosts, page, perPage) {
        const challengePostImageTransform = challengePosts.map(result => {
            const transformPostsImageUrl = {
                id: result.id,
                name_file: result.name_file,
                url: `${process.env.SERVER_DOMAIN}/posts/${result.name_file}`,
                created_at: result.created_at,
            };
            return transformPostsImageUrl;
        });
        this.posts = challengePostImageTransform;
        this.page = page;
        this.perPage = perPage;
    }
    getPaginatedData() {
        return {
            pagination: {
                page: this.page,
                perPage: this.perPage,
            },
            posts: this.posts,
        };
    }
}
exports.ChallengesPostsListDto = ChallengesPostsListDto;
//# sourceMappingURL=challegen-post-list.js.map