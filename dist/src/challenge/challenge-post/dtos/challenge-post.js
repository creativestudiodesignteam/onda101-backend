"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChallengePostDto = void 0;
const challenge_entity_1 = require("../../challenge.entity");
const profile_entity_1 = require("../../../profile/profile.entity");
class ChallengePostDto {
    constructor(id, name_file, profile, challenge, created_at) {
        this.id = id;
        this.name_file = name_file;
        this.profile = profile;
        this.challenge = challenge;
        this.created_at = created_at !== null && created_at !== void 0 ? created_at : undefined;
    }
}
exports.ChallengePostDto = ChallengePostDto;
//# sourceMappingURL=challenge-post.js.map