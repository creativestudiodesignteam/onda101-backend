import { PaginatedList } from "./../paginated-list";
import { ChallengesPosts } from "./../challenge-post.entity";
import { ChallengePostsURL } from "./challenge-posts-image-url";
export declare class ChallengesPostsListDto implements PaginatedList {
    posts: ChallengePostsURL[];
    page: number;
    perPage: number;
    constructor(challengePosts: ChallengesPosts[], page: number, perPage: number);
    getPaginatedData(): {
        pagination: {
            page: number;
            perPage: number;
        };
        posts: ChallengePostsURL[];
    };
}
