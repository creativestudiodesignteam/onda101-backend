export declare class ChallengePostsURL {
    id: number;
    name_file: string;
    url: string;
    created_at: Date;
}
