export declare class ChallengePostsQueryParams {
    perPage: number;
    page: number;
}
