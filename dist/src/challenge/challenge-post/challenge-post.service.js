"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChallengePostService = void 0;
const common_1 = require("@nestjs/common");
const profile_not_exists_1 = require("../../profile/exception/profile-not-exists");
const profile_service_1 = require("../../profile/profile.service");
const upload_files_service_1 = require("../../shared/upload-files/upload-files.service");
const challenge_service_1 = require("../challenge.service");
const exception_1 = require("../exception");
const challenge_post_entity_1 = require("./challenge-post.entity");
const challenge_post_repository_1 = require("./challenge-post.repository");
const ChallengeFileNameExists_1 = require("./exception/ChallengeFileNameExists");
const ChallengePostFileNotFound_1 = require("./exception/ChallengePostFileNotFound");
const ChallengePostsNotExists_1 = require("./exception/ChallengePostsNotExists");
let ChallengePostService = class ChallengePostService {
    constructor(challengesPostsRepository, profileService, challengeService, uploadFilesService) {
        this.challengesPostsRepository = challengesPostsRepository;
        this.profileService = profileService;
        this.challengeService = challengeService;
        this.uploadFilesService = uploadFilesService;
    }
    async create(challengePostToBeCreate) {
        if (!challengePostToBeCreate.file) {
            throw new ChallengePostFileNotFound_1.ChallengePostFileNotFound();
        }
        const givenChallengeNumberAlreadyExists = await this.challengesPostsRepository.findBy({
            name_file: challengePostToBeCreate.file.filename,
        });
        if (givenChallengeNumberAlreadyExists)
            throw new ChallengeFileNameExists_1.ChallengeFileExists();
        const findProfileByUser = await this.profileService.findByUser(challengePostToBeCreate.user_id);
        if (!findProfileByUser) {
            await this.uploadFilesService.delete("./upload", challengePostToBeCreate.file.filename);
            throw new profile_not_exists_1.ProfileNotExists();
        }
        let findChallenge;
        try {
            findChallenge = await this.challengeService.findById(challengePostToBeCreate.challenge_id);
        }
        catch (error) {
            await this.uploadFilesService.delete("./upload", challengePostToBeCreate.file.filename);
            throw new exception_1.ChallengeNotExists();
        }
        const fileCompressed = await this.uploadFilesService.compress(challengePostToBeCreate.file, 80, 500);
        const challengeAttachment = new challenge_post_entity_1.ChallengesPosts();
        challengeAttachment.name_file = fileCompressed.filename;
        challengeAttachment.challenge = findChallenge;
        challengeAttachment.profile = findProfileByUser;
        await this.challengesPostsRepository.store(challengeAttachment);
        return challengeAttachment;
    }
    async findByUser(user_id, queryParams) {
        const findProfileByUser = await this.profileService.findByUser(user_id);
        const findChallengesPosts = await this.challengesPostsRepository.findAllByProfile(findProfileByUser, queryParams);
        return findChallengesPosts;
    }
    async delete(user_id, id) {
        const findProfileByUser = await this.profileService.findByUser(user_id);
        const findPostsByProfile = await this.challengesPostsRepository.findBy({
            id,
            profile: findProfileByUser,
        });
        if (!findPostsByProfile)
            throw new ChallengePostsNotExists_1.ChallengePostNotExists();
        this.uploadFilesService.delete("./upload/posts_image", findPostsByProfile.name_file);
        this.challengesPostsRepository.destroy(findPostsByProfile);
        return findPostsByProfile;
    }
};
ChallengePostService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [challenge_post_repository_1.ChallengesPostsRepository,
        profile_service_1.ProfileService,
        challenge_service_1.ChallengeService,
        upload_files_service_1.UploadFilesService])
], ChallengePostService);
exports.ChallengePostService = ChallengePostService;
//# sourceMappingURL=challenge-post.service.js.map