import { ProfileService } from "src/profile/profile.service";
import { UploadFilesService } from "src/shared/upload-files/upload-files.service";
import { ChallengeService } from "../challenge.service";
import { ChallengesPosts } from "./challenge-post.entity";
import { ChallengesPostsRepository } from "./challenge-post.repository";
import { ChallengePostDto } from "./dtos/challenge-post";
import { CreateChallengePost } from "./dtos/challenge-post-create";
import { ChallengePostsQueryParams } from "./dtos/challenge-posts-query-params.dto";
export declare class ChallengePostService {
    private challengesPostsRepository;
    private profileService;
    private challengeService;
    private uploadFilesService;
    constructor(challengesPostsRepository: ChallengesPostsRepository, profileService: ProfileService, challengeService: ChallengeService, uploadFilesService: UploadFilesService);
    create(challengePostToBeCreate: CreateChallengePost): Promise<ChallengePostDto>;
    findByUser(user_id: number, queryParams: ChallengePostsQueryParams): Promise<ChallengesPosts[]>;
    delete(user_id: number, id: number): Promise<ChallengesPosts>;
}
