import { ChallengePostService } from "./challenge-post.service";
import { ChallengePostDto } from "./dtos/challenge-post";
import { ChallengePostsQueryParams } from "./dtos/challenge-posts-query-params.dto";
import { CustomSuccessResponse } from "./dtos/custom-success-response";
export declare class ChallengePostController {
    private challengePostService;
    constructor(challengePostService: ChallengePostService);
    findByUserId(req: any, { page, perPage }: ChallengePostsQueryParams): Promise<{
        pagination: {
            page: number;
            perPage: number;
        };
        posts: import("./dtos/challenge-posts-image-url").ChallengePostsURL[];
    }>;
    findById(param: any, { page, perPage }: ChallengePostsQueryParams): Promise<{
        pagination: {
            page: number;
            perPage: number;
        };
        posts: import("./dtos/challenge-posts-image-url").ChallengePostsURL[];
    }>;
    create(req: any, param: any, file: any): Promise<ChallengePostDto>;
    delete(param: any, req: any): Promise<CustomSuccessResponse>;
}
