"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChallengePostController = void 0;
const common_1 = require("@nestjs/common");
const platform_express_1 = require("@nestjs/platform-express");
const multer_1 = require("multer");
const jwt_auth_guard_1 = require("../../auth/jwt-auth.guard");
const profile_not_exists_1 = require("../../profile/exception/profile-not-exists");
const upload_files_1 = require("../../shared/utils/upload-files");
const exception_1 = require("../exception");
const challenge_post_service_1 = require("./challenge-post.service");
const challenge_post_1 = require("./dtos/challenge-post");
const ChallengeFileNameExists_1 = require("./exception/ChallengeFileNameExists");
const ChallengePostFileNotFound_1 = require("./exception/ChallengePostFileNotFound");
const challegen_post_list_1 = require("./dtos/challegen-post-list");
const validation_pipe_1 = require("../../shared/pipes/validation.pipe");
const challenge_posts_query_params_dto_1 = require("./dtos/challenge-posts-query-params.dto");
const custom_success_response_1 = require("./dtos/custom-success-response");
const ChallengePostsNotExists_1 = require("./exception/ChallengePostsNotExists");
const FileIsNotExists_1 = require("../../shared/upload-files/exception/FileIsNotExists");
let ChallengePostController = class ChallengePostController {
    constructor(challengePostService) {
        this.challengePostService = challengePostService;
    }
    async findByUserId(req, { page = 1, perPage = 500 }) {
        try {
            const findPostByUser = await this.challengePostService.findByUser(req.user.id, { page, perPage });
            const paginatedList = new challegen_post_list_1.ChallengesPostsListDto(findPostByUser, page, perPage);
            return paginatedList.getPaginatedData();
        }
        catch (error) {
            if (error instanceof ChallengePostFileNotFound_1.ChallengePostFileNotFound)
                throw new common_1.NotFoundException(error.message);
            throw new common_1.InternalServerErrorException({
                error,
                message: "Alguma coisa deu errado. Tente novamente em alguns instantes...",
            });
        }
    }
    async findById(param, { page = 1, perPage = 10 }) {
        try {
            const findPostByUser = await this.challengePostService.findByUser(param.user_id, { page, perPage });
            const paginatedList = new challegen_post_list_1.ChallengesPostsListDto(findPostByUser, page, perPage);
            return paginatedList.getPaginatedData();
        }
        catch (error) {
            if (error instanceof ChallengePostFileNotFound_1.ChallengePostFileNotFound)
                throw new common_1.NotFoundException(error.message);
            throw new common_1.InternalServerErrorException({
                error,
                message: "Alguma coisa deu errado. Tente novamente em alguns instantes...",
            });
        }
    }
    async create(req, param, file) {
        try {
            const challengesPosts = await this.challengePostService.create({
                file: file,
                user_id: req.user.id,
                challenge_id: param.challenge_id,
            });
            return new challenge_post_1.ChallengePostDto(challengesPosts.id, challengesPosts.name_file, challengesPosts.profile, challengesPosts.challenge, challengesPosts.created_at);
        }
        catch (error) {
            if (error instanceof exception_1.ChallengeNotExists)
                throw new common_1.NotFoundException(error.message);
            if (error instanceof FileIsNotExists_1.FileIsNotExists)
                throw new common_1.NotFoundException(error.message);
            if (error instanceof profile_not_exists_1.ProfileNotExists)
                throw new common_1.NotFoundException(error.message);
            if (error instanceof ChallengeFileNameExists_1.ChallengeFileExists)
                throw new common_1.NotFoundException(error.message);
            if (error instanceof ChallengePostFileNotFound_1.ChallengePostFileNotFound)
                throw new common_1.NotFoundException(error.message);
            throw new common_1.InternalServerErrorException({
                message: "Alguma coisa deu errado. Tente novamente em alguns instantes...",
            });
        }
    }
    async delete(param, req) {
        try {
            await this.challengePostService.delete(req.user.id, param.id);
            return new custom_success_response_1.CustomSuccessResponse("Desafio excluído com sucesso");
        }
        catch (error) {
            if (error instanceof ChallengePostsNotExists_1.ChallengePostNotExists) {
                throw new common_1.NotFoundException(error.message);
            }
            if (error instanceof FileIsNotExists_1.FileIsNotExists) {
                throw new common_1.NotFoundException(error.message);
            }
            throw new common_1.InternalServerErrorException({
                error,
                message: "Alguma coisa deu errado. Tente novamente em alguns instantes...",
            });
        }
    }
};
__decorate([
    common_1.Get(),
    common_1.UseGuards(jwt_auth_guard_1.JwtAuthGuard),
    __param(0, common_1.Req()),
    __param(1, common_1.Query(new validation_pipe_1.ValidationPipe())),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, challenge_posts_query_params_dto_1.ChallengePostsQueryParams]),
    __metadata("design:returntype", Promise)
], ChallengePostController.prototype, "findByUserId", null);
__decorate([
    common_1.Get(":user_id"),
    common_1.UseGuards(jwt_auth_guard_1.JwtAuthGuard),
    __param(0, common_1.Param()),
    __param(1, common_1.Query(new validation_pipe_1.ValidationPipe())),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, challenge_posts_query_params_dto_1.ChallengePostsQueryParams]),
    __metadata("design:returntype", Promise)
], ChallengePostController.prototype, "findById", null);
__decorate([
    common_1.Post(":challenge_id"),
    common_1.UseGuards(jwt_auth_guard_1.JwtAuthGuard),
    common_1.UseInterceptors(platform_express_1.FileInterceptor("file", {
        fileFilter: (req, file, cb) => {
            if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
                req.fileErrors = {
                    error: true,
                };
                return cb(null, false);
            }
            return cb(null, true);
        },
        storage: multer_1.diskStorage({
            destination: "./upload",
            filename: upload_files_1.editFileName,
        }),
    })),
    __param(0, common_1.Req()), __param(1, common_1.Param()), __param(2, common_1.UploadedFile()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object]),
    __metadata("design:returntype", Promise)
], ChallengePostController.prototype, "create", null);
__decorate([
    common_1.Delete(":id"),
    common_1.UseGuards(jwt_auth_guard_1.JwtAuthGuard),
    common_1.HttpCode(200),
    __param(0, common_1.Param()), __param(1, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], ChallengePostController.prototype, "delete", null);
ChallengePostController = __decorate([
    common_1.Controller("challenge-post"),
    __metadata("design:paramtypes", [challenge_post_service_1.ChallengePostService])
], ChallengePostController);
exports.ChallengePostController = ChallengePostController;
//# sourceMappingURL=challenge-post.controller.js.map