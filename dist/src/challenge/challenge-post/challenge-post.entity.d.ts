import { Profile } from "src/profile/profile.entity";
import { Challenge } from "../challenge.entity";
export declare class ChallengesPosts {
    id: number;
    name_file: string;
    profile: Profile;
    challenge: Challenge;
    created_at: Date;
}
