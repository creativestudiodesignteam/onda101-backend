import { ChallengesPosts } from "./challenge-post.entity";
import { DeepPartial, Repository } from "typeorm";
import { ChallengePostsQueryParams } from "./dtos/challenge-posts-query-params.dto";
import { Profile } from "src/profile/profile.entity";
export declare class ChallengesPostsRepository extends Repository<ChallengesPosts> {
    findById(id: number): Promise<ChallengesPosts>;
    findBy(paramToSearch: any): Promise<ChallengesPosts>;
    findAllByProfile(profile: Profile, queryParams: ChallengePostsQueryParams): Promise<ChallengesPosts[]>;
    store(challengeToBeCreated: ChallengesPosts): Promise<import("typeorm").InsertResult>;
    updateChallenge(challenge: DeepPartial<ChallengesPosts>): Promise<void>;
    destroy(challenge: ChallengesPosts): void;
}
