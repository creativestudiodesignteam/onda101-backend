"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChallengesPostsRepository = void 0;
const challenge_post_entity_1 = require("./challenge-post.entity");
const typeorm_1 = require("typeorm");
const profile_entity_1 = require("../../profile/profile.entity");
let ChallengesPostsRepository = class ChallengesPostsRepository extends typeorm_1.Repository {
    findById(id) {
        return this.findOne(id);
    }
    async findBy(paramToSearch) {
        return this.findOne({ where: paramToSearch });
    }
    async findAllByProfile(profile, queryParams) {
        return this.find({
            where: { profile: profile },
            take: queryParams.perPage,
            skip: queryParams.perPage * (queryParams.page - 1),
        });
    }
    store(challengeToBeCreated) {
        return this.insert(challengeToBeCreated);
    }
    async updateChallenge(challenge) {
        await this.update(challenge.id, challenge);
        return;
    }
    destroy(challenge) {
        this.remove(challenge);
    }
};
ChallengesPostsRepository = __decorate([
    typeorm_1.EntityRepository(challenge_post_entity_1.ChallengesPosts)
], ChallengesPostsRepository);
exports.ChallengesPostsRepository = ChallengesPostsRepository;
//# sourceMappingURL=challenge-post.repository.js.map