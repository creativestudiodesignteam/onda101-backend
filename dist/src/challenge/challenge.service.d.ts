import { CreateChallengeDto } from "./dto/create-challenge.dto";
import { DeepPartial, FindConditions } from "typeorm";
import { ChallengeRepository } from "./challenge.repository";
import { ChallengeQueryParams } from "./dto/challenge-query-params.dto";
import { Challenge } from "./challenge.entity";
import { ChallengeLevelService } from "./challenge-level/challenge-level.service";
export declare class ChallengeService {
    private challengeRepository;
    private challengeLevelService;
    constructor(challengeRepository: ChallengeRepository, challengeLevelService: ChallengeLevelService);
    findAll({ page, perPage }: ChallengeQueryParams): Promise<Challenge[]>;
    findById(id: number): Promise<Challenge>;
    create(challengeToBeCreated: CreateChallengeDto): Promise<Challenge>;
    update(id: number, challengeChanges: DeepPartial<Challenge>): Promise<Challenge>;
    delete(id: number): Promise<void>;
    findByCryteria(cryteria: FindConditions<Challenge>): Promise<Challenge[]>;
}
