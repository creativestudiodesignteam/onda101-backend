import { Profile } from "src/profile/profile.entity";
import { Challenge } from "../challenge.entity";
export declare class CompletedChallenge {
    id: number;
    first_time_done_at: Date;
    last_time_done_at: Date;
    redone_times: number;
    profile: Profile;
    challenge: Challenge;
}
