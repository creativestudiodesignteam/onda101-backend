import { ProfileService } from "src/profile/profile.service";
import { ChallengeService } from "../challenge.service";
import { CompletedChallenge } from "./completed-challenge.entity";
import { CompletedChallengeRepository } from "./challenge-completed.repository";
import { CreateCompletedChallengeDto } from "./dto/completed-challenge-create.dto";
import { CompletedChallengeQueryParams } from "./dto/completed-challenge-query-params.dto";
export declare class CompletedChallengeService {
    private completedChallengeRepository;
    private profileService;
    private challengeService;
    constructor(completedChallengeRepository: CompletedChallengeRepository, profileService: ProfileService, challengeService: ChallengeService);
    findByProfile(profile_id: any, { page, perPage }: CompletedChallengeQueryParams): Promise<CompletedChallenge[]>;
    create(profile_id: number, challengeCompletedToBeCreate: CreateCompletedChallengeDto): Promise<CompletedChallenge>;
    delete(id: number): Promise<void>;
}
