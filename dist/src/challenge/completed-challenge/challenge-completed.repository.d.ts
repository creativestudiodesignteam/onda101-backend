import { CompletedChallenge } from "./completed-challenge.entity";
import { DeepPartial, Repository } from "typeorm";
import { CompletedChallengeQueryParams } from "./dto/completed-challenge-query-params.dto";
export declare class CompletedChallengeRepository extends Repository<CompletedChallenge> {
    findAll(profile: any, queryParams: CompletedChallengeQueryParams, relation: ['profile', 'challenge']): Promise<CompletedChallenge[]>;
    findWithRelation(id: number, relation: ['profile']): Promise<CompletedChallenge>;
    findById(id: any): Promise<CompletedChallenge>;
    findBy(paramToSearch: any): Promise<CompletedChallenge>;
    store(challengeToBeCreated: CompletedChallenge): Promise<import("typeorm").InsertResult>;
    updateCompletedChallenge(challengeCompleted: DeepPartial<CompletedChallenge>): Promise<import("typeorm").UpdateResult>;
    destroy(challenge: CompletedChallenge): void;
}
