"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CompletedChallengeService = void 0;
const common_1 = require("@nestjs/common");
const profile_service_1 = require("../../profile/profile.service");
const challenge_service_1 = require("../challenge.service");
const completed_challenge_entity_1 = require("./completed-challenge.entity");
const challenge_completed_repository_1 = require("./challenge-completed.repository");
const completed_challenge_not_exists_1 = require("./exception/completed-challenge-not-exists");
let CompletedChallengeService = class CompletedChallengeService {
    constructor(completedChallengeRepository, profileService, challengeService) {
        this.completedChallengeRepository = completedChallengeRepository;
        this.profileService = profileService;
        this.challengeService = challengeService;
    }
    async findByProfile(profile_id, { page = 1, perPage = 10 }) {
        const profile = await this.profileService.findById(profile_id);
        const allChallenges = await this.completedChallengeRepository.findAll(profile, { page, perPage }, ["profile", "challenge"]);
        return allChallenges;
    }
    async create(profile_id, challengeCompletedToBeCreate) {
        try {
            const challenge = await this.challengeService.findById(challengeCompletedToBeCreate.challenge_id);
            const profile = await this.profileService.findById(profile_id);
            const checkExists = await this.completedChallengeRepository.findBy({
                challenge: challenge,
                profile: profile,
            });
            const challengeCompleted = new completed_challenge_entity_1.CompletedChallenge();
            if (checkExists) {
                challengeCompleted.id = checkExists.id;
                challengeCompleted.challenge = challenge;
                challengeCompleted.profile = profile;
                challengeCompleted.first_time_done_at = checkExists.first_time_done_at;
                challengeCompleted.last_time_done_at = new Date();
                challengeCompleted.redone_times = checkExists.redone_times + 1;
                await this.completedChallengeRepository.updateCompletedChallenge(challengeCompleted);
            }
            else {
                challengeCompleted.challenge = challenge;
                challengeCompleted.profile = profile;
                challengeCompleted.first_time_done_at = new Date();
                challengeCompleted.last_time_done_at = new Date();
                challengeCompleted.redone_times = 0;
                await this.completedChallengeRepository.store(challengeCompleted);
                await this.profileService.addChallengeScore(challenge.score, profile);
            }
            return challengeCompleted;
        }
        catch (error) {
            throw error;
        }
    }
    async delete(id) {
        const challenge = await this.completedChallengeRepository.findBy({
            id,
        });
        if (!challenge)
            throw new completed_challenge_not_exists_1.CompletedhallengeNotExists();
        this.completedChallengeRepository.destroy(challenge);
    }
};
CompletedChallengeService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [challenge_completed_repository_1.CompletedChallengeRepository,
        profile_service_1.ProfileService,
        challenge_service_1.ChallengeService])
], CompletedChallengeService);
exports.CompletedChallengeService = CompletedChallengeService;
//# sourceMappingURL=completed-challenge.service.js.map