"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CompletedChallengeRepository = void 0;
const completed_challenge_entity_1 = require("./completed-challenge.entity");
const typeorm_1 = require("typeorm");
let CompletedChallengeRepository = class CompletedChallengeRepository extends typeorm_1.Repository {
    async findAll(profile, queryParams, relation) {
        return this.find({
            relations: relation,
            take: queryParams.perPage,
            skip: queryParams.perPage * (queryParams.page - 1),
            where: { profile }
        });
    }
    findWithRelation(id, relation) {
        return this.findOne({ relations: relation, where: { id } });
    }
    async findById(id) {
        return this.findOne({ where: id });
    }
    async findBy(paramToSearch) {
        return this.findOne({ where: paramToSearch });
    }
    store(challengeToBeCreated) {
        return this.insert(challengeToBeCreated);
    }
    async updateCompletedChallenge(challengeCompleted) {
        return await this.update(challengeCompleted.id, challengeCompleted);
    }
    destroy(challenge) {
        this.remove(challenge);
    }
};
CompletedChallengeRepository = __decorate([
    typeorm_1.EntityRepository(completed_challenge_entity_1.CompletedChallenge)
], CompletedChallengeRepository);
exports.CompletedChallengeRepository = CompletedChallengeRepository;
//# sourceMappingURL=challenge-completed.repository.js.map