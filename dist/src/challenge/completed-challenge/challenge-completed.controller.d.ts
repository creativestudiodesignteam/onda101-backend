import { CustomSuccessResponse } from "./dto/custom-success-response";
import { CompletedChallengeService } from "./completed-challenge.service";
import { CompletedChallengeDto } from "./dto/completed-challenge.dto";
import { CompletedChallengeQueryParams } from "./dto/completed-challenge-query-params.dto";
import { ProfileService } from "src/profile/profile.service";
export declare class CompletedChallengeController {
    private completedChallengeService;
    private profileService;
    constructor(completedChallengeService: CompletedChallengeService, profileService: ProfileService);
    findByProfile(req: any, queryParams: CompletedChallengeQueryParams): Promise<{
        pagination: {
            page: number;
            perPage: number;
        };
        challenges_completed: import("./completed-challenge.entity").CompletedChallenge[];
    }>;
    create(req: any, challengeCompleted: any): Promise<CompletedChallengeDto>;
    delete(param: any): Promise<CustomSuccessResponse>;
}
