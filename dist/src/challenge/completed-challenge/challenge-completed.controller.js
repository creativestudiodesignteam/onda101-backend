"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CompletedChallengeController = void 0;
const common_1 = require("@nestjs/common");
const profile_not_exists_1 = require("../../profile/exception/profile-not-exists");
const validation_pipe_1 = require("../../shared/pipes/validation.pipe");
const custom_success_response_1 = require("./dto/custom-success-response");
const exception_1 = require("../exception");
const completed_challenge_service_1 = require("./completed-challenge.service");
const completed_challenge_list_dto_1 = require("./dto/completed-challenge-list.dto");
const completed_challenge_dto_1 = require("./dto/completed-challenge.dto");
const completed_challenge_query_params_dto_1 = require("./dto/completed-challenge-query-params.dto");
const completed_challenge_not_exists_1 = require("./exception/completed-challenge-not-exists");
const jwt_auth_guard_1 = require("../../auth/jwt-auth.guard");
const profile_service_1 = require("../../profile/profile.service");
let CompletedChallengeController = class CompletedChallengeController {
    constructor(completedChallengeService, profileService) {
        this.completedChallengeService = completedChallengeService;
        this.profileService = profileService;
    }
    async findByProfile(req, queryParams) {
        try {
            const findProfileByUser = await this.profileService.findByUser(req.user.id);
            const findedChallenges = await this.completedChallengeService.findByProfile(findProfileByUser.id, queryParams);
            const paginatedList = new completed_challenge_list_dto_1.CompletedChallengeListDto(findedChallenges, queryParams.page, queryParams.perPage);
            return paginatedList.getPaginatedData();
        }
        catch (error) {
            if (error instanceof profile_not_exists_1.ProfileNotExists)
                throw new common_1.NotFoundException(error.message);
            if (error instanceof exception_1.ChallengeNotExists)
                throw new common_1.NotFoundException(error.message);
            throw new common_1.InternalServerErrorException({
                error,
                message: "Alguma coisa deu errado. Tente novamente em alguns instantes...",
            });
        }
    }
    async create(req, challengeCompleted) {
        try {
            const findProfileByUser = await this.profileService.findByUser(req.user.id);
            const challengeCompletedCreated = await this.completedChallengeService.create(findProfileByUser.id, challengeCompleted);
            return new completed_challenge_dto_1.CompletedChallengeDto(challengeCompletedCreated.id, challengeCompletedCreated.profile, challengeCompletedCreated.challenge, challengeCompletedCreated.first_time_done_at, challengeCompletedCreated.last_time_done_at, challengeCompletedCreated.redone_times);
        }
        catch (error) {
            if (error instanceof profile_not_exists_1.ProfileNotExists)
                throw new common_1.NotFoundException(error.message);
            if (error instanceof exception_1.ChallengeNotExists)
                throw new common_1.NotFoundException(error.message);
            throw new common_1.InternalServerErrorException({
                message: "Alguma coisa deu errado. Tente novamente em alguns instantes...",
            });
        }
    }
    async delete(param) {
        try {
            await this.completedChallengeService.delete(param.id);
            return new custom_success_response_1.CustomSuccessResponse("Desafio concluido excluído com sucesso");
        }
        catch (error) {
            if (error instanceof exception_1.ChallengeNotExists) {
                throw new common_1.NotFoundException(error.message);
            }
            if (error instanceof completed_challenge_not_exists_1.CompletedhallengeNotExists) {
                throw new common_1.NotFoundException(error.message);
            }
            throw new common_1.InternalServerErrorException({
                message: "Alguma coisa deu errado. Tente novamente em alguns instantes...",
            });
        }
    }
};
__decorate([
    common_1.Get(),
    common_1.UseGuards(jwt_auth_guard_1.JwtAuthGuard),
    __param(0, common_1.Request()),
    __param(1, common_1.Query(new validation_pipe_1.ValidationPipe())),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, completed_challenge_query_params_dto_1.CompletedChallengeQueryParams]),
    __metadata("design:returntype", Promise)
], CompletedChallengeController.prototype, "findByProfile", null);
__decorate([
    common_1.Post(),
    common_1.UseGuards(jwt_auth_guard_1.JwtAuthGuard),
    __param(0, common_1.Request()), __param(1, common_1.Body(new validation_pipe_1.ValidationPipe())),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], CompletedChallengeController.prototype, "create", null);
__decorate([
    common_1.Delete(":id"),
    common_1.UseGuards(jwt_auth_guard_1.JwtAuthGuard),
    common_1.HttpCode(200),
    __param(0, common_1.Param()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], CompletedChallengeController.prototype, "delete", null);
CompletedChallengeController = __decorate([
    common_1.Controller("completed-challenge"),
    __metadata("design:paramtypes", [completed_challenge_service_1.CompletedChallengeService,
        profile_service_1.ProfileService])
], CompletedChallengeController);
exports.CompletedChallengeController = CompletedChallengeController;
//# sourceMappingURL=challenge-completed.controller.js.map