"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CompletedChallengeListDto = void 0;
class CompletedChallengeListDto {
    constructor(challenges_completed, page, perPage) {
        this.challenges_completed = challenges_completed;
        this.page = page;
        this.perPage = perPage;
    }
    getPaginatedData() {
        return {
            pagination: {
                page: this.page,
                perPage: this.perPage,
            },
            challenges_completed: this.challenges_completed,
        };
    }
}
exports.CompletedChallengeListDto = CompletedChallengeListDto;
//# sourceMappingURL=completed-challenge-list.dto.js.map