"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CompletedChallengeDto = void 0;
const challenge_entity_1 = require("../../challenge.entity");
const profile_entity_1 = require("../../../profile/profile.entity");
class CompletedChallengeDto {
    constructor(id, profile, challenge, first_time_done_at, last_time_done_at, redone_times) {
        this.id = id;
        this.profile = profile;
        this.challenge = challenge;
        this.first_time_done_at = first_time_done_at;
        this.last_time_done_at = last_time_done_at;
        this.redone_times = redone_times;
    }
}
exports.CompletedChallengeDto = CompletedChallengeDto;
//# sourceMappingURL=completed-challenge.dto.js.map