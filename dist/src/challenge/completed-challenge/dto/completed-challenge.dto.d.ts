import { Challenge } from "src/challenge/challenge.entity";
import { Profile } from "src/profile/profile.entity";
export declare class CompletedChallengeDto {
    id: number;
    profile: Profile;
    challenge: Challenge;
    first_time_done_at: Date;
    last_time_done_at: Date;
    redone_times: number;
    constructor(id: number, profile: Profile, challenge: Challenge, first_time_done_at: Date, last_time_done_at: Date, redone_times: number);
}
