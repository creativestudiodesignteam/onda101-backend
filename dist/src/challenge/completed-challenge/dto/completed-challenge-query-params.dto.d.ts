export declare class CompletedChallengeQueryParams {
    perPage: number;
    page: number;
}
