import { PaginatedList } from "./../paginated-list";
import { CompletedChallenge } from "./../completed-challenge.entity";
export declare class CompletedChallengeListDto implements PaginatedList {
    challenges_completed: CompletedChallenge[];
    page: number;
    perPage: number;
    constructor(challenges_completed: CompletedChallenge[], page: number, perPage: number);
    getPaginatedData(): {
        pagination: {
            page: number;
            perPage: number;
        };
        challenges_completed: CompletedChallenge[];
    };
}
