export declare class CreateCompletedChallengeDto {
    challenge_id: number;
    first_time_done_at: Date;
    last_time_done_at: Date;
    redone_times: number;
}
