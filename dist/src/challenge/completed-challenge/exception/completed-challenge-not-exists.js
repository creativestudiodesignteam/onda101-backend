"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CompletedhallengeNotExists = void 0;
class CompletedhallengeNotExists extends Error {
    constructor() {
        super(...arguments);
        this.message = "Desafio completo não existe";
    }
}
exports.CompletedhallengeNotExists = CompletedhallengeNotExists;
//# sourceMappingURL=completed-challenge-not-exists.js.map