export declare class CompletedhallengeNotExists extends Error {
    readonly message = "Desafio completo n\u00E3o existe";
}
