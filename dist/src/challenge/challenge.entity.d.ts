import { ChallengeLevel } from "./challenge-level/challenge-level.entity";
import { CompletedChallenge } from "./completed-challenge/completed-challenge.entity";
import { CurrentChallengeStatus } from "./current-challenge-status/current-challenge-status.entity";
import { ChallengeRating } from "./challenge-rating/challenge-rating.entity";
import { RedoneTimes } from "./redone-times/redone-times.entity";
export declare class Challenge {
    id: number;
    title: string;
    imperativePhrase: string;
    description: string;
    completionMessage: string;
    number: number;
    score: number;
    isExtra: boolean;
    completedChallenges: CompletedChallenge[];
    challengesInProgress: CurrentChallengeStatus[];
    level: ChallengeLevel;
    redoneTimes: RedoneTimes;
    challengeRatings: ChallengeRating[];
    created_at: Date;
    updated_at: Date;
}
