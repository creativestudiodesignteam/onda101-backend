import { InternalServerErrorException } from "@nestjs/common";
import { ProfileService } from "src/profile/profile.service";
import { UserService } from "src/user/user.service";
import { ChallengeService } from "../challenge.service";
import { CurrentChallengeStatusService } from "./current-challenge-status.service";
import { ResponseListDto } from "./dto/response-list.dto";
import { UpgradeCurrentChallengeStatusDto } from "./dto/upgrade-current-challenge-status.dto";
export declare class CurrentChallengeStatusController {
    private service;
    private profileService;
    private userService;
    private challengeService;
    constructor(service: CurrentChallengeStatusService, profileService: ProfileService, userService: UserService, challengeService: ChallengeService);
    challengeStatusListByCurrentProfilesChallengeLevel(request: any): Promise<typeof InternalServerErrorException | ResponseListDto>;
    challengeStatusListByCurrentProfilesChallengeLevelDone(request: any): Promise<typeof InternalServerErrorException | ResponseListDto>;
    update(request: any, param: any, incomingChanges: UpgradeCurrentChallengeStatusDto): Promise<import("./current-challenge-status.entity").CurrentChallengeStatus>;
}
