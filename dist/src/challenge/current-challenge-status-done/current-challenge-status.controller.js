"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CurrentChallengeStatusController = void 0;
const common_1 = require("@nestjs/common");
const jwt_auth_guard_1 = require("../../auth/jwt-auth.guard");
const profile_service_1 = require("../../profile/profile.service");
const validation_pipe_1 = require("../../shared/pipes/validation.pipe");
const user_service_1 = require("../../user/user.service");
const challenge_service_1 = require("../challenge.service");
const challenge_not_exists_1 = require("../exception/challenge-not-exists");
const current_challenge_status_service_1 = require("./current-challenge-status.service");
const response_list_dto_1 = require("./dto/response-list.dto");
const upgrade_current_challenge_status_dto_1 = require("./dto/upgrade-current-challenge-status.dto");
const same_status_1 = require("./exception/same-status");
let CurrentChallengeStatusController = class CurrentChallengeStatusController {
    constructor(service, profileService, userService, challengeService) {
        this.service = service;
        this.profileService = profileService;
        this.userService = userService;
        this.challengeService = challengeService;
    }
    async challengeStatusListByCurrentProfilesChallengeLevel(request) {
        try {
            const user = await this.userService.findById(request.user.id);
            const profile = await this.profileService.findByUser(user);
            const list = await this.service.getProfilesCurrentChallengeLevelStatusList(profile);
            return new response_list_dto_1.ResponseListDto(list);
        }
        catch (error) {
            return common_1.InternalServerErrorException;
        }
    }
    async challengeStatusListByCurrentProfilesChallengeLevelDone(request) {
        try {
            const user = await this.userService.findById(request.user.id);
            const profile = await this.profileService.findByUser(user);
            const list = await this.service.getProfilesCurrentChallengeLevelStatusListDone(profile);
            return new response_list_dto_1.ResponseListDto(list);
        }
        catch (error) {
            return common_1.InternalServerErrorException;
        }
    }
    async update(request, param, incomingChanges) {
        try {
            const user = await this.userService.findById(request.user.id);
            const profile = await this.profileService.findByUser(user);
            const challenge = await this.challengeService.findById(param.id);
            const currentChallengeStatus = await this.service.getByProfileAndChallenge(profile, challenge);
            await this.service.updateChallengeStatus(currentChallengeStatus, incomingChanges.nextStatus);
            const updatedRecord = await this.service.getById(currentChallengeStatus.id);
            return updatedRecord;
        }
        catch (error) {
            if (error instanceof challenge_not_exists_1.ChallengeNotExists)
                throw new common_1.NotFoundException("Desafio não encontrado. Verifique e tente novamente.");
            if (error instanceof same_status_1.SameStatusException)
                throw new common_1.BadRequestException(error.message);
            throw common_1.InternalServerErrorException;
        }
    }
};
__decorate([
    common_1.Get(),
    common_1.UseGuards(jwt_auth_guard_1.JwtAuthGuard),
    __param(0, common_1.Request()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], CurrentChallengeStatusController.prototype, "challengeStatusListByCurrentProfilesChallengeLevel", null);
__decorate([
    common_1.Get("/challenge"),
    common_1.UseGuards(jwt_auth_guard_1.JwtAuthGuard),
    __param(0, common_1.Request()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], CurrentChallengeStatusController.prototype, "challengeStatusListByCurrentProfilesChallengeLevelDone", null);
__decorate([
    common_1.Put("/challenge/:id/nextStatus"),
    common_1.UseGuards(jwt_auth_guard_1.JwtAuthGuard),
    common_1.HttpCode(200),
    __param(0, common_1.Request()),
    __param(1, common_1.Param()),
    __param(2, common_1.Body(new validation_pipe_1.ValidationPipe())),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, upgrade_current_challenge_status_dto_1.UpgradeCurrentChallengeStatusDto]),
    __metadata("design:returntype", Promise)
], CurrentChallengeStatusController.prototype, "update", null);
CurrentChallengeStatusController = __decorate([
    common_1.Controller("current-challenge-status"),
    __param(2, common_1.Inject(common_1.forwardRef(() => user_service_1.UserService))),
    __metadata("design:paramtypes", [current_challenge_status_service_1.CurrentChallengeStatusService,
        profile_service_1.ProfileService,
        user_service_1.UserService,
        challenge_service_1.ChallengeService])
], CurrentChallengeStatusController);
exports.CurrentChallengeStatusController = CurrentChallengeStatusController;
//# sourceMappingURL=current-challenge-status.controller.js.map