"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CurrentChallengeStatusRepository = void 0;
const profile_entity_1 = require("../../profile/profile.entity");
const typeorm_1 = require("typeorm");
const current_challenge_status_entity_1 = require("./current-challenge-status.entity");
let CurrentChallengeStatusRepository = class CurrentChallengeStatusRepository extends typeorm_1.Repository {
    findByProfileAndChallenge(profile, challenge) {
        return super.findOne({ where: { profile, challengeInProgress: challenge } });
    }
    findById(id) {
        return super.findOne(id);
    }
    updateStatus(id, changes) {
        return super.update(id, changes);
    }
    statusListFromGivenProfileLevel(profile) {
        return super
            .createQueryBuilder("progress")
            .innerJoin("progress.profile", "profile")
            .leftJoinAndSelect("progress.challengeInProgress", "challenge")
            .leftJoinAndSelect("progress.status", "status")
            .where("challenge.challenge_level_id = :desiredLevel", { desiredLevel: profile.challengeLevel.id })
            .andWhere("profile.id = :currentProfileId", { currentProfileId: profile.id })
            .getMany();
    }
};
CurrentChallengeStatusRepository = __decorate([
    typeorm_1.EntityRepository(current_challenge_status_entity_1.CurrentChallengeStatus)
], CurrentChallengeStatusRepository);
exports.CurrentChallengeStatusRepository = CurrentChallengeStatusRepository;
//# sourceMappingURL=current-challenge-status.repository.js.map