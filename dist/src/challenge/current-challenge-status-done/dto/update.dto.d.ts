import { ChallengeStatus } from "../challenge-status/challenge-status.entity";
export declare class CurrentChallengeStatusUpdateDto {
    challengeId: number;
    profileId: number;
    status: ChallengeStatus;
}
