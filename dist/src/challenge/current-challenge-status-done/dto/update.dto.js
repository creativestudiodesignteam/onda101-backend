"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CurrentChallengeStatusUpdateDto = void 0;
const class_validator_1 = require("class-validator");
const challenge_status_entity_1 = require("../challenge-status/challenge-status.entity");
class CurrentChallengeStatusUpdateDto {
}
__decorate([
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], CurrentChallengeStatusUpdateDto.prototype, "challengeId", void 0);
__decorate([
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], CurrentChallengeStatusUpdateDto.prototype, "profileId", void 0);
__decorate([
    class_validator_1.IsNotEmpty(),
    __metadata("design:type", challenge_status_entity_1.ChallengeStatus)
], CurrentChallengeStatusUpdateDto.prototype, "status", void 0);
exports.CurrentChallengeStatusUpdateDto = CurrentChallengeStatusUpdateDto;
//# sourceMappingURL=update.dto.js.map