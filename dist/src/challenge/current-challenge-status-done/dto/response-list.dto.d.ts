import { CurrentChallengeStatus } from "../current-challenge-status.entity";
export declare class ResponseListDto {
    currentChallengesStatus: CurrentChallengeStatus[];
    constructor(currentChallengesStatus: CurrentChallengeStatus[]);
}
