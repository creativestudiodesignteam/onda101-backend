"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChallengeAlreadyDoneException = void 0;
class ChallengeAlreadyDoneException extends Error {
    constructor() {
        super("Não é possível finalizar um desafafio já concluído.");
    }
}
exports.ChallengeAlreadyDoneException = ChallengeAlreadyDoneException;
//# sourceMappingURL=challenge-already-done.js.map