import { FindConditions } from "typeorm";
import { ChallengeStatus } from "./challenge-status.entity";
import { ChallengeStatusRepository } from "./challenge-status.repository";
export declare class ChallengeStatusService {
    private repository;
    constructor(repository: ChallengeStatusRepository);
    findOneByCryteria(cryteria: FindConditions<ChallengeStatus>): Promise<ChallengeStatus>;
}
