export declare class ChallengeQueryParams {
    perPage: number;
    page: number;
}
