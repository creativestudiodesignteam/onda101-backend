export declare class CreateChallengeDto {
    title: string;
    imperativePhrase: string;
    description: string;
    completionMessage: string;
    number: number;
    score: number;
    isExtra: boolean;
    challengeLevelId: number;
}
