"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LevelUpProfileChallengeLevelResponseDto = void 0;
class LevelUpProfileChallengeLevelResponseDto {
    constructor(challengeLevel, isFirstTimeFinishingThisChallenge) {
        this.challengeLevel = challengeLevel;
        this.isFirstTimeFinishingThisChallenge = isFirstTimeFinishingThisChallenge;
    }
}
exports.LevelUpProfileChallengeLevelResponseDto = LevelUpProfileChallengeLevelResponseDto;
//# sourceMappingURL=levelup-profile-challenge-level-response.dto.js.map