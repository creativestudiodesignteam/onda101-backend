"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChallengeDto = void 0;
class ChallengeDto {
    constructor(id, title, imperativePhrase, description, completionMessage, number, score, isExtra, created_at, updated_at) {
        this.id = id;
        this.title = title;
        this.imperativePhrase = imperativePhrase;
        this.description = description;
        this.completionMessage = completionMessage;
        this.number = number;
        this.score = score;
        this.isExtra = isExtra;
        this.created_at = created_at !== null && created_at !== void 0 ? created_at : undefined;
        this.updated_at = updated_at !== null && updated_at !== void 0 ? updated_at : undefined;
    }
}
exports.ChallengeDto = ChallengeDto;
//# sourceMappingURL=challenge.dto.js.map