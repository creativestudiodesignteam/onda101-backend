"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChallengeListDto = void 0;
class ChallengeListDto {
    constructor(challenges, page, perPage) {
        this.challenges = challenges;
        this.page = page;
        this.perPage = perPage;
    }
    getPaginatedData() {
        return {
            pagination: {
                page: this.page,
                perPage: this.perPage,
            },
            challenges: this.challenges,
        };
    }
}
exports.ChallengeListDto = ChallengeListDto;
//# sourceMappingURL=challenge-list.dto.js.map