export declare class CustomSuccessResponse {
    private message;
    private body?;
    constructor(message: string, body?: any);
    getMessage(): string;
    setMessage(message: string): void;
    getBody(): any | undefined;
    setBody(body: any): void;
}
