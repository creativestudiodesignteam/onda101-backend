export declare class ChallengeDto {
    id: number;
    title: string;
    imperativePhrase: string;
    description: string;
    completionMessage: string;
    number: number;
    score: number;
    isExtra: boolean;
    created_at?: Date;
    updated_at?: Date;
    constructor(id: number, title: string, imperativePhrase: string, description: string, completionMessage: string, number: number, score: number, isExtra: boolean, created_at?: Date, updated_at?: Date);
}
