export declare class LevelUpProfileChallengeLevelResponseDto {
    challengeLevel: number;
    isFirstTimeFinishingThisChallenge: boolean;
    constructor(challengeLevel: number, isFirstTimeFinishingThisChallenge: boolean);
}
