export declare class EditChallengeDto {
    title: string;
    imperativePhrase: string;
    description: string;
    completionMessage: string;
    number: number;
    isExtra: boolean;
}
