import { PaginatedList } from "./../paginated-list";
import { Challenge } from "./../challenge.entity";
export declare class ChallengeListDto implements PaginatedList {
    challenges: Challenge[];
    page: number;
    perPage: number;
    constructor(challenges: Challenge[], page: number, perPage: number);
    getPaginatedData(): {
        pagination: {
            page: number;
            perPage: number;
        };
        challenges: Challenge[];
    };
}
