import { Challenge } from "./challenge.entity";
import { DeepPartial, Repository } from "typeorm";
import { ChallengeQueryParams } from "./dto/challenge-query-params.dto";
export declare class ChallengeRepository extends Repository<Challenge> {
    findAll(queryParams: ChallengeQueryParams): Promise<Challenge[]>;
    findById(id: number): Promise<Challenge>;
    findBy(paramToSearch: any): Promise<Challenge>;
    store(challengeToBeCreated: Challenge): Promise<import("typeorm").InsertResult>;
    updateChallenge(challenge: DeepPartial<Challenge>): Promise<void>;
    destroy(challenge: Challenge): void;
}
