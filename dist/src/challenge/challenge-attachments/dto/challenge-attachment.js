"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChallengeAttachmentDto = void 0;
class ChallengeAttachmentDto {
    constructor(id, value, description, challenge_id, created_at, updated_at) {
        this.id = id;
        this.value = value;
        this.description = description !== null && description !== void 0 ? description : null;
        this.challenge_id = challenge_id;
        this.created_at = created_at !== null && created_at !== void 0 ? created_at : undefined;
        this.updated_at = updated_at !== null && updated_at !== void 0 ? updated_at : undefined;
    }
}
exports.ChallengeAttachmentDto = ChallengeAttachmentDto;
//# sourceMappingURL=challenge-attachment.js.map