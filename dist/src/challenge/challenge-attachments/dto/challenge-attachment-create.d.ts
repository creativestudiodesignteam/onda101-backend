export declare class CreateChallengeAttachment {
    description: string;
    challenge_id: number;
}
