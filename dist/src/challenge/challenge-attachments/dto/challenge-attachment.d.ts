export declare class ChallengeAttachmentDto {
    id: number;
    value: string;
    description?: string;
    challenge_id: number;
    created_at?: Date;
    updated_at?: Date;
    constructor(id: number, value: string, description?: string, challenge_id?: number, created_at?: Date, updated_at?: Date);
}
