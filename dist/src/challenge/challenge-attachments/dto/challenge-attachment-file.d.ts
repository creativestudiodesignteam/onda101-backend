export declare class ChallengeAttachmentFile {
    filename: string;
    originalname: string;
}
