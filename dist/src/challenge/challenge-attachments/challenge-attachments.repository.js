"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChallengeAttachmentsRepository = void 0;
const challenge_attachments_entity_1 = require("./challenge-attachments.entity");
const typeorm_1 = require("typeorm");
let ChallengeAttachmentsRepository = class ChallengeAttachmentsRepository extends typeorm_1.Repository {
    findById(id) {
        return this.findOne(id);
    }
    async findBy(paramToSearch) {
        return this.findOne({ where: paramToSearch });
    }
    store(challengeToBeCreated) {
        return this.insert(challengeToBeCreated);
    }
    async updateChallenge(challenge) {
        await this.update(challenge.id, challenge);
        return;
    }
    destroy(challenge) {
        this.remove(challenge);
    }
};
ChallengeAttachmentsRepository = __decorate([
    typeorm_1.EntityRepository(challenge_attachments_entity_1.ChallengeAttachments)
], ChallengeAttachmentsRepository);
exports.ChallengeAttachmentsRepository = ChallengeAttachmentsRepository;
//# sourceMappingURL=challenge-attachments.repository.js.map