export declare class ChallengeAttachments {
    id: number;
    description: string;
    value: string;
    challenge_id: number;
    created_at: Date;
    updated_at: Date;
}
