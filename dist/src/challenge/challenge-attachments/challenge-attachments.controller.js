"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChallengeAttachmentsController = void 0;
const common_1 = require("@nestjs/common");
const platform_express_1 = require("@nestjs/platform-express");
const multer_1 = require("multer");
const upload_files_1 = require("../../shared/utils/upload-files");
const challenge_attachments_service_1 = require("./challenge-attachments.service");
const validation_pipe_1 = require("../../shared/pipes/validation.pipe");
const challenge_attachment_file_1 = require("./dto/challenge-attachment-file");
const challenge_attachment_create_1 = require("./dto/challenge-attachment-create");
const challenge_attachment_not_send_file_1 = require("./exception/challenge-attachment-not-send-file");
const challenge_attachment_1 = require("./dto/challenge-attachment");
const challenge_attachment_not_exists_1 = require("./exception/challenge-attachment-not-exists");
const challenge_attachment_search_1 = require("./dto/challenge-attachment-search");
const custom_success_response_1 = require("./dto/custom-success-response");
const FileIsNotExists_1 = require("../../shared/upload-files/exception/FileIsNotExists");
const challenge_attachment_edit_1 = require("./dto/challenge-attachment-edit");
const exception_1 = require("../exception");
const rxjs_1 = require("rxjs");
const path_1 = require("path");
const object_not_found_exception_1 = require("../challenge-rating/exception/object-not-found-exception");
const challenge_service_1 = require("../challenge.service");
let ChallengeAttachmentsController = class ChallengeAttachmentsController {
    constructor(challengeAttachmentsService, challengeService) {
        this.challengeAttachmentsService = challengeAttachmentsService;
        this.challengeService = challengeService;
    }
    async migration() {
    }
    async create(req, file, challengeAttachments) {
        try {
            const challengeAttachment = await this.challengeAttachmentsService.create({
                description: challengeAttachments.description,
                challenge_id: challengeAttachments.challenge_id,
                file: file,
            });
            return new challenge_attachment_1.ChallengeAttachmentDto(challengeAttachment.id, challengeAttachment.value, challengeAttachment.description, challengeAttachment.challenge_id, challengeAttachment.created_at, challengeAttachment.updated_at);
        }
        catch (error) {
            if (error instanceof challenge_attachment_not_send_file_1.ChallengeAttachmentNotSendFile)
                throw new common_1.BadRequestException(error.message);
            if (error instanceof exception_1.ChallengeNotExists)
                throw new common_1.NotFoundException(error.message);
            throw new common_1.InternalServerErrorException({
                message: "Alguma coisa deu errado. Tente novamente em alguns instantes...",
            });
        }
    }
    async pickUpChallengeImage(param, res) {
        console.log("request send get image app");
        try {
            await this.challengeService.findById(param.id);
            const firstImage = await this.challengeAttachmentsService.pickUpFirstChallengeImage(param.id);
            console.log(firstImage);
            return rxjs_1.of(res.sendFile(path_1.join(process.cwd(), `upload/${firstImage.value}`)));
        }
        catch (error) {
            console.log(error);
            if (error instanceof object_not_found_exception_1.ObjectNotFoundException)
                throw new common_1.NotFoundException("Nenhuma imagem encontrada para o desafio solicitado");
            if (error instanceof exception_1.ChallengeNotExists)
                throw new common_1.NotFoundException(error.message);
            throw new common_1.InternalServerErrorException();
        }
    }
    async findById(param) {
        try {
            const challengeAttachment = await this.challengeAttachmentsService.findById(param.id);
            return new challenge_attachment_1.ChallengeAttachmentDto(challengeAttachment.id, challengeAttachment.value, challengeAttachment.description, challengeAttachment.challenge_id, challengeAttachment.created_at, challengeAttachment.updated_at);
        }
        catch (error) {
            if (error instanceof challenge_attachment_not_exists_1.ChallengeAttachmentsNotExists)
                throw new common_1.NotFoundException(error.message);
            throw new common_1.InternalServerErrorException({
                message: "Alguma coisa deu errado. Tente novamente em alguns instantes...",
            });
        }
    }
    async findByValue(query) {
        try {
            const challengeAttachmentFinded = await this.challengeAttachmentsService.findByValue(query.value);
            return new challenge_attachment_1.ChallengeAttachmentDto(challengeAttachmentFinded.id, challengeAttachmentFinded.value, challengeAttachmentFinded.description, challengeAttachmentFinded.challenge_id, challengeAttachmentFinded.created_at, challengeAttachmentFinded.updated_at);
        }
        catch (error) {
            if (error instanceof challenge_attachment_not_exists_1.ChallengeAttachmentsNotExists)
                throw new common_1.NotFoundException(error.message);
            throw new common_1.InternalServerErrorException({
                message: "Alguma coisa deu errado. Tente novamente em alguns instantes...",
            });
        }
    }
    async update(param, req, file, challengeAttachments) {
        var _a;
        try {
            const data = {
                file: file,
                description: (_a = challengeAttachments.description) !== null && _a !== void 0 ? _a : null,
                challenge_id: challengeAttachments.challenge_id,
            };
            const updatedChallenge = await this.challengeAttachmentsService.update(param.id, file, data);
            return new challenge_attachment_1.ChallengeAttachmentDto(updatedChallenge.id, updatedChallenge.value, updatedChallenge.description, updatedChallenge.challenge_id, updatedChallenge.created_at, updatedChallenge.updated_at);
        }
        catch (error) {
            if (error instanceof challenge_attachment_not_exists_1.ChallengeAttachmentsNotExists)
                throw new common_1.NotFoundException(error.message);
            if (error instanceof exception_1.ChallengeNotExists)
                throw new common_1.NotFoundException(error.message);
            if (error instanceof challenge_attachment_not_send_file_1.ChallengeAttachmentNotSendFile) {
                throw new common_1.BadRequestException(error.message);
            }
            throw new common_1.InternalServerErrorException({
                error,
                message: "Alguma coisa deu errado. Tente novamente em alguns instantes...",
            });
        }
    }
    async delete(param) {
        try {
            await this.challengeAttachmentsService.delete(param.id);
            return new custom_success_response_1.CustomSuccessResponse("Desafio excluído com sucesso");
        }
        catch (error) {
            if (error instanceof challenge_attachment_not_exists_1.ChallengeAttachmentsNotExists) {
                throw new common_1.NotFoundException(error.message);
            }
            if (error instanceof FileIsNotExists_1.FileIsNotExists) {
                throw new common_1.NotFoundException(error.message);
            }
            throw new common_1.InternalServerErrorException({
                error,
                message: "Alguma coisa deu errado. Tente novamente em alguns instantes...",
            });
        }
    }
};
__decorate([
    common_1.Get("/migration"),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], ChallengeAttachmentsController.prototype, "migration", null);
__decorate([
    common_1.Post(),
    common_1.UseInterceptors(platform_express_1.FileInterceptor("file", {
        fileFilter: (req, file, cb) => {
            if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
                req.fileErrors = {
                    error: true,
                };
                return cb(null, false);
            }
            return cb(null, true);
        },
        storage: multer_1.diskStorage({
            destination: "./upload",
            filename: upload_files_1.editFileName,
        }),
    })),
    __param(0, common_1.Req()),
    __param(1, common_1.UploadedFile()),
    __param(2, common_1.Body(new validation_pipe_1.ValidationPipe())),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, challenge_attachment_file_1.ChallengeAttachmentFile,
        challenge_attachment_create_1.CreateChallengeAttachment]),
    __metadata("design:returntype", Promise)
], ChallengeAttachmentsController.prototype, "create", null);
__decorate([
    common_1.Get("challenge/:id"),
    common_1.HttpCode(200),
    __param(0, common_1.Param()), __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], ChallengeAttachmentsController.prototype, "pickUpChallengeImage", null);
__decorate([
    common_1.Get(":id"),
    common_1.HttpCode(200),
    __param(0, common_1.Param()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ChallengeAttachmentsController.prototype, "findById", null);
__decorate([
    common_1.Get(),
    common_1.HttpCode(200),
    __param(0, common_1.Query()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [challenge_attachment_search_1.ChallengeAttachmentSearch]),
    __metadata("design:returntype", Promise)
], ChallengeAttachmentsController.prototype, "findByValue", null);
__decorate([
    common_1.Put(":id"),
    common_1.UseInterceptors(platform_express_1.FileInterceptor("file", {
        fileFilter: (req, file, cb) => {
            if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
                req.fileErrors = {
                    error: true,
                };
                return cb(null, false);
            }
            return cb(null, true);
        },
        storage: multer_1.diskStorage({
            destination: "./upload",
            filename: upload_files_1.editFileName,
        }),
    })),
    __param(0, common_1.Param()),
    __param(1, common_1.Req()),
    __param(2, common_1.UploadedFile()),
    __param(3, common_1.Body(new validation_pipe_1.ValidationPipe())),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, challenge_attachment_file_1.ChallengeAttachmentFile,
        challenge_attachment_edit_1.EditChallengeAttachment]),
    __metadata("design:returntype", Promise)
], ChallengeAttachmentsController.prototype, "update", null);
__decorate([
    common_1.Delete(":id"),
    common_1.HttpCode(200),
    __param(0, common_1.Param()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ChallengeAttachmentsController.prototype, "delete", null);
ChallengeAttachmentsController = __decorate([
    common_1.Controller("challenge-attachments"),
    __metadata("design:paramtypes", [challenge_attachments_service_1.ChallengeAttachmentsService,
        challenge_service_1.ChallengeService])
], ChallengeAttachmentsController);
exports.ChallengeAttachmentsController = ChallengeAttachmentsController;
//# sourceMappingURL=challenge-attachments.controller.js.map