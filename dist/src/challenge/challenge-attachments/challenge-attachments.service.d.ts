import { ChallengeAttachments } from "./challenge-attachments.entity";
import { ChallengeAttachmentsRepository } from "./challenge-attachments.repository";
import { UploadFilesService } from "src/shared/upload-files/upload-files.service";
import { DeepPartial } from "typeorm";
import { ChallengeRepository } from "../challenge.repository";
export declare class ChallengeAttachmentsService {
    challengeAttachmentsRepository: ChallengeAttachmentsRepository;
    challengeRepository: ChallengeRepository;
    private uploadFilesService;
    constructor(challengeAttachmentsRepository: ChallengeAttachmentsRepository, challengeRepository: ChallengeRepository, uploadFilesService: UploadFilesService);
    create(challengeAttachmentToBeCreated: any): Promise<ChallengeAttachments>;
    findByValue(value: string): Promise<ChallengeAttachments>;
    findById(id: number): Promise<ChallengeAttachments>;
    pickUpFirstChallengeImage(challengeId: number): Promise<ChallengeAttachments>;
    update(id: number, file: any, challengeAttachmentToBeCreated: DeepPartial<ChallengeAttachments>): Promise<ChallengeAttachments>;
    delete(id: number): Promise<void>;
}
