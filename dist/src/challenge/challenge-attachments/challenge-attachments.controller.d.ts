import { ChallengeAttachmentsService } from "./challenge-attachments.service";
import { ChallengeAttachmentFile } from "./dto/challenge-attachment-file";
import { CreateChallengeAttachment } from "./dto/challenge-attachment-create";
import { ChallengeAttachmentDto } from "./dto/challenge-attachment";
import { ChallengeAttachmentSearch } from "./dto/challenge-attachment-search";
import { CustomSuccessResponse } from "./dto/custom-success-response";
import { EditChallengeAttachment } from "./dto/challenge-attachment-edit";
import { ChallengeService } from "../challenge.service";
export declare class ChallengeAttachmentsController {
    private challengeAttachmentsService;
    private challengeService;
    constructor(challengeAttachmentsService: ChallengeAttachmentsService, challengeService: ChallengeService);
    migration(): Promise<void>;
    create(req: any, file: ChallengeAttachmentFile, challengeAttachments: CreateChallengeAttachment): Promise<ChallengeAttachmentDto>;
    pickUpChallengeImage(param: any, res: any): Promise<import("rxjs").Observable<any>>;
    findById(param: any): Promise<ChallengeAttachmentDto>;
    findByValue(query: ChallengeAttachmentSearch): Promise<ChallengeAttachmentDto>;
    update(param: any, req: any, file: ChallengeAttachmentFile, challengeAttachments: EditChallengeAttachment): Promise<any>;
    delete(param: any): Promise<CustomSuccessResponse>;
}
