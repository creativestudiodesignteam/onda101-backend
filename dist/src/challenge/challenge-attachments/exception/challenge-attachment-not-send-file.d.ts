export declare class ChallengeAttachmentNotSendFile extends Error {
    readonly message = "Arquivo inv\u00E1lido, verifique se o arquivo segue os padr\u00F5es estabelecidos";
}
