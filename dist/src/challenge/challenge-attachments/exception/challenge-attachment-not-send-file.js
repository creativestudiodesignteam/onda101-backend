"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChallengeAttachmentNotSendFile = void 0;
class ChallengeAttachmentNotSendFile extends Error {
    constructor() {
        super(...arguments);
        this.message = "Arquivo inválido, verifique se o arquivo segue os padrões estabelecidos";
    }
}
exports.ChallengeAttachmentNotSendFile = ChallengeAttachmentNotSendFile;
//# sourceMappingURL=challenge-attachment-not-send-file.js.map