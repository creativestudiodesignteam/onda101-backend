"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChallengeAttachmentsAlreadyExists = void 0;
class ChallengeAttachmentsAlreadyExists extends Error {
    constructor() {
        super(...arguments);
        this.message = "Anexos de desafios já existem";
    }
}
exports.ChallengeAttachmentsAlreadyExists = ChallengeAttachmentsAlreadyExists;
//# sourceMappingURL=challenge-attachments-already-exists.js.map