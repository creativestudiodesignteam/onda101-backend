"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChallengeAttachmentsNotExists = void 0;
class ChallengeAttachmentsNotExists extends Error {
    constructor() {
        super(...arguments);
        this.message = "Anexo não existe";
    }
}
exports.ChallengeAttachmentsNotExists = ChallengeAttachmentsNotExists;
//# sourceMappingURL=challenge-attachment-not-exists.js.map