export declare class ChallengeAttachmentsAlreadyExists extends Error {
    readonly message = "Anexos de desafios j\u00E1 existem";
}
