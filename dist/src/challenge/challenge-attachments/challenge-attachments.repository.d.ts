import { ChallengeAttachments } from "./challenge-attachments.entity";
import { DeepPartial, Repository } from "typeorm";
export declare class ChallengeAttachmentsRepository extends Repository<ChallengeAttachments> {
    findById(id: number): Promise<ChallengeAttachments>;
    findBy(paramToSearch: any): Promise<ChallengeAttachments>;
    store(challengeToBeCreated: ChallengeAttachments): Promise<import("typeorm").InsertResult>;
    updateChallenge(challenge: DeepPartial<ChallengeAttachments>): Promise<void>;
    destroy(challenge: ChallengeAttachments): void;
}
