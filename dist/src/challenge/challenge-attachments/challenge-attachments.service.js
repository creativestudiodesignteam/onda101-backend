"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChallengeAttachmentsService = void 0;
const common_1 = require("@nestjs/common");
const challenge_attachments_entity_1 = require("./challenge-attachments.entity");
const challenge_attachments_repository_1 = require("./challenge-attachments.repository");
const challenge_attachment_not_exists_1 = require("./exception/challenge-attachment-not-exists");
const challenge_attachment_not_send_file_1 = require("./exception/challenge-attachment-not-send-file");
const challenge_attachments_already_exists_1 = require("./exception/challenge-attachments-already-exists");
const upload_files_service_1 = require("../../shared/upload-files/upload-files.service");
const exception_1 = require("../exception");
const challenge_repository_1 = require("../challenge.repository");
const object_not_found_exception_1 = require("../challenge-rating/exception/object-not-found-exception");
let ChallengeAttachmentsService = class ChallengeAttachmentsService {
    constructor(challengeAttachmentsRepository, challengeRepository, uploadFilesService) {
        this.challengeAttachmentsRepository = challengeAttachmentsRepository;
        this.challengeRepository = challengeRepository;
        this.uploadFilesService = uploadFilesService;
    }
    async create(challengeAttachmentToBeCreated) {
        try {
            if (!challengeAttachmentToBeCreated.file)
                throw new challenge_attachment_not_send_file_1.ChallengeAttachmentNotSendFile();
            const givenChallengeNumberAlreadyExists = await this.challengeAttachmentsRepository.findBy({
                value: challengeAttachmentToBeCreated.file.filename,
            });
            if (givenChallengeNumberAlreadyExists)
                throw new challenge_attachments_already_exists_1.ChallengeAttachmentsAlreadyExists();
            const findedChallenge = await this.challengeRepository.findById(challengeAttachmentToBeCreated.challenge_id);
            if (!findedChallenge) {
                await this.uploadFilesService.delete("./upload", challengeAttachmentToBeCreated.file.filename);
                throw new exception_1.ChallengeNotExists();
            }
            const challengeAttachment = new challenge_attachments_entity_1.ChallengeAttachments();
            challengeAttachment.value = challengeAttachmentToBeCreated.file.filename;
            challengeAttachment.description =
                challengeAttachmentToBeCreated.description;
            challengeAttachment.challenge_id =
                challengeAttachmentToBeCreated.challenge_id;
            await this.challengeAttachmentsRepository.store(challengeAttachment);
            return challengeAttachment;
        }
        catch (error) {
            throw error;
        }
    }
    async findByValue(value) {
        const challengeAttachment = await this.challengeAttachmentsRepository.findBy({
            value,
        });
        if (!challengeAttachment)
            throw new challenge_attachment_not_exists_1.ChallengeAttachmentsNotExists();
        return challengeAttachment;
    }
    async findById(id) {
        const challengeAttachment = await this.challengeAttachmentsRepository.findById(id);
        if (!challengeAttachment)
            throw new challenge_attachment_not_exists_1.ChallengeAttachmentsNotExists();
        return challengeAttachment;
    }
    async pickUpFirstChallengeImage(challengeId) {
        try {
            const challengeImages = await this.challengeAttachmentsRepository.find({
                where: { challenge_id: challengeId },
            });
            if (!challengeImages || !challengeImages.length) {
                throw new object_not_found_exception_1.ObjectNotFoundException();
            }
            return challengeImages[0];
        }
        catch (error) {
            throw error;
        }
    }
    async update(id, file, challengeAttachmentToBeCreated) {
        if (!file)
            throw new challenge_attachment_not_send_file_1.ChallengeAttachmentNotSendFile();
        const findedChallengeAttachment = await this.challengeAttachmentsRepository.findById(id);
        if (!findedChallengeAttachment)
            throw new challenge_attachment_not_exists_1.ChallengeAttachmentsNotExists();
        const findedChallenge = await this.challengeRepository.findById(challengeAttachmentToBeCreated.challenge_id);
        if (!findedChallenge) {
            await this.uploadFilesService.delete("./upload", file.filename);
            throw new exception_1.ChallengeNotExists();
        }
        if (file.filename && file.filename !== findedChallengeAttachment.value)
            await this.uploadFilesService.delete("./upload", findedChallengeAttachment.value);
        challengeAttachmentToBeCreated.value = file.filename;
        const challengeAttachment = this.challengeAttachmentsRepository.create(Object.assign({ id }, challengeAttachmentToBeCreated));
        await this.challengeAttachmentsRepository.updateChallenge(challengeAttachment);
        return challengeAttachment;
    }
    async delete(id) {
        const challengeAttachment = await this.challengeAttachmentsRepository.findById(id);
        if (!challengeAttachment)
            throw new challenge_attachment_not_exists_1.ChallengeAttachmentsNotExists();
        this.uploadFilesService.delete("./upload", challengeAttachment.value);
        this.challengeAttachmentsRepository.destroy(challengeAttachment);
    }
};
ChallengeAttachmentsService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [challenge_attachments_repository_1.ChallengeAttachmentsRepository,
        challenge_repository_1.ChallengeRepository,
        upload_files_service_1.UploadFilesService])
], ChallengeAttachmentsService);
exports.ChallengeAttachmentsService = ChallengeAttachmentsService;
//# sourceMappingURL=challenge-attachments.service.js.map