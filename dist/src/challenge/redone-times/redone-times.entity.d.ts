import { Profile } from "src/profile/profile.entity";
import { Challenge } from "../challenge.entity";
export declare class RedoneTimes {
    id: number;
    profile: Profile;
    challenge: Challenge;
    doneAt: Date;
}
