"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RedoneTimesService = void 0;
const common_1 = require("@nestjs/common");
const profile_entity_1 = require("../../profile/profile.entity");
const redone_times_repository_1 = require("./redone-times.repository");
let RedoneTimesService = class RedoneTimesService {
    constructor(repository) {
        this.repository = repository;
    }
    async isFirstTimeFinishingThisChallenge(challenge, profile) {
        const alreadyDone = await this.repository.find({
            where: { challenge, profile },
        });
        if (!alreadyDone.length)
            return true;
        return false;
    }
    create(challenge, profile) {
        const redoneTimes = this.repository.create({ challenge, profile });
        return this.repository.insert(redoneTimes);
    }
};
RedoneTimesService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [redone_times_repository_1.RedoneTimesRepository])
], RedoneTimesService);
exports.RedoneTimesService = RedoneTimesService;
//# sourceMappingURL=redone-times.service.js.map