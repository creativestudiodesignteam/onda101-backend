import { Profile } from "src/profile/profile.entity";
import { Challenge } from "../challenge.entity";
import { RedoneTimesRepository } from "./redone-times.repository";
export declare class RedoneTimesService {
    private repository;
    constructor(repository: RedoneTimesRepository);
    isFirstTimeFinishingThisChallenge(challenge: Challenge, profile: Profile): Promise<boolean>;
    create(challenge: Challenge, profile: Profile): Promise<import("typeorm").InsertResult>;
}
