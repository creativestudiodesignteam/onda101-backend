"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChallengeController = void 0;
const edit_challenge_dto_1 = require("./dto/edit-challenge.dto");
const create_challenge_dto_1 = require("./dto/create-challenge.dto");
const custom_success_response_1 = require("./dto/custom-success-response");
const index_1 = require("./exception/index");
const validation_pipe_1 = require("./../shared/pipes/validation.pipe");
const challenge_list_dto_1 = require("./dto/challenge-list.dto");
const challenge_dto_1 = require("./dto/challenge.dto");
const challenge_query_params_dto_1 = require("./dto/challenge-query-params.dto");
const challenge_service_1 = require("./challenge.service");
const common_1 = require("@nestjs/common");
const current_challenge_status_service_1 = require("./current-challenge-status/current-challenge-status.service");
const jwt_auth_guard_1 = require("../auth/jwt-auth.guard");
const profile_service_1 = require("../profile/profile.service");
const challenge_already_done_1 = require("./current-challenge-status/exception/challenge-already-done");
const AllowedChallengeStatus_1 = require("./current-challenge-status/challenge-status/models/AllowedChallengeStatus");
const same_status_1 = require("./current-challenge-status/exception/same-status");
const levelup_profile_challenge_level_response_dto_1 = require("./dto/levelup-profile-challenge-level-response.dto");
const evaluate_dto_1 = require("./challenge-rating/dto/evaluate.dto");
const challenge_rating_service_1 = require("./challenge-rating/challenge-rating.service");
const object_not_found_exception_1 = require("./challenge-rating/exception/object-not-found-exception");
const rating_response_dto_1 = require("./challenge-rating/dto/rating-response.dto");
const challenge_level_service_1 = require("./challenge-level/challenge-level.service");
const there_is_no_next_level_1 = require("./challenge-level/exception/there-is-no-next-level");
const no_rating_exception_1 = require("./challenge-rating/exception/no-rating-exception");
const moment = require("moment");
let ChallengeController = class ChallengeController {
    constructor(challengeService, currentChallengeStatusService, profilesService, challengeRatingService, challengeLevelService) {
        this.challengeService = challengeService;
        this.currentChallengeStatusService = currentChallengeStatusService;
        this.profilesService = profilesService;
        this.challengeRatingService = challengeRatingService;
        this.challengeLevelService = challengeLevelService;
        console.log("send");
    }
    async monthlyAverage(param) {
        try {
            const challenge = await this.challengeService.findById(param.challengeId);
            const monthAnalyzed = moment().month();
            const average = await this.challengeRatingService.getChallengeRattingMonthlyAverage(challenge.id, monthAnalyzed);
            return {
                challenge: challenge.title,
                monthAnalyzed: monthAnalyzed + 1,
                average,
            };
        }
        catch (error) {
            if (error instanceof index_1.ChallengeNotExists)
                throw new common_1.BadRequestException(error.message);
            if (error instanceof no_rating_exception_1.NoRatingException)
                throw new common_1.NotFoundException(error.message);
            throw new common_1.InternalServerErrorException();
        }
    }
    async getNextChallengesBlocked(request) {
        try {
            const profile = await this.profilesService.findByUser(request.user.id);
            const nextChallengeLevel = await this.challengeLevelService.getNextLevel(profile.challengeLevel.levelNumber);
            const blockedChallenges = await this.challengeService.findByCryteria({
                level: nextChallengeLevel,
            });
            return blockedChallenges;
        }
        catch (error) {
            if (error instanceof there_is_no_next_level_1.ThereIsNoNextLevelException) {
                throw new common_1.BadRequestException(error.message);
            }
            throw new common_1.InternalServerErrorException();
        }
    }
    async findAll(queryParams) {
        try {
            const findedChallenges = await this.challengeService.findAll(queryParams);
            const paginatedList = new challenge_list_dto_1.ChallengeListDto(findedChallenges, queryParams.page, queryParams.perPage);
            return paginatedList.getPaginatedData();
        }
        catch (error) {
            throw new common_1.InternalServerErrorException({
                message: "Alguma coisa deu errado. Tente novamente em alguns instantes... findAll",
            });
        }
    }
    async findById(param) {
        try {
            const challenge = await this.challengeService.findById(param.id);
            return new challenge_dto_1.ChallengeDto(challenge.id, challenge.title, challenge.imperativePhrase, challenge.description, challenge.completionMessage, challenge.number, challenge.score, challenge.isExtra, challenge.created_at, challenge.updated_at);
        }
        catch (error) {
            if (error instanceof index_1.ChallengeNotExists)
                throw new common_1.NotFoundException(error.message);
            throw new common_1.InternalServerErrorException({
                message: "Alguma coisa deu errado. Tente novamente em alguns instantes... FindById",
            });
        }
    }
    async create(challenge) {
        try {
            const createdChallenge = await this.challengeService.create(challenge);
            return new challenge_dto_1.ChallengeDto(createdChallenge.id, createdChallenge.title, createdChallenge.imperativePhrase, createdChallenge.description, createdChallenge.completionMessage, createdChallenge.number, createdChallenge.score, createdChallenge.isExtra);
        }
        catch (error) {
            console.log("error---> ", error);
            if (error instanceof index_1.ChallengeNumberAlreadyExists)
                throw new common_1.BadRequestException(error.message);
            throw new common_1.InternalServerErrorException({
                message: "Alguma coisa deu errado. Tente novamente em alguns instantes... Create",
            });
        }
    }
    async update(param, challenge) {
        try {
            const updatedChallenge = await this.challengeService.update(param.id, challenge);
            return updatedChallenge;
        }
        catch (error) {
            if (error instanceof index_1.ChallengeNotExists) {
                throw new common_1.NotFoundException(error.message);
            }
            if (error instanceof index_1.ChallengeNumberAlreadyExists) {
                throw new common_1.BadRequestException(error.message);
            }
            throw new common_1.InternalServerErrorException({
                message: "Alguma coisa deu errado. Tente novamente em alguns instantes... update",
            });
        }
    }
    async delete(param) {
        try {
            await this.challengeService.delete(param.id);
            return new custom_success_response_1.CustomSuccessResponse("Desafio excluído com sucesso");
        }
        catch (error) {
            if (error instanceof index_1.ChallengeNotExists) {
                throw new common_1.NotFoundException(error.message);
            }
            throw new common_1.InternalServerErrorException({
                message: "Alguma coisa deu errado. Tente novamente em alguns instantes... delete",
            });
        }
    }
    async finishChallenge(param, request) {
        try {
            const profile = await this.profilesService.findByUser(request.user.id);
            const challenge = await this.challengeService.findById(param.id);
            const currentChallengeStatus = await this.currentChallengeStatusService.getByProfileAndChallenge(profile, challenge);
            const infoAboutConclusion = await this.currentChallengeStatusService.updateChallengeStatus(currentChallengeStatus, AllowedChallengeStatus_1.AllowedChallengeStatus.done);
            const profileEarnedNewChallengeLevel = infoAboutConclusion.newProfileChallengeLevel;
            const challengeLevelToBeDisplayed = profileEarnedNewChallengeLevel
                ? profileEarnedNewChallengeLevel.levelNumber
                : profile.challengeLevel.levelNumber;
            return new levelup_profile_challenge_level_response_dto_1.LevelUpProfileChallengeLevelResponseDto(challengeLevelToBeDisplayed, infoAboutConclusion.isFirstTimeFinishingThisChallenge);
        }
        catch (error) {
            if (error instanceof index_1.ChallengeNotExists) {
                throw new common_1.BadRequestException(error.message);
            }
            if (error instanceof challenge_already_done_1.ChallengeAlreadyDoneException) {
                throw new common_1.BadRequestException(error.message);
            }
            if (error instanceof same_status_1.SameStatusException) {
                throw new common_1.BadRequestException(error.message);
            }
            throw new common_1.InternalServerErrorException();
        }
    }
    async challengeEvaluate(param, request, evaluate) {
        try {
            const profile = await this.profilesService.findByUser(request.user.id);
            const challenge = await this.challengeService.findById(param.id);
            await this.challengeRatingService.rateForFirstTimeOrUpdateRating(profile, challenge, evaluate.rating);
            return new custom_success_response_1.CustomSuccessResponse("Avaliação realizada com sucesso!");
        }
        catch (error) {
            if (error instanceof index_1.ChallengeNotExists) {
                throw new common_1.BadRequestException(error.message);
            }
            if (error instanceof object_not_found_exception_1.ObjectNotFoundException) {
                throw new common_1.NotFoundException(error.message);
            }
            throw new common_1.InternalServerErrorException();
        }
    }
    async getChallengeRating(param, request) {
        try {
            const profile = await this.profilesService.findByUser(request.user.id);
            const challenge = await this.challengeService.findById(param.id);
            const rating = await this.challengeRatingService.findByProfileAndChallenge(profile, challenge);
            return new rating_response_dto_1.RatingResponseDto(profile, challenge, rating.rating);
        }
        catch (error) {
            if (error instanceof index_1.ChallengeNotExists) {
                return new common_1.BadRequestException(error.message);
            }
            if (error instanceof object_not_found_exception_1.ObjectNotFoundException) {
                return new common_1.NotFoundException(error.message);
            }
            return new common_1.InternalServerErrorException();
        }
    }
};
__decorate([
    common_1.UseGuards(jwt_auth_guard_1.JwtAuthGuard),
    common_1.Get(":challengeId/monthly-average"),
    __param(0, common_1.Param()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ChallengeController.prototype, "monthlyAverage", null);
__decorate([
    common_1.UseGuards(jwt_auth_guard_1.JwtAuthGuard),
    common_1.Get("/next-level-preview"),
    common_1.HttpCode(200),
    __param(0, common_1.Request()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ChallengeController.prototype, "getNextChallengesBlocked", null);
__decorate([
    common_1.Get(),
    __param(0, common_1.Query(new validation_pipe_1.ValidationPipe())),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [challenge_query_params_dto_1.ChallengeQueryParams]),
    __metadata("design:returntype", Promise)
], ChallengeController.prototype, "findAll", null);
__decorate([
    common_1.Get(":id"),
    common_1.HttpCode(200),
    __param(0, common_1.Param()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ChallengeController.prototype, "findById", null);
__decorate([
    common_1.Post(),
    __param(0, common_1.Body(new validation_pipe_1.ValidationPipe())),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_challenge_dto_1.CreateChallengeDto]),
    __metadata("design:returntype", Promise)
], ChallengeController.prototype, "create", null);
__decorate([
    common_1.Put(":id"),
    __param(0, common_1.Param()),
    __param(1, common_1.Body(new validation_pipe_1.ValidationPipe())),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, edit_challenge_dto_1.EditChallengeDto]),
    __metadata("design:returntype", Promise)
], ChallengeController.prototype, "update", null);
__decorate([
    common_1.Delete(":id"),
    common_1.HttpCode(200),
    __param(0, common_1.Param()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ChallengeController.prototype, "delete", null);
__decorate([
    common_1.UseGuards(jwt_auth_guard_1.JwtAuthGuard),
    common_1.Get(":id/finish"),
    __param(0, common_1.Param()), __param(1, common_1.Request()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], ChallengeController.prototype, "finishChallenge", null);
__decorate([
    common_1.UseGuards(jwt_auth_guard_1.JwtAuthGuard),
    common_1.Post(":id/evaluate"),
    common_1.HttpCode(200),
    __param(0, common_1.Param()),
    __param(1, common_1.Request()),
    __param(2, common_1.Body(new validation_pipe_1.ValidationPipe())),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, evaluate_dto_1.EvaluateDto]),
    __metadata("design:returntype", Promise)
], ChallengeController.prototype, "challengeEvaluate", null);
__decorate([
    common_1.UseGuards(jwt_auth_guard_1.JwtAuthGuard),
    common_1.Get(":id/rating"),
    common_1.HttpCode(200),
    __param(0, common_1.Param()), __param(1, common_1.Request()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], ChallengeController.prototype, "getChallengeRating", null);
ChallengeController = __decorate([
    common_1.Controller("challenges"),
    __metadata("design:paramtypes", [challenge_service_1.ChallengeService,
        current_challenge_status_service_1.CurrentChallengeStatusService,
        profile_service_1.ProfileService,
        challenge_rating_service_1.ChallengeRatingService,
        challenge_level_service_1.ChallengeLevelService])
], ChallengeController);
exports.ChallengeController = ChallengeController;
//# sourceMappingURL=challenge.controller.js.map