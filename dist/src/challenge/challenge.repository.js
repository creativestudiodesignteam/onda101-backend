"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChallengeRepository = void 0;
const challenge_entity_1 = require("./challenge.entity");
const typeorm_1 = require("typeorm");
let ChallengeRepository = class ChallengeRepository extends typeorm_1.Repository {
    async findAll(queryParams) {
        const challenges = this.find({
            take: queryParams.perPage,
            skip: queryParams.perPage * (queryParams.page - 1),
            order: { number: "ASC" },
        });
        return challenges;
    }
    findById(id) {
        return this.findOne(id);
    }
    async findBy(paramToSearch) {
        return this.findOne({ where: paramToSearch });
    }
    store(challengeToBeCreated) {
        return this.insert(challengeToBeCreated);
    }
    async updateChallenge(challenge) {
        const updateResult = await this.update(challenge.id, challenge);
        console.log("atualizado no repo: ", updateResult);
        return;
    }
    destroy(challenge) {
        this.remove(challenge);
    }
};
ChallengeRepository = __decorate([
    typeorm_1.EntityRepository(challenge_entity_1.Challenge)
], ChallengeRepository);
exports.ChallengeRepository = ChallengeRepository;
//# sourceMappingURL=challenge.repository.js.map