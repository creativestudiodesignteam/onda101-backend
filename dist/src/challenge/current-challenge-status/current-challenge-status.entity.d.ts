import { Profile } from "src/profile/profile.entity";
import { Challenge } from "../challenge.entity";
import { ChallengeStatus } from "./challenge-status/challenge-status.entity";
export declare class CurrentChallengeStatus {
    id: number;
    challengeInProgress: Challenge;
    profile: Profile;
    status: ChallengeStatus;
    createdAt: Date;
    updatedAt: Date;
}
