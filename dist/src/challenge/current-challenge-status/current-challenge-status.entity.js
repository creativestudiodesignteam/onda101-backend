"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CurrentChallengeStatus = void 0;
const profile_entity_1 = require("../../profile/profile.entity");
const typeorm_1 = require("typeorm");
const challenge_entity_1 = require("../challenge.entity");
const challenge_status_entity_1 = require("./challenge-status/challenge-status.entity");
let CurrentChallengeStatus = class CurrentChallengeStatus {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], CurrentChallengeStatus.prototype, "id", void 0);
__decorate([
    typeorm_1.ManyToOne(() => challenge_entity_1.Challenge, challenge => challenge.challengesInProgress, { eager: true }),
    typeorm_1.JoinColumn({ name: "challenge_id" }),
    __metadata("design:type", challenge_entity_1.Challenge)
], CurrentChallengeStatus.prototype, "challengeInProgress", void 0);
__decorate([
    typeorm_1.ManyToOne(() => profile_entity_1.Profile, profile => profile.challengesInProgress, { eager: true }),
    typeorm_1.JoinColumn({ name: "profile_id" }),
    __metadata("design:type", profile_entity_1.Profile)
], CurrentChallengeStatus.prototype, "profile", void 0);
__decorate([
    typeorm_1.ManyToOne(() => challenge_status_entity_1.ChallengeStatus, challengeStatus => challengeStatus, { eager: true }),
    typeorm_1.JoinColumn({ name: "challenge_status_id" }),
    __metadata("design:type", challenge_status_entity_1.ChallengeStatus)
], CurrentChallengeStatus.prototype, "status", void 0);
__decorate([
    typeorm_1.Column({ name: "created_at" }),
    __metadata("design:type", Date)
], CurrentChallengeStatus.prototype, "createdAt", void 0);
__decorate([
    typeorm_1.Column({ name: "updated_at" }),
    __metadata("design:type", Date)
], CurrentChallengeStatus.prototype, "updatedAt", void 0);
CurrentChallengeStatus = __decorate([
    typeorm_1.Entity({ name: "current_challenge_status" })
], CurrentChallengeStatus);
exports.CurrentChallengeStatus = CurrentChallengeStatus;
//# sourceMappingURL=current-challenge-status.entity.js.map