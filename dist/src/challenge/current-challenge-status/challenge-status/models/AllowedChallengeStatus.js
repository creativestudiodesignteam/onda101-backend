"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AllowedChallengeStatus = void 0;
var AllowedChallengeStatus;
(function (AllowedChallengeStatus) {
    AllowedChallengeStatus["incomplete"] = "incomplete";
    AllowedChallengeStatus["started"] = "started";
    AllowedChallengeStatus["done"] = "done";
})(AllowedChallengeStatus = exports.AllowedChallengeStatus || (exports.AllowedChallengeStatus = {}));
//# sourceMappingURL=AllowedChallengeStatus.js.map