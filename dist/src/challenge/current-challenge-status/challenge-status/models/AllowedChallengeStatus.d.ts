export declare enum AllowedChallengeStatus {
    incomplete = "incomplete",
    started = "started",
    done = "done"
}
