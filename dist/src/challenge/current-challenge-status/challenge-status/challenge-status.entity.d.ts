import { CurrentChallengeStatus } from "../current-challenge-status.entity";
import { AllowedChallengeStatus } from "./models/AllowedChallengeStatus";
export declare class ChallengeStatus {
    id: number;
    description: AllowedChallengeStatus;
    challengesInProgress: CurrentChallengeStatus;
}
