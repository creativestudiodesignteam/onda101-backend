"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChallengeStatus = void 0;
const typeorm_1 = require("typeorm");
const current_challenge_status_entity_1 = require("../current-challenge-status.entity");
const AllowedChallengeStatus_1 = require("./models/AllowedChallengeStatus");
let ChallengeStatus = class ChallengeStatus {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], ChallengeStatus.prototype, "id", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], ChallengeStatus.prototype, "description", void 0);
__decorate([
    typeorm_1.OneToMany(() => current_challenge_status_entity_1.CurrentChallengeStatus, currentChallengeStatus => currentChallengeStatus.status),
    __metadata("design:type", current_challenge_status_entity_1.CurrentChallengeStatus)
], ChallengeStatus.prototype, "challengesInProgress", void 0);
ChallengeStatus = __decorate([
    typeorm_1.Entity({ name: "challenge_status" })
], ChallengeStatus);
exports.ChallengeStatus = ChallengeStatus;
//# sourceMappingURL=challenge-status.entity.js.map