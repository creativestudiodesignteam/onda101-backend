import { Repository } from "typeorm";
import { ChallengeStatus } from "./challenge-status.entity";
export declare class ChallengeStatusRepository extends Repository<ChallengeStatus> {
}
