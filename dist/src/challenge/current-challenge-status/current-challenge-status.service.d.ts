import { ChallengeLevel } from "./../challenge-level/challenge-level.entity";
import { Profile } from "src/profile/profile.entity";
import { ProfileService } from "src/profile/profile.service";
import { ChallengeLevelService } from "../challenge-level/challenge-level.service";
import { Challenge } from "../challenge.entity";
import { ChallengeService } from "../challenge.service";
import { ChallengeStatusService } from "./challenge-status/challenge-status.service";
import { AllowedChallengeStatus } from "./challenge-status/models/AllowedChallengeStatus";
import { CurrentChallengeStatus } from "./current-challenge-status.entity";
import { CurrentChallengeStatusRepository } from "./current-challenge-status.repository";
import { RedoneTimesService } from "../redone-times/redone-times.service";
export declare class CurrentChallengeStatusService {
    repository: CurrentChallengeStatusRepository;
    private profileService;
    private challengeService;
    private challengeLevelService;
    challengeStatusService: ChallengeStatusService;
    private redoneTimesService;
    constructor(repository: CurrentChallengeStatusRepository, profileService: ProfileService, challengeService: ChallengeService, challengeLevelService: ChallengeLevelService, challengeStatusService: ChallengeStatusService, redoneTimesService: RedoneTimesService);
    getById(id: number): Promise<CurrentChallengeStatus>;
    getByProfileAndChallenge(profile: Profile, challenge: Challenge): Promise<CurrentChallengeStatus>;
    getProfilesCurrentChallengeLevelStatusList(profile: Profile): Promise<CurrentChallengeStatus[]>;
    getProfilesCurrentChallengeLevelStatusListDone(profile: Profile): Promise<CurrentChallengeStatus[]>;
    private getMissingChallengeStatus;
    private createAndGetDefaultPendingProgress;
    private createDefaultPendingProgressByChallengeLevel;
    updateChallengeStatus(currentChallengeStatus: CurrentChallengeStatus, nextStatus: AllowedChallengeStatus): Promise<{
        newProfileChallengeLevel: ChallengeLevel;
        isFirstTimeFinishingThisChallenge: boolean;
    }>;
    didProfileLevelUp(profile: Profile): Promise<boolean>;
}
