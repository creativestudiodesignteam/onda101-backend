"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ResponseListDto = void 0;
class ResponseListDto {
    constructor(currentChallengesStatus) {
        this.currentChallengesStatus = currentChallengesStatus;
    }
}
exports.ResponseListDto = ResponseListDto;
//# sourceMappingURL=response-list.dto.js.map