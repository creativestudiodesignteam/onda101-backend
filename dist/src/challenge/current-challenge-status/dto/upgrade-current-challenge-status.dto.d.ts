import { AllowedChallengeStatus } from "../challenge-status/models/AllowedChallengeStatus";
export declare class UpgradeCurrentChallengeStatusDto {
    nextStatus: AllowedChallengeStatus;
}
