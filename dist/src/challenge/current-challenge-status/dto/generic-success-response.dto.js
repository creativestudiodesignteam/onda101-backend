"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GenericSuccessResponseDto = void 0;
class GenericSuccessResponseDto {
    constructor(message) {
        this.message = message;
    }
}
exports.GenericSuccessResponseDto = GenericSuccessResponseDto;
//# sourceMappingURL=generic-success-response.dto.js.map