"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SameStatusException = void 0;
class SameStatusException extends Error {
    constructor() {
        super("O status atual é igual ao novo status desejado.");
    }
}
exports.SameStatusException = SameStatusException;
//# sourceMappingURL=same-status.js.map