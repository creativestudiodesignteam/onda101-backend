import { Profile } from "src/profile/profile.entity";
import { DeepPartial, Repository } from "typeorm";
import { Challenge } from "../challenge.entity";
import { CurrentChallengeStatus } from "./current-challenge-status.entity";
export declare class CurrentChallengeStatusRepository extends Repository<CurrentChallengeStatus> {
    findByProfileAndChallenge(profile: Profile, challenge: Challenge): Promise<CurrentChallengeStatus>;
    findById(id: number): Promise<CurrentChallengeStatus>;
    updateStatus(id: number, changes: DeepPartial<CurrentChallengeStatus>): Promise<import("typeorm").UpdateResult>;
    statusListFromGivenProfileLevel(profile: Profile): Promise<CurrentChallengeStatus[]>;
}
