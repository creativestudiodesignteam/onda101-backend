"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CurrentChallengeStatusService = void 0;
const common_1 = require("@nestjs/common");
const profile_entity_1 = require("../../profile/profile.entity");
const profile_service_1 = require("../../profile/profile.service");
const challenge_level_service_1 = require("../challenge-level/challenge-level.service");
const challenge_service_1 = require("../challenge.service");
const challenge_status_service_1 = require("./challenge-status/challenge-status.service");
const AllowedChallengeStatus_1 = require("./challenge-status/models/AllowedChallengeStatus");
const current_challenge_status_entity_1 = require("./current-challenge-status.entity");
const current_challenge_status_repository_1 = require("./current-challenge-status.repository");
const moment = require("moment");
const same_status_1 = require("./exception/same-status");
const redone_times_service_1 = require("../redone-times/redone-times.service");
const object_not_found_exception_1 = require("../challenge-rating/exception/object-not-found-exception");
let CurrentChallengeStatusService = class CurrentChallengeStatusService {
    constructor(repository, profileService, challengeService, challengeLevelService, challengeStatusService, redoneTimesService) {
        this.repository = repository;
        this.profileService = profileService;
        this.challengeService = challengeService;
        this.challengeLevelService = challengeLevelService;
        this.challengeStatusService = challengeStatusService;
        this.redoneTimesService = redoneTimesService;
    }
    async getById(id) {
        const currentChallengeStatus = await this.repository.findOne(id);
        if (!currentChallengeStatus)
            throw object_not_found_exception_1.ObjectNotFoundException;
        return currentChallengeStatus;
    }
    getByProfileAndChallenge(profile, challenge) {
        return this.repository.findByProfileAndChallenge(profile, challenge);
    }
    async getProfilesCurrentChallengeLevelStatusList(profile) {
        try {
            const challengesOfProfilesLevel = await this.challengeService.findByCryteria({ level: profile.challengeLevel });
            const challengesOfProfilesLevel2 = await this.challengeService.findByCryteria({ isExtra: true });
            const challengesProgressAsPromises = [];
            challengesOfProfilesLevel.forEach(challenge => challengesProgressAsPromises.push(this.repository.findByProfileAndChallenge(profile, challenge)));
            challengesOfProfilesLevel2.forEach(challenge => challengesProgressAsPromises.push(this.repository.findByProfileAndChallenge(profile, challenge)));
            const challengesProgressResolved = await Promise.all(challengesProgressAsPromises);
            const missingChallengeStatus = this.getMissingChallengeStatus(challengesProgressResolved);
            if (missingChallengeStatus === challengesProgressResolved.length) {
                return await this.createAndGetDefaultPendingProgress(profile);
            }
            else {
                return challengesProgressResolved;
            }
        }
        catch (error) {
            throw error;
        }
    }
    async getProfilesCurrentChallengeLevelStatusListDone(profile) {
        try {
            const challengesProgressAsPromises = [];
            const level = profile.challengeLevel.levelNumber;
            if (level > 1) {
                for (let index = 1; index < level; index++) {
                    profile.challengeLevel.levelNumber = index;
                    profile.challengeLevel.id = index;
                    profile.challengeLevel.description = `Nível ${index}`;
                    const challengesOfProfilesLevel = await this.challengeService.findByCryteria({ level: profile.challengeLevel });
                    challengesOfProfilesLevel.forEach(challenge => challengesProgressAsPromises.push(this.repository.findByProfileAndChallenge(profile, challenge)));
                }
            }
            const challengesProgressResolved = await Promise.all(challengesProgressAsPromises);
            const missingChallengeStatus = this.getMissingChallengeStatus(challengesProgressResolved);
            if (missingChallengeStatus === challengesProgressResolved.length) {
                return await this.createAndGetDefaultPendingProgress(profile);
            }
            else {
                return challengesProgressResolved;
            }
        }
        catch (error) {
            throw error;
        }
    }
    getMissingChallengeStatus(profilesCurrentChallengesStatusList) {
        const missingChallengeStatus = profilesCurrentChallengesStatusList.filter(currentChallengeStatus => currentChallengeStatus === undefined).length;
        return missingChallengeStatus;
    }
    async createAndGetDefaultPendingProgress(profile) {
        const currentChallengesStatusStored = await this.createDefaultPendingProgressByChallengeLevel(profile.challengeLevel.levelNumber, profile);
        return Promise.all(currentChallengesStatusStored.map(stored => this.repository.findById(stored.identifiers[0].id)));
    }
    async createDefaultPendingProgressByChallengeLevel(challengeLevelNumber, profile) {
        const nextChallengeLevel = await this.challengeLevelService.findOneByCryteria({ levelNumber: challengeLevelNumber });
        const nextPendingChallenges = await this.challengeService.findByCryteria({
            level: nextChallengeLevel,
            isExtra: false,
        });
        const nextPendingChallenges2 = await this.challengeService.findByCryteria({
            isExtra: true,
        });
        const defaultChallengeStatus = await this.challengeStatusService.findOneByCryteria({ description: AllowedChallengeStatus_1.AllowedChallengeStatus.incomplete });
        const storedChallengesStatusAsPromises = nextPendingChallenges.map(challenge => {
            const pendingChallengeStatus = new current_challenge_status_entity_1.CurrentChallengeStatus();
            pendingChallengeStatus.challengeInProgress = challenge;
            pendingChallengeStatus.profile = profile;
            pendingChallengeStatus.status = defaultChallengeStatus;
            return this.repository.insert(pendingChallengeStatus);
        });
        const storedChallengesStatusAsPromises2 = nextPendingChallenges2.map(challenge => {
            const pendingChallengeStatus = new current_challenge_status_entity_1.CurrentChallengeStatus();
            pendingChallengeStatus.challengeInProgress = challenge;
            pendingChallengeStatus.profile = profile;
            pendingChallengeStatus.status = defaultChallengeStatus;
            return this.repository.insert(pendingChallengeStatus);
        });
        await Promise.all(storedChallengesStatusAsPromises2);
        return await Promise.all(storedChallengesStatusAsPromises);
    }
    async updateChallengeStatus(currentChallengeStatus, nextStatus) {
        if (currentChallengeStatus.status.description === nextStatus &&
            nextStatus !== AllowedChallengeStatus_1.AllowedChallengeStatus.done)
            throw new same_status_1.SameStatusException();
        const newStatus = await this.challengeStatusService.findOneByCryteria({
            description: nextStatus,
        });
        const nextCurrentChallengeStatus = currentChallengeStatus;
        nextCurrentChallengeStatus.status = newStatus;
        nextCurrentChallengeStatus.updatedAt = moment().toDate();
        await this.repository.save(nextCurrentChallengeStatus);
        if (nextStatus === AllowedChallengeStatus_1.AllowedChallengeStatus.done) {
            const isFirstTimeFinishingThisChallenge = await this.redoneTimesService.isFirstTimeFinishingThisChallenge(currentChallengeStatus.challengeInProgress, currentChallengeStatus.profile);
            let newProfileChallengeLevel = null;
            if (isFirstTimeFinishingThisChallenge) {
                await this.profileService.addChallengeScore(currentChallengeStatus.challengeInProgress.score, currentChallengeStatus.profile);
                const didProfileLeveUp = await this.didProfileLevelUp(currentChallengeStatus.profile);
                if (didProfileLeveUp) {
                    newProfileChallengeLevel = await this.challengeLevelService.findOneByCryteria({
                        levelNumber: currentChallengeStatus.profile.challengeLevel.levelNumber + 1,
                    });
                    const profileLeveledUp = currentChallengeStatus.profile;
                    profileLeveledUp.challengeLevel = newProfileChallengeLevel;
                    await this.profileService.updateKnownProfile(profileLeveledUp);
                    await this.createDefaultPendingProgressByChallengeLevel(newProfileChallengeLevel.levelNumber, profileLeveledUp);
                }
            }
            await this.redoneTimesService.create(currentChallengeStatus.challengeInProgress, currentChallengeStatus.profile);
            return {
                newProfileChallengeLevel: newProfileChallengeLevel !== null && newProfileChallengeLevel !== void 0 ? newProfileChallengeLevel : null,
                isFirstTimeFinishingThisChallenge,
            };
        }
    }
    async didProfileLevelUp(profile) {
        const profilesChallengesStatusList = await this.repository.statusListFromGivenProfileLevel(profile);
        const completedChallengesInCurrentLevel = profilesChallengesStatusList.filter(progress => progress.status.description === AllowedChallengeStatus_1.AllowedChallengeStatus.done);
        if (completedChallengesInCurrentLevel.length ===
            profilesChallengesStatusList.length) {
            return true;
        }
        else {
            return false;
        }
    }
};
CurrentChallengeStatusService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [current_challenge_status_repository_1.CurrentChallengeStatusRepository,
        profile_service_1.ProfileService,
        challenge_service_1.ChallengeService,
        challenge_level_service_1.ChallengeLevelService,
        challenge_status_service_1.ChallengeStatusService,
        redone_times_service_1.RedoneTimesService])
], CurrentChallengeStatusService);
exports.CurrentChallengeStatusService = CurrentChallengeStatusService;
//# sourceMappingURL=current-challenge-status.service.js.map