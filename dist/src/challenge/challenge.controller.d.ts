import { EditChallengeDto } from "./dto/edit-challenge.dto";
import { CreateChallengeDto } from "./dto/create-challenge.dto";
import { CustomSuccessResponse } from "./dto/custom-success-response";
import { ChallengeDto } from "./dto/challenge.dto";
import { ChallengeQueryParams } from "./dto/challenge-query-params.dto";
import { ChallengeService } from "./challenge.service";
import { BadRequestException, InternalServerErrorException, NotFoundException } from "@nestjs/common";
import { CurrentChallengeStatusService } from "./current-challenge-status/current-challenge-status.service";
import { ProfileService } from "src/profile/profile.service";
import { LevelUpProfileChallengeLevelResponseDto } from "./dto/levelup-profile-challenge-level-response.dto";
import { EvaluateDto } from "./challenge-rating/dto/evaluate.dto";
import { ChallengeRatingService } from "./challenge-rating/challenge-rating.service";
import { RatingResponseDto } from "./challenge-rating/dto/rating-response.dto";
import { ChallengeLevelService } from "./challenge-level/challenge-level.service";
export declare class ChallengeController {
    private challengeService;
    private currentChallengeStatusService;
    private profilesService;
    private challengeRatingService;
    private challengeLevelService;
    constructor(challengeService: ChallengeService, currentChallengeStatusService: CurrentChallengeStatusService, profilesService: ProfileService, challengeRatingService: ChallengeRatingService, challengeLevelService: ChallengeLevelService);
    monthlyAverage(param: any): Promise<{
        challenge: string;
        monthAnalyzed: number;
        average: number;
    }>;
    getNextChallengesBlocked(request: any): Promise<import("./challenge.entity").Challenge[]>;
    findAll(queryParams: ChallengeQueryParams): Promise<{
        pagination: {
            page: number;
            perPage: number;
        };
        challenges: import("./challenge.entity").Challenge[];
    }>;
    findById(param: any): Promise<ChallengeDto>;
    create(challenge: CreateChallengeDto): Promise<ChallengeDto>;
    update(param: any, challenge: EditChallengeDto): Promise<any>;
    delete(param: any): Promise<CustomSuccessResponse>;
    finishChallenge(param: any, request: any): Promise<LevelUpProfileChallengeLevelResponseDto>;
    challengeEvaluate(param: any, request: any, evaluate: EvaluateDto): Promise<CustomSuccessResponse>;
    getChallengeRating(param: any, request: any): Promise<BadRequestException | NotFoundException | InternalServerErrorException | RatingResponseDto>;
}
