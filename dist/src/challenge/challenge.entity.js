"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Challenge = void 0;
const typeorm_1 = require("typeorm");
const challenge_level_entity_1 = require("./challenge-level/challenge-level.entity");
const completed_challenge_entity_1 = require("./completed-challenge/completed-challenge.entity");
const current_challenge_status_entity_1 = require("./current-challenge-status/current-challenge-status.entity");
const challenge_rating_entity_1 = require("./challenge-rating/challenge-rating.entity");
const redone_times_entity_1 = require("./redone-times/redone-times.entity");
let Challenge = class Challenge {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], Challenge.prototype, "id", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], Challenge.prototype, "title", void 0);
__decorate([
    typeorm_1.Column({ name: "imperative_phrase" }),
    __metadata("design:type", String)
], Challenge.prototype, "imperativePhrase", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], Challenge.prototype, "description", void 0);
__decorate([
    typeorm_1.Column({ name: "completion_message" }),
    __metadata("design:type", String)
], Challenge.prototype, "completionMessage", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", Number)
], Challenge.prototype, "number", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", Number)
], Challenge.prototype, "score", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", Boolean)
], Challenge.prototype, "isExtra", void 0);
__decorate([
    typeorm_1.OneToMany(() => completed_challenge_entity_1.CompletedChallenge, completedChallenge => completedChallenge.challenge),
    __metadata("design:type", Array)
], Challenge.prototype, "completedChallenges", void 0);
__decorate([
    typeorm_1.OneToMany(() => current_challenge_status_entity_1.CurrentChallengeStatus, currentChallengeStatus => currentChallengeStatus.challengeInProgress),
    __metadata("design:type", Array)
], Challenge.prototype, "challengesInProgress", void 0);
__decorate([
    typeorm_1.ManyToOne(() => challenge_level_entity_1.ChallengeLevel, challengeLevel => challengeLevel.challenges, { eager: true }),
    typeorm_1.JoinColumn({ name: "challenge_level_id" }),
    __metadata("design:type", challenge_level_entity_1.ChallengeLevel)
], Challenge.prototype, "level", void 0);
__decorate([
    typeorm_1.OneToMany(() => redone_times_entity_1.RedoneTimes, redoneTimes => redoneTimes.challenge),
    __metadata("design:type", redone_times_entity_1.RedoneTimes)
], Challenge.prototype, "redoneTimes", void 0);
__decorate([
    typeorm_1.OneToMany(() => challenge_rating_entity_1.ChallengeRating, challengeRating => challengeRating),
    __metadata("design:type", Array)
], Challenge.prototype, "challengeRatings", void 0);
__decorate([
    typeorm_1.CreateDateColumn(),
    __metadata("design:type", Date)
], Challenge.prototype, "created_at", void 0);
__decorate([
    typeorm_1.UpdateDateColumn(),
    __metadata("design:type", Date)
], Challenge.prototype, "updated_at", void 0);
Challenge = __decorate([
    typeorm_1.Entity({ name: "challenges" })
], Challenge);
exports.Challenge = Challenge;
//# sourceMappingURL=challenge.entity.js.map