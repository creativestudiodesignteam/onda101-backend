import { FindConditions, Repository } from "typeorm";
import { ChallengeRating } from "./challenge-rating.entity";
export declare class ChallengeRatingRepository extends Repository<ChallengeRating> {
    findBetweenDates(beginDate: Date, endDate: Date, challengeId: any): Promise<ChallengeRating[]>;
    findOneByCryteria(cryteria: FindConditions<ChallengeRating>): Promise<ChallengeRating>;
}
