"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RatingResponseDto = void 0;
const challenge_entity_1 = require("../../challenge.entity");
const profile_entity_1 = require("../../../profile/profile.entity");
class RatingResponseDto {
    constructor(profile, challenge, rating) {
        this.profile = profile;
        this.challenge = challenge;
        this.rating = rating;
    }
}
exports.RatingResponseDto = RatingResponseDto;
//# sourceMappingURL=rating-response.dto.js.map