import { Profile } from "src/profile/profile.entity";
import { DeepPartial, FindConditions } from "typeorm";
import { Challenge } from "../challenge.entity";
import { ChallengeRating } from "./challenge-rating.entity";
import { ChallengeRatingRepository } from "./challenge-rating.repository";
export declare class ChallengeRatingService {
    private repository;
    constructor(repository: ChallengeRatingRepository);
    findAll(): Promise<ChallengeRating[]>;
    findByCryteria(cryteria: FindConditions<ChallengeRating>): Promise<ChallengeRating[]>;
    findBetweenDates(beginDate: Date, endDate: Date, challengeId: number): Promise<ChallengeRating[]>;
    getChallengeRattingMonthlyAverage(challengeId: number, desiredMonth?: number): Promise<number>;
    rateForFirstTimeOrUpdateRating(profile: Profile, challenge: Challenge, ratingValue: number): Promise<void>;
    store(profile: Profile, challenge: Challenge, ratingValue: number): Promise<import("typeorm").InsertResult>;
    updateRating(id: number, newRating: DeepPartial<ChallengeRating>): Promise<void>;
    findByProfileAndChallenge(profile: Profile, challenge: Challenge): Promise<ChallengeRating>;
}
