import { Profile } from "src/profile/profile.entity";
import { Challenge } from "../challenge.entity";
export declare class ChallengeRating {
    id: number;
    profile: Profile;
    challenge: Challenge;
    rating: number;
    createdAt: Date;
    updatedAt: Date;
}
