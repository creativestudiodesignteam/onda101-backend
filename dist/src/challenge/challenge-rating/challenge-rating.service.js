"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChallengeRatingService = void 0;
const common_1 = require("@nestjs/common");
const profile_entity_1 = require("../../profile/profile.entity");
const challenge_rating_repository_1 = require("./challenge-rating.repository");
const object_not_found_exception_1 = require("./exception/object-not-found-exception");
const moment = require("moment");
const no_rating_exception_1 = require("./exception/no-rating-exception");
let ChallengeRatingService = class ChallengeRatingService {
    constructor(repository) {
        this.repository = repository;
    }
    findAll() {
        return this.repository.find();
    }
    findByCryteria(cryteria) {
        return this.repository.find({ where: cryteria });
    }
    findBetweenDates(beginDate, endDate, challengeId) {
        return this.repository.findBetweenDates(beginDate, endDate, challengeId);
    }
    async getChallengeRattingMonthlyAverage(challengeId, desiredMonth) {
        const currentMonth = moment().month();
        const monthAnalyzed = desiredMonth !== null && desiredMonth !== void 0 ? desiredMonth : currentMonth;
        const beginDate = moment()
            .month(monthAnalyzed)
            .date(1)
            .toDate();
        const endDate = moment()
            .month(monthAnalyzed)
            .toDate();
        const ratings = await this.findBetweenDates(beginDate, endDate, challengeId);
        if (!ratings.length)
            throw new no_rating_exception_1.NoRatingException();
        const total = ratings
            .map(rating => rating.rating)
            .reduce((a, b) => a + b, 0);
        const average = total / ratings.length;
        return average;
    }
    async rateForFirstTimeOrUpdateRating(profile, challenge, ratingValue) {
        const previousEvaluate = await this.repository.findOneByCryteria({
            profile,
            challenge,
        });
        if (!previousEvaluate) {
            await this.store(profile, challenge, ratingValue);
        }
        else {
            const updatedRating = previousEvaluate;
            updatedRating.rating = ratingValue;
            await this.updateRating(previousEvaluate.id, updatedRating);
        }
    }
    store(profile, challenge, ratingValue) {
        const rating = this.repository.create({
            profile,
            challenge,
            rating: ratingValue,
        });
        return this.repository.insert(rating);
    }
    async updateRating(id, newRating) {
        const rating = await this.repository.findOne(id);
        if (!rating)
            throw new object_not_found_exception_1.ObjectNotFoundException();
        await this.repository.save(newRating);
    }
    async findByProfileAndChallenge(profile, challenge) {
        const rating = await this.repository.findOneByCryteria({
            profile,
            challenge,
        });
        if (!rating)
            throw new object_not_found_exception_1.ObjectNotFoundException();
        return rating;
    }
};
ChallengeRatingService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [challenge_rating_repository_1.ChallengeRatingRepository])
], ChallengeRatingService);
exports.ChallengeRatingService = ChallengeRatingService;
//# sourceMappingURL=challenge-rating.service.js.map