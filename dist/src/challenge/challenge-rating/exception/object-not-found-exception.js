"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ObjectNotFoundException = void 0;
class ObjectNotFoundException extends Error {
    constructor(message) {
        super(message !== null && message !== void 0 ? message : "Objeto não encontrado");
    }
}
exports.ObjectNotFoundException = ObjectNotFoundException;
//# sourceMappingURL=object-not-found-exception.js.map