export declare class ObjectNotFoundException extends Error {
    constructor(message?: string);
}
