export declare class NoRatingException extends Error {
    constructor(message?: string);
}
