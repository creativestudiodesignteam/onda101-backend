"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.NoRatingException = void 0;
class NoRatingException extends Error {
    constructor(message) {
        super(message !== null && message !== void 0 ? message : "Nenhuma avaliação encontrada para o período informado");
    }
}
exports.NoRatingException = NoRatingException;
//# sourceMappingURL=no-rating-exception.js.map