"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChallengeRatingRepository = void 0;
const typeorm_1 = require("typeorm");
const challenge_rating_entity_1 = require("./challenge-rating.entity");
let ChallengeRatingRepository = class ChallengeRatingRepository extends typeorm_1.Repository {
    findBetweenDates(beginDate, endDate, challengeId) {
        return super
            .createQueryBuilder("rating")
            .where("rating.created_at >= :beginDate", { beginDate })
            .andWhere("rating.created_at <= :endDate", { endDate })
            .andWhere("rating.challenge_id = :challengeId", { challengeId })
            .getMany();
    }
    findOneByCryteria(cryteria) {
        return super.findOne({ where: cryteria });
    }
};
ChallengeRatingRepository = __decorate([
    typeorm_1.EntityRepository(challenge_rating_entity_1.ChallengeRating)
], ChallengeRatingRepository);
exports.ChallengeRatingRepository = ChallengeRatingRepository;
//# sourceMappingURL=challenge-rating.repository.js.map