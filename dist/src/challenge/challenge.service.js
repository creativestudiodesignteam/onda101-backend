"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChallengeService = void 0;
const index_1 = require("./exception/index");
const challenge_repository_1 = require("./challenge.repository");
const common_1 = require("@nestjs/common");
const challenge_entity_1 = require("./challenge.entity");
const challenge_level_service_1 = require("./challenge-level/challenge-level.service");
let ChallengeService = class ChallengeService {
    constructor(challengeRepository, challengeLevelService) {
        this.challengeRepository = challengeRepository;
        this.challengeLevelService = challengeLevelService;
    }
    async findAll({ page = 1, perPage = 10 }) {
        const allChallenges = await this.challengeRepository.findAll({
            page,
            perPage,
        });
        return allChallenges;
    }
    async findById(id) {
        const challenge = await this.challengeRepository.findById(id);
        if (!challenge)
            throw new index_1.ChallengeNotExists();
        return challenge;
    }
    async create(challengeToBeCreated) {
        try {
            const givenChallengeNumberAlreadyExists = await this.challengeRepository.findBy({
                number: challengeToBeCreated.number,
            });
            if (givenChallengeNumberAlreadyExists)
                throw new index_1.ChallengeNumberAlreadyExists();
            const challenge = new challenge_entity_1.Challenge();
            challenge.title = challengeToBeCreated.title;
            challenge.description = challengeToBeCreated.description;
            challenge.number = challengeToBeCreated.number;
            challenge.score = challengeToBeCreated.score;
            challenge.isExtra = challengeToBeCreated.isExtra;
            const challengeLevel = await this.challengeLevelService.findById(challengeToBeCreated.challengeLevelId);
            if (!challengeLevel)
                throw common_1.InternalServerErrorException;
            challenge.level = challengeLevel;
            await this.challengeRepository.store(challenge);
            return challenge;
        }
        catch (error) {
            throw error;
        }
    }
    async update(id, challengeChanges) {
        const findedChallenge = await this.challengeRepository.findById(id);
        if (!findedChallenge)
            throw new index_1.ChallengeNotExists();
        if (challengeChanges.number) {
            const challengeNumberAlreadyExists = await this.challengeRepository.findBy({ number: challengeChanges.number });
            if (challengeNumberAlreadyExists)
                throw new index_1.ChallengeNumberAlreadyExists();
        }
        const challenge = this.challengeRepository.create(Object.assign({ id }, challengeChanges));
        await this.challengeRepository.updateChallenge(challenge);
        return challenge;
    }
    async delete(id) {
        const challenge = await this.challengeRepository.findById(id);
        if (!challenge)
            throw new index_1.ChallengeNotExists();
        this.challengeRepository.destroy(challenge);
    }
    findByCryteria(cryteria) {
        return this.challengeRepository.find({ where: cryteria });
    }
};
ChallengeService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [challenge_repository_1.ChallengeRepository,
        challenge_level_service_1.ChallengeLevelService])
], ChallengeService);
exports.ChallengeService = ChallengeService;
//# sourceMappingURL=challenge.service.js.map