"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChallengeModule = void 0;
const challenge_repository_1 = require("./challenge.repository");
const typeorm_1 = require("@nestjs/typeorm");
const common_1 = require("@nestjs/common");
const challenge_service_1 = require("./challenge.service");
const challenge_controller_1 = require("./challenge.controller");
const platform_express_1 = require("@nestjs/platform-express");
const challenge_attachments_service_1 = require("./challenge-attachments/challenge-attachments.service");
const upload_files_service_1 = require("../shared/upload-files/upload-files.service");
const challenge_attachments_controller_1 = require("./challenge-attachments/challenge-attachments.controller");
const challenge_attachments_repository_1 = require("./challenge-attachments/challenge-attachments.repository");
const challenge_completed_controller_1 = require("./completed-challenge/challenge-completed.controller");
const completed_challenge_service_1 = require("./completed-challenge/completed-challenge.service");
const challenge_completed_repository_1 = require("./completed-challenge/challenge-completed.repository");
const profile_module_1 = require("../profile/profile.module");
const profile_service_1 = require("../profile/profile.service");
const challenge_level_service_1 = require("./challenge-level/challenge-level.service");
const challenge_level_repository_1 = require("./challenge-level/challenge-level.repository");
const current_challenge_status_controller_1 = require("./current-challenge-status/current-challenge-status.controller");
const current_challenge_status_service_1 = require("./current-challenge-status/current-challenge-status.service");
const current_challenge_status_repository_1 = require("./current-challenge-status/current-challenge-status.repository");
const user_module_1 = require("../user/user.module");
const challenge_status_repository_1 = require("./current-challenge-status/challenge-status/challenge-status.repository");
const challenge_status_service_1 = require("./current-challenge-status/challenge-status/challenge-status.service");
const redone_times_repository_1 = require("./redone-times/redone-times.repository");
const redone_times_service_1 = require("./redone-times/redone-times.service");
const challenge_post_controller_1 = require("./challenge-post/challenge-post.controller");
const challenge_post_service_1 = require("./challenge-post/challenge-post.service");
const challenge_post_repository_1 = require("./challenge-post/challenge-post.repository");
const challenge_rating_service_1 = require("./challenge-rating/challenge-rating.service");
const challenge_rating_repository_1 = require("./challenge-rating/challenge-rating.repository");
let ChallengeModule = class ChallengeModule {
};
ChallengeModule = __decorate([
    common_1.Module({
        imports: [
            typeorm_1.TypeOrmModule.forFeature([
                challenge_repository_1.ChallengeRepository,
                challenge_attachments_repository_1.ChallengeAttachmentsRepository,
                challenge_completed_repository_1.CompletedChallengeRepository,
                challenge_level_repository_1.ChallengeLevelRepository,
                current_challenge_status_repository_1.CurrentChallengeStatusRepository,
                challenge_status_repository_1.ChallengeStatusRepository,
                redone_times_repository_1.RedoneTimesRepository,
                challenge_post_repository_1.ChallengesPostsRepository,
                challenge_rating_repository_1.ChallengeRatingRepository,
            ]),
            platform_express_1.MulterModule.register({
                dest: "./upload",
            }),
            profile_module_1.ProfileModule,
            upload_files_service_1.UploadFilesService,
            common_1.forwardRef(() => user_module_1.UserModule),
        ],
        providers: [
            challenge_service_1.ChallengeService,
            challenge_attachments_service_1.ChallengeAttachmentsService,
            upload_files_service_1.UploadFilesService,
            profile_service_1.ProfileService,
            completed_challenge_service_1.CompletedChallengeService,
            challenge_level_service_1.ChallengeLevelService,
            current_challenge_status_service_1.CurrentChallengeStatusService,
            challenge_status_service_1.ChallengeStatusService,
            redone_times_service_1.RedoneTimesService,
            challenge_post_service_1.ChallengePostService,
            challenge_rating_service_1.ChallengeRatingService,
        ],
        controllers: [
            challenge_controller_1.ChallengeController,
            challenge_attachments_controller_1.ChallengeAttachmentsController,
            challenge_completed_controller_1.CompletedChallengeController,
            current_challenge_status_controller_1.CurrentChallengeStatusController,
            challenge_post_controller_1.ChallengePostController,
        ],
        exports: [challenge_level_service_1.ChallengeLevelService],
    })
], ChallengeModule);
exports.ChallengeModule = ChallengeModule;
//# sourceMappingURL=challenge.module.js.map