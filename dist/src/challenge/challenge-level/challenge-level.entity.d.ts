import { Profile } from "src/profile/profile.entity";
import { Challenge } from "../challenge.entity";
export declare class ChallengeLevel {
    id: number;
    description: string;
    levelNumber: number;
    profiles: Profile[];
    challenges: Challenge[];
}
