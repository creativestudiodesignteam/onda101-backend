import { FindConditions } from "typeorm";
import { ChallengeLevel } from "./challenge-level.entity";
import { ChallengeLevelRepository } from "./challenge-level.repository";
export declare class ChallengeLevelService {
    private repository;
    constructor(repository: ChallengeLevelRepository);
    findById(id: number): Promise<ChallengeLevel>;
    findByCryteria(cryteria: FindConditions<ChallengeLevel>): Promise<ChallengeLevel[]>;
    findOneByCryteria(cryteria: FindConditions<ChallengeLevel>): Promise<ChallengeLevel>;
    getNextLevel(currentLevelNumber: number): Promise<ChallengeLevel>;
}
