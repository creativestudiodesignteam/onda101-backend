"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChallengeLevelService = void 0;
const common_1 = require("@nestjs/common");
const challenge_level_repository_1 = require("./challenge-level.repository");
const there_is_no_next_level_1 = require("./exception/there-is-no-next-level");
let ChallengeLevelService = class ChallengeLevelService {
    constructor(repository) {
        this.repository = repository;
    }
    findById(id) {
        return this.repository.findOne(id);
    }
    findByCryteria(cryteria) {
        return this.repository.find({ where: cryteria });
    }
    findOneByCryteria(cryteria) {
        return this.repository.findOne({ where: cryteria });
    }
    getNextLevel(currentLevelNumber) {
        const invalidNumber = currentLevelNumber > 100;
        if (invalidNumber)
            throw new there_is_no_next_level_1.ThereIsNoNextLevelException();
        return this.repository.findOne({
            where: { levelNumber: currentLevelNumber + 1 },
        });
    }
};
ChallengeLevelService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [challenge_level_repository_1.ChallengeLevelRepository])
], ChallengeLevelService);
exports.ChallengeLevelService = ChallengeLevelService;
//# sourceMappingURL=challenge-level.service.js.map