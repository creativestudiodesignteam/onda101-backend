"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ThereIsNoNextLevelException = void 0;
class ThereIsNoNextLevelException extends Error {
    constructor() {
        super("Você já está no último nível de desafios");
    }
}
exports.ThereIsNoNextLevelException = ThereIsNoNextLevelException;
//# sourceMappingURL=there-is-no-next-level.js.map