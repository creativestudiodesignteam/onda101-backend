import { Repository } from "typeorm";
import { ChallengeLevel } from "./challenge-level.entity";
export declare class ChallengeLevelRepository extends Repository<ChallengeLevel> {
}
