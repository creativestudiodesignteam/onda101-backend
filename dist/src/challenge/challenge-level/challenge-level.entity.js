"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChallengeLevel = void 0;
const profile_entity_1 = require("../../profile/profile.entity");
const typeorm_1 = require("typeorm");
const challenge_entity_1 = require("../challenge.entity");
let ChallengeLevel = class ChallengeLevel {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], ChallengeLevel.prototype, "id", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], ChallengeLevel.prototype, "description", void 0);
__decorate([
    typeorm_1.Column({ name: "level_number" }),
    __metadata("design:type", Number)
], ChallengeLevel.prototype, "levelNumber", void 0);
__decorate([
    typeorm_1.OneToMany(() => profile_entity_1.Profile, profile => profile.challengeLevel),
    __metadata("design:type", Array)
], ChallengeLevel.prototype, "profiles", void 0);
__decorate([
    typeorm_1.OneToMany(() => challenge_entity_1.Challenge, challenge => challenge.level),
    __metadata("design:type", Array)
], ChallengeLevel.prototype, "challenges", void 0);
ChallengeLevel = __decorate([
    typeorm_1.Entity({ name: "challenge_levels" })
], ChallengeLevel);
exports.ChallengeLevel = ChallengeLevel;
//# sourceMappingURL=challenge-level.entity.js.map