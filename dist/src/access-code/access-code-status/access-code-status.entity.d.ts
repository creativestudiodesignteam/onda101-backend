import { AccessCode } from "../access-code.entity";
export declare class AccessCodeStatus {
    id: number;
    description: string;
    codes: AccessCode;
}
