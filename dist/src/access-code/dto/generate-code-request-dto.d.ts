import { FutureCodeReceiver } from "../models/future-code-receiver";
import { GenerateCodeRequest } from "../models/generate-code-request";
export declare class GenerateCodeRequestDto implements GenerateCodeRequest {
    email: string;
    names: FutureCodeReceiver[];
    paymentId: string;
    order: number;
}
