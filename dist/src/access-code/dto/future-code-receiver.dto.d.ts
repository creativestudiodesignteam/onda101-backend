import { FutureCodeReceiver } from "../models/future-code-receiver";
export declare class FutureCodeReceiverDto implements FutureCodeReceiver {
    name: string;
    hasWelcomeMessage: boolean;
}
