"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GeneratedCodesResponseDto = void 0;
class GeneratedCodesResponseDto {
    constructor(codes) {
        this.codes = codes;
    }
}
exports.GeneratedCodesResponseDto = GeneratedCodesResponseDto;
//# sourceMappingURL=generated-codes-response.dto.js.map