import { AccessCodeStatus } from "./access-code-status/access-code-status.entity";
export declare class AccessCode {
    id: number;
    value: string;
    created_at: Date;
    due_date: Date;
    codeStatus: AccessCodeStatus;
}
