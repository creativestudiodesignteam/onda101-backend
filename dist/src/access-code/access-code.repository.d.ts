import { Repository } from "typeorm";
import { AccessCode } from "./access-code.entity";
export declare class AccessCodeRepository extends Repository<AccessCode> {
    store(code: AccessCode): Promise<import("typeorm").InsertResult>;
    findByCodeValue(code: string): Promise<AccessCode>;
}
