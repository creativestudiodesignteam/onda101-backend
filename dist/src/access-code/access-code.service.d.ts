import { ProfileService } from "src/profile/profile.service";
import { AccessCode } from "./access-code.entity";
import { AccessCodeRepository } from "./access-code.repository";
export declare class AccessCodeService {
    private profilesService;
    private repository;
    constructor(profilesService: ProfileService, repository: AccessCodeRepository);
    provideAccessCodesToNewUsers(totalAskedCodes: number): Promise<string[]>;
    private generateCode;
    store(code: string): Promise<void>;
    findByCodeValue(code: string): Promise<AccessCode>;
    isCodeValidTonewUser(code: string): Promise<boolean>;
    updateAccessCodeStatus(codeValue: string, newCodeStatus: "pending" | "active" | "invalid"): Promise<void>;
}
