"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AccessCodeRepository = void 0;
const typeorm_1 = require("typeorm");
const access_code_entity_1 = require("./access-code.entity");
let AccessCodeRepository = class AccessCodeRepository extends typeorm_1.Repository {
    store(code) {
        try {
            return super.insert(code);
        }
        catch (error) {
            console.log("erro aqui: ", error);
        }
    }
    findByCodeValue(code) {
        return super.findOne({ where: { value: code } });
    }
};
AccessCodeRepository = __decorate([
    typeorm_1.EntityRepository(access_code_entity_1.AccessCode)
], AccessCodeRepository);
exports.AccessCodeRepository = AccessCodeRepository;
//# sourceMappingURL=access-code.repository.js.map