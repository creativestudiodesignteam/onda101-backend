import { BadRequestException } from "@nestjs/common";
export declare class InvalidRegisterAccessCode extends BadRequestException {
    readonly message = "C\u00F3digo de acesso inv\u00E1lido para um novo registro";
}
