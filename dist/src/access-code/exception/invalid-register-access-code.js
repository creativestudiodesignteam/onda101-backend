"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.InvalidRegisterAccessCode = void 0;
const common_1 = require("@nestjs/common");
class InvalidRegisterAccessCode extends common_1.BadRequestException {
    constructor() {
        super(...arguments);
        this.message = "Código de acesso inválido para um novo registro";
    }
}
exports.InvalidRegisterAccessCode = InvalidRegisterAccessCode;
//# sourceMappingURL=invalid-register-access-code.js.map