"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AccessCodeModule = void 0;
const common_1 = require("@nestjs/common");
const access_code_service_1 = require("./access-code.service");
const profile_module_1 = require("../profile/profile.module");
const typeorm_1 = require("@nestjs/typeorm");
const access_code_repository_1 = require("./access-code.repository");
const code_receiver_data_module_1 = require("../code-receiver-data/code-receiver-data.module");
let AccessCodeModule = class AccessCodeModule {
};
AccessCodeModule = __decorate([
    common_1.Module({
        providers: [access_code_service_1.AccessCodeService],
        imports: [
            profile_module_1.ProfileModule,
            typeorm_1.TypeOrmModule.forFeature([access_code_repository_1.AccessCodeRepository]),
            code_receiver_data_module_1.CodeReceiverDataModule,
        ],
        exports: [access_code_service_1.AccessCodeService],
    })
], AccessCodeModule);
exports.AccessCodeModule = AccessCodeModule;
//# sourceMappingURL=access-code.module.js.map