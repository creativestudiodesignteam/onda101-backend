"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AccessCodeService = void 0;
const common_1 = require("@nestjs/common");
const moment = require("moment");
const crypto = require("crypto");
const profile_service_1 = require("../profile/profile.service");
const access_code_entity_1 = require("./access-code.entity");
const access_code_repository_1 = require("./access-code.repository");
const access_code_status_entity_1 = require("./access-code-status/access-code-status.entity");
const invalid_register_access_code_1 = require("./exception/invalid-register-access-code");
const typeorm_1 = require("typeorm");
let AccessCodeService = class AccessCodeService {
    constructor(profilesService, repository) {
        this.profilesService = profilesService;
        this.repository = repository;
    }
    async provideAccessCodesToNewUsers(totalAskedCodes) {
        const generatedCode = [];
        for (let i = 0; i < totalAskedCodes; i++) {
            const code = this.generateCode();
            await this.store(code);
            generatedCode.push(code);
        }
        return generatedCode;
    }
    generateCode() {
        const accessCode = crypto.randomBytes(3).toString("hex");
        return accessCode;
    }
    async store(code) {
        const codeToBeStored = new access_code_entity_1.AccessCode();
        codeToBeStored.value = code;
        codeToBeStored.due_date = moment()
            .add(2, "days")
            .toDate();
        const codeStatus = new access_code_status_entity_1.AccessCodeStatus();
        codeStatus.id = 1;
        codeToBeStored.codeStatus = codeStatus;
        await this.repository.store(codeToBeStored);
    }
    findByCodeValue(code) {
        return this.repository.findByCodeValue(code);
    }
    async isCodeValidTonewUser(code) {
        const foundCode = await this.repository.findOne({
            where: { value: code },
        });
        console.log("found code", code, foundCode);
        if (foundCode) {
            const codeStatus = foundCode.codeStatus;
            if (codeStatus.description !== "pending") {
                throw new invalid_register_access_code_1.InvalidRegisterAccessCode();
            }
            return;
        }
        else {
            throw new invalid_register_access_code_1.InvalidRegisterAccessCode();
        }
    }
    async updateAccessCodeStatus(codeValue, newCodeStatus) {
        const foundCode = await this.findByCodeValue(codeValue);
        if (foundCode) {
            const newAccessCodeStatus = await typeorm_1.getRepository(access_code_status_entity_1.AccessCodeStatus).findOne({ where: { description: newCodeStatus } });
            foundCode.codeStatus = newAccessCodeStatus;
            await this.repository.save(foundCode);
        }
    }
};
AccessCodeService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [profile_service_1.ProfileService,
        access_code_repository_1.AccessCodeRepository])
], AccessCodeService);
exports.AccessCodeService = AccessCodeService;
//# sourceMappingURL=access-code.service.js.map