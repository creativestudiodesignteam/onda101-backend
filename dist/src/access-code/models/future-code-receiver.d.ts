export interface FutureCodeReceiver {
    name: string;
    hasWelcomeMessage: boolean;
}
