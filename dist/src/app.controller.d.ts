import { BadRequestException, InternalServerErrorException } from "@nestjs/common";
import { AppService } from "./app.service";
import { GeneratedCodesResponseDto } from "./access-code/dto/generated-codes-response.dto";
import { AccessCodeService } from "./access-code/access-code.service";
import { CodeResetService } from "./code-reset/code-reset.service";
import { GenerateCodeRequestDto } from "./access-code/dto/generate-code-request-dto";
import { CodeReceiverDataService } from "./code-receiver-data/code-receiver-data.service";
import { MailSenderService } from "./shared/mail-sender/mail-sender.service";
import { ProfileAndUserDataUpdateDto } from "./shared/dto/profile-and-user-data-update.dto";
import { EncryptedService } from "./shared/encrypted/encrypted.service";
import { EntityManager } from "typeorm";
export declare class AppController {
    private appService;
    private accessCodeservice;
    private codeResetService;
    private codeReceiverService;
    private mailSenderService;
    private encryptService;
    private entityManager;
    constructor(appService: AppService, accessCodeservice: AccessCodeService, codeResetService: CodeResetService, codeReceiverService: CodeReceiverDataService, mailSenderService: MailSenderService, encryptService: EncryptedService, entityManager: EntityManager);
    getHello(): string;
    getFiles(param: any, res: any): Promise<import("rxjs").Observable<any>>;
    getposts(param: any, res: any): Promise<import("rxjs").Observable<any>>;
    receiveNewUsers(bodyRequest: GenerateCodeRequestDto): Promise<BadRequestException | InternalServerErrorException | GeneratedCodesResponseDto>;
    updateProfileAndUserData(req: any, bodyRequest: ProfileAndUserDataUpdateDto): Promise<{
        message: string;
    }>;
}
