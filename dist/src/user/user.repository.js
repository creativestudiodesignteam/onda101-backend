"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserRepository = void 0;
const user_entity_1 = require("./user.entity");
const typeorm_1 = require("typeorm");
let UserRepository = class UserRepository extends typeorm_1.Repository {
    async findAll(queryParams) {
        const users = await this.find({
            take: queryParams.perPage,
            skip: queryParams.perPage * (queryParams.page - 1),
            order: { id: "ASC" },
        });
        return users.map(user => {
            delete user.password;
            return user;
        });
    }
    async findById(id) {
        const userFind = await this.findOne(id);
        return userFind;
    }
    async findBy(paramToSearch) {
        return await this.findOne({ where: paramToSearch });
    }
    async store(userToBeCreated) {
        return await this.insert(userToBeCreated);
    }
    async updateUser(user) {
        return await this.update(user.id, user);
    }
    async destroy(user) {
        await this.remove(user);
    }
};
UserRepository = __decorate([
    typeorm_1.EntityRepository(user_entity_1.User)
], UserRepository);
exports.UserRepository = UserRepository;
//# sourceMappingURL=user.repository.js.map