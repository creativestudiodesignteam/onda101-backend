"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserService = void 0;
const common_1 = require("@nestjs/common");
const exception_1 = require("./exception");
const user_repository_1 = require("./user.repository");
const user_entity_1 = require("./user.entity");
const typeorm_1 = require("typeorm");
const user_password_not_match_1 = require("./exception/user-password-not-match");
const users_create_dto_1 = require("./dto/users-create.dto");
const encrypted_service_1 = require("../shared/encrypted/encrypted.service");
const profile_creation_dto_1 = require("../profile/dto/profile-creation.dto");
const profile_entity_1 = require("../profile/profile.entity");
const profile_and_user_data_dto_1 = require("../profile/dto/profile-and-user-data.dto");
const groups_service_1 = require("../profile/groups/groups.service");
const challenge_level_entity_1 = require("../challenge/challenge-level/challenge-level.entity");
const challenge_level_service_1 = require("../challenge/challenge-level/challenge-level.service");
const profile_service_1 = require("../profile/profile.service");
const groups_not_exists_1 = require("../profile/groups/exception/groups-not-exists");
let UserService = class UserService {
    constructor(userRepository, encryptService, groupsService, profileService, challengeLevelService) {
        this.userRepository = userRepository;
        this.encryptService = encryptService;
        this.groupsService = groupsService;
        this.profileService = profileService;
        this.challengeLevelService = challengeLevelService;
    }
    async findAll({ page = 1, perPage = 10 }) {
        const allUsers = await this.userRepository.findAll({
            page: page,
            perPage: perPage,
        });
        return allUsers;
    }
    async findById(id) {
        try {
            const user = await this.userRepository.findById(id);
            if (!user)
                throw new exception_1.UserNotExists();
            delete user.password;
            return user;
        }
        catch (error) {
            throw error;
        }
    }
    async findByUsername(username) {
        try {
            const givenUserAlreadyExists = await this.userRepository.findBy({
                username,
            });
            if (!givenUserAlreadyExists)
                throw new exception_1.UserNotExists();
            return givenUserAlreadyExists;
        }
        catch (error) {
            throw error;
        }
    }
    async findByEmail(email) {
        try {
            const givenUserAlreadyExists = await this.userRepository.findBy({
                email,
            });
            if (!givenUserAlreadyExists)
                throw new exception_1.UserNotExists();
            return givenUserAlreadyExists;
        }
        catch (error) {
            throw error;
        }
    }
    async create(userToBeCreated) {
        try {
            const givenUserAlreadyExists = await this.userRepository.findBy({
                username: userToBeCreated.username,
            });
            if (givenUserAlreadyExists)
                throw new exception_1.UserAlreadyExists();
            const givenEmailAlreadyExists = await this.userRepository.findBy({
                email: userToBeCreated.email,
            });
            if (givenEmailAlreadyExists)
                throw new exception_1.UserAlreadyExists();
            const password_hash = await this.encryptService.encrypt(userToBeCreated.password);
            const user = new user_entity_1.User();
            user.username = userToBeCreated.username;
            user.email = userToBeCreated.email;
            user.password = password_hash;
            await this.userRepository.store(user);
            delete user.password;
            return user;
        }
        catch (error) {
            throw error;
        }
    }
    async createUserAndHisProfile(user, profile, usersRepo, profilesRepo) {
        try {
            console.log("Creating user and profile...");
            const givenUserAlreadyExists = await usersRepo.findOne({
                where: {
                    username: user.username,
                },
            });
            const givenEmailAlreadyExists = await usersRepo.findOne({
                where: {
                    email: user.email,
                },
            });
            if (givenUserAlreadyExists || givenEmailAlreadyExists)
                throw new exception_1.UserAlreadyExists();
            const hashedPassword = await this.encryptService.encrypt(user.password);
            const preparedUser = usersRepo.create(Object.assign(Object.assign({}, user), { password: hashedPassword }));
            const creationUserResponse = await usersRepo.insert(preparedUser);
            const userId = creationUserResponse.identifiers[0];
            const profilesUser = usersRepo.create(Object.assign({ id: userId }, preparedUser));
            const preparedProfile = new profile_entity_1.Profile();
            preparedProfile.user = profilesUser;
            preparedProfile.first_name = profile.firstName;
            preparedProfile.last_name = profile.lastName;
            preparedProfile.birth_date = new Date(profile.birthDate);
            const currentChallengeLevel = await this.challengeLevelService.findById(1);
            preparedProfile.challengeLevel = currentChallengeLevel;
            const lastUser = await this.userRepository.findOne({
                order: { id: "DESC" },
            });
            const lastProfileGroup = await this.profileService.findByUserWithRelations(lastUser);
            let groupForRegister;
            try {
                groupForRegister = await this.groupsService.findById(lastProfileGroup.group.id + 1);
            }
            catch (err) {
                console.log("Erro ao pegar um grupo");
                if (err instanceof groups_not_exists_1.GroupsNotExists) {
                    const firstGroup = await this.groupsService.selectGroups();
                    groupForRegister = await this.groupsService.findById(firstGroup.id);
                }
                else {
                    const firstGroup = await this.groupsService.selectGroups();
                    groupForRegister = await this.groupsService.findById(firstGroup.id);
                }
            }
            const profileGroup = groupForRegister;
            preparedProfile.group = profileGroup;
            await profilesRepo.insert(preparedProfile);
            return new profile_and_user_data_dto_1.ProfileAndUserDataDto(preparedUser.username, preparedUser.email, preparedProfile.first_name, preparedProfile.last_name);
        }
        catch (error) {
            console.log("Error to create profile and user");
            throw error;
        }
    }
    async update(id, userChanges) {
        try {
            const findedUser = await this.userRepository.findById(id);
            if (!findedUser)
                throw new exception_1.UserNotExists();
            const checkEmail = await this.userRepository.findBy({
                email: userChanges.email,
            });
            if (checkEmail)
                throw new exception_1.UserAlreadyExists();
            const checkUsername = await this.userRepository.findBy({
                username: userChanges.username,
            });
            if (checkUsername)
                throw new exception_1.UserAlreadyExists();
            const user = this.userRepository.create(Object.assign({ id }, userChanges));
            await this.userRepository.updateUser(user);
            return user;
        }
        catch (error) {
            throw error;
        }
    }
    async changePassword(id, newPassword) {
        try {
            console.log("Service changePassword: ", id, newPassword);
            const findedUser = await this.userRepository.findById(id);
            if (!findedUser)
                throw new exception_1.UserNotExists();
            const hashedPassword = await this.encryptService.encrypt(newPassword);
            const preparedUser = Object.assign(Object.assign({}, findedUser), { password: hashedPassword });
            await this.userRepository.updateUser(preparedUser);
            return preparedUser;
        }
        catch (error) {
            throw error;
        }
    }
    async update_password(id, userChanges) {
        try {
            const findedUser = await this.userRepository.findById(id);
            if (!findedUser)
                throw new exception_1.UserNotExists();
            const compare = await this.encryptService.compare(userChanges.password_old, findedUser.password);
            if (!compare) {
                throw new user_password_not_match_1.UserPasswordNotMatch();
            }
            const password_hash = await this.encryptService.encrypt(userChanges.password_new);
            const user = this.userRepository.create({
                id,
                email: findedUser.email,
                username: findedUser.username,
                password: password_hash,
            });
            await this.userRepository.updateUser(user);
            delete user.password;
            return user;
        }
        catch (error) {
            throw error;
        }
    }
    async delete(id) {
        try {
            const user = await this.userRepository.findById(id);
            if (!user)
                throw new exception_1.UserNotExists();
            this.userRepository.destroy(user);
        }
        catch (error) {
            throw error;
        }
    }
};
__decorate([
    typeorm_1.Transaction(),
    __param(2, typeorm_1.TransactionRepository(user_entity_1.User)),
    __param(3, typeorm_1.TransactionRepository(profile_entity_1.Profile)),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [users_create_dto_1.UserCreateDto,
        profile_creation_dto_1.ProfileCreationDto,
        typeorm_1.Repository,
        typeorm_1.Repository]),
    __metadata("design:returntype", Promise)
], UserService.prototype, "createUserAndHisProfile", null);
UserService = __decorate([
    common_1.Injectable(),
    __param(4, common_1.Inject(common_1.forwardRef(() => challenge_level_service_1.ChallengeLevelService))),
    __metadata("design:paramtypes", [user_repository_1.UserRepository,
        encrypted_service_1.EncryptedService,
        groups_service_1.GroupsService,
        profile_service_1.ProfileService,
        challenge_level_service_1.ChallengeLevelService])
], UserService);
exports.UserService = UserService;
//# sourceMappingURL=user.service.js.map