import { User } from "./user.entity";
import { DeepPartial, Repository } from "typeorm";
import { UserQueryParams } from "./dto/users-query-params.dto";
import { UserDto } from "./dto/user.dto";
export declare class UserRepository extends Repository<User> {
    findAll(queryParams: UserQueryParams): Promise<User[]>;
    findById(id: number): Promise<User>;
    findBy(paramToSearch: any): Promise<User>;
    store(userToBeCreated: User): Promise<import("typeorm").InsertResult>;
    updateUser(user: DeepPartial<UserDto>): Promise<import("typeorm").UpdateResult>;
    destroy(user: User): Promise<void>;
}
