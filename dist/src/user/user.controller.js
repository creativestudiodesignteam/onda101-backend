"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserController = void 0;
const validation_pipe_1 = require("./../shared/pipes/validation.pipe");
const user_service_1 = require("./user.service");
const common_1 = require("@nestjs/common");
const moment = require("moment");
const user_dto_1 = require("./dto/user.dto");
const user_list_dto_1 = require("./dto/user-list.dto");
const exception_1 = require("./exception");
const users_query_params_dto_1 = require("./dto/users-query-params.dto");
const custom_success_response_dto_1 = require("./dto/custom-success-response.dto");
const user_edit_dto_1 = require("./dto/user-edit.dto");
const user_password_not_match_1 = require("./exception/user-password-not-match");
const users_create_dto_1 = require("./dto/users-create.dto");
const user_edit_password_dto_1 = require("./dto/user-edit-password.dto");
const profile_creation_dto_1 = require("../profile/dto/profile-creation.dto");
const user_repository_1 = require("./user.repository");
const profile_repository_1 = require("../profile/profile.repository");
const typeorm_1 = require("@nestjs/typeorm");
const profile_entity_1 = require("../profile/profile.entity");
const jwt_auth_guard_1 = require("../auth/jwt-auth.guard");
const user_register_dto_1 = require("./dto/user-register.dto");
const access_code_service_1 = require("../access-code/access-code.service");
const code_reset_service_1 = require("../code-reset/code-reset.service");
const create_code_reset_dto_1 = require("../code-reset/dto/create-code-reset.dto");
const mail_sender_service_1 = require("../shared/mail-sender/mail-sender.service");
let UserController = class UserController {
    constructor(codeResetService, userService, accessCodeService, usersRepository, mailSenderService, profileRepository) {
        this.codeResetService = codeResetService;
        this.userService = userService;
        this.accessCodeService = accessCodeService;
        this.usersRepository = usersRepository;
        this.mailSenderService = mailSenderService;
        this.profileRepository = profileRepository;
    }
    async findAll(queryParams) {
        try {
            const findedUsers = await this.userService.findAll(queryParams);
            const paginatedList = new user_list_dto_1.UserListDto(findedUsers, queryParams.page, queryParams.perPage);
            return paginatedList.getPaginatedData();
        }
        catch (error) {
            throw new common_1.InternalServerErrorException({
                message: "Alguma coisa deu errado. Tente novamente em alguns instantes...",
            });
        }
    }
    async findById(req) {
        try {
            const user = await this.userService.findById(req.user.id);
            return new user_dto_1.UserDto(user.id, user.username, user.email, user.password, user.created_at, user.updated_at);
        }
        catch (error) {
            if (error instanceof exception_1.UserNotExists)
                throw new common_1.NotFoundException(error.message);
            throw new common_1.InternalServerErrorException({
                message: "Alguma coisa deu errado. Tente novamente em alguns instantes...",
            });
        }
    }
    async create(user) {
        try {
            const createdUser = await this.userService.create(user);
            return new user_dto_1.UserDto(createdUser.id, createdUser.username, createdUser.email, createdUser.password);
        }
        catch (error) {
            if (error instanceof exception_1.UserAlreadyExists)
                throw new common_1.BadRequestException(error.message);
            if (error instanceof user_password_not_match_1.UserPasswordNotMatch)
                throw new common_1.BadRequestException(error.message);
            throw new common_1.InternalServerErrorException({
                message: "Alguma coisa deu errado. Tente novamente em alguns instantes...",
            });
        }
    }
    async register(userRegisterDto) {
        const user = new users_create_dto_1.UserCreateDto(userRegisterDto.username, userRegisterDto.email, userRegisterDto.password);
        const profile = new profile_creation_dto_1.ProfileCreationDto(userRegisterDto.firstName, userRegisterDto.lastName, moment().toString());
        try {
            await this.accessCodeService.isCodeValidTonewUser(userRegisterDto.accessCode);
            const profileAndUserResponse = await this.userService.createUserAndHisProfile(user, profile, this.usersRepository, this.profileRepository);
            console.log("Passou");
            await this.accessCodeService.updateAccessCodeStatus(userRegisterDto.accessCode, "active");
            return profileAndUserResponse;
        }
        catch (error) {
            console.log(">>>> ", error);
            if (error instanceof common_1.BadRequestException)
                throw new common_1.BadRequestException(error.message);
            throw new common_1.InternalServerErrorException({
                message: "Alguma coisa deu errado. Tente novamente em alguns instantes... hhh",
            });
        }
    }
    async resetPassword(resetPassword) {
        try {
            const user = await this.userService.findByEmail(resetPassword.email);
            let codeResult = null;
            let codeResetEmail = null;
            if (user) {
                codeResetEmail = Math.random().toString().substr(2, 6);
                const resetDto = new create_code_reset_dto_1.CreateCodeResetDto(resetPassword.email, user, codeResetEmail);
                codeResult = await this.codeResetService.create(resetDto);
                const bodyEmail = `Seu codigo de reset de senha: ${codeResetEmail}`;
                await this.mailSenderService.sendEmail(resetPassword.email, bodyEmail);
            }
            return { message: `Email enviado com sucesso!` };
        }
        catch (error) {
            if (error instanceof exception_1.UserNotExists) {
                return new common_1.BadRequestException(error.message);
            }
            else {
                console.log(">>>> ", error);
                if (error instanceof common_1.BadRequestException)
                    throw new common_1.BadRequestException(error.message);
                throw new common_1.InternalServerErrorException({
                    message: "Alguma coisa deu errado. Tente novamente em alguns instantes... hhh",
                });
            }
        }
    }
    async changePassword(changePassword) {
        try {
            const user = await this.userService.findByEmail(changePassword.email);
            if (user) {
                const objCode = await this.codeResetService.findOne(changePassword.code);
                if (objCode) {
                    const userChanged = await this.userService.changePassword(user.id, changePassword.password);
                    const resultDelete = await this.codeResetService.delete(objCode.id);
                    console.log("resultDelete Find: ");
                }
            }
            return { message: `Senha alterada com sucesso!` };
        }
        catch (error) {
            if (error instanceof exception_1.UserNotExists) {
                return new common_1.BadRequestException(error.message);
            }
            else {
                console.log(">>>> ", error);
                if (error instanceof common_1.BadRequestException)
                    throw new common_1.BadRequestException(error.message);
                throw new common_1.InternalServerErrorException({
                    message: "Alguma coisa deu errado. Tente novamente em alguns instantes... hhh",
                });
            }
        }
    }
    async update(req, user) {
        try {
            console.log(req.user.id);
            const updatedUser = await this.userService.update(req.user.id, user);
            return updatedUser;
        }
        catch (error) {
            if (error instanceof exception_1.UserNotExists)
                throw new common_1.NotFoundException(error.message);
            if (error instanceof exception_1.UserAlreadyExists)
                throw new common_1.BadRequestException(error.message);
            throw new common_1.InternalServerErrorException({
                message: "Alguma coisa deu errado. Tente novamente em alguns instantes...",
            });
        }
    }
    async updatePassword(req, user) {
        try {
            const updatedUser = await this.userService.update_password(req.user.id, user);
            return updatedUser;
        }
        catch (error) {
            if (error instanceof exception_1.UserNotExists)
                throw new common_1.NotFoundException(error.message);
            if (error instanceof user_password_not_match_1.UserPasswordNotMatch)
                throw new common_1.BadRequestException(error.message);
            if (error instanceof exception_1.UserAlreadyExists)
                throw new common_1.BadRequestException(error.message);
            throw new common_1.InternalServerErrorException({
                message: "Alguma coisa deu errado. Tente novamente em alguns instantes...",
            });
        }
    }
    async delete(req) {
        try {
            await this.userService.delete(req.user.id);
            return new custom_success_response_dto_1.CustomSuccessResponse("Usuário excluído com sucesso");
        }
        catch (error) {
            if (error instanceof exception_1.UserNotExists) {
                throw new common_1.NotFoundException(error.message);
            }
            throw new common_1.InternalServerErrorException({
                message: "Alguma coisa deu errado. Tente novamente em alguns instantes...",
            });
        }
    }
};
__decorate([
    common_1.UseInterceptors(common_1.ClassSerializerInterceptor),
    common_1.Get(),
    __param(0, common_1.Query(new validation_pipe_1.ValidationPipe())),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [users_query_params_dto_1.UserQueryParams]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "findAll", null);
__decorate([
    common_1.Get("/find"),
    common_1.UseGuards(jwt_auth_guard_1.JwtAuthGuard),
    common_1.HttpCode(200),
    __param(0, common_1.Request()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "findById", null);
__decorate([
    common_1.Post(),
    __param(0, common_1.Body(new validation_pipe_1.ValidationPipe())),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [users_create_dto_1.UserCreateDto]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "create", null);
__decorate([
    common_1.Post("/register"),
    common_1.HttpCode(200),
    __param(0, common_1.Body(new validation_pipe_1.ValidationPipe())),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [user_register_dto_1.UserRegisterDTo]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "register", null);
__decorate([
    common_1.Post("/resetPassword"),
    common_1.HttpCode(200),
    __param(0, common_1.Body(new validation_pipe_1.ValidationPipe())),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "resetPassword", null);
__decorate([
    common_1.Post("/changePassword"),
    common_1.HttpCode(200),
    __param(0, common_1.Body(new validation_pipe_1.ValidationPipe())),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "changePassword", null);
__decorate([
    common_1.UseGuards(jwt_auth_guard_1.JwtAuthGuard),
    common_1.Put(),
    __param(0, common_1.Request()),
    __param(1, common_1.Body(new validation_pipe_1.ValidationPipe())),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, user_edit_dto_1.UserEditDTO]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "update", null);
__decorate([
    common_1.UseGuards(jwt_auth_guard_1.JwtAuthGuard),
    common_1.Put(":id/password"),
    __param(0, common_1.Request()),
    __param(1, common_1.Body(new validation_pipe_1.ValidationPipe())),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, user_edit_password_dto_1.UserEditPasswordDTO]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "updatePassword", null);
__decorate([
    common_1.Delete(),
    common_1.UseGuards(jwt_auth_guard_1.JwtAuthGuard),
    common_1.HttpCode(200),
    __param(0, common_1.Request()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "delete", null);
UserController = __decorate([
    common_1.Controller("users"),
    __param(5, typeorm_1.InjectRepository(profile_entity_1.Profile)),
    __metadata("design:paramtypes", [code_reset_service_1.CodeResetService,
        user_service_1.UserService,
        access_code_service_1.AccessCodeService,
        user_repository_1.UserRepository,
        mail_sender_service_1.MailSenderService,
        profile_repository_1.ProfileRepository])
], UserController);
exports.UserController = UserController;
//# sourceMappingURL=user.controller.js.map