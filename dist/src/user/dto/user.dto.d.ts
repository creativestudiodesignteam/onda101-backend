export declare class UserDto {
    id: number;
    username: string;
    email: string;
    password: string;
    created_at?: Date;
    updated_at?: Date;
    constructor(id: number, username: string, email: string, password: string, created_at?: Date, updated_at?: Date);
}
