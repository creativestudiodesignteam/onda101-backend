"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserListDto = void 0;
class UserListDto {
    constructor(users, page, perPage) {
        this.users = users;
        this.page = page;
        this.perPage = perPage;
    }
    getPaginatedData() {
        return {
            pagination: {
                page: this.page,
                perPage: this.perPage,
            },
            users: this.users,
        };
    }
}
exports.UserListDto = UserListDto;
//# sourceMappingURL=user-list.dto.js.map