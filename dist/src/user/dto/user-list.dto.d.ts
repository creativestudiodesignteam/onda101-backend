import { PaginatedList } from "./../paginated-list";
import { User } from "./../user.entity";
export declare class UserListDto implements PaginatedList {
    users: User[];
    page: number;
    perPage: number;
    constructor(users: User[], page: number, perPage: number);
    getPaginatedData(): {
        pagination: {
            page: number;
            perPage: number;
        };
        users: User[];
    };
}
