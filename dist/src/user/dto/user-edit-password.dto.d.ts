export declare class UserEditPasswordDTO {
    password_old: string;
    password_new: string;
}
