export declare class UserRegisterDTo {
    username: string;
    email: string;
    password: string;
    firstName: string;
    lastName: string;
    birthDate: string;
    accessCode: string;
    constructor(username: string, email: string, password: string, firstName: string, lastName: string, birthDate: string, accessCode: string);
}
