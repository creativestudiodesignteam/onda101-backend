export declare class UserQueryParams {
    perPage: number;
    page: number;
}
