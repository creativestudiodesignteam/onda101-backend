"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CustomSuccessResponse = void 0;
class CustomSuccessResponse {
    constructor(message, body) {
        this.message = message;
        this.body = body !== null && body !== void 0 ? body : undefined;
    }
    getMessage() {
        return this.message;
    }
    setMessage(message) {
        this.message = message;
    }
    getBody() {
        return this.body;
    }
    setBody(body) {
        this.body = body;
    }
}
exports.CustomSuccessResponse = CustomSuccessResponse;
//# sourceMappingURL=custom-success-response.dto.js.map