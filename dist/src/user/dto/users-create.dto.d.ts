export declare class UserCreateDto {
    username: string;
    email: string;
    password: string;
    constructor(username: string, email: string, password: string);
}
