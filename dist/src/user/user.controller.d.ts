import { UserService } from "./user.service";
import { BadRequestException } from "@nestjs/common";
import { UserDto } from "./dto/user.dto";
import { UserQueryParams } from "./dto/users-query-params.dto";
import { CustomSuccessResponse } from "./dto/custom-success-response.dto";
import { UserEditDTO } from "./dto/user-edit.dto";
import { UserCreateDto } from "./dto/users-create.dto";
import { UserEditPasswordDTO } from "./dto/user-edit-password.dto";
import { UserRepository } from "./user.repository";
import { ProfileRepository } from "src/profile/profile.repository";
import { UserRegisterDTo } from "./dto/user-register.dto";
import { AccessCodeService } from "src/access-code/access-code.service";
import { CodeResetService } from "src/code-reset/code-reset.service";
import { MailSenderService } from "src/shared/mail-sender/mail-sender.service";
interface ResetPasswordProps {
    email: string;
}
interface ChangePasswordProps {
    code: string;
    password: string;
    email: string;
}
export declare class UserController {
    private codeResetService;
    private userService;
    private accessCodeService;
    private usersRepository;
    private mailSenderService;
    private profileRepository;
    constructor(codeResetService: CodeResetService, userService: UserService, accessCodeService: AccessCodeService, usersRepository: UserRepository, mailSenderService: MailSenderService, profileRepository: ProfileRepository);
    findAll(queryParams: UserQueryParams): Promise<{
        pagination: {
            page: number;
            perPage: number;
        };
        users: import("./user.entity").User[];
    }>;
    findById(req: any): Promise<UserDto>;
    create(user: UserCreateDto): Promise<UserDto>;
    register(userRegisterDto: UserRegisterDTo): Promise<import("../profile/dto/profile-and-user-data.dto").ProfileAndUserDataDto>;
    resetPassword(resetPassword: ResetPasswordProps): Promise<BadRequestException | {
        message: string;
    }>;
    changePassword(changePassword: ChangePasswordProps): Promise<BadRequestException | {
        message: string;
    }>;
    update(req: any, user: UserEditDTO): Promise<any>;
    updatePassword(req: any, user: UserEditPasswordDTO): Promise<any>;
    delete(req: any): Promise<CustomSuccessResponse>;
}
export {};
