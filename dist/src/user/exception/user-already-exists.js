"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserAlreadyExists = void 0;
const common_1 = require("@nestjs/common");
class UserAlreadyExists extends common_1.BadRequestException {
    constructor() {
        super("O usuário já existe");
    }
}
exports.UserAlreadyExists = UserAlreadyExists;
//# sourceMappingURL=user-already-exists.js.map