import { BadRequestException } from "@nestjs/common";
export declare class UserAlreadyExists extends BadRequestException {
    constructor();
}
