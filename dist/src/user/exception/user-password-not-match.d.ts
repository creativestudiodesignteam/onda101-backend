import { BadRequestException } from "@nestjs/common";
export declare class UserPasswordNotMatch extends BadRequestException {
    constructor();
}
