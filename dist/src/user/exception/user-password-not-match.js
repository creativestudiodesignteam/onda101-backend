"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserPasswordNotMatch = void 0;
const common_1 = require("@nestjs/common");
class UserPasswordNotMatch extends common_1.BadRequestException {
    constructor() {
        super("Senhas não são iguais");
    }
}
exports.UserPasswordNotMatch = UserPasswordNotMatch;
//# sourceMappingURL=user-password-not-match.js.map