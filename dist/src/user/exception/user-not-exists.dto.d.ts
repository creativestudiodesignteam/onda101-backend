export declare class UserNotExists extends Error {
    readonly message = "Usu\u00E1rio n\u00E3o existe";
}
