"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserNotExists = void 0;
class UserNotExists extends Error {
    constructor() {
        super(...arguments);
        this.message = "Usuário não existe";
    }
}
exports.UserNotExists = UserNotExists;
//# sourceMappingURL=user-not-exists.dto.js.map