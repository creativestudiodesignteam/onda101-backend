"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserModule = void 0;
const common_1 = require("@nestjs/common");
const user_service_1 = require("./user.service");
const user_controller_1 = require("./user.controller");
const typeorm_1 = require("@nestjs/typeorm");
const user_repository_1 = require("./user.repository");
const encrypted_service_1 = require("../shared/encrypted/encrypted.service");
const profile_module_1 = require("../profile/profile.module");
const groups_module_1 = require("../profile/groups/groups.module");
const access_code_module_1 = require("../access-code/access-code.module");
const challenge_module_1 = require("../challenge/challenge.module");
const code_reset_module_1 = require("../code-reset/code-reset.module");
const shared_module_1 = require("../shared/shared.module");
let UserModule = class UserModule {
};
UserModule = __decorate([
    common_1.Module({
        imports: [
            typeorm_1.TypeOrmModule.forFeature([user_repository_1.UserRepository]),
            encrypted_service_1.EncryptedService,
            profile_module_1.ProfileModule,
            shared_module_1.SharedModule,
            groups_module_1.GroupsModule,
            code_reset_module_1.CodeResetModule,
            access_code_module_1.AccessCodeModule,
            common_1.forwardRef(() => challenge_module_1.ChallengeModule),
        ],
        providers: [user_service_1.UserService, encrypted_service_1.EncryptedService],
        controllers: [user_controller_1.UserController],
        exports: [user_service_1.UserService],
    })
], UserModule);
exports.UserModule = UserModule;
//# sourceMappingURL=user.module.js.map