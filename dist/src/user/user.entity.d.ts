import { CodeReset } from "src/code-reset/code-reset.entity";
export declare class User {
    id: number;
    username: string;
    email: string;
    password: string;
    created_at: Date;
    updated_at: Date;
    codeResets: CodeReset;
}
