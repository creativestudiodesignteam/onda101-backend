import { Connection, DeepPartial } from "typeorm";
import { User } from "./user/user.entity";
import { Profile } from "./profile/profile.entity";
export declare class AppService {
    private connection;
    constructor(connection: Connection);
    getHello(): string;
    getImage(filename: any, path: any): Promise<string>;
    profileAndUserDataUpdate(userId: number, profileChanges: DeepPartial<Profile>, userChanges: DeepPartial<User>): Promise<void>;
}
