"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.down = exports.up = void 0;
async function up(knex) {
    await knex.schema.createTable("code_receivers_data", table => {
        table.increments("id").primary();
        table.string("known_receiver_name");
        table.string("code_requester_email", 100).notNullable();
        table
            .integer("app_access_code_id")
            .references("id")
            .inTable("app_access_codes")
            .notNullable();
        table
            .boolean("has_welcome_message")
            .notNullable()
            .defaultTo(false);
    });
}
exports.up = up;
async function down(knex) {
    await knex.schema.dropTable("code_receivers_data");
}
exports.down = down;
//# sourceMappingURL=20201107144603_code_receivers_data.js.map