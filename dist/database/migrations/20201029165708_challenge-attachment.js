"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.down = exports.up = void 0;
async function up(knex) {
    await knex.schema.createTable("challenge_attachments", table => {
        table.increments("id").primary();
        table.string("description", 250);
        table.string("value", 250).notNullable();
        table.integer('challenge_id').references('id').inTable('challenges').onDelete("cascade").notNullable();
        table.timestamps(undefined, true);
    });
}
exports.up = up;
async function down(knex) {
    await knex.schema.dropTable("challenge_attachments");
}
exports.down = down;
//# sourceMappingURL=20201029165708_challenge-attachment.js.map