"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.down = exports.up = void 0;
async function up(knex) {
    await knex.schema.createTable("profiles", table => {
        table.increments("id").primary();
        table.string("first_name", 250).notNullable();
        table.string("last_name", 250).notNullable();
        table.date("birth_date").notNullable();
        table.string("avatar");
        table
            .integer("user_id")
            .references("id")
            .inTable("users")
            .onDelete("cascade")
            .notNullable();
        table
            .integer("group_id")
            .references("id")
            .inTable("groups")
            .onDelete("set null")
            .notNullable();
        table
            .integer("total_score")
            .notNullable()
            .defaultTo(0);
        table
            .integer("challenge_level_id")
            .references("id")
            .inTable("challenge_levels")
            .notNullable();
        table.timestamps(undefined, true);
    });
}
exports.up = up;
async function down(knex) {
    await knex.schema.dropTable("profiles");
}
exports.down = down;
//# sourceMappingURL=20201028155610_profiles.js.map