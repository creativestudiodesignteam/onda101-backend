"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.down = exports.up = void 0;
async function up(knex) {
    await knex.schema.createTable("access_code_status", table => {
        table.increments("id").primary();
        table.string("description", 45).notNullable();
    });
}
exports.up = up;
async function down(knex) {
    await knex.schema.dropTable("access_code_status");
}
exports.down = down;
//# sourceMappingURL=20201106150757_access_code_status.js.map