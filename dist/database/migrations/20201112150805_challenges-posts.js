"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.down = exports.up = void 0;
async function up(knex) {
    await knex.schema.createTable("challenges_posts", table => {
        table.increments("id").primary();
        table.string("name_file").notNullable();
        table
            .integer("challenge_id")
            .references("id")
            .inTable("challenges")
            .onDelete("cascade")
            .notNullable();
        table
            .integer("profile_id ")
            .references("id")
            .inTable("profiles")
            .onDelete("cascade")
            .notNullable();
        table
            .dateTime("created_at")
            .notNullable()
            .defaultTo(knex.raw("CURRENT_TIMESTAMP"));
    });
}
exports.up = up;
async function down(knex) {
    await knex.schema.dropTable("challenges_posts");
}
exports.down = down;
//# sourceMappingURL=20201112150805_challenges-posts.js.map