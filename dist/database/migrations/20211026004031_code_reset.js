"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.down = exports.up = void 0;
async function up(knex) {
    await knex.schema.createTable("code_reset", table => {
        table.increments("id").primary();
        table.string("code").notNullable();
        table
            .dateTime("created_at")
            .notNullable()
            .defaultTo(knex.fn.now());
        table.dateTime("update_at")
            .notNullable()
            .defaultTo(knex.fn.now());
        table.dateTime("due_date")
            .notNullable()
            .defaultTo(knex.fn.now());
        table
            .integer("code_reset_status_id")
            .references("id")
            .inTable("code_reset_status");
        table
            .integer("user_id")
            .references("id")
            .inTable("users");
    });
}
exports.up = up;
async function down(knex) {
    await knex.schema.dropTable("code_reset");
}
exports.down = down;
//# sourceMappingURL=20211026004031_code_reset.js.map