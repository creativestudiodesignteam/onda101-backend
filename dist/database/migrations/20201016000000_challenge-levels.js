"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.down = exports.up = void 0;
async function up(knex) {
    await knex.schema.createTable("challenge_levels", table => {
        table.increments().primary();
        table.string("description", 80).notNullable();
        table.integer("level_number").notNullable();
    });
}
exports.up = up;
async function down(knex) {
    await knex.schema.dropTable("challenge_levels");
}
exports.down = down;
//# sourceMappingURL=20201016000000_challenge-levels.js.map