"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.down = exports.up = void 0;
async function up(knex) {
    await knex.schema.createTable("challenges", table => {
        table.increments("id").primary();
        table.string("title", 45).notNullable();
        table.string("imperative_phrase", 250).notNullable();
        table.text("description").notNullable();
        table.text("completion_message").notNullable();
        table.integer("number").notNullable();
        table.integer("score").notNullable();
        table.boolean("isExtra").notNullable();
        table
            .integer("challenge_level_id")
            .references("id")
            .inTable("challenge_levels")
            .notNullable();
        table.timestamps(undefined, true);
    });
}
exports.up = up;
async function down(knex) {
    await knex.schema.dropTable("challenges");
}
exports.down = down;
//# sourceMappingURL=20201016160515_challenges.js.map