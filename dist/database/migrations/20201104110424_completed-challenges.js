"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.down = exports.up = void 0;
async function up(knex) {
    await knex.schema.createTable("completed_challenges", table => {
        table.increments("id").primary();
        table.dateTime("first_time_done_at").notNullable();
        table.dateTime("last_time_done_at");
        table.integer("redone_times").defaultTo(0).notNullable();
        table.integer('challenge_id').references('id').inTable('challenges').onDelete("cascade").notNullable();
        table.integer('profile_id ').references('id').inTable('profiles').onDelete("cascade").notNullable();
    });
}
exports.up = up;
async function down(knex) {
    await knex.schema.dropTable("completed_challenges");
}
exports.down = down;
//# sourceMappingURL=20201104110424_completed-challenges.js.map