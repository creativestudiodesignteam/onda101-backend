"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.down = exports.up = void 0;
async function up(knex) {
    await knex.schema.createTable("challenge_status", table => {
        table.increments().primary();
        table.string("description", 120);
    });
}
exports.up = up;
async function down(knex) {
    await knex.schema.dropTable("challenge_status");
}
exports.down = down;
//# sourceMappingURL=20201028160000_challenge_status.js.map