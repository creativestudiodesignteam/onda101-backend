"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.down = exports.up = void 0;
async function up(knex) {
    await knex.schema.createTable("app_access_codes", table => {
        table.increments("id").primary();
        table.string("value").notNullable();
        table
            .dateTime("created_at")
            .notNullable()
            .defaultTo(knex.fn.now());
        table.dateTime("due_date").notNullable();
        table
            .integer("access_code_status_id")
            .references("id")
            .inTable("access_code_status");
    });
}
exports.up = up;
async function down(knex) {
    await knex.schema.dropTable("app_access_codes");
}
exports.down = down;
//# sourceMappingURL=20201106150952_app_access_codes.js.map