"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.down = exports.up = void 0;
async function up(knex) {
    await knex.schema.createTable("current_challenge_status", table => {
        table.increments().primary();
        table
            .integer("challenge_id")
            .references("id")
            .inTable("challenges")
            .notNullable();
        table
            .integer("profile_id")
            .references("id")
            .inTable("profiles")
            .notNullable();
        table
            .integer("challenge_status_id")
            .references("id")
            .inTable("challenge_status")
            .notNullable();
        table.timestamps(undefined, true);
    });
}
exports.up = up;
async function down(knex) {
    await knex.schema.dropTable("current_challenge_status");
}
exports.down = down;
//# sourceMappingURL=20201028160500_current_challenge_status.js.map