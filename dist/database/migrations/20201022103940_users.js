"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.down = exports.up = void 0;
async function up(knex) {
    await knex.schema.createTable("users", table => {
        table.increments("id").primary();
        table.string("username", 80).notNullable();
        table.string("email", 80).notNullable();
        table.string("password", 80).notNullable();
        table.timestamps(undefined, true);
    });
}
exports.up = up;
async function down(knex) {
    await knex.schema.dropTable("users");
}
exports.down = down;
//# sourceMappingURL=20201022103940_users.js.map