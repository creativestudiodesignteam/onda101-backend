"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.down = exports.up = void 0;
async function up(knex) {
    await knex.schema.createTable("groups", table => {
        table.increments("id").primary();
        table.string("name", 250).notNullable();
        table.integer("wave_number").notNullable();
        table.timestamps(undefined, true);
    });
}
exports.up = up;
async function down(knex) {
    await knex.schema.dropTable("groups");
}
exports.down = down;
//# sourceMappingURL=20201027150859_groups.js.map