"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.down = exports.up = void 0;
async function up(knex) {
    await knex.schema.createTable("redone_times", table => {
        table.increments().primary();
        table
            .integer("profile_id")
            .references("id")
            .inTable("profiles")
            .notNullable();
        table
            .integer("challenge_id")
            .references("id")
            .inTable("challenges")
            .notNullable();
        table
            .dateTime("done_at")
            .notNullable()
            .defaultTo(knex.fn.now());
    });
}
exports.up = up;
async function down(knex) {
    await knex.schema.dropTable("redone_times");
}
exports.down = down;
//# sourceMappingURL=20201028161000_redone_times.js.map