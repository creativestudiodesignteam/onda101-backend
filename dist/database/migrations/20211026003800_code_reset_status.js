"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.down = exports.up = void 0;
async function up(knex) {
    await knex.schema.createTable("code_reset_status", table => {
        table.increments("id").primary();
        table.string("description", 45).notNullable();
        table.timestamps(undefined, true);
    });
}
exports.up = up;
async function down(knex) {
    await knex.schema.dropTable("code_reset_status");
}
exports.down = down;
//# sourceMappingURL=20211026003800_code_reset_status.js.map