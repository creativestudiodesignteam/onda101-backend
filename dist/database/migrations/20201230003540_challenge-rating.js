"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.down = exports.up = void 0;
async function up(knex) {
    await knex.schema.createTable("challenge_ratings", table => {
        table.increments("id").primary();
        table
            .integer("profile_id")
            .references("id")
            .inTable("profiles")
            .notNullable();
        table
            .integer("challenge_id")
            .references("id")
            .inTable("challenges")
            .notNullable();
        table.integer("rating").notNullable();
        table.timestamps(undefined, true);
    });
}
exports.up = up;
async function down(knex) {
    await knex.schema.dropTable("challenge_ratings");
}
exports.down = down;
//# sourceMappingURL=20201230003540_challenge-rating.js.map