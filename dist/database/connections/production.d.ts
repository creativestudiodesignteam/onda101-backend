declare const _default: {
    host: string;
    database: string;
    user: string;
    password: string;
    port: string;
};
export default _default;
