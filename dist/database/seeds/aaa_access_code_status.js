"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.seed = void 0;
async function seed(knex) {
    await knex("access_code_status").del();
    await knex("access_code_status").insert([
        { id: 1, description: "pending" },
        { id: 2, description: "active" },
        { id: 3, description: "invalid" },
    ]);
}
exports.seed = seed;
//# sourceMappingURL=aaa_access_code_status.js.map