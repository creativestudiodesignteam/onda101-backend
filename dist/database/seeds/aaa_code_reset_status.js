"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.seed = void 0;
async function seed(knex) {
    await knex("code_reset_status").del();
    await knex("code_reset_status").insert([
        { id: 1, description: "active" },
        { id: 2, description: "used" },
        { id: 3, description: "inative" }
    ]);
}
exports.seed = seed;
;
//# sourceMappingURL=aaa_code_reset_status.js.map