"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.seed = void 0;
async function seed(knex) {
    await knex("challenge_levels").del();
    await knex("challenge_levels").insert([
        { id: 1, description: "Nível 1", level_number: 1 },
        { id: 2, description: "Nível 2", level_number: 2 },
        { id: 3, description: "Nível 3", level_number: 3 },
    ]);
}
exports.seed = seed;
//# sourceMappingURL=aac_challenge_levels.js.map