"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.seed = void 0;
async function seed(knex) {
    await knex("groups").del();
    await knex("groups").insert([
        {
            id: 1,
            name: "Marola",
            wave_number: 1,
            created_at: knex.fn.now(),
        },
        {
            id: 2,
            name: "Ondinha",
            wave_number: 2,
            created_at: knex.fn.now(),
        },
        {
            id: 3,
            name: "Big Wave",
            wave_number: 3,
        },
    ]);
}
exports.seed = seed;
//# sourceMappingURL=aad_groups.js.map