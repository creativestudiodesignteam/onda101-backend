"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.seed = void 0;
async function seed(knex) {
    await knex("challenge_status").del();
    await knex("challenge_status").insert([
        { id: 1, description: "incomplete" },
        { id: 2, description: "started" },
        { id: 3, description: "done" },
    ]);
}
exports.seed = seed;
//# sourceMappingURL=aab_challenge_status.js.map