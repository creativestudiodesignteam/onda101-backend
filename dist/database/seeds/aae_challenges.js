"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.seed = void 0;
const challenges_1 = require("./resources/challenges");
async function seed(knex) {
    await knex("challenges").del();
    await knex("challenges").insert(challenges_1.challenges);
}
exports.seed = seed;
//# sourceMappingURL=aae_challenges.js.map