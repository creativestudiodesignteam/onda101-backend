interface ChallengeTable {
    id: number;
    title: string;
    imperative_phrase: string;
    description: string;
    completion_message: string;
    number: number;
    score: number;
    isExtra: boolean;
    challenge_level_id: number;
}
export declare const challenges: ChallengeTable[];
export {};
