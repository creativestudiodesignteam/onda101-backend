export declare const config: {
    host: string;
    port: string;
    user: string;
    password: string;
};
