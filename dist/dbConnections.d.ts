export declare const dbConnections: {
    name: string;
    type: string;
    host: string;
    port: string;
    username: string;
    password: string;
    database: string;
    synchronize: boolean;
    entities: string[];
}[];
