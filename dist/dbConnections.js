"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.dbConnections = void 0;
const dotenv = require("dotenv");
dotenv.config();
exports.dbConnections = [
    {
        name: "ecommerceConnection",
        type: process.env.DB_ECOMMERCE_TYPE,
        host: process.env.DB_ECOMMERCE_HOST,
        port: process.env.DB_ECOMMERCE_PORT,
        username: process.env.DB_ECOMMERCE_USERNAME,
        password: process.env.DB_ECOMMERCE_PASSWORD,
        database: process.env.DB_ECOMMERCE_DATABASE,
        synchronize: false,
        entities: [
            "./dist/src/shop-window/**/*.entity.js",
            "./dist/src/blog/**/*.entity.js"
        ],
    },
    {
        name: "default",
        type: process.env.DB_TYPE,
        host: process.env.DB_HOST,
        port: process.env.DB_PORT,
        username: process.env.DB_USERNAME,
        password: process.env.DB_PASSWORD,
        database: process.env.DB_DATABASE,
        synchronize: false,
        entities: ["./dist/src/**/*.entity.js"],
    },
];
//# sourceMappingURL=dbConnections.js.map