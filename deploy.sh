#!/bin/bash
SCRIPT="cd /var/www/api.onda101.com.br; git pull"
HOSTS=("onda101.com.br")
USERNAMES=("root")
PASSWORDS=("Abacaxi1997*")
echo "-> Connecting to remote host"
for i in ${!HOSTS[*]} ; do
     echo "-> Connected to ${HOSTS[i]}"
     echo "-> Running scripts"
     SCR=${SCRIPT/PASSWORD/${PASSWORDS[i]}}
     sshpass -p ${PASSWORDS[i]} ssh -l ${USERNAMES[i]} ${HOSTS[i]} "${SCR}"
done