import { Injectable } from '@nestjs/common';
import { User } from 'src/user/user.entity';
import { CodeReset } from './code-reset.entity';
import { CodeResetRepository } from './code-reset.repository';
import { CreateCodeResetDto } from './dto/create-code-reset.dto';
import { CodeResetStatus } from 'src/code-reset-status/code-reset-status.entity';
import { CodeResetNotExists } from 'src/code-reset/exception/codereset-not-exists.dto';

enum CodeResetStatusValue {
  active = 1,
  used = 2,
  inative = 3
}

@Injectable()
export class CodeResetService {
  constructor(
    private codeResetRepository: CodeResetRepository,
  ) { }

  async create(codeResetDTO: CreateCodeResetDto) {

    const user = new User()
    const resetStatus = new CodeResetStatus()
    resetStatus.id = CodeResetStatusValue.active
    user.id = codeResetDTO.user.id
    const newObj = new CodeReset();
    newObj.code = codeResetDTO.codeRandom;
    newObj.user = user
    newObj.codeStatus = resetStatus
    // newObj.due_date = moment().add(1, 'day').toDate();
    user.id = codeResetDTO.user.id
    let obj = await this.codeResetRepository.store(newObj);
    return obj;
  }

  findAll() {
    return `This action returns all codeReset`;
  }

  async findOne(code: string) {
    try {
      //"user.username = :username", { username: username }
      const codeExists = await this.codeResetRepository.findBy({
        code,
        //"user.id": userId
      });
      //if (!codeExists) throw new CodeResetNotExists;

      return codeExists;
    } catch (error) {
      throw error;
    }
    //return `This action returns a #${id} codeReset`;
  }

  update(id: number, updateCodeResetDto: CreateCodeResetDto) {
    return `This action updates a #${id} codeReset`;
  }

  async delete(id: number) {
    try {
      const obj = await this.codeResetRepository.findById(id);
      if (!obj) throw new CodeResetNotExists();

      this.codeResetRepository.destroy(obj);
      return obj;
    } catch (error) {
      throw error;
    }
  }
}
