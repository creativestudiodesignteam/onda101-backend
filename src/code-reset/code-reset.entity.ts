import { countReset } from "console";
import { User } from "src/user/user.entity";
import {
    Column,
    Entity,
    JoinColumn,
    ManyToOne,
    PrimaryGeneratedColumn,
} from "typeorm";
import { CodeResetStatus } from "../code-reset-status/code-reset-status.entity";

@Entity({ name: "code_reset" })
export class CodeReset {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    code: string;

    @Column()
    created_at: Date;

    @Column()
    update_at: Date;

    @Column()
    due_date: Date;

    @ManyToOne(
        () => CodeResetStatus,
        codeStatus => codeStatus.codes,
        { eager: true },
    )
    @JoinColumn({ name: "code_reset_status_id" })
    codeStatus: CodeResetStatus;
    @ManyToOne(
        () => User,
        user => user.codeResets,
        { eager: true },
    )
    @JoinColumn({ name: "user_id" })
    user: User;
}
