import { IsDate } from "class-validator";

export class UpdateCodeResetDto {
    id: number;
    @IsDate()
    date: Date;

    constructor(date: Date) {
        this.date = date;
    }
}
