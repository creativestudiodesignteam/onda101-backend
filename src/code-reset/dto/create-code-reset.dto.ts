import { IsEmail, IsObject } from "class-validator";
import { UserDto } from "src/user/dto/user.dto";

export class CreateCodeResetDto {
    @IsEmail()
    email: string;
    @IsObject()
    user: UserDto;

    codeRandom: string;

    constructor(email: string, user: UserDto, codeRandom) {
        this.email = email;
        this.user = user;
        this.codeRandom = codeRandom;
    }
}
