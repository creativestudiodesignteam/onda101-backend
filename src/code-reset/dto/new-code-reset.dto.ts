import { IsString } from "class-validator";

export class NewCodeResetDTO {
    id: number;
    @IsString()
    code: string;
    created_at?: Date;
    updated_at?: Date;

    constructor(
        id: number,
        code: string,
        created_at?: Date,
        updated_at?: Date,
    ) {
        this.id = id;
        this.code = code;
        this.created_at = created_at ?? undefined;
        this.updated_at = updated_at ?? undefined;
    }
}
