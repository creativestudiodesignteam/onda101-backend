import { CodeReset } from "./code-reset.entity";
import { DeepPartial, EntityRepository, Repository } from "typeorm";
//import { UserQueryParams } from "./dto/users-query-params.dto";
import { UpdateCodeResetDto } from "./dto/update-code-reset.dto";


@EntityRepository(CodeReset)
export class CodeResetRepository extends Repository<CodeReset> {

    // async findAll(queryParams: UserQueryParams): Promise<User[]> {
    //     const users = await this.find({

    //         take: queryParams.perPage,
    //         skip: queryParams.perPage * (queryParams.page - 1),
    //         order: { id: "ASC" },
    //     });

    //     return users.map(user => {
    //         delete user.password;
    //         return user;
    //     });
    // }


    async findById(id: number): Promise<CodeReset> {
        const codeReset = await this.findOne(id);
        return codeReset
    }

    async findBy(paramToSearch) {
        return await this.findOne({ where: paramToSearch });
    }
    // async findBy(paramToSearch) {
    //     return await this.findOne({ where: paramToSearch });
    // }

    async store(codeReset: CodeReset) {
        return await this.insert(codeReset);
    }

    async updateCodeReset(codeReset: DeepPartial<UpdateCodeResetDto>) {
        return await this.update(codeReset.id, codeReset);
    }

    async destroy(codeReset: CodeReset): Promise<void> {
        await this.remove(codeReset);
    }
}
