import { Controller, Get, Post, Body, Put, Param, Delete } from '@nestjs/common';
import { CodeResetService } from './code-reset.service';
import { CreateCodeResetDto } from './dto/create-code-reset.dto';

@Controller('code-reset')
export class CodeResetController {
  constructor(
    private readonly codeResetService: CodeResetService,
  ) { }

  // @Post()
  // create(@Body() createCodeResetDto: CreateCodeResetDto) {
  //   return this.codeResetService.create(createCodeResetDto);
  // }

  // @Get()
  // findAll() {
  //   return this.codeResetService.findAll();
  // }

  // @Get(':id')
  // findOne(@Param('id') id: string) {
  //   return this.codeResetService.findOne(+id);
  // }

  // @Put(':id')
  // update(@Param('id') id: string, @Body() updateCodeResetDto: UpdateCodeResetDto) {
  //   return this.codeResetService.update(+id, updateCodeResetDto);
  // }

  // @Delete(':id')
  // remove(@Param('id') id: string) {
  //   return this.codeResetService.remove(+id);
  // }
}
