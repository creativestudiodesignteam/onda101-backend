import { Module } from '@nestjs/common';
import { CodeResetService } from './code-reset.service';
import { CodeResetController } from './code-reset.controller';
import { CodeResetRepository } from './code-reset.repository';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  controllers: [CodeResetController],
  providers: [CodeResetService],
  imports: [
    TypeOrmModule.forFeature([CodeResetRepository]),
  ],
  exports: [
    CodeResetService,
    TypeOrmModule.forFeature([CodeResetRepository]),
  ]
})
export class CodeResetModule { }
