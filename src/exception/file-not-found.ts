export class FileNotFound extends Error {
  readonly message = "Arquivo não encontrado";
}
