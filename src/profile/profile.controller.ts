import {
  Controller,
  Delete,
  Get,
  HttpCode,
  InternalServerErrorException,
  NotFoundException,
  Query,
  UseGuards,
  ValidationPipe,
  Request,
  Post,
  UseInterceptors,
  Res,
  Req,
  UploadedFile,
} from "@nestjs/common";
import {ProfileService} from "./profile.service";
import {ProfileNotExists} from "./exception/profile-not-exists";
import {CustomSuccessResponse} from "./dto/custom-success-response.dto";
import {ProfileQueryParams} from "./dto/profile-query-params.dto";
import {ProfileListDto} from "./dto/profile-list-user.dto";
import {ProfileRelationsDto} from "./dto/profile-realtions.dto";
import {JwtAuthGuard} from "src/auth/jwt-auth.guard";
import {FileInterceptor} from "@nestjs/platform-express";
import {diskStorage} from "multer";
import {editFileName} from "src/shared/utils/upload-files";
import {FileNotFound} from "src/exception/file-not-found";
import {of} from "rxjs";
import {join} from "path";
import {existsSync} from "fs";
import {UploadFilesService} from "src/shared/upload-files/upload-files.service";

@Controller("profile")
export class ProfileController {
  constructor(
    private profileService: ProfileService,
    private uploadFilesService: UploadFilesService,
  ) {}

  @Get()
  async findBySearch(
    @Query(new ValidationPipe()) queryParams: ProfileQueryParams,
  ) {
    try {
      const findedUsers = await this.profileService.findUser(queryParams);

      const paginatedList = new ProfileListDto(
        findedUsers,
        queryParams.page,
        queryParams.perPage,
      );

      return paginatedList.getPaginatedData();
    } catch (error) {
      throw new InternalServerErrorException({
        message:
          "Alguma coisa deu errado. Tente novamente em alguns instantes...",
      });
    }
  }

  @Get("/find")
  @UseGuards(JwtAuthGuard)
  @HttpCode(200)
  async findById(@Request() req): Promise<ProfileRelationsDto> {
    try {
      const findProfileByUser = await this.profileService.findByUser(
        req.user.id,
      );
      const findedProfile = await this.profileService.findById(
        findProfileByUser.id,
      );

      return new ProfileRelationsDto(
        findedProfile.id,
        findedProfile.user,
        findedProfile.first_name,
        findedProfile.last_name,
        findedProfile.birth_date,
        findedProfile.total_score,
        findedProfile.challengeLevel,
        findedProfile.group,
        findedProfile.avatar,
      );
    } catch (error) {
      if (error instanceof ProfileNotExists)
        throw new NotFoundException(error.message);

      throw new InternalServerErrorException({
        error,
        message:
          "Alguma coisa deu errado. Tente novamente em alguns instantes...",
      });
    }
  }

  @Delete()
  @UseGuards(JwtAuthGuard)
  @HttpCode(200)
  async delete(@Request() req): Promise<CustomSuccessResponse> {
    try {
      const findProfileByUser = await this.profileService.findByUser(
        req.user.id,
      );

      await this.profileService.delete(findProfileByUser.id);

      return new CustomSuccessResponse("Perfil excluído com sucesso");
    } catch (error) {
      if (error instanceof ProfileNotExists)
        throw new NotFoundException(error.message);

      throw new InternalServerErrorException({
        message:
          "Alguma coisa deu errado. Tente novamente em alguns instantes...",
      });
    }
  }

  @Post("set-avatar")
  @HttpCode(200)
  @UseGuards(JwtAuthGuard)
  @UseInterceptors(
    FileInterceptor("file", {
      fileFilter: (req, file, cb) => {
        if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
          req.fileErrors = {
            error: true,
          };
          return cb(null, false);
        }
        return cb(null, true);
      },
      storage: diskStorage({
        destination: "./upload/profile-avatar",
        filename: editFileName,
      }),
    }),
  )
  async setProfileAvatar(@Req() req, @UploadedFile() file) {
    const profile = await this.profileService.findByUser(req.user.id);

    const compressFile = await this.uploadFilesService.compress(file, 80, 500);

    profile.avatar = compressFile.filename;
    await this.profileService.updateKnownProfile(profile);
    return {
      message: "avatar atualizado com sucesso!",
    };
  }

  @Get("avatar")
  @HttpCode(200)
  @UseGuards(JwtAuthGuard)
  async getProfileAvatar(@Req() req, @Res() res) {
    try {
      const profile = await this.profileService.findByUser(req.user.id);
      const fileName = profile.avatar ?? "";
      if (fileName !== "") {
        const filePath = await this.getPathToAvatar(
          fileName,
          "upload/profile-avatar",
        );
        return of(res.sendFile(join(process.cwd(), filePath)));
      } else {
        throw new NotFoundException("Perfil ainda não tem um avatar");
      }
    } catch (error) {
      if (error instanceof FileNotFound || error instanceof NotFoundException) {
        throw new NotFoundException(error.message);
      }
      throw new InternalServerErrorException({
        message:
          "Alguma coisa deu errado. Tente novamente em alguns instantes...",
      });
    }
  }

  private async getPathToAvatar(filename: string, path: string) {
    const filePath = join(path, filename);
    const checkFileExists = existsSync(filePath);

    if (!checkFileExists) {
      throw new FileNotFound();
    }

    return filePath;
  }
}
