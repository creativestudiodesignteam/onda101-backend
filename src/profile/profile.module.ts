import {Module} from "@nestjs/common";
import {ProfileService} from "./profile.service";
import {ProfileController} from "./profile.controller";
import {ProfileRepository} from "./profile.repository";
import {TypeOrmModule} from "@nestjs/typeorm";
import {ProfileCreationDto} from "./dto/profile-creation.dto";
import {GroupsModule} from "./groups/groups.module";
import {SharedModule} from "src/shared/shared.module";
import {UploadFilesService} from "src/shared/upload-files/upload-files.service";

@Module({
  imports: [
    TypeOrmModule.forFeature([ProfileRepository]),
    GroupsModule,
    SharedModule,
  ],
  providers: [ProfileService, ProfileCreationDto, UploadFilesService],
  controllers: [ProfileController],
  exports: [
    ProfileService,
    ProfileCreationDto,
    TypeOrmModule.forFeature([ProfileRepository]),
  ],
})
export class ProfileModule {}
