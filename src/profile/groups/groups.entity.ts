import { Column, CreateDateColumn, Entity, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { Profile } from "../profile.entity";

@Entity({ name: "groups" })
export class Groups {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    wave_number: number;

    @OneToMany(() => Profile, profile => profile.group)
    profiles: Profile[];

    @CreateDateColumn()
    created_at: Date;

    @UpdateDateColumn()
    updated_at: Date;
}
