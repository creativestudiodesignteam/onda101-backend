import { IsNumberString, IsOptional } from "class-validator";
export class GroupsQueryParams {
  @IsNumberString()
  @IsOptional()
  perPage: number;

  @IsNumberString()
  @IsOptional()
  page: number;
}
