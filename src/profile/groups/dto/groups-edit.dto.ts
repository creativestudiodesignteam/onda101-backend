import { IsOptional, IsString, IsInt } from "class-validator";

export class GroupsEditDTO {
    id: number;
    @IsOptional()
    @IsString()
    name: string;

    @IsOptional()
    @IsInt()
    wave_number: number;

}
