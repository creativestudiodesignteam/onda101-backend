import { IsInt, IsString } from "class-validator";

export class GroupsCreateDto {
    id: number;
    @IsString()
    name: string;
    @IsInt()
    wave_number: number;
    created_at?: Date;
    updated_at?: Date;

    constructor(
        id: number,
        name: string,
        wave_number: number,
        created_at?: Date,
        updated_at?: Date,
    ) {
        this.id = id;
        this.name = name;
        this.wave_number = wave_number;
        this.created_at = created_at ?? undefined;
        this.updated_at = updated_at ?? undefined;
    }
}
