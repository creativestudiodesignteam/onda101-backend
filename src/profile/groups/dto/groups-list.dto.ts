import { PaginatedList } from "./../paginated-list";
import { Groups } from "./../groups.entity";

export class GroupsListsDto implements PaginatedList {
  groups: Groups[];
  page: number;
  perPage: number;

  constructor(groups: Groups[], page: number, perPage: number) {
    this.groups = groups;
    this.page = page;
    this.perPage = perPage;
  }

  public getPaginatedData() {
    return {
      pagination: {
        page: this.page,
        perPage: this.perPage,
      },
      groups: this.groups,
    };
  }
}
