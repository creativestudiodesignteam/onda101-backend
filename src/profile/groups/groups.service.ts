import { Injectable } from '@nestjs/common';
import { DeepPartial } from 'typeorm';
import { GroupsCreateDto } from './dto/groups-create.dto';
import { GroupsEditDTO } from './dto/groups-edit.dto';
import { GroupsQueryParams } from './dto/groups-query-params.dto';
import { GroupsDto } from './dto/groups.dto';
import { GroupsNotExists } from './exception/groups-not-exists';
import { Groups } from './groups.entity';
import { GroupsRepository } from './groups.repository'

@Injectable()
export class GroupsService {
    constructor(
        private groupsRepository: GroupsRepository,
    ) { }

    async findAll({ perPage = 10, page = 1 }: GroupsQueryParams): Promise<Groups[]> {
        const findedGroups = await this.groupsRepository.findAll({
            perPage,
            page
        });

        return findedGroups
    }

    async findById(id: number) {
        try {
            const findedProfile = await this.groupsRepository.findById(id)

            if (!findedProfile) throw new GroupsNotExists()

            return findedProfile;

        } catch (error) {
            throw error;
        }
    }

    async selectGroups (){

        const lastGroup = await this.groupsRepository.findByAndOrder()

        return lastGroup
    }

    async create(groupsToBeCreated: GroupsCreateDto) {
        try {
            const groups = new Groups();
            groups.name = groupsToBeCreated.name;
            groups.wave_number = groupsToBeCreated.wave_number;

            await this.groupsRepository.store(groups);

            return groups;
        } catch (error) {
            throw error;
        }
    }

    async update(id: number, groupsChanges: DeepPartial<GroupsEditDTO>) {
        try {
            const findedGroups = await this.groupsRepository.findById(id);

            if (!findedGroups) throw new GroupsNotExists

            const groups = this.groupsRepository.create({
                id,
                ...groupsChanges,
            });

            await this.groupsRepository.updateGroups(groups);

            return groups;
        } catch (error) {
            throw error;
        }
    }

    async delete(id: number) {
        try {
            const groups = await this.groupsRepository.findById(id);
            if (!groups) throw new GroupsNotExists();

            this.groupsRepository.destroy(groups);
        } catch (error) {
            throw error;
        }
    }

}
