import {Groups} from "./groups.entity";
import {DeepPartial, EntityRepository, Repository} from "typeorm";
import {GroupsQueryParams} from "./dto/groups-query-params.dto";
import {GroupsEditDTO} from "./dto/groups-edit.dto";
/* import { ProfileEditDto } from "./dto/profile-edit.dto";
 */
@EntityRepository(Groups)
export class GroupsRepository extends Repository<Groups> {
  async findAll(queryParams: GroupsQueryParams): Promise<Groups[]> {
    const groups = await this.find({
      take: queryParams.perPage,
      skip: queryParams.perPage * (queryParams.page - 1),
      order: {id: "ASC"},
    });

    return groups;
  }

  findById(id: number): Promise<Groups> {
    return this.findOne(id);
  }

  async findBy(paramToSearch) {
    return await this.findOne({where: paramToSearch});
  }
  async findByAndOrder() {
    return await this.findOne({order: {id: "ASC"}});
  }

  async store(groupsToBeCreated: Groups) {
    const groups = await this.insert(groupsToBeCreated);
    return groups;
  }

  async updateGroups(groups: DeepPartial<GroupsEditDTO>) {
    return await this.update(groups.id, groups);
  }

  async destroy(profile: Groups): Promise<void> {
    await this.remove(profile);
  }
}
