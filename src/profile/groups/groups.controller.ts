import { Body, ClassSerializerInterceptor, Controller, Delete, Get, HttpCode, InternalServerErrorException, NotFoundException, Param, Post, Put, Query, UseInterceptors } from '@nestjs/common';
import { GroupsDto } from './dto/groups.dto';
import { GroupsNotExists } from './exception/groups-not-exists';
import { GroupsService } from './groups.service';
import { ValidationPipe } from "../../shared/pipes/validation.pipe";
import { GroupsQueryParams } from './dto/groups-query-params.dto';
import { GroupsListsDto } from './dto/groups-list.dto';
import { GroupsCreateDto } from './dto/groups-create.dto';
import { GroupsEditDTO } from './dto/groups-edit.dto';
import { CustomSuccessResponse } from './dto/custom-success-response.dto';

@Controller('groups')
export class GroupsController {
    constructor(
        private groupsRepository: GroupsService,
    ) { }

    @UseInterceptors(ClassSerializerInterceptor)
    @Get()
    async findAll(
        @Query(new ValidationPipe()) queryParams: GroupsQueryParams,
    ) {
        try {
            const findedGroups = await this.groupsRepository.findAll(queryParams);

            const paginatedList = new GroupsListsDto(
                findedGroups,
                queryParams.page,
                queryParams.perPage,
            );

            return paginatedList.getPaginatedData();
        } catch (error) {
            throw new InternalServerErrorException({
                message:
                    "Alguma coisa deu errado. Tente novamente em alguns instantes...",
            });
        }
    }

    @Get(':id')
    @HttpCode(200)
    async findById(@Param() param): Promise<GroupsDto> {
        try {
            const findedGroup = await this.groupsRepository.findById(param.id);

            return new GroupsDto(
                findedGroup.id,
                findedGroup.name,
                findedGroup.wave_number
            )

        } catch (error) {
            if (error instanceof GroupsNotExists)
                throw new NotFoundException(error.message)

            throw new InternalServerErrorException({
                message:
                    "Alguma coisa deu errado. Tente novamente em alguns instantes...",
            });
        }

    }

    @Post()
    async create(
        @Body(new ValidationPipe()) groups: GroupsCreateDto,
    ): Promise<GroupsDto> {
        try {
            const createdGroups = await this.groupsRepository.create(groups);

            return new GroupsDto(
                createdGroups.id,
                createdGroups.name,
                createdGroups.wave_number,
            );
        } catch (error) {
            throw new InternalServerErrorException({
                message:
                    "Alguma coisa deu errado. Tente novamente em alguns instantes...",
            });
        }
    }

    @Put(":id")
    async update(
        @Param() param,
        @Body(new ValidationPipe())
        groups: GroupsEditDTO,
    ): Promise<any> {
        try {
            const updatedUser = await this.groupsRepository.update(
                param.id,
                groups,
            );
            return updatedUser;
        } catch (error) {
            if (error instanceof GroupsNotExists)
                throw new NotFoundException(error.message);

            throw new InternalServerErrorException({
                message:
                    "Alguma coisa deu errado. Tente novamente em alguns instantes...",
            });
        }
    }

    @Delete(":id")
    @HttpCode(200)
    async delete(@Param() param): Promise<CustomSuccessResponse> {
        try {

            await this.groupsRepository.delete(param.id);

            return new CustomSuccessResponse("Usuário excluído com sucesso");
        } catch (error) {
            if (error instanceof GroupsNotExists) {
                throw new NotFoundException(error.message);
            }
            throw new InternalServerErrorException({
                message:
                    "Alguma coisa deu errado. Tente novamente em alguns instantes...",
            });
        }
    }

}
