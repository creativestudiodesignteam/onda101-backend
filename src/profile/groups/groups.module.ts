import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { GroupsController } from './groups.controller';
import { GroupsRepository } from './groups.repository';
import { GroupsService } from './groups.service';

@Module({
  imports: [TypeOrmModule.forFeature([GroupsRepository]), GroupsModule],
  providers: [GroupsService],
  controllers: [GroupsController],
  exports: [GroupsService]
})

export class GroupsModule { }
