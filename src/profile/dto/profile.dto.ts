
export class ProfileDto {
    id: number;
    first_name: string;
    last_name: string;
    birth_date: Date;
    group_name: string;
    user_id: number;
    created_at?: Date;
    updated_at?: Date;


    constructor(
        id: number,
        first_name: string,
        last_name: string,
        birth_date: Date,
        group_name: string,
        user_id: number,
        created_at?: Date,
        updated_at?: Date,
    ) {
        this.id = id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.birth_date = birth_date;
        this.group_name = group_name;
        this.user_id = user_id;
        this.created_at = created_at ?? undefined;
        this.updated_at = updated_at ?? undefined;
    }
}
