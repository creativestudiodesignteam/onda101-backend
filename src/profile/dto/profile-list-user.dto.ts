import { PaginatedList } from "./../paginated-list";
import { Profile } from "../profile.entity";

export class ProfileListDto implements PaginatedList {
  profiles: Profile[];
  page: number;
  perPage: number;

  constructor(profiles: Profile[], page: number, perPage: number) {
    this.profiles = profiles;
    this.page = page;
    this.perPage = perPage;
  }

  public getPaginatedData() {
    return {
      pagination: {
        page: this.page,
        perPage: this.perPage,
      },
      profiles: this.profiles,
    };
  }
}
