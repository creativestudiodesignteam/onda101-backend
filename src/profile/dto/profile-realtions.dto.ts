import {ChallengeLevel} from "src/challenge/challenge-level/challenge-level.entity";
import {User} from "src/user/user.entity";
import {Groups} from "../groups/groups.entity";
import {ProfileRelations} from "../models/profile-relations";

export class ProfileRelationsDto implements ProfileRelations {
  id: number;
  firstName: string;
  lastName: string;
  birth_date: Date;
  avatar: string | null;
  total_score: number;
  challengeLevel: ChallengeLevel;
  user: User;
  group: Groups;

  constructor(
    id: number,
    user: User,
    firstName: string,
    lastName: string,
    birth_date: Date,
    total_score: number,
    challengeLevel: ChallengeLevel,
    group: Groups,
    avatar: string,
  ) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.birth_date = birth_date;
    this.total_score = total_score;
    this.user = user;
    this.challengeLevel = challengeLevel;
    this.group = group;
    this.avatar = avatar ? this.toDownloadUrl() : null;
  }

  private toDownloadUrl() {
    return `${process.env.SERVER_DOMAIN}/profile/avatar`;
  }
}
