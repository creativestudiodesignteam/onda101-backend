import { ProfileAndUserData } from "../models/profile-and-user-data";

export class ProfileAndUserDataDto implements ProfileAndUserData {
  username: string;
  email: string;
  firstName: string;
  lastName: string;

  constructor(
    username: string,
    email: string,
    firstName: string,
    lastName: string,
  ) {
    this.username = username;
    this.email = email;
    this.firstName = firstName;
    this.lastName = lastName;
  }
}
