import { IsNumberString, IsOptional, IsString } from "class-validator";
export class ProfileQueryParams {
  @IsString()
  @IsOptional()
  user: string;

  @IsNumberString()
  @IsOptional()
  perPage: number;

  @IsNumberString()
  @IsOptional()
  page: number;
}
