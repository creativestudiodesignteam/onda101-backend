import {Profile} from "./profile.entity";
import {EntityRepository, Repository, Like} from "typeorm";
import {ProfileQueryParams} from "./dto/profile-query-params.dto";
import {AllowedProfilerelation} from "./models/allowed-profile-relation";

@EntityRepository(Profile)
export class ProfileRepository extends Repository<Profile> {
  async findById(id: number): Promise<Profile> {
    return await this.findOne(id);
  }

  findWithRelation(id: number, relation: AllowedProfilerelation[]) {
    return this.findOne({relations: relation, where: {id}});
  }
  async findUser(queryParams: ProfileQueryParams) {
    return this.find({
      first_name: Like(`%${queryParams.user}%`),
    });
  }

  async findBy(paramToSearch) {
    return await this.findOne({where: paramToSearch});
  }

  async findByWithRelations(paramToSearch, relations: AllowedProfilerelation[]) {
    return await this.findOne({where: paramToSearch, relations: relations});
  }

  async destroy(profile: Profile): Promise<void> {
    await this.remove(profile);
  }

  getLast() {
    return super
      .createQueryBuilder()
      .orderBy("id", "DESC")
      .getOne();
  }
}
