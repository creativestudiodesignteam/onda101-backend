import {DeepPartial} from "typeorm";
import {Injectable} from "@nestjs/common";
import {ProfileQueryParams} from "./dto/profile-query-params.dto";
import {ProfileNotExists} from "./exception/profile-not-exists";
import {AllowedProfilerelation} from "./models/allowed-profile-relation";
import {Profile} from "./profile.entity";
import {ProfileRepository} from "./profile.repository";

@Injectable()
export class ProfileService {
  constructor(private profileRepository: ProfileRepository) {}

  async findById(id: number, withRelations?: AllowedProfilerelation[]) {
    try {
      const findedProfile = await this.profileRepository.findWithRelation(
        id,
        withRelations ?? ["user", "group"],
      );

      if (!findedProfile) throw new ProfileNotExists();

      delete findedProfile.user.password;

      return findedProfile;
    } catch (error) {
      throw error;
    }
  }

  async findUser({user, page = 1, perPage = 10}: ProfileQueryParams) {
    try {
      const findedProfile = await this.profileRepository.findUser({
        user,
        page,
        perPage,
      });

      return findedProfile;
    } catch (error) {
      throw error;
    }
  }

  async findByUser(user) {
    try {
      const findedProfileByUser = await this.profileRepository.findBy({
        user,
      });

      return findedProfileByUser;
    } catch (error) {
      throw error;
    }
  }

  async findByUserWithRelations(user) {
    try {
      const findedProfileByUser = await this.profileRepository.findByWithRelations(
        {
          user,
        },
        ["group"],
      );

      return findedProfileByUser;
    } catch (error) {
      throw error;
    }
  }

  async delete(id: number): Promise<any> {
    const profile = await this.profileRepository.findById(id);
    if (!profile) throw new ProfileNotExists();

    this.profileRepository.destroy(profile);
  }

  async getLastProfileRegistered() {
    const profile = await this.profileRepository.getLast();
    if (!profile) {
      return null;
    } else {
      return profile;
    }
  }

  async addChallengeScore(score: number, profile: Profile) {
    const updatedProfile = profile;
    updatedProfile.total_score = updatedProfile.total_score + score;
    await this.profileRepository.save(profile);
  }

  updateKnownProfile(profile: DeepPartial<Profile>) {
    return this.profileRepository.save(profile);
  }
}
