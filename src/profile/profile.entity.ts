import {ChallengeLevel} from "src/challenge/challenge-level/challenge-level.entity";
import {CompletedChallenge} from "src/challenge/completed-challenge/completed-challenge.entity";
import {CurrentChallengeStatus} from "src/challenge/current-challenge-status/current-challenge-status.entity";
import {ChallengeRating} from "src/challenge/challenge-rating/challenge-rating.entity";
import {RedoneTimes} from "src/challenge/redone-times/redone-times.entity";
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";

import {User} from "../user/user.entity";
import {Groups} from "./groups/groups.entity";

@Entity({name: "profiles"})
export class Profile {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  first_name: string;

  @Column()
  last_name: string;

  @Column()
  birth_date: Date;

  @Column()
  avatar?: string;

  @OneToOne(() => User)
  @JoinColumn({name: "user_id"})
  user: User;

  @ManyToOne(
    () => Groups,
    group => group.profiles,
  )
  @JoinColumn({name: "group_id"})
  group: Groups;

  @OneToMany(
    () => CompletedChallenge,
    completedChallenge => completedChallenge.profile,
  )
  completedChallenges: CompletedChallenge;

  @Column()
  total_score: number;

  @OneToMany(
    () => CurrentChallengeStatus,
    currentChallengeStatus => currentChallengeStatus.challengeInProgress,
  )
  challengesInProgress!: CurrentChallengeStatus[];

  @ManyToOne(
    () => ChallengeLevel,
    challengeLevel => challengeLevel.profiles,
    {eager: true},
  )
  @JoinColumn({name: "challenge_level_id"})
  challengeLevel: ChallengeLevel;

  @OneToMany(
    () => RedoneTimes,
    redoneTimes => redoneTimes.profile,
  )
  redoneTimes: RedoneTimes[];

  @OneToMany(
    () => ChallengeRating,
    challengeRating => challengeRating.profile,
  )
  challengeRatings: ChallengeRating[];

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;
}
