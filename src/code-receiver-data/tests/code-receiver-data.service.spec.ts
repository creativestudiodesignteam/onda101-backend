import {Test, TestingModule} from "@nestjs/testing";
import {CodeReceiverDataService} from "../code-receiver-data.service";

describe("CodeReceiverDataService", () => {
  let service: CodeReceiverDataService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CodeReceiverDataService],
    }).compile();

    service = module.get<CodeReceiverDataService>(CodeReceiverDataService);
  });

  it("should be defined", () => {
    expect(service).toBeDefined();
  });
});
