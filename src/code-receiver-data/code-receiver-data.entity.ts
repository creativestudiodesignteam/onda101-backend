import {AccessCode} from "src/access-code/access-code.entity";
import {
  Column,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
} from "typeorm";

@Entity({name: "code_receivers_data"})
export class CodeReceiverData {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  known_receiver_name: string;

  @Column()
  code_requester_email: string;

  @OneToOne(() => AccessCode)
  @JoinColumn({name: "app_access_code_id"})
  accessCode: AccessCode;

  @Column()
  has_welcome_message: boolean;
}
