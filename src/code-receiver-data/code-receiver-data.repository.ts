import {EntityRepository, Repository} from "typeorm";
import {CodeReceiverData} from "./code-receiver-data.entity";

@EntityRepository(CodeReceiverData)
export class CodeReceiverDataRepository extends Repository<CodeReceiverData> {
  store(codeReceiver: CodeReceiverData) {
    return super.insert(codeReceiver);
  }
}
