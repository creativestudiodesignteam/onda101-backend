import {Injectable} from "@nestjs/common";
import {CodeReceiverData} from "./code-receiver-data.entity";
import {CodeReceiverDataRepository} from "./code-receiver-data.repository";

@Injectable()
export class CodeReceiverDataService {
  constructor(private repository: CodeReceiverDataRepository) {}

  async store(codeReceiver: CodeReceiverData) {
    await this.repository.store(codeReceiver);
  }
}
