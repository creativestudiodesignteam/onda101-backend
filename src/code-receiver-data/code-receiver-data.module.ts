import {Module} from "@nestjs/common";
import {TypeOrmModule} from "@nestjs/typeorm";
import {CodeReceiverDataRepository} from "./code-receiver-data.repository";
import {CodeReceiverDataService} from "./code-receiver-data.service";

@Module({
  providers: [CodeReceiverDataService],
  imports: [TypeOrmModule.forFeature([CodeReceiverDataRepository])],
  exports: [CodeReceiverDataService],
})
export class CodeReceiverDataModule {}
