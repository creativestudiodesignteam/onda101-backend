import {Strategy} from "passport-local";
import {PassportStrategy} from "@nestjs/passport";
import {
  Injectable,
  UnauthorizedException,
} from "@nestjs/common";
import {AuthService} from "./auth.service";
import {UnauthorizedAuth} from "./exception/unauthorized-auth";
import {UserNotExists} from "src/user/exception";

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
  constructor(private authService: AuthService) {
    super();
  }

  async validate(username: string, password: string): Promise<any> {
    try {
      const user = await this.authService.validateUser(username, password);

      if (!user) throw new UnauthorizedAuth();

      return user;
    } catch (error) {
      if (error instanceof UnauthorizedAuth)
        throw new UnauthorizedException(error.message);
      if (error instanceof UserNotExists)
        throw new UnauthorizedException(error.message);
    }
  }
}
