import {Module} from "@nestjs/common";
import {PassportModule} from "@nestjs/passport";
import {SharedModule} from "src/shared/shared.module";
import {UserModule} from "src/user/user.module";
import {AuthService} from "./auth.service";
import {LocalStrategy} from "./local.strategy";
import {AuthController} from "./auth.controller";
import {JwtModule} from "@nestjs/jwt";
import {jwtConstants} from "./constants";

import {JwtStrategy} from "./jwt.strategy";
import {ApiKeyStrategy} from "./apiKey/apiKey.strategy";

@Module({
  imports: [
    UserModule,
    SharedModule,
    PassportModule,
    JwtModule.register({
      secret: jwtConstants.secret,
      signOptions: {expiresIn: "7d"},
    }),
  ],
  providers: [AuthService, LocalStrategy, JwtStrategy, ApiKeyStrategy],
  controllers: [AuthController],
})
export class AuthModule {}
