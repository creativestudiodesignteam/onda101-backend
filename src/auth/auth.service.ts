import { EncryptedService } from "src/shared/encrypted/encrypted.service";
import { UserService } from "src/user/user.service";
import { Injectable } from "@nestjs/common";
import { JwtService } from "@nestjs/jwt";
import { UnauthorizedAuth } from "./exception/unauthorized-auth";
import { UserNotExists } from "src/user/exception";

@Injectable()
export class AuthService {
  constructor(
    private userService: UserService,
    private encryptService: EncryptedService,
    private jwtService: JwtService,
  ) { }
  private apiKeys: string[] = [
    "ca03na188ame03u1d78620de67282882a84",
    "d2e621a6646a4211768cd68e26f21228a81",
  ];

  async validateUser(username: string, password: string): Promise<any> {
    try {
      let user = null;
      if (username.match(/@/)) {
        user = await this.userService.findByEmail(username);
      } else {
        user = await this.userService.findByUsername(username);
      }

      const comparePasswords = await this.encryptService.compare(
        password,
        user.password,
      );

      if (!user || !comparePasswords) throw new UnauthorizedAuth();

      const { password: password_user, ...result } = user;

      return result;
    } catch (error) {
      if (error instanceof UserNotExists) throw new UnauthorizedAuth();
    }
  }

  async login(user: any) {
    const payload = { id: user.id, username: user.username, email: user.email };

    return {
      user: payload,
      access_token: this.jwtService.sign(payload),
    };
  }

  validateApiKey(apiKey: string) {
    return this.apiKeys.find(apiK => apiKey === apiK);
  }
}
