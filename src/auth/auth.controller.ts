import {
  Controller,
  Request,
  Post,
  UseGuards,
  InternalServerErrorException,
} from "@nestjs/common";
import {AuthService} from "./auth.service";
import {UnauthorizedAuth} from "./exception/unauthorized-auth";
import {LocalAuthGuard} from "./local-auth.guard";

@Controller("auth")
export class AuthController {
  constructor(private authService: AuthService) {}
  @UseGuards(LocalAuthGuard)
  @Post("login")
  async login(@Request() req) {
    try {
      return this.authService.login(req.user);
    } catch (error) {
      if (error instanceof UnauthorizedAuth)
        throw new UnauthorizedAuth(error.message);
      throw new InternalServerErrorException({
        message:
          "Alguma coisa deu errado. Tente novamente em alguns instantes...",
      });
    }
  }
}
