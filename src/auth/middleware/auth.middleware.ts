import {
  Injectable,
  NestMiddleware,
  UnauthorizedException,
} from "@nestjs/common";
import * as passport from "passport";
@Injectable()
export class AuthMiddleware implements NestMiddleware {
  use(req: any, res: any, next: () => void) {
    console.log("middleware");
    passport.authenticate(
      "headerapikey",
      {session: false, failureRedirect: "/api/unauthorized"},
      value => {
        if (value) {
          console.log("nexti");
          next();
        } else {
          throw new UnauthorizedException();
        }
      },
    )(req, res, next);
  }
}
