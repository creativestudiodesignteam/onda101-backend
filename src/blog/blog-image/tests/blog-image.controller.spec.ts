import { Test, TestingModule } from '@nestjs/testing';
import { BlogImageController } from '../blog-image.controller';

describe('BlogImageController', () => {
  let controller: BlogImageController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [BlogImageController],
    }).compile();

    controller = module.get<BlogImageController>(BlogImageController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
