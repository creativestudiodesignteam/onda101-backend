import {
  Column,
  Entity,
  EntityMetadata,
  JoinColumn,
  ManyToOne,
  PrimaryColumn,
} from "typeorm";
import {Blog} from "../blog.entity";
@Entity({name: "oc_blog_article_gallery"})
export class BlogImage {
  @PrimaryColumn()
  article_item_id: number;

  @PrimaryColumn()
  article_id: number;

  @Column()
  path: string;

  @Column()
  type: string;

  @ManyToOne(
    () => Blog,
    blog => blog.articleImages,
  )
  @JoinColumn({name: "article_id"})
  article: Blog;
}
