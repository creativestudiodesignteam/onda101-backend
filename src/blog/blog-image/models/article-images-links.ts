export interface ArticleImagesLinks {
  id: number;
  url: string;
}
