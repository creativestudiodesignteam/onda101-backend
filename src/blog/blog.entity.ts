import {Column, Entity, OneToMany, PrimaryColumn} from "typeorm";
import {BlogImage} from "./blog-image/blog-image.entity";

@Entity({name: "oc_blog_article_description"})
export class Blog {
  @PrimaryColumn()
  article_id: number;

  @Column()
  title: string;

  @OneToMany(
    () => BlogImage,
    blogImage => blogImage.article,
  )
  articleImages: BlogImage[];
}
