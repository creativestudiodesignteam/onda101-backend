import {Injectable} from "@nestjs/common";
import {BlogQueryParams} from "./dto/blog-query-params.dto";
import {BlogRepository} from "./blog.repository";
import {InjectRepository} from "@nestjs/typeorm";

@Injectable()
export class BlogService {
  constructor(
    @InjectRepository(BlogRepository, "ecommerceConnection")
    private repository: BlogRepository,
  ) {}
  async findWithRelations(queryParams: BlogQueryParams) {
    const blogPosts = await this.repository.findWithRelations(queryParams);

    return blogPosts;
  }
}
