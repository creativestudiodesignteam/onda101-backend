import {EntityRepository, Repository} from "typeorm";
import {BlogQueryParams} from "./dto/blog-query-params.dto";
import {Blog} from "./blog.entity";
import {BlogWithImage} from "./dto/blog-with-image";

@EntityRepository(Blog)
export class BlogRepository extends Repository<Blog> {
  async findWithRelations(
    queryParams: BlogQueryParams,
  ): Promise<BlogWithImage[]> {
    return await this.find({
      relations: ["articleImages"],
      take: queryParams.perPage ? queryParams.perPage : 10,
      skip:
        queryParams.page && queryParams.perPage
          ? queryParams.perPage * (queryParams.page - 1)
          : 0,
      order: {article_id: "DESC"},
    });
  }
}
