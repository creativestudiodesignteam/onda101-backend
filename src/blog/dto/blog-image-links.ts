import {IsDateString, IsNumber, IsString} from "class-validator";
import {BlogImage} from "../blog-image/blog-image.entity";
import {ArticleImagesLinks} from "../blog-image/models/article-images-links";
export class BlogLinks {
  @IsNumber()
  id: number;
  @IsString()
  title: string;
  @IsString()
  article_images: ArticleImagesLinks[];
  @IsString()
  url_article: string;
}
