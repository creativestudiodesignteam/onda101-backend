import {IsDateString, IsNumber, IsString} from "class-validator";
import {BlogImage} from "../blog-image/blog-image.entity";
export class BlogWithImage {
  @IsNumber()
  article_id: number;
  @IsString()
  title: string;
  @IsString()
  articleImages: BlogImage[];
}
