import {IsNumberString, IsOptional} from "class-validator";
export class BlogQueryParams {
  @IsNumberString()
  @IsOptional()
  perPage: number;

  @IsNumberString()
  @IsOptional()
  page: number;
}
