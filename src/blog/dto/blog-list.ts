import {PaginatedList} from "./../paginated-list";
import {Blog} from "./../blog.entity";
import {BlogLinks} from "./blog-image-links";
import {BlogWithImage} from "./blog-with-image";

export class BlogListDto implements PaginatedList {
  articles: BlogLinks[];
  page: number;
  perPage: number;

  constructor(article: BlogWithImage[], page: number, perPage: number) {
    const BlogLinks = article.map(result => {
      const transformWithLinks = {
        id: result.article_id,
        title: result.title,
        url_article: `${process.env.URL_ECOMMERCE}/index.php?route=blog/article&article_id=${result.article_id}`,
        article_images: result.articleImages.map(articleImage => {
          return {
            id: articleImage.article_id,
            url: articleImage.path
              ? `${process.env.URL_ECOMMERCE}/image/${articleImage.path}`
              : null,
          };
        }),
      };

      return transformWithLinks;
    });

    this.articles = BlogLinks;
    this.page = page;
    this.perPage = perPage;
  }

  public getPaginatedData() {
    return {
      pagination: {
        page: this.page,
        perPage: this.perPage,
      },
      articles: this.articles,
    };
  }
}
