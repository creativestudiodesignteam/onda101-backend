import {
  Controller,
  Get,
  InternalServerErrorException,
  Query,
} from "@nestjs/common";
import {ValidationPipe} from "src/shared/pipes/validation.pipe";
import {BlogService} from "./blog.service";
import {BlogListDto} from "./dto/blog-list";
import {BlogQueryParams} from "./dto/blog-query-params.dto";

@Controller("blog")
export class BlogController {
  constructor(private service: BlogService) {}

  @Get()
  async findAll(
    @Query(new ValidationPipe())
    queryParams: BlogQueryParams,
  ) {
    try {
      const allBlogsPosts = await this.service.findWithRelations(queryParams);

      const paginatedList = new BlogListDto(
        allBlogsPosts,
        queryParams.page,
        queryParams.perPage,
      );

      return paginatedList.getPaginatedData();
    } catch (error) {
      throw new InternalServerErrorException({
        error,
        message:
          "Alguma coisa deu errado. Tente novamente em alguns instantes...",
      });
    }
  }
}
