import {IsBoolean, IsString} from "class-validator";
import {FutureCodeReceiver} from "../models/future-code-receiver";

export class FutureCodeReceiverDto implements FutureCodeReceiver {
  @IsString()
  name: string;

  @IsBoolean()
  hasWelcomeMessage: boolean;
}
