import {IsEmail, IsNotEmpty} from "class-validator";
import {FutureCodeReceiver} from "../models/future-code-receiver";
import {GenerateCodeRequest} from "../models/generate-code-request";

export class GenerateCodeRequestDto implements GenerateCodeRequest {
  @IsEmail()
  @IsNotEmpty()
  email: string;

  @IsNotEmpty()
  names: FutureCodeReceiver[];

  @IsNotEmpty()
  paymentId: string;

  @IsNotEmpty()
  order: number;
}
