export class GeneratedCodesResponseDto {
  codes: string[] | object;

  constructor(codes: string[] | object) {
    this.codes = codes;
  }
}
