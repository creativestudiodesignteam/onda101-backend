import {FutureCodeReceiver} from "./future-code-receiver";

export interface GenerateCodeRequest {
  email: string;
  names: FutureCodeReceiver[];
}
