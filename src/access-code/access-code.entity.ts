import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from "typeorm";
import {AccessCodeStatus} from "./access-code-status/access-code-status.entity";

@Entity({name: "app_access_codes"})
export class AccessCode {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  value: string; // TODO: Utilizar ENUM

  @Column()
  created_at: Date;

  @Column()
  due_date: Date;

  @ManyToOne(
    () => AccessCodeStatus,
    codeStatus => codeStatus.codes,
    {eager: true},
  )
  @JoinColumn({name: "access_code_status_id"})
  codeStatus: AccessCodeStatus;
}
