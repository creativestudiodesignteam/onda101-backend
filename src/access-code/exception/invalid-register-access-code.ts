import {BadRequestException} from "@nestjs/common";

export class InvalidRegisterAccessCode extends BadRequestException {
  readonly message = "Código de acesso inválido para um novo registro";
}
