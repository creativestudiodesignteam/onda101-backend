import { Injectable } from "@nestjs/common";
import * as moment from "moment";
import * as crypto from "crypto";

import { ProfileService } from "src/profile/profile.service";
import { AccessCode } from "./access-code.entity";
import { AccessCodeRepository } from "./access-code.repository";
import { AccessCodeStatus } from "./access-code-status/access-code-status.entity";
import { InvalidRegisterAccessCode } from "./exception/invalid-register-access-code";
import { getRepository, Like } from "typeorm";

@Injectable()
export class AccessCodeService {
  constructor(
    private profilesService: ProfileService,
    private repository: AccessCodeRepository,
  ) { }

  async provideAccessCodesToNewUsers(totalAskedCodes: number) {
    const generatedCode: string[] = [];

    for (let i = 0; i < totalAskedCodes; i++) {
      const code = this.generateCode();

      await this.store(code);
      generatedCode.push(code);
    }

    return generatedCode;
  }

  private generateCode(): string {
    const accessCode = crypto.randomBytes(3).toString("hex");

    return accessCode;
  }

  async store(code: string) {

    const codeToBeStored = new AccessCode();
    codeToBeStored.value = code;
    codeToBeStored.due_date = moment()
      .add(2, "days")
      .toDate();
    const codeStatus = new AccessCodeStatus();
    codeStatus.id = 1;
    codeToBeStored.codeStatus = codeStatus;
    await this.repository.store(codeToBeStored);
  }

  findByCodeValue(code: string): Promise<AccessCode> {
    return this.repository.findByCodeValue(code);
  }

  async isCodeValidTonewUser(code: string): Promise<boolean> {
    const foundCode = await this.repository.findOne({
      where: { value: code },
    });

    console.log("found code", code, foundCode)
    if (foundCode) {
      const codeStatus = foundCode.codeStatus;
      if (codeStatus.description !== "pending") {
        throw new InvalidRegisterAccessCode();
      }
      return;
    } else {
      throw new InvalidRegisterAccessCode();
    }
  }

  async updateAccessCodeStatus(
    codeValue: string,
    newCodeStatus: "pending" | "active" | "invalid", // TODO: criar ENUM e utilizá-lo aqui e na entidade
  ) {
    const foundCode = await this.findByCodeValue(codeValue);
    if (foundCode) {
      const newAccessCodeStatus = await getRepository(
        AccessCodeStatus,
      ).findOne({ where: { description: newCodeStatus } });
      foundCode.codeStatus = newAccessCodeStatus;
      await this.repository.save(foundCode);
    }
  }
}
