import {EntityRepository, Repository} from "typeorm";
import {AccessCode} from "./access-code.entity";

@EntityRepository(AccessCode)
export class AccessCodeRepository extends Repository<AccessCode> {
  store(code: AccessCode) {
    try {
      return super.insert(code);
    } catch (error) {
      console.log("erro aqui: ", error);
    }
  }

  findByCodeValue(code: string) {
    return super.findOne({where: {value: code}});
  }
}
