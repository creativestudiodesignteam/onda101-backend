import {Module} from "@nestjs/common";
import {AccessCodeService} from "./access-code.service";
import {ProfileModule} from "src/profile/profile.module";
import {TypeOrmModule} from "@nestjs/typeorm";
import {AccessCodeRepository} from "./access-code.repository";
import {CodeReceiverDataModule} from "src/code-receiver-data/code-receiver-data.module";

@Module({
  providers: [AccessCodeService],
  imports: [
    ProfileModule,
    TypeOrmModule.forFeature([AccessCodeRepository]),
    CodeReceiverDataModule,
  ],
  exports: [AccessCodeService],
})
export class AccessCodeModule {}
