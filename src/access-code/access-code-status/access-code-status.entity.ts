import {Column, Entity, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {AccessCode} from "../access-code.entity";

@Entity({name: "access_code_status"})
export class AccessCodeStatus {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  description: string;

  @OneToMany(
    () => AccessCode,
    code => code.codeStatus,
  )
  codes: AccessCode;
}
