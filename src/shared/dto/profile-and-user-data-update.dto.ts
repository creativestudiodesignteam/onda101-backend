import {Type} from "class-transformer";
import {IsOptional, IsString, ValidateNested} from "class-validator";
import {ProfileOnlyEditDto} from "./profile-only-edit.dto";
import {UserOnlyEditDto} from "./user-only-edit.dto";

export class ProfileAndUserDataUpdateDto {
  @ValidateNested()
  @IsOptional()
  user: UserOnlyEditDto;

  @ValidateNested()
  @Type(() => ProfileOnlyEditDto)
  @IsOptional()
  profile: ProfileOnlyEditDto;

  @IsString()
  password: string;
}
