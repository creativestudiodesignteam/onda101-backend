import {IsOptional} from "class-validator";

export class ProfileOnlyEditDto {
  @IsOptional()
  first_name: string;
  @IsOptional()
  last_name: string;

  constructor(first_name: string, last_name: string) {
    this.first_name = first_name;
    this.last_name = last_name;
  }
}
