import {IsOptional, IsString, IsEmail} from "class-validator";

export class UserOnlyEditDto {
  @IsOptional()
  @IsString()
  username: string;

  @IsOptional()
  @IsString()
  @IsEmail()
  email: string;

  constructor(username: string, email: string) {
    this.username = username;
    this.email = email;
  }
}
