import {ValidationPipe} from "./pipes/validation.pipe";
import {Module} from "@nestjs/common";
import {EncryptedService} from "./encrypted/encrypted.service";
import {UploadFilesService} from "./upload-files/upload-files.service";
import {MailSenderService} from "./mail-sender/mail-sender.service";

@Module({
  providers: [
    ValidationPipe,
    EncryptedService,
    UploadFilesService,
    MailSenderService,
  ],
  exports: [ValidationPipe, EncryptedService, MailSenderService],
  imports: [EncryptedService],
})
export class SharedModule {}
