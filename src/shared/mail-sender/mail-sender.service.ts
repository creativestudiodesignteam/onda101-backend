import { Injectable } from "@nestjs/common";
import { MailSender } from "./models/mail-sender";
import * as nodemailer from "nodemailer";
import { config } from "../../../mail.config";

@Injectable()
export class MailSenderService implements MailSender {
  async sendEmail(to: string, message: string) {
    // console.log("To:", to)
    // console.log("Message:", message)
    // console.log("Host:", config.host)
    // console.log("Port:", config.port)
    // console.log("User:", config.user)
    // let testAccount = await nodemailer.createTestAccount();
    // console.log("TesteAccout", testAccount)
    try {
      const transporter = nodemailer.createTransport({
        host: config.host,
        secure: true,
        port: config.port,
        auth: {
          user: config.user,
          pass: config.password,
        },
      });

      const info = await transporter.sendMail({
        from: `Equipe Onda101 <${config.user}>`,
        to,
        subject: "Onda 101 - Códigos de acesso ao App",
        text: message,
      });
    } catch (error) {
      console.log(error);
      throw error;
    }
  }

  sendEmails(emails: string[]) {
    // TODO: Implementar envio de emails a múltiplos destinos
    return;
  }
}
