export interface MailSender {
  sendEmail: (to: string, message: string) => void;
  sendEmails: (emails: string[], message: string) => void;
}
