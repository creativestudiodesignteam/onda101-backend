export class CompressDto {
  path: string;
  filename: string;

  constructor(path: string, filename: string) {
    this.path = path;
    this.filename = filename;
  }
}
