import {join} from "path";
import {promises as fs} from "fs";
import {writeFileSync} from "fs";
import {Injectable} from "@nestjs/common";
import {FileIsNotExists} from "./exception/FileIsNotExists";
import * as sharp from "sharp";
import {CompressDto} from "./dto/compress.dto";
import {UnexpectedErrorToSendImage} from "./exception/UnexpectedError";
@Injectable()
export class UploadFilesService {
  async delete(path, filename) {
    const filePath = join(path, filename);
    const fileExists = await fs.stat(filePath);

    if (!fileExists) {
      throw new FileIsNotExists();
    }

    await fs.unlink(filePath);
  }

  async compress(file, quality: number, size: number): Promise<CompressDto> {
    const newPath = file.path.split(".")[0] + ".webp";

    const compressed = await sharp(file.path)
      .resize(size ?? 80)
      .toFormat("webp")
      .webp({
        quality: quality ?? 80,
      })
      .toBuffer()
      .then(async data => {
        const fileExists = await fs.stat(file.path);

        if (!fileExists) {
          throw new FileIsNotExists();
        }

        try {
          await fs.writeFile(newPath, data);
        } catch (err) {
          if (err) {
            console.log("erro send image line 46-->", err);
            await fs.unlink(newPath);

            throw new UnexpectedErrorToSendImage();
          }
        }
        await fs.unlink(file.path);

        return newPath;
      });

    const brokenToString = compressed.split("/");

    let pathString = "";

    for (let cont = 0; cont < brokenToString.length - 1; cont++) {
      pathString = pathString + "/" + brokenToString[cont];
    }

    console.log(brokenToString);
    return new CompressDto(
      pathString,
      brokenToString[brokenToString.length - 1],
    );
  }
}
