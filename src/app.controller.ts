import {
  BadRequestException,
  Body,
  Controller,
  Get,
  HttpCode,
  InternalServerErrorException,
  NotFoundException,
  Param,
  Post,
  Put,
  Req,
  Res,
  UseGuards,
  ValidationPipe,
} from "@nestjs/common";
import { join } from "path";
import { of } from "rxjs";
import { AppService } from "./app.service";
import { FileNotFound } from "./exception/file-not-found";
import { CodeReceiverData } from "./code-receiver-data/code-receiver-data.entity";
import { GeneratedCodesResponseDto } from "./access-code/dto/generated-codes-response.dto";
import { AccessCodeService } from "./access-code/access-code.service";
import { CodeResetService } from "./code-reset/code-reset.service";
import { GenerateCodeRequestDto } from "./access-code/dto/generate-code-request-dto";
import { CodeReceiverDataService } from "./code-receiver-data/code-receiver-data.service";
import { MailSenderService } from "./shared/mail-sender/mail-sender.service";
import { JwtAuthGuard } from "./auth/jwt-auth.guard";
import { ProfileAndUserDataUpdateDto } from "./shared/dto/profile-and-user-data-update.dto";
import { EncryptedService } from "./shared/encrypted/encrypted.service";
import { EntityManager } from "typeorm";
import { User } from "./user/user.entity";
import { UserPasswordNotMatch } from "./user/exception/user-password-not-match";
import { UnauthorizedExceptionAcessRoute } from "./auth/middleware/exception/UnauthorizedExceptionAcessRoute";
import axios from "axios";

@Controller()
export class AppController {
  constructor(
    private appService: AppService,
    private accessCodeservice: AccessCodeService,
    private codeResetService: CodeResetService,
    private codeReceiverService: CodeReceiverDataService,
    private mailSenderService: MailSenderService,
    private encryptService: EncryptedService,
    private entityManager: EntityManager,
  ) { }

  @Get()
  getHello(): string {
    return "Mudou o commit deploy automático";
  }

  @Get("files/:filename")
  @HttpCode(200)
  async getFiles(@Param() param, @Res() res) {
    try {
      const filePath = await this.appService.getImage(param.filename, "upload");

      return of(res.sendFile(join(process.cwd(), `upload/${filePath}`)));
    } catch (error) {
      if (error instanceof FileNotFound)
        throw new NotFoundException(error.message);
      throw new InternalServerErrorException({
        message:
          "Alguma coisa deu errado. Tente novamente em alguns instantes... fd",
      });
    }
  }

  @Get("posts/:filename")
  @HttpCode(200)
  async getposts(@Param() param, @Res() res) {
    try {
      const filePath = await this.appService.getImage(param.filename, "upload");

      return of(res.sendFile(join(process.cwd(), `upload/${filePath}`)));
    } catch (error) {
      if (error instanceof FileNotFound)
        throw new NotFoundException(error.message);
      throw new InternalServerErrorException({
        message:
          "Alguma coisa deu errado. Tente novamente em alguns instantes...",
      });
    }
  }

  @Post("access-code/generate")
  async receiveNewUsers(
    @Body(new ValidationPipe()) bodyRequest: GenerateCodeRequestDto,
  ) {
    // Pegando o id do pagamento enviado no body -> bodyRequest.paymentId
    // Verificando se esse pagamento existe, e se o status dele é aprovado
    try {
      const verifyPayment = await axios.get(
        `https://api.mercadopago.com/v1/payments/${bodyRequest.paymentId}`,
        {
          headers: {
            Authorization: `Bearer ${process.env.MP_ACESS_TOKEN}`,
          },
        },
      );
      if (verifyPayment.data.status !== "rejected") {
        return new BadRequestException(
          "Desculpe, o seu pagamento não foi aprovado :D!",
        );
      }
    } catch (error) {
      return new BadRequestException("Desculpe, não foi encontrado :D!");
    }

    try {
      const totalCodesNeeded = bodyRequest.names.length;
      const codes = await this.accessCodeservice.provideAccessCodesToNewUsers(
        totalCodesNeeded,
      );

      const storedAcccessCodeAsPromises = codes.map(code =>
        this.accessCodeservice.findByCodeValue(code),
      );
      const resolvedStoredAccessCode = await Promise.all(
        storedAcccessCodeAsPromises,
      );
      resolvedStoredAccessCode.forEach(async (accessCode, index) => {
        const codeReceiverData = new CodeReceiverData();
        codeReceiverData.accessCode = accessCode;
        codeReceiverData.code_requester_email = bodyRequest.email;
        codeReceiverData.has_welcome_message =
          bodyRequest.names[index].hasWelcomeMessage;
        const knownName = bodyRequest.names[index].name;
        if (knownName) {
          codeReceiverData.known_receiver_name = knownName;
        }
        await this.codeReceiverService.store(codeReceiverData);
      });

      // Todo: Elaborar uma estrutura de criação de mensagens melhor
      const allCodesInSingleLine = codes.reduce(
        (singleString, email) => singleString.concat(`${email}; `),
        "",
      );
      const mailMessage = `Aqui estão seus códigos: ${allCodesInSingleLine}`;

      //await this.mailSenderService.sendEmail(bodyRequest.email, mailMessage);
      let rsp = [];
      for (let codeU in codes) {
        const code = codes[codeU];
        const name = bodyRequest.names[codeU];

        rsp.push({ name: name.name, code });
      }

      try {
        axios
          .get(
            "https://onda101.com.br/api-codes?data=" +
            JSON.stringify({
              order: bodyRequest.order,
              codes: rsp,
            }),
          )
          .then(resd => console.log(resd.data, resd.config.url));
      } catch (error) {
        console.log("error", error);
      }

      return new GeneratedCodesResponseDto({
        order: bodyRequest.order,
        codes: rsp,
      });
    } catch (error) {
      if (error instanceof UnauthorizedExceptionAcessRoute)
        throw new BadRequestException(error.message);
      return new InternalServerErrorException();
    }
  }
  @Put("profile-data")
  @UseGuards(JwtAuthGuard)
  async updateProfileAndUserData(
    @Req() req,
    @Body(new ValidationPipe()) bodyRequest: ProfileAndUserDataUpdateDto,
  ) {
    try {
      const user = await this.entityManager.findOne(User, req.user.id);
      const samePassword = await this.encryptService.compare(
        bodyRequest.password,
        user.password,
      );
      if (!samePassword) throw new UserPasswordNotMatch();

      await this.appService.profileAndUserDataUpdate(
        req.user.id,
        bodyRequest.profile,
        bodyRequest.user,
      );
      return { message: "Informações atualizadas com sucesso" };
    } catch (error) {
      if (error instanceof UserPasswordNotMatch)
        throw new BadRequestException("Senha informada está incorreta");
      throw new InternalServerErrorException({
        message:
          "Alguma coisa deu errado. Tente novamente em alguns instantes...",
      });
    }
  }
}
