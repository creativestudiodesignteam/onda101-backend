import {BadRequestException} from "@nestjs/common";

export class UserPasswordNotMatch extends BadRequestException {
  constructor() {
    super("Senhas não são iguais");
  }
}
