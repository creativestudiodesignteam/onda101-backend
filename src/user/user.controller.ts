import { ValidationPipe } from "./../shared/pipes/validation.pipe";
import { UserService } from "./user.service";
import {
  BadRequestException,
  Body,
  ClassSerializerInterceptor,
  Controller,
  Delete,
  Get,
  HttpCode,
  InternalServerErrorException,
  NotFoundException,
  Param,
  Post,
  Put,
  Query,
  UseGuards,
  UseInterceptors,
  Request,
} from "@nestjs/common";

import * as moment from "moment";
import { UserDto } from "./dto/user.dto";
import { UserListDto } from "./dto/user-list.dto";
import { UserNotExists, UserAlreadyExists } from "./exception";
import { UserQueryParams } from "./dto/users-query-params.dto";
import { CustomSuccessResponse } from "./dto/custom-success-response.dto";
import { UserEditDTO } from "./dto/user-edit.dto";
import { UserPasswordNotMatch } from "./exception/user-password-not-match";
import { UserCreateDto } from "./dto/users-create.dto";
import { UserEditPasswordDTO } from "./dto/user-edit-password.dto";
import { ProfileCreationDto } from "src/profile/dto/profile-creation.dto";
import { UserRepository } from "./user.repository";
import { ProfileRepository } from "src/profile/profile.repository";
import { InjectRepository } from "@nestjs/typeorm";
import { Profile } from "src/profile/profile.entity";
import { JwtAuthGuard } from "src/auth/jwt-auth.guard";
import { UserRegisterDTo } from "./dto/user-register.dto";
import { AccessCodeService } from "src/access-code/access-code.service";
import { CodeResetService } from "src/code-reset/code-reset.service";
import { ifError } from "assert";
import { CreateCodeResetDto } from "src/code-reset/dto/create-code-reset.dto";
import { MailSenderService } from "src/shared/mail-sender/mail-sender.service";
interface ResetPasswordProps {
  email: string
}
interface ChangePasswordProps {
  code: string
  password: string
  email: string
}
@Controller("users")
export class UserController {
  constructor(
    private codeResetService: CodeResetService,
    private userService: UserService,
    private accessCodeService: AccessCodeService,
    private usersRepository: UserRepository,
    private mailSenderService: MailSenderService,
    @InjectRepository(Profile)
    private profileRepository: ProfileRepository,
  ) { }

  @UseInterceptors(ClassSerializerInterceptor)
  @Get()
  async findAll(@Query(new ValidationPipe()) queryParams: UserQueryParams) {
    try {
      const findedUsers = await this.userService.findAll(queryParams);

      const paginatedList = new UserListDto(
        findedUsers,
        queryParams.page,
        queryParams.perPage,
      );

      return paginatedList.getPaginatedData();
    } catch (error) {
      throw new InternalServerErrorException({
        message:
          "Alguma coisa deu errado. Tente novamente em alguns instantes...",
      });
    }
  }

  @Get("/find")
  @UseGuards(JwtAuthGuard)
  @HttpCode(200)
  async findById(@Request() req): Promise<UserDto> {
    try {
      const user = await this.userService.findById(req.user.id);

      return new UserDto(
        user.id,
        user.username,
        user.email,
        user.password,
        user.created_at,
        user.updated_at,
      );
    } catch (error) {
      if (error instanceof UserNotExists)
        throw new NotFoundException(error.message);
      throw new InternalServerErrorException({
        message:
          "Alguma coisa deu errado. Tente novamente em alguns instantes...",
      });
    }
  }

  @Post()
  async create(
    @Body(new ValidationPipe()) user: UserCreateDto,
  ): Promise<UserDto> {
    try {
      const createdUser = await this.userService.create(user);

      return new UserDto(
        createdUser.id,
        createdUser.username,
        createdUser.email,
        createdUser.password,
      );
    } catch (error) {
      if (error instanceof UserAlreadyExists)
        throw new BadRequestException(error.message);
      if (error instanceof UserPasswordNotMatch)
        throw new BadRequestException(error.message);
      throw new InternalServerErrorException({
        message:
          "Alguma coisa deu errado. Tente novamente em alguns instantes...",
      });
    }
  }

  @Post("/register")
  @HttpCode(200)
  async register(@Body(new ValidationPipe()) userRegisterDto: UserRegisterDTo) {
    const user = new UserCreateDto(
      userRegisterDto.username,
      userRegisterDto.email,
      userRegisterDto.password,
    );
    const profile = new ProfileCreationDto(
      userRegisterDto.firstName,
      userRegisterDto.lastName,
      moment().toString(), // TODO: Discutir sobre remoção deste campo para essa tabela
    );
    try {
      await this.accessCodeService.isCodeValidTonewUser(
        userRegisterDto.accessCode,
      );
      const profileAndUserResponse = await this.userService.createUserAndHisProfile(
        user,
        profile,
        this.usersRepository,
        this.profileRepository,
      );

      console.log("Passou");

      await this.accessCodeService.updateAccessCodeStatus(
        userRegisterDto.accessCode,
        "active",
      );
      return profileAndUserResponse;
    } catch (error) {
      console.log(">>>> ", error);
      if (error instanceof BadRequestException)
        throw new BadRequestException(error.message);
      throw new InternalServerErrorException({
        message:
          "Alguma coisa deu errado. Tente novamente em alguns instantes... hhh",
      });
    }
  }

  @Post("/resetPassword")
  @HttpCode(200)
  async resetPassword(@Body(new ValidationPipe()) resetPassword: ResetPasswordProps) {
    try {

      const user = await this.userService.findByEmail(resetPassword.email);
      let codeResult = null;
      let codeResetEmail = null;
      if (user) {
        codeResetEmail = Math.random().toString().substr(2, 6);
        const resetDto = new CreateCodeResetDto(
          resetPassword.email,
          user,
          codeResetEmail
        );
        codeResult = await this.codeResetService.create(resetDto)
        const bodyEmail = `Seu codigo de reset de senha: ${codeResetEmail}`;
        await this.mailSenderService.sendEmail(resetPassword.email, bodyEmail);
      }
      return { message: `Email enviado com sucesso!` };
    } catch (error) {
      if (error instanceof UserNotExists) {
        return new BadRequestException(error.message);
      } else {
        console.log(">>>> ", error);
        if (error instanceof BadRequestException)
          throw new BadRequestException(error.message);
        throw new InternalServerErrorException({
          message:
            "Alguma coisa deu errado. Tente novamente em alguns instantes... hhh",
        });
      }
    }
  }
  @Post("/changePassword")
  @HttpCode(200)
  async changePassword(@Body(new ValidationPipe()) changePassword: ChangePasswordProps) {
    try {
      const user = await this.userService.findByEmail(changePassword.email);
      if (user) {
        const objCode = await this.codeResetService.findOne(changePassword.code);
        if (objCode) {
          const userChanged = await this.userService.changePassword(user.id, changePassword.password)
          const resultDelete = await this.codeResetService.delete(objCode.id);
          console.log("resultDelete Find: "/*, resultDelete*/)
        }
      }
      return { message: `Senha alterada com sucesso!` };
    } catch (error) {
      if (error instanceof UserNotExists) {
        return new BadRequestException(error.message);
      } else {
        console.log(">>>> ", error);
        if (error instanceof BadRequestException)
          throw new BadRequestException(error.message);
        throw new InternalServerErrorException({
          message:
            "Alguma coisa deu errado. Tente novamente em alguns instantes... hhh",
        });
      }
    }
  }

  @UseGuards(JwtAuthGuard)
  @Put()
  async update(
    @Request() req,
    @Body(new ValidationPipe())
    user: UserEditDTO,
  ): Promise<any> {
    try {
      console.log(req.user.id);
      const updatedUser = await this.userService.update(req.user.id, user);

      return updatedUser;
    } catch (error) {
      if (error instanceof UserNotExists)
        throw new NotFoundException(error.message);

      if (error instanceof UserAlreadyExists)
        throw new BadRequestException(error.message);

      throw new InternalServerErrorException({
        message:
          "Alguma coisa deu errado. Tente novamente em alguns instantes...",
      });
    }
  }

  @UseGuards(JwtAuthGuard)
  @Put(":id/password")
  async updatePassword(
    @Request() req,
    @Body(new ValidationPipe())
    user: UserEditPasswordDTO,
  ): Promise<any> {
    try {
      const updatedUser = await this.userService.update_password(
        req.user.id,
        user,
      );

      return updatedUser;
    } catch (error) {
      if (error instanceof UserNotExists)
        throw new NotFoundException(error.message);

      if (error instanceof UserPasswordNotMatch)
        throw new BadRequestException(error.message);

      if (error instanceof UserAlreadyExists)
        throw new BadRequestException(error.message);

      throw new InternalServerErrorException({
        message:
          "Alguma coisa deu errado. Tente novamente em alguns instantes...",
      });
    }
  }

  @Delete()
  @UseGuards(JwtAuthGuard)
  @HttpCode(200)
  async delete(@Request() req): Promise<CustomSuccessResponse> {
    try {
      await this.userService.delete(req.user.id);

      return new CustomSuccessResponse("Usuário excluído com sucesso");
    } catch (error) {
      if (error instanceof UserNotExists) {
        throw new NotFoundException(error.message);
      }
      throw new InternalServerErrorException({
        message:
          "Alguma coisa deu errado. Tente novamente em alguns instantes...",
      });
    }
  }
}
