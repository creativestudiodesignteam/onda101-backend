import { forwardRef, Module } from "@nestjs/common";
import { UserService } from "./user.service";
import { UserController } from "./user.controller";
import { TypeOrmModule } from "@nestjs/typeorm";
import { UserRepository } from "./user.repository";
import { EncryptedService } from "../shared/encrypted/encrypted.service";
import { ProfileModule } from "src/profile/profile.module";
import { GroupsModule } from "src/profile/groups/groups.module";
import { AccessCodeModule } from "src/access-code/access-code.module";
import { ChallengeModule } from "src/challenge/challenge.module";
import { CodeResetModule } from "src/code-reset/code-reset.module";
import { SharedModule } from "src/shared/shared.module";

@Module({
  imports: [
    TypeOrmModule.forFeature([UserRepository]),
    EncryptedService,
    ProfileModule,
    SharedModule,
    GroupsModule,
    CodeResetModule,
    AccessCodeModule,
    forwardRef(() => ChallengeModule),
  ],
  providers: [UserService, EncryptedService],
  controllers: [UserController],
  exports: [UserService],
})
export class UserModule { }
