import { forwardRef, Inject, Injectable } from "@nestjs/common";
import { UserNotExists, UserAlreadyExists } from "./exception";
import { UserRepository } from "./user.repository";
import { UserDto } from "./dto/user.dto";
import { User } from "./user.entity";
import { UserQueryParams } from "./dto/users-query-params.dto";
import {
  DeepPartial,
  Repository,
  Transaction,
  TransactionRepository,
} from "typeorm";
import { UserPasswordNotMatch } from "./exception/user-password-not-match";
import { UserCreateDto } from "./dto/users-create.dto";
import { UserEditDTO } from "./dto/user-edit.dto";
import { UserEditPasswordDTO } from "./dto/user-edit-password.dto";
import { EncryptedService } from "src/shared/encrypted/encrypted.service";
import { ProfileCreationDto } from "src/profile/dto/profile-creation.dto";
import { Profile } from "src/profile/profile.entity";
import { ProfileAndUserDataDto } from "src/profile/dto/profile-and-user-data.dto";
import { GroupsService } from "src/profile/groups/groups.service";
import { ChallengeLevel } from "src/challenge/challenge-level/challenge-level.entity";
import { ChallengeLevelService } from "src/challenge/challenge-level/challenge-level.service";
import { ProfileService } from "src/profile/profile.service";
import { GroupsNotExists } from "../profile/groups/exception/groups-not-exists";
import { UrlWithStringQuery } from "url";

@Injectable()
export class UserService {
  constructor(
    private userRepository: UserRepository,
    private encryptService: EncryptedService,
    private groupsService: GroupsService,
    private profileService: ProfileService,
    @Inject(forwardRef(() => ChallengeLevelService))
    private challengeLevelService: ChallengeLevelService,
  ) { }

  async findAll({ page = 1, perPage = 10 }: UserQueryParams) {
    const allUsers = await this.userRepository.findAll({
      page: page,
      perPage: perPage,
    });

    return allUsers;
  }

  async findById(id: number): Promise<UserDto> {
    try {
      const user = await this.userRepository.findById(id);

      if (!user) throw new UserNotExists();

      delete user.password;

      return user;
    } catch (error) {
      throw error;
    }
  }

  async findByUsername(username: string): Promise<UserDto> {
    try {
      const givenUserAlreadyExists = await this.userRepository.findBy({
        username,
      });

      if (!givenUserAlreadyExists) throw new UserNotExists();

      return givenUserAlreadyExists;
    } catch (error) {
      throw error;
    }
  }
  async findByEmail(email: string): Promise<UserDto> {
    try {
      const givenUserAlreadyExists = await this.userRepository.findBy({
        email,
      });

      if (!givenUserAlreadyExists) throw new UserNotExists();

      return givenUserAlreadyExists;
    } catch (error) {
      throw error;
    }
  }

  async create(userToBeCreated: UserCreateDto) {
    try {
      const givenUserAlreadyExists = await this.userRepository.findBy({
        username: userToBeCreated.username,
      });

      if (givenUserAlreadyExists) throw new UserAlreadyExists();

      const givenEmailAlreadyExists = await this.userRepository.findBy({
        email: userToBeCreated.email,
      });

      if (givenEmailAlreadyExists) throw new UserAlreadyExists();

      const password_hash = await this.encryptService.encrypt(
        userToBeCreated.password,
      );

      const user = new User();
      user.username = userToBeCreated.username;
      user.email = userToBeCreated.email;
      user.password = password_hash;

      await this.userRepository.store(user);

      delete user.password;
      return user;
    } catch (error) {
      throw error;
    }
  }

  @Transaction()
  async createUserAndHisProfile(
    user: UserCreateDto,
    profile: ProfileCreationDto,
    @TransactionRepository(User) usersRepo: Repository<User>,
    @TransactionRepository(Profile) profilesRepo: Repository<Profile>,
  ) {
    try {
      console.log("Creating user and profile...");
      const givenUserAlreadyExists = await usersRepo.findOne({
        where: {
          username: user.username,
        },
      });

      const givenEmailAlreadyExists = await usersRepo.findOne({
        where: {
          email: user.email,
        },
      });

      if (givenUserAlreadyExists || givenEmailAlreadyExists)
        throw new UserAlreadyExists();

      const hashedPassword = await this.encryptService.encrypt(user.password);

      const preparedUser = usersRepo.create({
        ...user,
        password: hashedPassword,
      });

      const creationUserResponse = await usersRepo.insert(preparedUser);
      const userId = creationUserResponse.identifiers[0];
      const profilesUser = usersRepo.create({ id: userId, ...preparedUser });

      const preparedProfile = new Profile();
      preparedProfile.user = profilesUser;
      preparedProfile.first_name = profile.firstName;
      preparedProfile.last_name = profile.lastName;
      preparedProfile.birth_date = new Date(profile.birthDate);

      const currentChallengeLevel = await this.challengeLevelService.findById(
        1,
      ); // TODO: refatorar atribuição imperativa

      preparedProfile.challengeLevel = currentChallengeLevel;

      const lastUser = await this.userRepository.findOne({
        order: { id: "DESC" },
      });

      const lastProfileGroup = await this.profileService.findByUserWithRelations(
        lastUser,
      );
      let groupForRegister;

      try {
        groupForRegister = await this.groupsService.findById(
          lastProfileGroup.group.id + 1,
        );
      } catch (err) {
        console.log("Erro ao pegar um grupo");
        if (err instanceof GroupsNotExists) {
          const firstGroup = await this.groupsService.selectGroups();
          groupForRegister = await this.groupsService.findById(firstGroup.id);
        } else {
          const firstGroup = await this.groupsService.selectGroups();
          groupForRegister = await this.groupsService.findById(firstGroup.id);
        }
      }

      const profileGroup = groupForRegister;
      preparedProfile.group = profileGroup;

      await profilesRepo.insert(preparedProfile);

      return new ProfileAndUserDataDto(
        preparedUser.username,
        preparedUser.email,
        preparedProfile.first_name,
        preparedProfile.last_name,
      );
    } catch (error) {
      console.log("Error to create profile and user");
      throw error;
    }
  }

  async update(id: number, userChanges: DeepPartial<UserEditDTO>) {
    try {
      const findedUser = await this.userRepository.findById(id);

      if (!findedUser) throw new UserNotExists();

      const checkEmail = await this.userRepository.findBy({
        email: userChanges.email,
      });

      if (checkEmail) throw new UserAlreadyExists();

      const checkUsername = await this.userRepository.findBy({
        username: userChanges.username,
      });

      if (checkUsername) throw new UserAlreadyExists();

      const user = this.userRepository.create({
        id,
        ...userChanges,
      });

      await this.userRepository.updateUser(user);

      return user;
    } catch (error) {
      throw error;
    }
  }
  async changePassword(id: number, newPassword: string) {
    try {
      console.log("Service changePassword: ", id, newPassword)
      const findedUser = await this.userRepository.findById(id);

      if (!findedUser) throw new UserNotExists();

      const hashedPassword = await this.encryptService.encrypt(newPassword);

      const preparedUser = {
        ...findedUser,
        password: hashedPassword,
      };

      await this.userRepository.updateUser(preparedUser);

      return preparedUser;
    } catch (error) {
      throw error;
    }
  }

  async update_password(
    id: number,
    userChanges: DeepPartial<UserEditPasswordDTO>,
  ) {
    try {
      const findedUser = await this.userRepository.findById(id);

      if (!findedUser) throw new UserNotExists();

      const compare = await this.encryptService.compare(
        userChanges.password_old,
        findedUser.password,
      );
      if (!compare) {
        throw new UserPasswordNotMatch();
      }

      const password_hash = await this.encryptService.encrypt(
        userChanges.password_new,
      );

      const user = this.userRepository.create({
        id,
        email: findedUser.email,
        username: findedUser.username,
        password: password_hash,
      });

      await this.userRepository.updateUser(user);

      delete user.password;

      return user;
    } catch (error) {
      throw error;
    }
  }

  async delete(id: number) {
    try {
      const user = await this.userRepository.findById(id);
      if (!user) throw new UserNotExists();

      this.userRepository.destroy(user);
    } catch (error) {
      throw error;
    }
  }
}
