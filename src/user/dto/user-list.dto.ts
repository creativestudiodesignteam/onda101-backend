import { PaginatedList } from "./../paginated-list";
import { User } from "./../user.entity";

export class UserListDto implements PaginatedList {
  users: User[];
  page: number;
  perPage: number;

  constructor(users: User[], page: number, perPage: number) {
    this.users = users;
    this.page = page;
    this.perPage = perPage;
  }

  public getPaginatedData() {
    return {
      pagination: {
        page: this.page,
        perPage: this.perPage,
      },
      users: this.users,
    };
  }
}
