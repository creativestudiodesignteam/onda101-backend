import {IsEmail, IsNotEmpty, IsOptional, IsString} from "class-validator";

import * as moment from "moment";

export class UserRegisterDTo {
  // User
  @IsString()
  username: string;
  @IsEmail()
  email: string;
  @IsString()
  password: string;

  // Profile
  @IsString()
  firstName: string;

  @IsString()
  lastName: string;

  @IsString()
  @IsOptional()
  birthDate: string;

  // Access Code
  @IsString()
  @IsNotEmpty()
  accessCode: string;

  constructor(
    username: string,
    email: string,
    password: string,
    firstName: string,
    lastName: string,
    birthDate: string,
    accessCode: string,
  ) {
    this.username = username;
    this.email = email;
    this.password = password;
    this.firstName = firstName;
    this.lastName = lastName;
    this.birthDate = birthDate ?? moment().toString(); // TODO: Discutir sobre permanencia deste campo na tabela
    this.accessCode = accessCode;
  }
}
