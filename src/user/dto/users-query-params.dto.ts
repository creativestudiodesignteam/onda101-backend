import { IsNumberString, IsOptional } from "class-validator";
export class UserQueryParams {
  @IsNumberString()
  @IsOptional()
  perPage: number;

  @IsNumberString()
  @IsOptional()
  page: number;
}
