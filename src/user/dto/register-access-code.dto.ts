import {IsNotEmpty, IsString} from "class-validator";

export class RegisterAcessCodeDto {
  @IsString()
  @IsNotEmpty()
  accessCode: string;

  constructor(accessCode: string) {
    this.accessCode = accessCode;
  }
}
