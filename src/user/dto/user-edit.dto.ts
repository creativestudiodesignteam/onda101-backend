import { IsOptional, IsString, IsEmail } from "class-validator";

export class UserEditDTO {

    @IsOptional()
    @IsString()
    username: string;

    @IsOptional()
    @IsString()
    @IsEmail()
    email: string;

}
