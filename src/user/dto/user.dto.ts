import { Exclude } from "class-transformer";
import { IsEmail } from "class-validator";
import { Column } from "typeorm";

export class UserDto {
    id: number;
    username: string;
    @IsEmail()
    email: string;
    @Exclude()
    @Column()
    password: string;
    created_at?: Date;
    updated_at?: Date;

    constructor(
        id: number,
        username: string,
        email: string,
        password: string,
        created_at?: Date,
        updated_at?: Date,
    ) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.password = password;
        this.created_at = created_at ?? undefined;
        this.updated_at = updated_at ?? undefined;
    }
}
