import { IsString } from "class-validator";

export class UserEditPasswordDTO {
    @IsString()
    password_old: string;

    @IsString()
    password_new: string;
}
