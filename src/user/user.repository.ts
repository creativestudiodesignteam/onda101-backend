import { User } from "./user.entity";
import { DeepPartial, EntityRepository, Repository } from "typeorm";
import { UserQueryParams } from "./dto/users-query-params.dto";
import { UserDto } from "./dto/user.dto";

@EntityRepository(User)
export class UserRepository extends Repository<User> {

    async findAll(queryParams: UserQueryParams): Promise<User[]> {
        const users = await this.find({

            take: queryParams.perPage,
            skip: queryParams.perPage * (queryParams.page - 1),
            order: { id: "ASC" },
        });

        return users.map(user => {
            delete user.password;
            return user;
        });
    }

    async findById(id: number): Promise<User> {
        const userFind = await this.findOne(id);
        return userFind
    }

    async findBy(paramToSearch) {
        return await this.findOne({ where: paramToSearch });
    }

    async store(userToBeCreated: User) {
        return await this.insert(userToBeCreated);
    }

    async updateUser(user: DeepPartial<UserDto>) {
        return await this.update(user.id, user);
    }

    async destroy(user: User): Promise<void> {
        await this.remove(user);
    }
}
