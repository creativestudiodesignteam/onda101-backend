import {Challenge} from "src/challenge/challenge.entity";
import {Profile} from "src/profile/profile.entity";

export class RatingResponseDto {
  profile: Profile;
  challenge: Challenge;
  rating: number;

  constructor(profile: Profile, challenge: Challenge, rating: number) {
    this.profile = profile;
    this.challenge = challenge;
    this.rating = rating;
  }
}
