export class NoRatingException extends Error {
  constructor(message?: string) {
    super(message ?? "Nenhuma avaliação encontrada para o período informado");
  }
}
