// TODO: transformar em exceção global.
export class ObjectNotFoundException extends Error {
  constructor(message?: string) {
    super(message ?? "Objeto não encontrado");
  }
}
