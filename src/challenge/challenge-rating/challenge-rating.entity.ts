import {Profile} from "src/profile/profile.entity";
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from "typeorm";
import {Challenge} from "../challenge.entity";

@Entity({name: "challenge_ratings"})
export class ChallengeRating {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(
    () => Profile,
    profile => profile.challengeRatings,
  )
  @JoinColumn({name: "profile_id"})
  profile: Profile;

  @ManyToOne(
    () => Challenge,
    challenge => challenge.challengeRatings,
  )
  @JoinColumn({name: "challenge_id"})
  challenge: Challenge;

  @Column()
  rating: number;

  @Column({name: "created_at"})
  createdAt: Date;

  @Column({name: "updated_at"})
  updatedAt: Date;
}
