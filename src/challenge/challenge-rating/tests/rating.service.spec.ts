import {Test, TestingModule} from "@nestjs/testing";
import {ChallengeRatingService} from "../challenge-rating.service";

describe("ChallengeRatingService", () => {
  let service: ChallengeRatingService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ChallengeRatingService],
    }).compile();

    service = module.get<ChallengeRatingService>(ChallengeRatingService);
  });

  it("should be defined", () => {
    expect(service).toBeDefined();
  });
});
