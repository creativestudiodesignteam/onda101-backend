import {Injectable} from "@nestjs/common";
import {Profile} from "src/profile/profile.entity";
import {DeepPartial, FindConditions} from "typeorm";
import {Challenge} from "../challenge.entity";

import {ChallengeRating} from "./challenge-rating.entity";
import {ChallengeRatingRepository} from "./challenge-rating.repository";
import {ObjectNotFoundException} from "./exception/object-not-found-exception";
import * as moment from "moment";
import {NoRatingException} from "./exception/no-rating-exception";

@Injectable()
export class ChallengeRatingService {
  constructor(private repository: ChallengeRatingRepository) {}

  findAll() {
    return this.repository.find();
  }

  findByCryteria(cryteria: FindConditions<ChallengeRating>) {
    return this.repository.find({where: cryteria});
  }

  findBetweenDates(beginDate: Date, endDate: Date, challengeId: number) {
    return this.repository.findBetweenDates(beginDate, endDate, challengeId);
  }

  async getChallengeRattingMonthlyAverage(
    challengeId: number,
    desiredMonth?: number,
  ) {
    const currentMonth = moment().month();
    const monthAnalyzed = desiredMonth ?? currentMonth;

    const beginDate = moment()
      .month(monthAnalyzed)
      .date(1)
      .toDate();

    const endDate = moment()
      .month(monthAnalyzed)
      .toDate();

    const ratings = await this.findBetweenDates(
      beginDate,
      endDate,
      challengeId,
    );

    if (!ratings.length) throw new NoRatingException();

    const total = ratings
      .map(rating => rating.rating)
      .reduce((a, b) => a + b, 0);

    const average = total / ratings.length;
    return average;
  }

  async rateForFirstTimeOrUpdateRating(
    profile: Profile,
    challenge: Challenge,
    ratingValue: number,
  ) {
    const previousEvaluate = await this.repository.findOneByCryteria({
      profile,
      challenge,
    });
    if (!previousEvaluate) {
      await this.store(profile, challenge, ratingValue);
    } else {
      const updatedRating = previousEvaluate;
      updatedRating.rating = ratingValue;
      await this.updateRating(previousEvaluate.id, updatedRating);
    }
  }

  store(profile: Profile, challenge: Challenge, ratingValue: number) {
    const rating = this.repository.create({
      profile,
      challenge,
      rating: ratingValue,
    });
    return this.repository.insert(rating);
  }

  async updateRating(id: number, newRating: DeepPartial<ChallengeRating>) {
    const rating = await this.repository.findOne(id);
    if (!rating) throw new ObjectNotFoundException();

    await this.repository.save(newRating);
  }

  async findByProfileAndChallenge(profile: Profile, challenge: Challenge) {
    const rating = await this.repository.findOneByCryteria({
      profile,
      challenge,
    });
    if (!rating) throw new ObjectNotFoundException();

    return rating;
  }
}
