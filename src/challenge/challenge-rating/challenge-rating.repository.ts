import {EntityRepository, FindConditions, Repository} from "typeorm";
import {ChallengeRating} from "./challenge-rating.entity";

@EntityRepository(ChallengeRating)
export class ChallengeRatingRepository extends Repository<ChallengeRating> {
  findBetweenDates(beginDate: Date, endDate: Date, challengeId) {
    return super
      .createQueryBuilder("rating")
      .where("rating.created_at >= :beginDate", {beginDate})
      .andWhere("rating.created_at <= :endDate", {endDate})
      .andWhere("rating.challenge_id = :challengeId", {challengeId})
      .getMany();
  }

  findOneByCryteria(cryteria: FindConditions<ChallengeRating>) {
    return super.findOne({where: cryteria});
  }
}
