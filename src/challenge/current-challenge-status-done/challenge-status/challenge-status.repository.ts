import {EntityRepository, Repository} from "typeorm";
import {ChallengeStatus} from "./challenge-status.entity";

@EntityRepository(ChallengeStatus)
export class ChallengeStatusRepository extends Repository<ChallengeStatus> {}
