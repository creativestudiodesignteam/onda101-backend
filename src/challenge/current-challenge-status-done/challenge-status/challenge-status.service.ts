import {Injectable} from "@nestjs/common";
import {FindConditions} from "typeorm";
import {ChallengeStatus} from "./challenge-status.entity";
import {ChallengeStatusRepository} from "./challenge-status.repository";

@Injectable()
export class ChallengeStatusService {
  constructor(private repository: ChallengeStatusRepository) {}

  findOneByCryteria(cryteria: FindConditions<ChallengeStatus>) {
    return this.repository.findOne({where: cryteria});
  }
}
