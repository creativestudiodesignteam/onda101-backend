export class SameStatusException extends Error {
  constructor() {
    super("O status atual é igual ao novo status desejado.");
  }
}
