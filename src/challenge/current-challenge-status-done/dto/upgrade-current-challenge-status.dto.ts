import {IsEnum, IsNotEmpty} from "class-validator";
import {AllowedChallengeStatus} from "../challenge-status/models/AllowedChallengeStatus";

export class UpgradeCurrentChallengeStatusDto {
  @IsNotEmpty()
  @IsEnum(AllowedChallengeStatus)
  nextStatus: AllowedChallengeStatus;
}
