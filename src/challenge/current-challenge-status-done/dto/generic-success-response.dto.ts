export class GenericSuccessResponseDto {
  message: string;

  constructor(message: string) {
    this.message = message;
  }
}
