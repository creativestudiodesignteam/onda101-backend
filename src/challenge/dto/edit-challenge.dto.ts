import {IsBoolean, IsInt, IsOptional, IsString} from "class-validator";

export class EditChallengeDto {
  @IsOptional()
  @IsString()
  title: string;

  @IsOptional()
  @IsString()
  imperativePhrase: string;

  @IsOptional()
  @IsString()
  description: string;

  @IsOptional()
  @IsString()
  completionMessage: string;

  @IsOptional()
  @IsInt()
  number: number;

  @IsOptional()
  @IsBoolean()
  isExtra: boolean;
}
