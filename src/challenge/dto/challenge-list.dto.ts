import { PaginatedList } from "./../paginated-list";
import { Challenge } from "./../challenge.entity";

export class ChallengeListDto implements PaginatedList {
  challenges: Challenge[];
  page: number;
  perPage: number;

  constructor(challenges: Challenge[], page: number, perPage: number) {
    this.challenges = challenges;
    this.page = page;
    this.perPage = perPage;
  }

  public getPaginatedData() {
    return {
      pagination: {
        page: this.page,
        perPage: this.perPage,
      },
      challenges: this.challenges,
    };
  }
}
