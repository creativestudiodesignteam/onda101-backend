export class LevelUpProfileChallengeLevelResponseDto {
  challengeLevel: number;
  isFirstTimeFinishingThisChallenge: boolean;

  constructor(
    challengeLevel: number,
    isFirstTimeFinishingThisChallenge: boolean,
  ) {
    this.challengeLevel = challengeLevel;
    this.isFirstTimeFinishingThisChallenge = isFirstTimeFinishingThisChallenge;
  }
}
