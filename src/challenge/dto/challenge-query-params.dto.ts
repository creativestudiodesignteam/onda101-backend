import { IsNumberString, IsOptional } from "class-validator";
export class ChallengeQueryParams {

  @IsNumberString()
  @IsOptional()
  perPage: number;

  @IsNumberString()
  @IsOptional()
  page: number;
  
}
