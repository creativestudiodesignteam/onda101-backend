import {
  IsBoolean,
  IsInt,
  IsNotEmpty,
  IsNumber,
  IsString,
} from "class-validator";

export class CreateChallengeDto {
  @IsNotEmpty()
  @IsString()
  title: string;

  @IsNotEmpty()
  @IsString()
  imperativePhrase: string;

  @IsNotEmpty()
  @IsString()
  description: string;

  @IsNotEmpty()
  @IsString()
  completionMessage: string;

  @IsInt()
  number: number;

  @IsInt()
  score: number;

  @IsBoolean()
  isExtra: boolean;

  @IsNumber()
  challengeLevelId: number;
}
