export class ChallengeDto {
  id: number;
  title: string;
  imperativePhrase: string;
  description: string;
  completionMessage: string;
  number: number;
  score: number;
  isExtra: boolean;
  created_at?: Date;
  updated_at?: Date;

  constructor(
    id: number,
    title: string,
    imperativePhrase: string,
    description: string,
    completionMessage: string,
    number: number,
    score: number,
    isExtra: boolean,
    created_at?: Date,
    updated_at?: Date,
  ) {
    this.id = id;
    this.title = title;
    this.imperativePhrase = imperativePhrase;
    this.description = description;
    this.completionMessage = completionMessage;
    this.number = number;
    this.score = score;
    this.isExtra = isExtra;
    this.created_at = created_at ?? undefined;
    this.updated_at = updated_at ?? undefined;
  }
}
