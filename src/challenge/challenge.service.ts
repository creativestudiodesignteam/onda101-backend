import {CreateChallengeDto} from "./dto/create-challenge.dto";
import {DeepPartial, FindConditions} from "typeorm";

import {
  ChallengeNotExists,
  ChallengeNumberAlreadyExists,
} from "./exception/index";

import {ChallengeRepository} from "./challenge.repository";
import {Injectable, InternalServerErrorException} from "@nestjs/common";
import {ChallengeQueryParams} from "./dto/challenge-query-params.dto";
import {Challenge} from "./challenge.entity";
import {ChallengeLevelService} from "./challenge-level/challenge-level.service";

@Injectable()
export class ChallengeService {
  constructor(
    private challengeRepository: ChallengeRepository,
    private challengeLevelService: ChallengeLevelService,
  ) {}

  async findAll({page = 1, perPage = 10}: ChallengeQueryParams) {
    const allChallenges = await this.challengeRepository.findAll({
      page,
      perPage,
    });

    return allChallenges;
  }

  async findById(id: number): Promise<Challenge> {
    const challenge = await this.challengeRepository.findById(id);
    if (!challenge) throw new ChallengeNotExists();

    return challenge;
  }

  async create(challengeToBeCreated: CreateChallengeDto) {
    try {
      const givenChallengeNumberAlreadyExists = await this.challengeRepository.findBy(
        {
          number: challengeToBeCreated.number,
        },
      );
      if (givenChallengeNumberAlreadyExists)
        throw new ChallengeNumberAlreadyExists();

      const challenge = new Challenge();
      challenge.title = challengeToBeCreated.title;
      challenge.description = challengeToBeCreated.description;
      challenge.number = challengeToBeCreated.number;
      challenge.score = challengeToBeCreated.score;
      challenge.isExtra = challengeToBeCreated.isExtra;

      const challengeLevel = await this.challengeLevelService.findById(
        challengeToBeCreated.challengeLevelId,
      );
      if (!challengeLevel) throw InternalServerErrorException; // TODO: estudar viabilidade de criar um challenge level caso não encontre nenhum
      challenge.level = challengeLevel;

      await this.challengeRepository.store(challenge);
      return challenge;
    } catch (error) {
      throw error;
    }
  }

  async update(id: number, challengeChanges: DeepPartial<Challenge>) {
    const findedChallenge = await this.challengeRepository.findById(id);

    if (!findedChallenge) throw new ChallengeNotExists();

    if (challengeChanges.number) {
      const challengeNumberAlreadyExists = await this.challengeRepository.findBy(
        {number: challengeChanges.number},
      );

      if (challengeNumberAlreadyExists)
        throw new ChallengeNumberAlreadyExists();
    }

    const challenge = this.challengeRepository.create({
      id,
      ...challengeChanges,
    });

    await this.challengeRepository.updateChallenge(challenge);

    return challenge;
  }

  async delete(id: number) {
    const challenge = await this.challengeRepository.findById(id);
    if (!challenge) throw new ChallengeNotExists();

    this.challengeRepository.destroy(challenge);
  }

  findByCryteria(cryteria: FindConditions<Challenge>) {
    return this.challengeRepository.find({where: cryteria});
  }
}
