import {EditChallengeDto} from "./dto/edit-challenge.dto";
import {CreateChallengeDto} from "./dto/create-challenge.dto";
import {CustomSuccessResponse} from "./dto/custom-success-response";
import {
  ChallengeNotExists,
  ChallengeNumberAlreadyExists,
} from "./exception/index";

import {ValidationPipe} from "./../shared/pipes/validation.pipe";
import {ChallengeListDto} from "./dto/challenge-list.dto";
import {ChallengeDto} from "./dto/challenge.dto";
import {ChallengeQueryParams} from "./dto/challenge-query-params.dto";
import {ChallengeService} from "./challenge.service";

import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  InternalServerErrorException,
  NotFoundException,
  Param,
  Post,
  Put,
  Query,
  UseGuards,
  Request,
} from "@nestjs/common";
import {CurrentChallengeStatusService} from "./current-challenge-status/current-challenge-status.service";
import {JwtAuthGuard} from "src/auth/jwt-auth.guard";
import {ProfileService} from "src/profile/profile.service";
import {ChallengeAlreadyDoneException} from "./current-challenge-status/exception/challenge-already-done";
import {AllowedChallengeStatus} from "./current-challenge-status/challenge-status/models/AllowedChallengeStatus";
import {SameStatusException} from "./current-challenge-status/exception/same-status";
import {LevelUpProfileChallengeLevelResponseDto} from "./dto/levelup-profile-challenge-level-response.dto";
import {EvaluateDto} from "./challenge-rating/dto/evaluate.dto";
import {ChallengeRatingService} from "./challenge-rating/challenge-rating.service";
import {ObjectNotFoundException} from "./challenge-rating/exception/object-not-found-exception";
import {RatingResponseDto} from "./challenge-rating/dto/rating-response.dto";
import {ChallengeLevelService} from "./challenge-level/challenge-level.service";
import {ThereIsNoNextLevelException} from "./challenge-level/exception/there-is-no-next-level";
import {NoRatingException} from "./challenge-rating/exception/no-rating-exception";
import * as moment from "moment";

@Controller("challenges")
export class ChallengeController {
  constructor(
    private challengeService: ChallengeService,
    private currentChallengeStatusService: CurrentChallengeStatusService,
    private profilesService: ProfileService,
    private challengeRatingService: ChallengeRatingService,
    private challengeLevelService: ChallengeLevelService,
  ) {
    console.log("send");
  }

  @UseGuards(JwtAuthGuard)
  @Get(":challengeId/monthly-average")
  async monthlyAverage(@Param() param) {
    try {
      const challenge = await this.challengeService.findById(param.challengeId);

      const monthAnalyzed = moment().month(); // janeiro = 0
      const average = await this.challengeRatingService.getChallengeRattingMonthlyAverage(
        challenge.id,
        monthAnalyzed,
      );
      return {
        challenge: challenge.title,
        monthAnalyzed: monthAnalyzed + 1,
        average,
      };
    } catch (error) {
      if (error instanceof ChallengeNotExists)
        throw new BadRequestException(error.message);
      if (error instanceof NoRatingException)
        throw new NotFoundException(error.message);
      throw new InternalServerErrorException();
    }
  }

  @UseGuards(JwtAuthGuard)
  @Get("/next-level-preview")
  @HttpCode(200)
  async getNextChallengesBlocked(@Request() request) {
    try {
      const profile = await this.profilesService.findByUser(request.user.id);
      const nextChallengeLevel = await this.challengeLevelService.getNextLevel(
        profile.challengeLevel.levelNumber,
      );

      const blockedChallenges = await this.challengeService.findByCryteria({
        level: nextChallengeLevel,
      });
      return blockedChallenges;
    } catch (error) {
      if (error instanceof ThereIsNoNextLevelException) {
        throw new BadRequestException(error.message);
      }
      throw new InternalServerErrorException();
    }
  }

  @Get()
  async findAll(
    @Query(new ValidationPipe()) queryParams: ChallengeQueryParams,
  ) {
    try {
      const findedChallenges = await this.challengeService.findAll(queryParams);
      const paginatedList = new ChallengeListDto(
        findedChallenges,
        queryParams.page,
        queryParams.perPage,
      );

      return paginatedList.getPaginatedData();
    } catch (error) {
      throw new InternalServerErrorException({
        message:
          "Alguma coisa deu errado. Tente novamente em alguns instantes... findAll",
      });
    }
  }

  @Get(":id")
  @HttpCode(200)
  async findById(@Param() param): Promise<ChallengeDto> {
    try {
      const challenge = await this.challengeService.findById(param.id);

      return new ChallengeDto(
        challenge.id,
        challenge.title,
        challenge.imperativePhrase,
        challenge.description,
        challenge.completionMessage,
        challenge.number,
        challenge.score,
        challenge.isExtra,
        challenge.created_at,
        challenge.updated_at,
      );
    } catch (error) {
      if (error instanceof ChallengeNotExists)
        throw new NotFoundException(error.message);
      throw new InternalServerErrorException({
        message:
          "Alguma coisa deu errado. Tente novamente em alguns instantes... FindById",
      });
    }
  }

  @Post()
  async create(
    @Body(new ValidationPipe()) challenge: CreateChallengeDto,
  ): Promise<ChallengeDto> {
    try {
      const createdChallenge = await this.challengeService.create(challenge);

      return new ChallengeDto(
        createdChallenge.id,
        createdChallenge.title,
        createdChallenge.imperativePhrase,
        createdChallenge.description,
        createdChallenge.completionMessage,
        createdChallenge.number,
        createdChallenge.score,
        createdChallenge.isExtra,
      );
    } catch (error) {
      console.log("error---> ", error);
      if (error instanceof ChallengeNumberAlreadyExists)
        throw new BadRequestException(error.message);

      throw new InternalServerErrorException({
        message:
          "Alguma coisa deu errado. Tente novamente em alguns instantes... Create",
      });
    }
  }

  @Put(":id")
  async update(
    @Param() param,
    @Body(new ValidationPipe())
    challenge: EditChallengeDto,
  ): Promise<any> {
    try {
      const updatedChallenge = await this.challengeService.update(
        param.id,
        challenge,
      );

      return updatedChallenge;
    } catch (error) {
      if (error instanceof ChallengeNotExists) {
        throw new NotFoundException(error.message);
      }
      if (error instanceof ChallengeNumberAlreadyExists) {
        throw new BadRequestException(error.message);
      }
      throw new InternalServerErrorException({
        message:
          "Alguma coisa deu errado. Tente novamente em alguns instantes... update",
      });
    }
  }

  @Delete(":id")
  @HttpCode(200)
  async delete(@Param() param): Promise<CustomSuccessResponse> {
    try {
      await this.challengeService.delete(param.id);
      return new CustomSuccessResponse("Desafio excluído com sucesso");
    } catch (error) {
      if (error instanceof ChallengeNotExists) {
        throw new NotFoundException(error.message);
      }
      throw new InternalServerErrorException({
        message:
          "Alguma coisa deu errado. Tente novamente em alguns instantes... delete",
      });
    }
  }

  @UseGuards(JwtAuthGuard)
  @Get(":id/finish")
  async finishChallenge(@Param() param, @Request() request) {
    try {
      const profile = await this.profilesService.findByUser(request.user.id);
      const challenge = await this.challengeService.findById(param.id);
      const currentChallengeStatus = await this.currentChallengeStatusService.getByProfileAndChallenge(
        profile,
        challenge,
      );
      const infoAboutConclusion = await this.currentChallengeStatusService.updateChallengeStatus(
        currentChallengeStatus,
        AllowedChallengeStatus.done,
      );

      const profileEarnedNewChallengeLevel =
        infoAboutConclusion.newProfileChallengeLevel;

      const challengeLevelToBeDisplayed = profileEarnedNewChallengeLevel
        ? profileEarnedNewChallengeLevel.levelNumber
        : profile.challengeLevel.levelNumber;

      return new LevelUpProfileChallengeLevelResponseDto(
        challengeLevelToBeDisplayed,
        infoAboutConclusion.isFirstTimeFinishingThisChallenge,
      );
    } catch (error) {
      if (error instanceof ChallengeNotExists) {
        throw new BadRequestException(error.message);
      }
      if (error instanceof ChallengeAlreadyDoneException) {
        throw new BadRequestException(error.message);
      }
      if (error instanceof SameStatusException) {
        throw new BadRequestException(error.message);
      }
      throw new InternalServerErrorException();
    }
  }

  @UseGuards(JwtAuthGuard)
  @Post(":id/evaluate")
  @HttpCode(200)
  async challengeEvaluate(
    @Param() param,
    @Request() request,
    @Body(new ValidationPipe()) evaluate: EvaluateDto,
  ) {
    try {
      const profile = await this.profilesService.findByUser(request.user.id);
      const challenge = await this.challengeService.findById(param.id);

      await this.challengeRatingService.rateForFirstTimeOrUpdateRating(
        profile,
        challenge,
        evaluate.rating,
      );

      return new CustomSuccessResponse("Avaliação realizada com sucesso!");
    } catch (error) {
      if (error instanceof ChallengeNotExists) {
        throw new BadRequestException(error.message);
      }
      if (error instanceof ObjectNotFoundException) {
        throw new NotFoundException(error.message);
      }
      throw new InternalServerErrorException();
    }
  }

  @UseGuards(JwtAuthGuard)
  @Get(":id/rating")
  @HttpCode(200)
  async getChallengeRating(@Param() param, @Request() request) {
    try {
      const profile = await this.profilesService.findByUser(request.user.id);
      const challenge = await this.challengeService.findById(param.id);

      const rating = await this.challengeRatingService.findByProfileAndChallenge(
        profile,
        challenge,
      );
      return new RatingResponseDto(profile, challenge, rating.rating);
    } catch (error) {
      if (error instanceof ChallengeNotExists) {
        return new BadRequestException(error.message);
      }
      if (error instanceof ObjectNotFoundException) {
        return new NotFoundException(error.message);
      }
      return new InternalServerErrorException();
    }
  }
}
