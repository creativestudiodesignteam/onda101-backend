import {ChallengeRepository} from "./challenge.repository";
import {TypeOrmModule} from "@nestjs/typeorm";
import {forwardRef, Module} from "@nestjs/common";
import {ChallengeService} from "./challenge.service";
import {ChallengeController} from "./challenge.controller";
import {MulterModule} from "@nestjs/platform-express";
import {ChallengeAttachmentsService} from "./challenge-attachments/challenge-attachments.service";
import {UploadFilesService} from "src/shared/upload-files/upload-files.service";
import {ChallengeAttachmentsController} from "./challenge-attachments/challenge-attachments.controller";
import {ChallengeAttachmentsRepository} from "./challenge-attachments/challenge-attachments.repository";
import {CompletedChallengeController} from "./completed-challenge/challenge-completed.controller";
import {CompletedChallengeService} from "./completed-challenge/completed-challenge.service";
import {CompletedChallengeRepository} from "./completed-challenge/challenge-completed.repository";
import {ProfileModule} from "src/profile/profile.module";
import {ProfileService} from "src/profile/profile.service";
import {ChallengeLevelService} from "./challenge-level/challenge-level.service";
import {ChallengeLevelRepository} from "./challenge-level/challenge-level.repository";
import {CurrentChallengeStatusController} from "./current-challenge-status/current-challenge-status.controller";
import {CurrentChallengeStatusService} from "./current-challenge-status/current-challenge-status.service";
import {CurrentChallengeStatusRepository} from "./current-challenge-status/current-challenge-status.repository";
import {UserModule} from "src/user/user.module";
import {ChallengeStatusRepository} from "./current-challenge-status/challenge-status/challenge-status.repository";
import {ChallengeStatusService} from "./current-challenge-status/challenge-status/challenge-status.service";
import {RedoneTimesRepository} from "./redone-times/redone-times.repository";
import {RedoneTimesService} from "./redone-times/redone-times.service";
import {ChallengePostController} from "./challenge-post/challenge-post.controller";
import {ChallengePostService} from "./challenge-post/challenge-post.service";
import {ChallengesPostsRepository} from "./challenge-post/challenge-post.repository";
import {ChallengeRatingService} from "./challenge-rating/challenge-rating.service";
import {ChallengeRatingRepository} from "./challenge-rating/challenge-rating.repository";

@Module({
  imports: [
    TypeOrmModule.forFeature([
      ChallengeRepository,
      ChallengeAttachmentsRepository,
      CompletedChallengeRepository,
      ChallengeLevelRepository,
      CurrentChallengeStatusRepository,
      ChallengeStatusRepository,
      RedoneTimesRepository,
      ChallengesPostsRepository,
      ChallengeRatingRepository,
    ]),
    MulterModule.register({
      dest: "./upload",
    }),
    ProfileModule,
    UploadFilesService,
    forwardRef(() => UserModule),
  ],
  providers: [
    ChallengeService,
    ChallengeAttachmentsService,
    UploadFilesService,
    ProfileService,
    CompletedChallengeService,
    ChallengeLevelService,
    CurrentChallengeStatusService,
    ChallengeStatusService,
    RedoneTimesService,
    ChallengePostService,
    ChallengeRatingService,
  ],
  controllers: [
    ChallengeController,
    ChallengeAttachmentsController,
    CompletedChallengeController,
    CurrentChallengeStatusController,
    ChallengePostController,
  ],
  exports: [ChallengeLevelService],
})
export class ChallengeModule {}
