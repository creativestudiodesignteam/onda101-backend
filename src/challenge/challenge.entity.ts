import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";
import {ChallengeLevel} from "./challenge-level/challenge-level.entity";
import {CompletedChallenge} from "./completed-challenge/completed-challenge.entity";
import {CurrentChallengeStatus} from "./current-challenge-status/current-challenge-status.entity";
import {ChallengeRating} from "./challenge-rating/challenge-rating.entity";
import {RedoneTimes} from "./redone-times/redone-times.entity";

@Entity({name: "challenges"})
export class Challenge {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  title: string;

  @Column({name: "imperative_phrase"})
  imperativePhrase: string;

  @Column()
  description: string;

  @Column({name: "completion_message"})
  completionMessage: string;

  @Column()
  number: number;

  @Column()
  score: number;

  @Column()
  isExtra: boolean;

  @OneToMany(
    () => CompletedChallenge,
    completedChallenge => completedChallenge.challenge,
  )
  completedChallenges: CompletedChallenge[];

  @OneToMany(
    () => CurrentChallengeStatus,
    currentChallengeStatus => currentChallengeStatus.challengeInProgress,
  )
  challengesInProgress!: CurrentChallengeStatus[];

  @ManyToOne(
    () => ChallengeLevel,
    challengeLevel => challengeLevel.challenges,
    {eager: true},
  )
  @JoinColumn({name: "challenge_level_id"})
  level: ChallengeLevel;

  @OneToMany(
    () => RedoneTimes,
    redoneTimes => redoneTimes.challenge,
  )
  redoneTimes: RedoneTimes;

  @OneToMany(
    () => ChallengeRating,
    challengeRating => challengeRating,
  )
  challengeRatings: ChallengeRating[];

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;
}
