import {Injectable} from "@nestjs/common";
import {Profile} from "src/profile/profile.entity";
import {Challenge} from "../challenge.entity";
import {RedoneTimesRepository} from "./redone-times.repository";

@Injectable()
export class RedoneTimesService {
  constructor(private repository: RedoneTimesRepository) {}

  async isFirstTimeFinishingThisChallenge(
    challenge: Challenge,
    profile: Profile,
  ) {
    const alreadyDone = await this.repository.find({
      where: {challenge, profile},
    });
    if (!alreadyDone.length) return true;
    return false;
  }

  create(challenge: Challenge, profile: Profile) {
    const redoneTimes = this.repository.create({challenge, profile});
    return this.repository.insert(redoneTimes);
  }
}
