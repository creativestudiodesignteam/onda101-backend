import {EntityRepository, Repository} from "typeorm";
import {RedoneTimes} from "./redone-times.entity";

@EntityRepository(RedoneTimes)
export class RedoneTimesRepository extends Repository<RedoneTimes> {}
