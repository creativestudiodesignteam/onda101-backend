import {Profile} from "src/profile/profile.entity";
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from "typeorm";
import {Challenge} from "../challenge.entity";

@Entity({name: "redone_times"})
export class RedoneTimes {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(
    () => Profile,
    profile => profile.redoneTimes,
  )
  @JoinColumn({name: "profile_id"})
  profile: Profile;

  @ManyToOne(
    () => Challenge,
    challenge => challenge.redoneTimes,
  )
  @JoinColumn({name: "challenge_id"})
  challenge: Challenge;

  @Column({name: "done_at"})
  doneAt: Date;
}
