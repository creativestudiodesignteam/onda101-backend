import { Challenge } from "./challenge.entity";
import { DeepPartial, EntityRepository, Repository } from "typeorm";
import { ChallengeQueryParams } from "./dto/challenge-query-params.dto";

@EntityRepository(Challenge)
export class ChallengeRepository extends Repository<Challenge> {
  async findAll(queryParams: ChallengeQueryParams): Promise<Challenge[]> {
    const challenges = this.find({
      take: queryParams.perPage,
      skip: queryParams.perPage * (queryParams.page - 1),
      order: { number: "ASC" },
    });

    return challenges;
  }

  findById(id: number): Promise<Challenge> {
    return this.findOne(id);
  }

  async findBy(paramToSearch) {
    return this.findOne({ where: paramToSearch });
  }

  store(challengeToBeCreated: Challenge) {
    return this.insert(challengeToBeCreated);
  }

  async updateChallenge(challenge: DeepPartial<Challenge>) {
    const updateResult = await this.update(challenge.id, challenge);
    console.log("atualizado no repo: ", updateResult);
    return;
  }

  destroy(challenge: Challenge) {
    this.remove(challenge);
  }
}
