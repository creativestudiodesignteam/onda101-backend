import { Test, TestingModule } from '@nestjs/testing';
import { ChallengePostController } from './challenge-post.controller';

describe('ChallengePostController', () => {
  let controller: ChallengePostController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ChallengePostController],
    }).compile();

    controller = module.get<ChallengePostController>(ChallengePostController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
