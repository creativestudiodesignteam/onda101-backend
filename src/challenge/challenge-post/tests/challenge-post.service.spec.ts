import { Test, TestingModule } from '@nestjs/testing';
import { ChallengePostService } from './challenge-post.service';

describe('ChallengePostService', () => {
  let service: ChallengePostService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ChallengePostService],
    }).compile();

    service = module.get<ChallengePostService>(ChallengePostService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
