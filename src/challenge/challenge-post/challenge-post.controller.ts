import {
  Controller,
  Delete,
  Get,
  HttpCode,
  InternalServerErrorException,
  NotFoundException,
  Param,
  Post,
  Query,
  Req,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from "@nestjs/common";
import {FileInterceptor} from "@nestjs/platform-express";
import {diskStorage} from "multer";
import {JwtAuthGuard} from "src/auth/jwt-auth.guard";
import {ProfileNotExists} from "src/profile/exception/profile-not-exists";
import {editFileName} from "src/shared/utils/upload-files";
import {ChallengeNotExists} from "../exception";
import {ChallengePostService} from "./challenge-post.service";
import {ChallengePostDto} from "./dtos/challenge-post";
import {ChallengeFileExists} from "./exception/ChallengeFileNameExists";
import {ChallengePostFileNotFound} from "./exception/ChallengePostFileNotFound";
import {ChallengesPostsListDto} from "./dtos/challegen-post-list";
import {ValidationPipe} from "src/shared/pipes/validation.pipe";
import {ChallengePostsQueryParams} from "./dtos/challenge-posts-query-params.dto";
import {CustomSuccessResponse} from "./dtos/custom-success-response";
import {ChallengePostNotExists} from "./exception/ChallengePostsNotExists";
import {FileIsNotExists} from "src/shared/upload-files/exception/FileIsNotExists";

@Controller("challenge-post")
export class ChallengePostController {
  constructor(private challengePostService: ChallengePostService) {}
  @Get()
  @UseGuards(JwtAuthGuard)
  async findByUserId(
    @Req() req,
    @Query(new ValidationPipe())
    {page = 1, perPage = 500}: ChallengePostsQueryParams,
  ) {
    try {
      const findPostByUser = await this.challengePostService.findByUser(
        req.user.id,
        {page, perPage},
      );
      const paginatedList = new ChallengesPostsListDto(
        findPostByUser,
        page,
        perPage,
      );

      return paginatedList.getPaginatedData();
    } catch (error) {
      if (error instanceof ChallengePostFileNotFound)
        throw new NotFoundException(error.message);
      throw new InternalServerErrorException({
        error,
        message:
          "Alguma coisa deu errado. Tente novamente em alguns instantes...",
      });
    }
  }

  @Get(":user_id")
  @UseGuards(JwtAuthGuard)
  async findById(
    @Param() param,
    @Query(new ValidationPipe())
    {page = 1, perPage = 10}: ChallengePostsQueryParams,
  ) {
    try {
      const findPostByUser = await this.challengePostService.findByUser(
        param.user_id,
        {page, perPage},
      );
      const paginatedList = new ChallengesPostsListDto(
        findPostByUser,
        page,
        perPage,
      );

      return paginatedList.getPaginatedData();
    } catch (error) {
      if (error instanceof ChallengePostFileNotFound)
        throw new NotFoundException(error.message);
      throw new InternalServerErrorException({
        error,
        message:
          "Alguma coisa deu errado. Tente novamente em alguns instantes...",
      });
    }
  }

  @Post(":challenge_id")
  @UseGuards(JwtAuthGuard)
  @UseInterceptors(
    FileInterceptor("file", {
      fileFilter: (req, file, cb) => {
        if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
          req.fileErrors = {
            error: true,
          };
          return cb(null, false);
        }
        return cb(null, true);
      },
      storage: diskStorage({
        destination: "./upload",
        filename: editFileName,
      }),
    }),
  )
  async create(@Req() req, @Param() param, @UploadedFile() file) {
    try {
      const challengesPosts = await this.challengePostService.create({
        file: file,
        user_id: req.user.id,
        challenge_id: param.challenge_id,
      });

      return new ChallengePostDto(
        challengesPosts.id,
        challengesPosts.name_file,
        challengesPosts.profile,
        challengesPosts.challenge,
        challengesPosts.created_at,
      );
    } catch (error) {
      if (error instanceof ChallengeNotExists)
        throw new NotFoundException(error.message);
      if (error instanceof FileIsNotExists)
        throw new NotFoundException(error.message);
      if (error instanceof ProfileNotExists)
        throw new NotFoundException(error.message);
      if (error instanceof ChallengeFileExists)
        throw new NotFoundException(error.message);
      if (error instanceof ChallengePostFileNotFound)
        throw new NotFoundException(error.message);
      throw new InternalServerErrorException({
        message:
          "Alguma coisa deu errado. Tente novamente em alguns instantes...",
      });
    }
  }

  @Delete(":id")
  @UseGuards(JwtAuthGuard)
  @HttpCode(200)
  async delete(@Param() param, @Req() req): Promise<CustomSuccessResponse> {
    try {
      await this.challengePostService.delete(req.user.id, param.id);
      return new CustomSuccessResponse("Desafio excluído com sucesso");
    } catch (error) {
      if (error instanceof ChallengePostNotExists) {
        throw new NotFoundException(error.message);
      }
      if (error instanceof FileIsNotExists) {
        throw new NotFoundException(error.message);
      }
      throw new InternalServerErrorException({
        error,
        message:
          "Alguma coisa deu errado. Tente novamente em alguns instantes...",
      });
    }
  }
}
