import {ChallengesPosts} from "./challenge-post.entity";
import {DeepPartial, EntityRepository, Repository} from "typeorm";
import {ChallengePostsQueryParams} from "./dtos/challenge-posts-query-params.dto";
import {Profile} from "src/profile/profile.entity";

@EntityRepository(ChallengesPosts)
export class ChallengesPostsRepository extends Repository<ChallengesPosts> {
  findById(id: number): Promise<ChallengesPosts> {
    return this.findOne(id);
  }

  async findBy(paramToSearch) {
    return this.findOne({where: paramToSearch});
  }
  async findAllByProfile(
    profile: Profile,
    queryParams: ChallengePostsQueryParams,
  ) {
    return this.find({
      where: {profile: profile},
      take: queryParams.perPage,
      skip: queryParams.perPage * (queryParams.page - 1),
    });
  }

  store(challengeToBeCreated: ChallengesPosts) {
    return this.insert(challengeToBeCreated);
  }

  async updateChallenge(challenge: DeepPartial<ChallengesPosts>) {
    await this.update(challenge.id, challenge);
    return;
  }

  destroy(challenge: ChallengesPosts) {
    this.remove(challenge);
  }
}
