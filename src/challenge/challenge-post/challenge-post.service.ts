import {Injectable} from "@nestjs/common";
import {ProfileNotExists} from "src/profile/exception/profile-not-exists";
import {ProfileService} from "src/profile/profile.service";
import {UploadFilesService} from "src/shared/upload-files/upload-files.service";
import {ChallengeService} from "../challenge.service";
import {ChallengeNotExists} from "../exception";
import {ChallengesPosts} from "./challenge-post.entity";
import {ChallengesPostsRepository} from "./challenge-post.repository";
import {ChallengePostDto} from "./dtos/challenge-post";
import {CreateChallengePost} from "./dtos/challenge-post-create";
import {ChallengePostsQueryParams} from "./dtos/challenge-posts-query-params.dto";
import {ChallengeFileExists} from "./exception/ChallengeFileNameExists";
import {ChallengePostFileNotFound} from "./exception/ChallengePostFileNotFound";
import {ChallengePostNotExists} from "./exception/ChallengePostsNotExists";

@Injectable()
export class ChallengePostService {
  constructor(
    private challengesPostsRepository: ChallengesPostsRepository,
    private profileService: ProfileService,
    private challengeService: ChallengeService,
    private uploadFilesService: UploadFilesService,
  ) {}

  async create(
    challengePostToBeCreate: CreateChallengePost,
  ): Promise<ChallengePostDto> {
    if (!challengePostToBeCreate.file) {
      throw new ChallengePostFileNotFound();
    }

    const givenChallengeNumberAlreadyExists = await this.challengesPostsRepository.findBy(
      {
        name_file: challengePostToBeCreate.file.filename,
      },
    );

    if (givenChallengeNumberAlreadyExists) throw new ChallengeFileExists();

    const findProfileByUser = await this.profileService.findByUser(
      challengePostToBeCreate.user_id,
    );

    if (!findProfileByUser) {
      await this.uploadFilesService.delete(
        "./upload",
        challengePostToBeCreate.file.filename,
      );
      throw new ProfileNotExists();
    }

    let findChallenge;

    try {
      findChallenge = await this.challengeService.findById(
        challengePostToBeCreate.challenge_id,
      );
    } catch (error) {
      await this.uploadFilesService.delete(
        "./upload",
        challengePostToBeCreate.file.filename,
      );
      throw new ChallengeNotExists();
    }
    const fileCompressed = await this.uploadFilesService.compress(
      challengePostToBeCreate.file,
      80,
      500,
    );
    
    const challengeAttachment = new ChallengesPosts();
    challengeAttachment.name_file = fileCompressed.filename;
    challengeAttachment.challenge = findChallenge;
    challengeAttachment.profile = findProfileByUser;

    await this.challengesPostsRepository.store(challengeAttachment);

    return challengeAttachment;
  }

  async findByUser(user_id: number, queryParams: ChallengePostsQueryParams) {
    const findProfileByUser = await this.profileService.findByUser(user_id);

    const findChallengesPosts = await this.challengesPostsRepository.findAllByProfile(
      findProfileByUser,
      queryParams,
    );

    return findChallengesPosts;
  }

  async delete(user_id: number, id: number) {
    const findProfileByUser = await this.profileService.findByUser(user_id);

    const findPostsByProfile = await this.challengesPostsRepository.findBy({
      id,
      profile: findProfileByUser,
    });

    if (!findPostsByProfile) throw new ChallengePostNotExists();

    this.uploadFilesService.delete(
      "./upload/posts_image",
      findPostsByProfile.name_file,
    );

    this.challengesPostsRepository.destroy(findPostsByProfile);

    return findPostsByProfile;
  }
}
