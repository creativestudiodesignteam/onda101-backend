import {Profile} from "src/profile/profile.entity";
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from "typeorm";
import {Challenge} from "../challenge.entity";

@Entity({name: "challenges_posts"})
export class ChallengesPosts {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name_file: string;

  @ManyToOne(
    () => Profile,
    profile => profile.completedChallenges,
  )
  @JoinColumn({name: "profile_id"})
  profile: Profile;

  @ManyToOne(
    () => Challenge,
    challenge => challenge.completedChallenges,
  )
  @JoinColumn({name: "challenge_id"})
  challenge: Challenge;

  @CreateDateColumn()
  created_at: Date;
}
