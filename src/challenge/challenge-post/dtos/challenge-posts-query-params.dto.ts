import { IsNumberString, IsOptional } from "class-validator";
export class ChallengePostsQueryParams {

  @IsNumberString()
  @IsOptional()
  perPage: number;

  @IsNumberString()
  @IsOptional()
  page: number;
  
}
