import {IsNotEmpty, IsOptional, IsString, IsObject} from "class-validator";

interface File {
  filename: string;
  originalname: string;
  encoding: string;
  mimetype: string;
  destination: string;
  path: string;
  size: number;
}

export class CreateChallengePost {
  @IsObject()
  file: File;

  @IsNotEmpty()
  user_id: number;

  @IsNotEmpty()
  challenge_id: number;
}
