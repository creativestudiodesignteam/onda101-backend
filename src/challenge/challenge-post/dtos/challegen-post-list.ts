import {PaginatedList} from "./../paginated-list";
import {ChallengesPosts} from "./../challenge-post.entity";
import {ChallengePostsURL} from "./challenge-posts-image-url";

export class ChallengesPostsListDto implements PaginatedList {
  posts: ChallengePostsURL[];
  page: number;
  perPage: number;

  constructor(
    challengePosts: ChallengesPosts[],
    page: number,
    perPage: number,
  ) {
    const challengePostImageTransform = challengePosts.map(result => {
      const transformPostsImageUrl = {
        id: result.id,
        name_file: result.name_file,
        url: `${process.env.SERVER_DOMAIN}/posts/${result.name_file}`,
        created_at: result.created_at,
      };

      return transformPostsImageUrl;
    });

    this.posts = challengePostImageTransform;
    this.page = page;
    this.perPage = perPage;
  }

  public getPaginatedData() {
    return {
      pagination: {
        page: this.page,
        perPage: this.perPage,
      },
      posts: this.posts,
    };
  }
}
