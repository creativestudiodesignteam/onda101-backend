import {Challenge} from "src/challenge/challenge.entity";
import {Profile} from "src/profile/profile.entity";

export class ChallengePostDto {
  id: number;
  name_file: string;
  challenge: Challenge;
  profile: Profile;
  created_at?: Date;

  constructor(
    id: number,
    name_file: string,
    profile: Profile,
    challenge: Challenge,
    created_at?: Date,
  ) {
    this.id = id;
    this.name_file = name_file;
    this.profile = profile;
    this.challenge = challenge;
    this.created_at = created_at ?? undefined;
  }
}
