import {
  IsDateString,
  IsNumber,
  IsString,
} from "class-validator";
export class ChallengePostsURL {
  @IsNumber()
  id: number;
  @IsString()
  name_file: string;
  @IsString()
  url: string;
  @IsDateString()
  created_at: Date;
}
