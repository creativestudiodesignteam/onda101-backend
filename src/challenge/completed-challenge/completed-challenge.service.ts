import {Injectable} from "@nestjs/common";
import {ProfileService} from "src/profile/profile.service";
import {ChallengeService} from "../challenge.service";
import {CompletedChallenge} from "./completed-challenge.entity";
import {CompletedChallengeRepository} from "./challenge-completed.repository";

import {CreateCompletedChallengeDto} from "./dto/completed-challenge-create.dto";
import {CompletedChallengeQueryParams} from "./dto/completed-challenge-query-params.dto";
import {CompletedhallengeNotExists} from "./exception/completed-challenge-not-exists";
@Injectable()
export class CompletedChallengeService {
  constructor(
    private completedChallengeRepository: CompletedChallengeRepository,
    private profileService: ProfileService,
    private challengeService: ChallengeService,
  ) {}

  async findByProfile(
    profile_id,
    {page = 1, perPage = 10}: CompletedChallengeQueryParams,
  ) {
    const profile = await this.profileService.findById(profile_id);

    const allChallenges = await this.completedChallengeRepository.findAll(
      profile,
      {page, perPage},
      ["profile", "challenge"],
    );

    return allChallenges;
  }

  async create(
    profile_id: number,
    challengeCompletedToBeCreate: CreateCompletedChallengeDto,
  ) {
    try {
      /* Verificando a existencia das Foreing Keys  */
      const challenge = await this.challengeService.findById(
        challengeCompletedToBeCreate.challenge_id,
      );
      const profile = await this.profileService.findById(profile_id);

      const checkExists = await this.completedChallengeRepository.findBy({
        challenge: challenge,
        profile: profile,
      });

      const challengeCompleted = new CompletedChallenge();

      if (checkExists) {
        challengeCompleted.id = checkExists.id;
        challengeCompleted.challenge = challenge;
        challengeCompleted.profile = profile;
        challengeCompleted.first_time_done_at = checkExists.first_time_done_at;
        challengeCompleted.last_time_done_at = new Date();
        challengeCompleted.redone_times = checkExists.redone_times + 1;

        await this.completedChallengeRepository.updateCompletedChallenge(
          challengeCompleted,
        );
      } else {
        challengeCompleted.challenge = challenge;
        challengeCompleted.profile = profile;
        challengeCompleted.first_time_done_at = new Date();
        challengeCompleted.last_time_done_at = new Date();
        challengeCompleted.redone_times = 0;

        await this.completedChallengeRepository.store(challengeCompleted);
        await this.profileService.addChallengeScore(challenge.score, profile);
      }

      return challengeCompleted;
    } catch (error) {
      throw error;
    }
  }

  async delete(id: number) {
    const challenge = await this.completedChallengeRepository.findBy({
      id,
    });

    if (!challenge) throw new CompletedhallengeNotExists();

    this.completedChallengeRepository.destroy(challenge);
  }
}
