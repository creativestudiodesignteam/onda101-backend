import {Profile} from "src/profile/profile.entity";
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from "typeorm";
import {Challenge} from "../challenge.entity";

@Entity({name: "completed_challenges"})
export class CompletedChallenge {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  first_time_done_at: Date;

  @Column()
  last_time_done_at: Date;

  @Column()
  redone_times: number;

  @ManyToOne(
    () => Profile,
    profile => profile.completedChallenges,
  )
  @JoinColumn({name: "profile_id"})
  profile: Profile;

  @ManyToOne(
    () => Challenge,
    challenge => challenge.completedChallenges,
  )
  @JoinColumn({name: "challenge_id"})
  challenge: Challenge;
}
