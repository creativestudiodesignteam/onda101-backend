import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  InternalServerErrorException,
  NotFoundException,
  Param,
  Post,
  Query,
  UseGuards,
  Request,
} from "@nestjs/common";
import {ProfileNotExists} from "src/profile/exception/profile-not-exists";
import {ValidationPipe} from "src/shared/pipes/validation.pipe";
import {CustomSuccessResponse} from "./dto/custom-success-response";
import {ChallengeNotExists} from "../exception";
import {CompletedChallengeService} from "./completed-challenge.service";
import {CompletedChallengeListDto} from "./dto/completed-challenge-list.dto";
import {CompletedChallengeDto} from "./dto/completed-challenge.dto";
import {CompletedChallengeQueryParams} from "./dto/completed-challenge-query-params.dto";
import {CompletedhallengeNotExists} from "./exception/completed-challenge-not-exists";
import {JwtAuthGuard} from "src/auth/jwt-auth.guard";
import {ProfileService} from "src/profile/profile.service";

@Controller("completed-challenge")
export class CompletedChallengeController {
  constructor(
    private completedChallengeService: CompletedChallengeService,
    private profileService: ProfileService,
  ) {}

  @Get()
  @UseGuards(JwtAuthGuard)
  async findByProfile(
    @Request() req,
    @Query(new ValidationPipe()) queryParams: CompletedChallengeQueryParams,
  ) {
    try {
      const findProfileByUser = await this.profileService.findByUser(
        req.user.id,
      );

      const findedChallenges = await this.completedChallengeService.findByProfile(
        findProfileByUser.id,
        queryParams,
      );

      const paginatedList = new CompletedChallengeListDto(
        findedChallenges,
        queryParams.page,
        queryParams.perPage,
      );

      return paginatedList.getPaginatedData();
    } catch (error) {
      if (error instanceof ProfileNotExists)
        throw new NotFoundException(error.message);

      if (error instanceof ChallengeNotExists)
        throw new NotFoundException(error.message);

      throw new InternalServerErrorException({
        error,
        message:
          "Alguma coisa deu errado. Tente novamente em alguns instantes...",
      });
    }
  }

  @Post()
  @UseGuards(JwtAuthGuard)
  async create(@Request() req, @Body(new ValidationPipe()) challengeCompleted) {
    try {
      const findProfileByUser = await this.profileService.findByUser(
        req.user.id,
      );
      const challengeCompletedCreated = await this.completedChallengeService.create(
        findProfileByUser.id,
        challengeCompleted,
      );

      return new CompletedChallengeDto(
        challengeCompletedCreated.id,
        challengeCompletedCreated.profile,
        challengeCompletedCreated.challenge,
        challengeCompletedCreated.first_time_done_at,
        challengeCompletedCreated.last_time_done_at,
        challengeCompletedCreated.redone_times,
      );
    } catch (error) {
      if (error instanceof ProfileNotExists)
        throw new NotFoundException(error.message);

      if (error instanceof ChallengeNotExists)
        throw new NotFoundException(error.message);

      throw new InternalServerErrorException({
        message:
          "Alguma coisa deu errado. Tente novamente em alguns instantes...",
      });
    }
  }

  @Delete(":id")
  @UseGuards(JwtAuthGuard)
  @HttpCode(200)
  async delete(@Param() param): Promise<CustomSuccessResponse> {
    try {
      await this.completedChallengeService.delete(param.id);
      return new CustomSuccessResponse("Desafio concluido excluído com sucesso");
    } catch (error) {
      if (error instanceof ChallengeNotExists) {
        throw new NotFoundException(error.message);
      }
      if (error instanceof CompletedhallengeNotExists) {
        throw new NotFoundException(error.message);
      }

      throw new InternalServerErrorException({
        message:
          "Alguma coisa deu errado. Tente novamente em alguns instantes...",
      });
    }
  }
}
