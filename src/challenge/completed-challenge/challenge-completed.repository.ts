import { CompletedChallenge } from "./completed-challenge.entity";
import { DeepPartial, EntityRepository, Repository } from "typeorm";
import { CompletedChallengeQueryParams } from "./dto/completed-challenge-query-params.dto";

@EntityRepository(CompletedChallenge)
export class CompletedChallengeRepository extends Repository<CompletedChallenge> {

    async findAll(profile, queryParams: CompletedChallengeQueryParams, relation: ['profile', 'challenge']): Promise<CompletedChallenge[]> {
        return this.find({
            relations: relation,
            take: queryParams.perPage,
            skip: queryParams.perPage * (queryParams.page - 1),
            where: { profile }
        });
    }

    findWithRelation(id: number, relation: ['profile']) {
        return this.findOne({ relations: relation, where: { id } });
    }

    async findById(id) {
        return this.findOne({ where: id });
    }

    async findBy(paramToSearch) {
        return this.findOne({ where: paramToSearch });
    }

    store(challengeToBeCreated: CompletedChallenge) {
        return this.insert(challengeToBeCreated);
    }

    async updateCompletedChallenge(challengeCompleted: DeepPartial<CompletedChallenge>) {
        return await this.update(challengeCompleted.id, challengeCompleted);
    }

    destroy(challenge: CompletedChallenge) {
        this.remove(challenge);
    }
}
