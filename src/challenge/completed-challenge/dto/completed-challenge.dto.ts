import { Challenge } from "src/challenge/challenge.entity";
import { Profile } from "src/profile/profile.entity";

export class CompletedChallengeDto {
    id: number;
    profile: Profile;
    challenge: Challenge;
    first_time_done_at: Date;
    last_time_done_at: Date;
    redone_times: number;

    constructor(
        id: number,
        profile: Profile,
        challenge: Challenge,
        first_time_done_at: Date,
        last_time_done_at: Date,
        redone_times: number,
    ) {
        this.id = id;
        this.profile = profile;
        this.challenge = challenge;
        this.first_time_done_at = first_time_done_at;
        this.last_time_done_at = last_time_done_at;
        this.redone_times = redone_times;
    }
}
