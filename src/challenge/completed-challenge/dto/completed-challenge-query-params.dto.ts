import { IsNumberString, IsOptional } from "class-validator";
export class CompletedChallengeQueryParams {

  @IsNumberString()
  @IsOptional()
  perPage: number;

  @IsNumberString()
  @IsOptional()
  page: number;
  
}
