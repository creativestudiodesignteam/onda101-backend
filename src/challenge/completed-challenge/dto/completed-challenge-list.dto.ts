import { PaginatedList } from "./../paginated-list";
import { CompletedChallenge } from "./../completed-challenge.entity";

export class CompletedChallengeListDto implements PaginatedList {
  challenges_completed: CompletedChallenge[];
  page: number;
  perPage: number;

  constructor(challenges_completed: CompletedChallenge[], page: number, perPage: number) {
    this.challenges_completed = challenges_completed;
    this.page = page;
    this.perPage = perPage;
  }

  public getPaginatedData() {
    return {
      pagination: {
        page: this.page,
        perPage: this.perPage,
      },
      challenges_completed: this.challenges_completed,
    };
  }
}
