import { IsDate, IsNumber, IsNumberString } from "class-validator";

export class CreateCompletedChallengeDto {

    @IsNumberString()
    challenge_id: number;

    @IsDate()
    first_time_done_at: Date;

    @IsDate()
    last_time_done_at: Date;

    @IsNumber()
    redone_times: number;
}
