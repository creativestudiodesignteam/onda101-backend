import { Test, TestingModule } from '@nestjs/testing';
import { ChallengeCompletedController } from './challenge-completed.controller';

describe('ChallengeCompletedController', () => {
  let controller: ChallengeCompletedController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ChallengeCompletedController],
    }).compile();

    controller = module.get<ChallengeCompletedController>(ChallengeCompletedController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
