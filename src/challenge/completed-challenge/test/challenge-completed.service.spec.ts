import { Test, TestingModule } from '@nestjs/testing';
import { ChallengeCompletedService } from './challenge-completed.service';

describe('ChallengeCompletedService', () => {
  let service: ChallengeCompletedService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ChallengeCompletedService],
    }).compile();

    service = module.get<ChallengeCompletedService>(ChallengeCompletedService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
