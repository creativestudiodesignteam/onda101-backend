import {
  Controller,
  Get,
  UseGuards,
  Request,
  Inject,
  forwardRef,
  InternalServerErrorException,
  Put,
  Body,
  Param,
  HttpCode,
  NotFoundException,
  BadRequestException,
} from "@nestjs/common";
import {JwtAuthGuard} from "src/auth/jwt-auth.guard";
import {ProfileService} from "src/profile/profile.service";
import {ValidationPipe} from "src/shared/pipes/validation.pipe";
import {UserService} from "src/user/user.service";
import {ChallengeService} from "../challenge.service";
import {ChallengeNotExists} from "../exception/challenge-not-exists";
import {CurrentChallengeStatusService} from "./current-challenge-status.service";
import {ResponseListDto} from "./dto/response-list.dto";
import {UpgradeCurrentChallengeStatusDto} from "./dto/upgrade-current-challenge-status.dto";
import {SameStatusException} from "./exception/same-status";

@Controller("current-challenge-status")
export class CurrentChallengeStatusController {
  constructor(
    private service: CurrentChallengeStatusService,
    private profileService: ProfileService,
    @Inject(forwardRef(() => UserService))
    private userService: UserService,
    private challengeService: ChallengeService,
  ) {}

  @Get()
  @UseGuards(JwtAuthGuard)
  async challengeStatusListByCurrentProfilesChallengeLevel(@Request() request) {
    try {
      const user = await this.userService.findById(request.user.id);
      const profile = await this.profileService.findByUser(user);

      const list = await this.service.getProfilesCurrentChallengeLevelStatusList(
        profile,
      );
      return new ResponseListDto(list);
    } catch (error) {
      return InternalServerErrorException;
    }
  }

  @Get("/challenge")
  @UseGuards(JwtAuthGuard)
  async challengeStatusListByCurrentProfilesChallengeLevelDone(
    @Request() request,
  ) {
    try {
      const user = await this.userService.findById(request.user.id);
      const profile = await this.profileService.findByUser(user);

      const list = await this.service.getProfilesCurrentChallengeLevelStatusListDone(
        profile,
      );
      return new ResponseListDto(list);
    } catch (error) {
      return InternalServerErrorException;
    }
  }

  @Put("/challenge/:id/nextStatus")
  @UseGuards(JwtAuthGuard)
  @HttpCode(200)
  async update(
    @Request() request,
    @Param() param,
    @Body(new ValidationPipe())
    incomingChanges: UpgradeCurrentChallengeStatusDto,
  ) {
    try {
      const user = await this.userService.findById(request.user.id);
      const profile = await this.profileService.findByUser(user);

      const challenge = await this.challengeService.findById(param.id);

      // if (!(await this.service.getByProfileAndChallenge(profile, challenge))) {
      //   const statusW = await this.service.challengeStatusService.repository.findOne(
      //     {where: {id: 1}},
      //   );
      //   await this.service.repository.save({
      //     profile,
      //     challengeInProgress: challenge,
      //     status: statusW,
      //   });
      // }

      const currentChallengeStatus = await this.service.getByProfileAndChallenge(
        profile,
        challenge,
      );

      console.log("currentChallengeStatus", currentChallengeStatus);

      await this.service.updateChallengeStatus(
        currentChallengeStatus,
        incomingChanges.nextStatus,
      );

      const updatedRecord = await this.service.getById(
        currentChallengeStatus.id,
      );

      console.log("updatedRecord", updatedRecord);

      return updatedRecord;
    } catch (error) {
      console.log(error);
      if (error instanceof ChallengeNotExists)
        throw new NotFoundException(
          "Desafio não encontrado. Verifique e tente novamente.",
        );
      if (error instanceof SameStatusException)
        throw new BadRequestException(error);
      throw InternalServerErrorException;
    }
  }
}
