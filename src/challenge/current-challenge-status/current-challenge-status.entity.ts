import {Profile} from "src/profile/profile.entity";
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from "typeorm";
import {Challenge} from "../challenge.entity";
import {ChallengeStatus} from "./challenge-status/challenge-status.entity";

@Entity({name: "current_challenge_status"})
export class CurrentChallengeStatus {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(
    () => Challenge,
    challenge => challenge.challengesInProgress,
    {eager: true},
  )
  @JoinColumn({name: "challenge_id"})
  challengeInProgress!: Challenge;

  @ManyToOne(
    () => Profile,
    profile => profile.challengesInProgress,
    {eager: true},
  )
  @JoinColumn({name: "profile_id"})
  profile!: Profile;

  @ManyToOne(
    () => ChallengeStatus,
    challengeStatus => challengeStatus,
    {eager: true},
  )
  @JoinColumn({name: "challenge_status_id"})
  status: ChallengeStatus;

  @Column({name: "created_at"})
  createdAt: Date;

  @Column({name: "updated_at"})
  updatedAt: Date;
}
