import {ChallengeLevel} from "./../challenge-level/challenge-level.entity";
import {Profile} from "src/profile/profile.entity";
import {DeepPartial, EntityRepository, Repository} from "typeorm";
import {Challenge} from "../challenge.entity";
import {CurrentChallengeStatus} from "./current-challenge-status.entity";

@EntityRepository(CurrentChallengeStatus)
export class CurrentChallengeStatusRepository extends Repository<
  CurrentChallengeStatus
> {
  findByProfileAndChallenge(profile: Profile, challenge: Challenge) {
    return super.findOne({where: {profile, challengeInProgress: challenge}});
  }

  findById(id: number) {
    return super.findOne(id);
  }

  updateStatus(id: number, changes: DeepPartial<CurrentChallengeStatus>) {
    return super.update(id, changes);
  }

  statusListFromGivenProfileLevel(profile: Profile) {
    return super
      .createQueryBuilder("progress")
      .innerJoin(
        "progress.profile",
        "profile",
      )
      .leftJoinAndSelect(
        "progress.challengeInProgress",
        "challenge",
      )
      .leftJoinAndSelect("progress.status", "status")
      .where("challenge.challenge_level_id = :desiredLevel", {desiredLevel: profile.challengeLevel.id})
      .andWhere("profile.id = :currentProfileId", {currentProfileId: profile.id})
      .getMany();
  }
}
