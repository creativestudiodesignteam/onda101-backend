import {Column, Entity, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {CurrentChallengeStatus} from "../current-challenge-status.entity";
import {AllowedChallengeStatus} from "./models/AllowedChallengeStatus";

@Entity({name: "challenge_status"})
export class ChallengeStatus {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  description: AllowedChallengeStatus;

  @OneToMany(
    () => CurrentChallengeStatus,
    currentChallengeStatus => currentChallengeStatus.status,
  )
  challengesInProgress: CurrentChallengeStatus;
}
