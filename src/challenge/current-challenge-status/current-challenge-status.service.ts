import {ChallengeLevel} from "./../challenge-level/challenge-level.entity";
import {Injectable} from "@nestjs/common";
import {Profile} from "src/profile/profile.entity";
import {ProfileService} from "src/profile/profile.service";
import {ChallengeLevelService} from "../challenge-level/challenge-level.service";
import {Challenge} from "../challenge.entity";
import {ChallengeService} from "../challenge.service";
import {ChallengeStatusService} from "./challenge-status/challenge-status.service";
import {AllowedChallengeStatus} from "./challenge-status/models/AllowedChallengeStatus";
import {CurrentChallengeStatus} from "./current-challenge-status.entity";
import {CurrentChallengeStatusRepository} from "./current-challenge-status.repository";
import * as moment from "moment";
import {SameStatusException} from "./exception/same-status";
import {RedoneTimesService} from "../redone-times/redone-times.service";
import {ObjectNotFoundException} from "../challenge-rating/exception/object-not-found-exception";

@Injectable()
export class CurrentChallengeStatusService {
  constructor(
    public repository: CurrentChallengeStatusRepository,
    private profileService: ProfileService,
    private challengeService: ChallengeService,
    private challengeLevelService: ChallengeLevelService,
    public challengeStatusService: ChallengeStatusService,
    private redoneTimesService: RedoneTimesService,
  ) {}

  async getById(id: number) {
    const currentChallengeStatus = await this.repository.findOne(id);
    if (!currentChallengeStatus) throw ObjectNotFoundException;
    return currentChallengeStatus;
  }

  getByProfileAndChallenge(
    profile: Profile,
    challenge: Challenge,
  ): Promise<CurrentChallengeStatus> {
    return this.repository.findByProfileAndChallenge(profile, challenge);
  }

  async getProfilesCurrentChallengeLevelStatusList(profile: Profile) {
    try {
      const challengesOfProfilesLevel = await this.challengeService.findByCryteria(
        {level: profile.challengeLevel},
      );

      const challengesOfProfilesLevel2 = await this.challengeService.findByCryteria(
        {isExtra: true},
      );

      const challengesProgressAsPromises: Promise<
        CurrentChallengeStatus
      >[] = [];

      challengesOfProfilesLevel.forEach(challenge =>
        challengesProgressAsPromises.push(
          this.repository.findByProfileAndChallenge(profile, challenge),
        ),
      );

      challengesOfProfilesLevel2.forEach(challenge =>
        challengesProgressAsPromises.push(
          this.repository.findByProfileAndChallenge(profile, challenge),
        ),
      );

      const challengesProgressResolved = await Promise.all(
        challengesProgressAsPromises,
      );

      // TODO: implementar tratamento para quando a quantidade de desafios for maior que a quantidade de progressos criados
      const missingChallengeStatus = this.getMissingChallengeStatus(
        challengesProgressResolved,
      );
      if (missingChallengeStatus === challengesProgressResolved.length) {
        return await this.createAndGetDefaultPendingProgress(profile);
      } else {
        return challengesProgressResolved;
      }
    } catch (error) {
      throw error;
    }
  }

  async getProfilesCurrentChallengeLevelStatusListDone(profile: Profile) {
    try {
      const challengesProgressAsPromises: Promise<
        CurrentChallengeStatus
      >[] = [];

      const level = profile.challengeLevel.levelNumber;

      if (level > 1) {
        for (let index = 1; index < level; index++) {
          profile.challengeLevel.levelNumber = index;
          profile.challengeLevel.id = index;
          profile.challengeLevel.description = `Nível ${index}`;

          const challengesOfProfilesLevel = await this.challengeService.findByCryteria(
            {level: profile.challengeLevel},
          );

          challengesOfProfilesLevel.forEach(challenge =>
            challengesProgressAsPromises.push(
              this.repository.findByProfileAndChallenge(profile, challenge),
            ),
          );
        }
      }

      const challengesProgressResolved = await Promise.all(
        challengesProgressAsPromises,
      );

      // TODO: implementar tratamento para quando a quantidade de desafios for maior que a quantidade de progressos criados
      const missingChallengeStatus = this.getMissingChallengeStatus(
        challengesProgressResolved,
      );
      if (missingChallengeStatus === challengesProgressResolved.length) {
        return await this.createAndGetDefaultPendingProgress(profile);
      } else {
        return challengesProgressResolved;
      }
    } catch (error) {
      throw error;
    }
  }

  private getMissingChallengeStatus(
    profilesCurrentChallengesStatusList: CurrentChallengeStatus[],
  ): number {
    const missingChallengeStatus = profilesCurrentChallengesStatusList.filter(
      currentChallengeStatus => currentChallengeStatus === undefined,
    ).length;
    return missingChallengeStatus;
  }

  private async createAndGetDefaultPendingProgress(profile: Profile) {
    const currentChallengesStatusStored = await this.createDefaultPendingProgressByChallengeLevel(
      profile.challengeLevel.levelNumber,
      profile,
    );

    return Promise.all(
      currentChallengesStatusStored.map(stored =>
        this.repository.findById(stored.identifiers[0].id),
      ),
    );
  }

  private async createDefaultPendingProgressByChallengeLevel(
    challengeLevelNumber: number,
    profile: Profile,
  ) {
    // TODO: implementar tratamento de exceções
    const nextChallengeLevel = await this.challengeLevelService.findOneByCryteria(
      {levelNumber: challengeLevelNumber},
    );

    const nextPendingChallenges = await this.challengeService.findByCryteria({
      level: nextChallengeLevel,
      isExtra: false,
    });

    const nextPendingChallenges2 = await this.challengeService.findByCryteria({
      isExtra: true,
    });

    const defaultChallengeStatus = await this.challengeStatusService.findOneByCryteria(
      {description: AllowedChallengeStatus.incomplete},
    );

    const storedChallengesStatusAsPromises = nextPendingChallenges.map(
      challenge => {
        const pendingChallengeStatus = new CurrentChallengeStatus();
        pendingChallengeStatus.challengeInProgress = challenge;
        pendingChallengeStatus.profile = profile;
        pendingChallengeStatus.status = defaultChallengeStatus;
        return this.repository.insert(pendingChallengeStatus);
      },
    );

    const storedChallengesStatusAsPromises2 = nextPendingChallenges2.map(
      challenge => {
        const pendingChallengeStatus = new CurrentChallengeStatus();
        pendingChallengeStatus.challengeInProgress = challenge;
        pendingChallengeStatus.profile = profile;
        pendingChallengeStatus.status = defaultChallengeStatus;
        return this.repository.insert(pendingChallengeStatus);
      },
    );

    await Promise.all(storedChallengesStatusAsPromises2);
    return await Promise.all(storedChallengesStatusAsPromises);
  }

  async updateChallengeStatus(
    currentChallengeStatus: CurrentChallengeStatus,
    nextStatus: AllowedChallengeStatus,
  ) {
    if (
      currentChallengeStatus.status.description === nextStatus &&
      nextStatus !== AllowedChallengeStatus.done
    )
      throw new SameStatusException();

    const newStatus = await this.challengeStatusService.findOneByCryteria({
      description: nextStatus,
    });

    const nextCurrentChallengeStatus = currentChallengeStatus;
    nextCurrentChallengeStatus.status = newStatus;
    nextCurrentChallengeStatus.updatedAt = moment().toDate();
    await this.repository.save(nextCurrentChallengeStatus);

    if (nextStatus === AllowedChallengeStatus.done) {
      const isFirstTimeFinishingThisChallenge = await this.redoneTimesService.isFirstTimeFinishingThisChallenge(
        currentChallengeStatus.challengeInProgress,
        currentChallengeStatus.profile,
      );
      let newProfileChallengeLevel: ChallengeLevel | null = null;
      if (isFirstTimeFinishingThisChallenge) {
        await this.profileService.addChallengeScore(
          currentChallengeStatus.challengeInProgress.score,
          currentChallengeStatus.profile,
        );

        const didProfileLeveUp = await this.didProfileLevelUp(
          currentChallengeStatus.profile,
        );
        if (didProfileLeveUp) {
          newProfileChallengeLevel = await this.challengeLevelService.findOneByCryteria(
            {
              levelNumber:
                currentChallengeStatus.profile.challengeLevel.levelNumber + 1,
            },
          );
          const profileLeveledUp = currentChallengeStatus.profile;
          profileLeveledUp.challengeLevel = newProfileChallengeLevel;
          await this.profileService.updateKnownProfile(profileLeveledUp);

          await this.createDefaultPendingProgressByChallengeLevel(
            newProfileChallengeLevel.levelNumber,
            profileLeveledUp,
          );
        }
      }

      await this.redoneTimesService.create(
        currentChallengeStatus.challengeInProgress,
        currentChallengeStatus.profile,
      );

      return {
        newProfileChallengeLevel: newProfileChallengeLevel ?? null,
        isFirstTimeFinishingThisChallenge,
      };
    }
  }

  async didProfileLevelUp(profile: Profile) {
    const profilesChallengesStatusList = await this.repository.statusListFromGivenProfileLevel(
      profile,
    );

    const completedChallengesInCurrentLevel = profilesChallengesStatusList.filter(
      progress => progress.status.description === AllowedChallengeStatus.done,
    );
    if (
      completedChallengesInCurrentLevel.length ===
      profilesChallengesStatusList.length
    ) {
      return true;
    } else {
      return false;
    }
  }
}
