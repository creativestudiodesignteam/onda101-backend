import {CurrentChallengeStatus} from "../current-challenge-status.entity";

export class ResponseListDto {
  currentChallengesStatus: CurrentChallengeStatus[];

  constructor(currentChallengesStatus: CurrentChallengeStatus[]) {
    this.currentChallengesStatus = currentChallengesStatus;
  }
}
