import {IsNotEmpty, IsNumber} from "class-validator";
import {ChallengeStatus} from "../challenge-status/challenge-status.entity";

export class CurrentChallengeStatusUpdateDto {
  // TODO: refatorar DTO já que profile é obtido a partir do token do solicitante
  @IsNotEmpty()
  @IsNumber()
  challengeId: number;

  @IsNotEmpty()
  @IsNumber()
  profileId: number;

  @IsNotEmpty()
  status: ChallengeStatus;
}
