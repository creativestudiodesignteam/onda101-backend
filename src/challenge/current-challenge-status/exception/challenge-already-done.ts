export class ChallengeAlreadyDoneException extends Error {
  constructor() {
    super("Não é possível finalizar um desafafio já concluído.");
  }
}
