export class ChallengeNotExists extends Error {
  readonly message = "Desafio inexistente";
}
