export class ChallengeNumberAlreadyExists extends Error {
  readonly message = "Número do desafio já existe";
}
