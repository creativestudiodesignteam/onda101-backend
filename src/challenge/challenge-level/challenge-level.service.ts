import {Injectable} from "@nestjs/common";
import {FindConditions} from "typeorm";
import {ChallengeLevel} from "./challenge-level.entity";
import {ChallengeLevelRepository} from "./challenge-level.repository";
import {ThereIsNoNextLevelException} from "./exception/there-is-no-next-level";

@Injectable()
export class ChallengeLevelService {
  constructor(private repository: ChallengeLevelRepository) {}

  findById(id: number) {
    return this.repository.findOne(id);
  }

  findByCryteria(cryteria: FindConditions<ChallengeLevel>) {
    return this.repository.find({where: cryteria});
  }

  findOneByCryteria(cryteria: FindConditions<ChallengeLevel>) {
    return this.repository.findOne({where: cryteria});
  }

  getNextLevel(currentLevelNumber: number) {
    const invalidNumber = currentLevelNumber > 100;
    if (invalidNumber) throw new ThereIsNoNextLevelException();
    return this.repository.findOne({
      where: {levelNumber: currentLevelNumber + 1},
    });
  }
}
