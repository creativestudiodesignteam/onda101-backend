import {Profile} from "src/profile/profile.entity";
import {Column, Entity, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {Challenge} from "../challenge.entity";

@Entity({name: "challenge_levels"})
export class ChallengeLevel {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  description: string;

  @Column({name: "level_number"})
  levelNumber: number;

  @OneToMany(
    () => Profile,
    profile => profile.challengeLevel,
  )
  profiles: Profile[];

  @OneToMany(
    () => Challenge,
    challenge => challenge.level,
  )
  challenges: Challenge[];
}
