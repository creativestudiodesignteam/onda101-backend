export class ThereIsNoNextLevelException extends Error {
  constructor() {
    super("Você já está no último nível de desafios");
  }
}
