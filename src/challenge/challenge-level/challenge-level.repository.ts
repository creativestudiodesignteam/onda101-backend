import {EntityRepository, Repository} from "typeorm";
import {ChallengeLevel} from "./challenge-level.entity";

@EntityRepository(ChallengeLevel)
export class ChallengeLevelRepository extends Repository<ChallengeLevel> {}
