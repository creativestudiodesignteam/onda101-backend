import { ChallengeAttachments } from "./challenge-attachments.entity";
import { DeepPartial, EntityRepository, Repository } from "typeorm";

@EntityRepository(ChallengeAttachments)
export class ChallengeAttachmentsRepository extends Repository<ChallengeAttachments> {

  findById(id: number): Promise<ChallengeAttachments> {
    return this.findOne(id);
  }

  async findBy(paramToSearch) {
    return this.findOne({ where: paramToSearch });
  }

  store(challengeToBeCreated: ChallengeAttachments) {
    return this.insert(challengeToBeCreated);
  }

  async updateChallenge(challenge: DeepPartial<ChallengeAttachments>) {
    await this.update(challenge.id, challenge);
    return;
  }

  destroy(challenge: ChallengeAttachments) {
    this.remove(challenge);
  }
}
