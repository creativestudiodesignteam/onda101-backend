import {Injectable} from "@nestjs/common";
import {ChallengeAttachments} from "./challenge-attachments.entity";
import {ChallengeAttachmentsRepository} from "./challenge-attachments.repository";
import {ChallengeAttachmentsNotExists} from "./exception/challenge-attachment-not-exists";
import {ChallengeAttachmentNotSendFile} from "./exception/challenge-attachment-not-send-file";
import {ChallengeAttachmentsAlreadyExists} from "./exception/challenge-attachments-already-exists";
import {UploadFilesService} from "src/shared/upload-files/upload-files.service";
import {DeepPartial} from "typeorm";
import {ChallengeNotExists} from "../exception";
import {ChallengeRepository} from "../challenge.repository";
import {ObjectNotFoundException} from "../challenge-rating/exception/object-not-found-exception";
@Injectable()
export class ChallengeAttachmentsService {
  constructor(
    public challengeAttachmentsRepository: ChallengeAttachmentsRepository,
    public challengeRepository: ChallengeRepository,
    private uploadFilesService: UploadFilesService,
  ) {}

  async create(challengeAttachmentToBeCreated): Promise<ChallengeAttachments> {
    try {
      if (!challengeAttachmentToBeCreated.file)
        throw new ChallengeAttachmentNotSendFile();

      const givenChallengeNumberAlreadyExists = await this.challengeAttachmentsRepository.findBy(
        {
          value: challengeAttachmentToBeCreated.file.filename,
        },
      );

      if (givenChallengeNumberAlreadyExists)
        throw new ChallengeAttachmentsAlreadyExists();

      const findedChallenge = await this.challengeRepository.findById(
        challengeAttachmentToBeCreated.challenge_id,
      );

      if (!findedChallenge) {
        await this.uploadFilesService.delete(
          "./upload",
          challengeAttachmentToBeCreated.file.filename,
        );
        throw new ChallengeNotExists();
      }

      const challengeAttachment = new ChallengeAttachments();
      challengeAttachment.value = challengeAttachmentToBeCreated.file.filename;
      challengeAttachment.description =
        challengeAttachmentToBeCreated.description;
      challengeAttachment.challenge_id =
        challengeAttachmentToBeCreated.challenge_id;

      await this.challengeAttachmentsRepository.store(challengeAttachment);

      return challengeAttachment;
    } catch (error) {
      throw error;
    }
  }

  async findByValue(value: string): Promise<ChallengeAttachments> {
    const challengeAttachment = await this.challengeAttachmentsRepository.findBy(
      {
        value,
      },
    );

    if (!challengeAttachment) throw new ChallengeAttachmentsNotExists();

    return challengeAttachment;
  }

  async findById(id: number): Promise<ChallengeAttachments> {
    const challengeAttachment = await this.challengeAttachmentsRepository.findById(
      id,
    );

    if (!challengeAttachment) throw new ChallengeAttachmentsNotExists();

    return challengeAttachment;
  }

  async pickUpFirstChallengeImage(challengeId: number) {
    try {
      const challengeImages = await this.challengeAttachmentsRepository.find({
        where: {challenge_id: challengeId},
      });
      if (!challengeImages || !challengeImages.length) {
        throw new ObjectNotFoundException();
      }
      return challengeImages[0];
    } catch (error) {
      throw error;
    }
  }

  async update(
    id: number,
    file,
    challengeAttachmentToBeCreated: DeepPartial<ChallengeAttachments>,
  ) {
    if (!file) throw new ChallengeAttachmentNotSendFile();

    const findedChallengeAttachment = await this.challengeAttachmentsRepository.findById(
      id,
    );

    if (!findedChallengeAttachment) throw new ChallengeAttachmentsNotExists();

    const findedChallenge = await this.challengeRepository.findById(
      challengeAttachmentToBeCreated.challenge_id,
    );

    if (!findedChallenge) {
      await this.uploadFilesService.delete("./upload", file.filename);
      throw new ChallengeNotExists();
    }

    if (file.filename && file.filename !== findedChallengeAttachment.value)
      await this.uploadFilesService.delete(
        "./upload",
        findedChallengeAttachment.value,
      );

    challengeAttachmentToBeCreated.value = file.filename;
    const challengeAttachment = this.challengeAttachmentsRepository.create({
      id,
      ...challengeAttachmentToBeCreated,
    });

    await this.challengeAttachmentsRepository.updateChallenge(
      challengeAttachment,
    );

    return challengeAttachment;
  }

  async delete(id: number) {
    const challengeAttachment = await this.challengeAttachmentsRepository.findById(
      id,
    );

    if (!challengeAttachment) throw new ChallengeAttachmentsNotExists();

    this.uploadFilesService.delete("./upload", challengeAttachment.value);

    this.challengeAttachmentsRepository.destroy(challengeAttachment);
  }
}
