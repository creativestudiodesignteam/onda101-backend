import { Test, TestingModule } from '@nestjs/testing';
import { ChallengeAttachmentsController } from '../challenge-attachments.controller';

describe('ChallengeAttachmentsController', () => {
  let controller: ChallengeAttachmentsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ChallengeAttachmentsController],
    }).compile();

    controller = module.get<ChallengeAttachmentsController>(ChallengeAttachmentsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
