import { Test, TestingModule } from '@nestjs/testing';
import { ChallengeAttachmentsService } from './challenge-attachments.service';

describe('ChallengeAttachmentsService', () => {
  let service: ChallengeAttachmentsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ChallengeAttachmentsService],
    }).compile();

    service = module.get<ChallengeAttachmentsService>(ChallengeAttachmentsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
