import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  InternalServerErrorException,
  NotFoundException,
  Param,
  Post,
  Put,
  Query,
  Req,
  Res,
  UploadedFile,
  UseInterceptors,
} from "@nestjs/common";
import {FileInterceptor} from "@nestjs/platform-express";
import {diskStorage} from "multer";
import {editFileName} from "src/shared/utils/upload-files";
import {ChallengeAttachmentsService} from "./challenge-attachments.service";
import {ValidationPipe} from "../../shared/pipes/validation.pipe";
import {ChallengeAttachmentFile} from "./dto/challenge-attachment-file";
import {CreateChallengeAttachment} from "./dto/challenge-attachment-create";
import {ChallengeAttachmentNotSendFile} from "./exception/challenge-attachment-not-send-file";
import {ChallengeAttachmentDto} from "./dto/challenge-attachment";
import {ChallengeAttachmentsNotExists} from "./exception/challenge-attachment-not-exists";
import {ChallengeAttachmentSearch} from "./dto/challenge-attachment-search";
import {CustomSuccessResponse} from "./dto/custom-success-response";
import {FileIsNotExists} from "src/shared/upload-files/exception/FileIsNotExists";
import {EditChallengeAttachment} from "./dto/challenge-attachment-edit";
import {ChallengeNotExists} from "../exception";
import {of} from "rxjs";
import {join} from "path";
import {ObjectNotFoundException} from "../challenge-rating/exception/object-not-found-exception";
import {ChallengeService} from "../challenge.service";

@Controller("challenge-attachments")
export class ChallengeAttachmentsController {
  constructor(
    private challengeAttachmentsService: ChallengeAttachmentsService,
    private challengeService: ChallengeService,
  ) {}

  // Isso aí vai registrar todas as imagens no bacno
  @Get("/migration")
  async migration() {
    // const challenges = await this.challengeAttachmentsService.challengeRepository.find();
    // const zeroPad = (num, places) => String(num).padStart(places, "0");
    // challenges.map(async ch => {
    //   await this.challengeAttachmentsService.challengeAttachmentsRepository.save(
    //     {
    //       challenge_id: ch.id,
    //       description: `${ch.title}`,
    //       value: `${zeroPad(ch.number, 3)}.png`,
    //     },
    //   );
    // });
  }

  @Post()
  @UseInterceptors(
    FileInterceptor("file", {
      fileFilter: (req, file, cb) => {
        if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
          req.fileErrors = {
            error: true,
          };
          return cb(null, false);
        }
        return cb(null, true);
      },
      storage: diskStorage({
        destination: "./upload",
        filename: editFileName,
      }),
    }),
  )
  async create(
    @Req() req,
    @UploadedFile() file: ChallengeAttachmentFile,
    @Body(new ValidationPipe()) challengeAttachments: CreateChallengeAttachment,
  ) {
    try {
      const challengeAttachment = await this.challengeAttachmentsService.create(
        {
          description: challengeAttachments.description,
          challenge_id: challengeAttachments.challenge_id,
          file: file,
        },
      );
      return new ChallengeAttachmentDto(
        challengeAttachment.id,
        challengeAttachment.value,
        challengeAttachment.description,
        challengeAttachment.challenge_id,
        challengeAttachment.created_at,
        challengeAttachment.updated_at,
      );
    } catch (error) {
      if (error instanceof ChallengeAttachmentNotSendFile)
        throw new BadRequestException(error.message);
      if (error instanceof ChallengeNotExists)
        throw new NotFoundException(error.message);
      throw new InternalServerErrorException({
        message:
          "Alguma coisa deu errado. Tente novamente em alguns instantes...",
      });
    }
  }

  @Get("challenge/:id")
  @HttpCode(200)
  async pickUpChallengeImage(@Param() param, @Res() res) {
    console.log("request send get image app");
    try {
      await this.challengeService.findById(param.id);
      const firstImage = await this.challengeAttachmentsService.pickUpFirstChallengeImage(
        param.id,
      );
      console.log(firstImage);
      return of(
        res.sendFile(join(process.cwd(), `upload/${firstImage.value}`)),
      );
    } catch (error) {
      console.log(error);
      if (error instanceof ObjectNotFoundException)
        throw new NotFoundException(
          "Nenhuma imagem encontrada para o desafio solicitado",
        );
      if (error instanceof ChallengeNotExists)
        throw new NotFoundException(error.message);
      throw new InternalServerErrorException();
    }
  }

  @Get(":id")
  @HttpCode(200)
  async findById(@Param() param): Promise<ChallengeAttachmentDto> {
    try {
      const challengeAttachment = await this.challengeAttachmentsService.findById(
        param.id,
      );

      return new ChallengeAttachmentDto(
        challengeAttachment.id,
        challengeAttachment.value,
        challengeAttachment.description,
        challengeAttachment.challenge_id,
        challengeAttachment.created_at,
        challengeAttachment.updated_at,
      );
    } catch (error) {
      if (error instanceof ChallengeAttachmentsNotExists)
        throw new NotFoundException(error.message);
      throw new InternalServerErrorException({
        message:
          "Alguma coisa deu errado. Tente novamente em alguns instantes...",
      });
    }
  }

  @Get()
  @HttpCode(200)
  async findByValue(
    @Query() query: ChallengeAttachmentSearch,
  ): Promise<ChallengeAttachmentDto> {
    try {
      const challengeAttachmentFinded = await this.challengeAttachmentsService.findByValue(
        query.value,
      );

      return new ChallengeAttachmentDto(
        challengeAttachmentFinded.id,
        challengeAttachmentFinded.value,
        challengeAttachmentFinded.description,
        challengeAttachmentFinded.challenge_id,
        challengeAttachmentFinded.created_at,
        challengeAttachmentFinded.updated_at,
      );
    } catch (error) {
      if (error instanceof ChallengeAttachmentsNotExists)
        throw new NotFoundException(error.message);
      throw new InternalServerErrorException({
        message:
          "Alguma coisa deu errado. Tente novamente em alguns instantes...",
      });
    }
  }

  @Put(":id")
  @UseInterceptors(
    FileInterceptor("file", {
      fileFilter: (req, file, cb) => {
        if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
          req.fileErrors = {
            error: true,
          };
          return cb(null, false);
        }
        return cb(null, true);
      },
      storage: diskStorage({
        destination: "./upload",
        filename: editFileName,
      }),
    }),
  )
  async update(
    @Param() param,
    @Req() req,
    @UploadedFile() file: ChallengeAttachmentFile,
    @Body(new ValidationPipe()) challengeAttachments: EditChallengeAttachment,
  ): Promise<any> {
    try {
      const data = {
        file: file,
        description: challengeAttachments.description ?? null,
        challenge_id: challengeAttachments.challenge_id,
      };
      const updatedChallenge = await this.challengeAttachmentsService.update(
        param.id,
        file,
        data,
      );

      return new ChallengeAttachmentDto(
        updatedChallenge.id,
        updatedChallenge.value,
        updatedChallenge.description,
        updatedChallenge.challenge_id,
        updatedChallenge.created_at,
        updatedChallenge.updated_at,
      );
    } catch (error) {
      if (error instanceof ChallengeAttachmentsNotExists)
        throw new NotFoundException(error.message);

      if (error instanceof ChallengeNotExists)
        throw new NotFoundException(error.message);

      if (error instanceof ChallengeAttachmentNotSendFile) {
        throw new BadRequestException(error.message);
      }
      throw new InternalServerErrorException({
        error,
        message:
          "Alguma coisa deu errado. Tente novamente em alguns instantes...",
      });
    }
  }

  @Delete(":id")
  @HttpCode(200)
  async delete(@Param() param): Promise<CustomSuccessResponse> {
    try {
      await this.challengeAttachmentsService.delete(param.id);
      return new CustomSuccessResponse("Desafio excluído com sucesso");
    } catch (error) {
      if (error instanceof ChallengeAttachmentsNotExists) {
        throw new NotFoundException(error.message);
      }
      if (error instanceof FileIsNotExists) {
        throw new NotFoundException(error.message);
      }

      throw new InternalServerErrorException({
        error,
        message:
          "Alguma coisa deu errado. Tente novamente em alguns instantes...",
      });
    }
  }
}
