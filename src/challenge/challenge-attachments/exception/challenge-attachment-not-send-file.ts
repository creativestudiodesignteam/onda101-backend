export class ChallengeAttachmentNotSendFile extends Error {
    readonly message = "Arquivo inválido, verifique se o arquivo segue os padrões estabelecidos";
  }
  