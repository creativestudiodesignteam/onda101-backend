export class ChallengeAttachmentsAlreadyExists extends Error {
    readonly message = "Anexos de desafios já existem";
  }
  