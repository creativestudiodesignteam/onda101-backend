import { IsNotEmpty, IsOptional, IsString } from "class-validator";

export class CreateChallengeAttachment {
  @IsOptional()
  @IsString()
  description: string;

  @IsNotEmpty()
  challenge_id: number
}
