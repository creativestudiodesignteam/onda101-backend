import { IsNotEmpty, IsString } from "class-validator";

export class ChallengeAttachmentFile {

    @IsString()
    filename: string;
    
    @IsString()
    originalname: string;
}
