export class ChallengeAttachmentDto {
    id: number;
    value: string;
    description?: string;
    challenge_id: number;
    created_at?: Date;
    updated_at?: Date;

    constructor(
        id: number,
        value: string,
        description?: string,
        challenge_id?: number,
        created_at?: Date,
        updated_at?: Date,
    ) {
        this.id = id;
        this.value = value;
        this.description = description ?? null;
        this.challenge_id = challenge_id;
        
        this.created_at = created_at ?? undefined;
        this.updated_at = updated_at ?? undefined;
    }
}
