import { IsNotEmpty, IsOptional, IsString } from "class-validator";

export class EditChallengeAttachment {
  @IsOptional()
  @IsString()
  description: string;

  @IsOptional()
  @IsNotEmpty()
  challenge_id: number
}
