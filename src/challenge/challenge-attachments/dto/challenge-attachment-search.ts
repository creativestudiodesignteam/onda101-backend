import { IsString } from "class-validator";

export class ChallengeAttachmentSearch {
    @IsString()
    value: string;
}
