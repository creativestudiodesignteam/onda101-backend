import {Injectable} from "@nestjs/common";
import {join} from "path";
import {existsSync} from "fs";
import {FileNotFound} from "./exception/file-not-found";
import {Connection, DeepPartial} from "typeorm";
import {User} from "./user/user.entity";
import {Profile} from "./profile/profile.entity";

@Injectable()
export class AppService {
  constructor(private connection: Connection) {}

  getHello(): string {
    return "Hello World!";
  }

  async getImage(filename, path): Promise<string> {
    const filePath = join(path, filename);
    const checkFileExists = existsSync(filePath);

    if (!checkFileExists) {
      throw new FileNotFound();
    }

    return filename;
  }

  async profileAndUserDataUpdate(
    userId: number,
    profileChanges: DeepPartial<Profile>,
    userChanges: DeepPartial<User>,
  ) {
    const queryRunner = this.connection.createQueryRunner();

    await queryRunner.connect();
    await queryRunner.startTransaction();
    try {
      const user = await queryRunner.manager.findOne(User, userId);
      const profile = await queryRunner.manager.findOne(Profile, {
        where: {user},
      });

      const partialProfile = queryRunner.manager.create(Profile, {
        id: profile.id,
        ...profileChanges,
      });

      await queryRunner.manager.update(
        Profile,
        {id: profile.id},
        partialProfile,
      );

      const partialuser = queryRunner.manager.create(User, {
        id: userId,
        ...userChanges,
      });
      await queryRunner.manager.update(User, {id: userId}, partialuser);

      await queryRunner.commitTransaction();
    } catch (error) {
      await queryRunner.rollbackTransaction();
      throw error;
    } finally {
      await queryRunner.release();
    }
  }
}
