import {ShopWindowWithLink} from "../model/shopWindowWithLink";
import {Settings} from "../settings/settings.entity";
import {PaginatedList} from "./../paginated-list";
import {ShopWindow} from "./../shop-window.entity";
import {ShopWindowLinks} from "./shop-window-links";

export class ShopWindowListDto implements PaginatedList {
  products: ShopWindowLinks[];
  page: number;
  perPage: number;

  constructor(
    products: ShopWindow[],
    page: number,
    perPage: number,
    installment?: Settings,
  ) {
    const ShopWindowLinks = products.map(result => {
      const transformWithLinks: ShopWindowWithLink = {
        id: result.product_id,
        name: result.name,
        price: result.price,
        url_image: `${process.env.URL_ECOMMERCE}/image/${result.image}`,
        url_product: `${process.env.URL_ECOMMERCE}/index.php?route=product/product&product_id=${result.product_id}`,
      };
      if (installment) {
        transformWithLinks.installment = installment.value;
      }

      return transformWithLinks;
    });

    this.products = ShopWindowLinks;
    this.page = page;
    this.perPage = perPage;
  }

  public getPaginatedData() {
    return {
      pagination: {
        page: this.page,
        perPage: this.perPage,
      },
      products: this.products,
    };
  }
}
