import {IsDateString, IsNumber, IsString} from "class-validator";
export class ShopWindowLinks {
  @IsNumber()
  id: number;
  @IsString()
  name: string;
  @IsString()
  price: number;
  @IsString()
  url_image: string;
  @IsString()
  url_product: string;
}
