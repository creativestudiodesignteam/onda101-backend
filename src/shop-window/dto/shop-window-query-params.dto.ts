import { IsNumberString, IsOptional } from "class-validator";
export class ShopWindowQueryParams {

  @IsNumberString()
  @IsOptional()
  perPage: number;

  @IsNumberString()
  @IsOptional()
  page: number;
  
}
