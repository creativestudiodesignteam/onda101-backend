import {Module} from "@nestjs/common";
import {TypeOrmModule} from "@nestjs/typeorm";
import {ShopWindowRepository} from "./shop-window.repository";
import {ShopWindowController} from "./shop-window.controller";
import {ShopWindowService} from "./shop-window.service";
import {SettingsService} from "./settings/settings.service";
import {SettingsRepository} from "./settings/settings.repository";

@Module({
  imports: [
    TypeOrmModule.forFeature(
      [ShopWindowRepository, SettingsRepository],
      "ecommerceConnection",
    ),
  ],
  controllers: [ShopWindowController],
  providers: [ShopWindowService, SettingsService],
  exports: [ShopWindowService],
})
export class ShopWindowModule {}
