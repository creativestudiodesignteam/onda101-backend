import {EntityRepository, Repository} from "typeorm";
import {ShopWindowQueryParams} from "./dto/shop-window-query-params.dto";
import {ShopWindow} from "./shop-window.entity";

@EntityRepository(ShopWindow)
export class ShopWindowRepository extends Repository<ShopWindow> {
  async findAll(queryParams: ShopWindowQueryParams): Promise<ShopWindow[]> {
    const products = this.find({
      take: queryParams.perPage,
      skip: queryParams.perPage * (queryParams.page - 1),
    });

    return products;
  }
}
