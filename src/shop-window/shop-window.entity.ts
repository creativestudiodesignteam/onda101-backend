import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";

@Entity({name: "oc_product"})
export class ShopWindow {
  @PrimaryGeneratedColumn()
  product_id: number;

  @Column()
  name: string;

  @Column()
  quantity: number;

  @Column()
  image: string;

  @Column()
  price: number;
}
