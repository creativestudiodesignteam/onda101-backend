import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";

@Entity({name: "oc_setting"})
export class Settings {
  @PrimaryGeneratedColumn()
  setting_id: number;

  @Column()
  store_id: string;

  @Column()
  code: number;

  @Column()
  key: string;

  @Column()
  value: number;

  @Column()
  serialized: number;
}
