import {Injectable} from "@nestjs/common";
import {InjectRepository} from "@nestjs/typeorm";
import {SettingsRepository} from "./settings.repository";

@Injectable()
export class SettingsService {
  constructor(
    @InjectRepository(SettingsRepository, "ecommerceConnection")
    private repository: SettingsRepository,
  ) {}

  async findInstallment() {
    const settingsInstallment = await this.repository.findInstallment();
    
    return settingsInstallment;
  }
}
