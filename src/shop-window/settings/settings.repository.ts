import {EntityRepository, Repository} from "typeorm";
import {Settings} from "./settings.entity";

@EntityRepository(Settings)
export class SettingsRepository extends Repository<Settings> {
  async findInstallment(): Promise<Settings> {
    const installment = this.findOne({
      where: {setting_id: 1292, code: "payment_mp_standard"},
    });

    return installment;
  }
}
