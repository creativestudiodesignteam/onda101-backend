import {Test, TestingModule} from "@nestjs/testing";
import {ShopWindowService} from "../shop-window.service";

describe("ShopWindowService", () => {
  let service: ShopWindowService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ShopWindowService],
    }).compile();

    service = module.get<ShopWindowService>(ShopWindowService);
  });

  it("should be defined", () => {
    expect(service).toBeDefined();
  });
});
