import {Test, TestingModule} from "@nestjs/testing";
import {ShopWindowController} from "../shop-window.controller";

describe("ShopWindowController", () => {
  let controller: ShopWindowController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ShopWindowController],
    }).compile();

    controller = module.get<ShopWindowController>(ShopWindowController);
  });

  it("should be defined", () => {
    expect(controller).toBeDefined();
  });
});
