import {Controller, Get, Query} from "@nestjs/common";
import {ValidationPipe} from "src/shared/pipes/validation.pipe";
import {ShopWindowListDto} from "./dto/shop-window-list";
import {ShopWindowQueryParams} from "./dto/shop-window-query-params.dto";
import {ShopWindowService} from "./shop-window.service";

@Controller("shop-window")
export class ShopWindowController {
  constructor(private service: ShopWindowService) {}

  @Get()
  async findAll(
    @Query(new ValidationPipe())
    {page = 1, perPage = 10}: ShopWindowQueryParams,
  ) {
    const products = await this.service.findAll({page, perPage});
    const paginatedList = new ShopWindowListDto(
      products.allProducts,
      page,
      perPage,
      products.installment ?? undefined,
    );

    return paginatedList.getPaginatedData();
  }
}
