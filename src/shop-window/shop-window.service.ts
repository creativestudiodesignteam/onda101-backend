import {Injectable} from "@nestjs/common";
import {InjectRepository} from "@nestjs/typeorm";
import {getConnection} from "typeorm";
import {ShopWindowQueryParams} from "./dto/shop-window-query-params.dto";
import {SettingsService} from "./settings/settings.service";
import {ShopWindowRepository} from "./shop-window.repository";

@Injectable()
export class ShopWindowService {
  constructor(
    @InjectRepository(ShopWindowRepository, "ecommerceConnection")
    private repository: ShopWindowRepository,
    private settingsService: SettingsService,
  ) {}
  async findAll(queryParams: ShopWindowQueryParams) {
    const allProducts = await this.repository.findAll(queryParams);
    const installment = await this.settingsService.findInstallment();

    return {allProducts, installment};
  }
}
