import { MiddlewareConsumer, Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { ConfigModule, ConfigService } from "@nestjs/config";

import { AppController } from "./app.controller";
import { Connection } from "typeorm";
import { AppService } from "./app.service";
import { SharedModule } from "./shared/shared.module";
import { ChallengeModule } from "./challenge/challenge.module";
import { UserModule } from "./user/user.module";
import { ProfileModule } from "./profile/profile.module";
import { AuthModule } from "./auth/auth.module";
import { AccessCodeModule } from "./access-code/access-code.module";
import { CodeReceiverDataModule } from "./code-receiver-data/code-receiver-data.module";
import { ShopWindowModule } from "./shop-window/shop-window.module";
import { dbConnections } from "../dbConnections";
import { BlogModule } from "./blog/blog.module";
import { AuthMiddleware } from "./auth/middleware/auth.middleware";
import { CodeResetStatusModule } from './code-reset-status/code-reset-status.module';
import { CodeResetModule } from './code-reset/code-reset.module';
import { MailSenderService } from "./shared/mail-sender/mail-sender.service";

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    TypeOrmModule.forRoot({
      name: dbConnections[0].name, // TODO: refatorar para importação dinâmica
      type: "mysql",
      port: parseInt(dbConnections[0].port, 10),
      username: dbConnections[0].username,
      password: dbConnections[0].password,
      database: dbConnections[0].database,
      host: dbConnections[0].host,
      entities: dbConnections[0].entities,
    }),
    TypeOrmModule.forRoot({
      name: dbConnections[1].name, // TODO: refatorar para importação dinâmica
      type: "postgres",
      port: parseInt(dbConnections[1].port, 10),
      username: dbConnections[1].username,
      password: dbConnections[1].password,
      database: dbConnections[1].database,
      host: dbConnections[1].host,
      entities: dbConnections[1].entities,
    }),
    SharedModule,
    ChallengeModule,
    UserModule,
    ProfileModule,
    AccessCodeModule,
    CodeReceiverDataModule,
    AuthModule,
    ShopWindowModule,
    BlogModule,
    CodeResetStatusModule,
    CodeResetModule,
    MailSenderService,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(
    private connection: Connection,
    private configService: ConfigService,
  ) { }

  configure(consumer: MiddlewareConsumer) {
    consumer.apply(AuthMiddleware).forRoutes("access-code/generate");
  }
}
