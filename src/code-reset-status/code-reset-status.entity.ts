import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { CodeReset } from "../code-reset/code-reset.entity";

@Entity({ name: "code_reset_status" })
export class CodeResetStatus {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    description: string;

    @OneToMany(
        () => CodeReset,
        code => code.codeStatus,
    )
    codes: CodeReset;
}
