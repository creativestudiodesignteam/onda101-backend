import { Controller } from '@nestjs/common';
import { CodeResetStatusService } from './code-reset-status.service';
import { CreateCodeResetStatusDto } from './dto/create-code-reset-status.dto';

@Controller()
export class CodeResetStatusController {
  // constructor(private readonly codeResetStatusService: CodeResetStatusService) {}

  // @MessagePattern('createCodeResetStatus')
  // create(@Payload() createCodeResetStatusDto: CreateCodeResetStatusDto) {
  //   return this.codeResetStatusService.create(createCodeResetStatusDto);
  // }

  // @MessagePattern('findAllCodeResetStatus')
  // findAll() {
  //   return this.codeResetStatusService.findAll();
  // }

  // @MessagePattern('findOneCodeResetStatus')
  // findOne(@Payload() id: number) {
  //   return this.codeResetStatusService.findOne(id);
  // }

  // @MessagePattern('updateCodeResetStatus')
  // update(@Payload() updateCodeResetStatusDto: UpdateCodeResetStatusDto) {
  //   return this.codeResetStatusService.update(updateCodeResetStatusDto.id, updateCodeResetStatusDto);
  // }

  // @MessagePattern('removeCodeResetStatus')
  // remove(@Payload() id: number) {
  //   return this.codeResetStatusService.remove(id);
  // }
}
