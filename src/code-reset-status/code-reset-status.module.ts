import { Module } from '@nestjs/common';
import { CodeResetStatusService } from './code-reset-status.service';
import { CodeResetStatusController } from './code-reset-status.controller';

@Module({
  controllers: [CodeResetStatusController],
  providers: [CodeResetStatusService]
})
export class CodeResetStatusModule {}
