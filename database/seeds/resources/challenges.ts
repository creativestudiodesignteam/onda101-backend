interface ChallengeTable {
  id: number;
  title: string;
  imperative_phrase: string;
  description: string;
  completion_message: string;
  number: number;
  score: number;
  isExtra: boolean;
  challenge_level_id: number;
}

export const challenges: ChallengeTable[] = [
  {
    id: 1,
    title: "Comece Direito",
    imperative_phrase: "Reflita, medite, e faça uma declaração",
    description:
      "Sente-se em um lugar calmo, feche os olhos e faça uma reflexão se você está realmente disposto a participar desta onda do bem. Estes desafios buscam melhorar sua qualidade de vida através da prática de desafios para o seu bem e o de outras pessoas. Caso você esteja disposto a realizar os 101 desafios e completar essa missão diga em voz alta: “Estou disposto a realizar os 101 desafios do bem para ajudar a mim e a outras pessoas”. Após refletir siga para o DESAFIO 02.",
    completion_message:
      "Parabéns, desafio concluído. Você agora faz parte da onda de desafios que transformará o mundo em um lugar melhor. A meditação e a reflexão ajudam a acalmar e concentrar sua energia em pensamentos positivos em torno de um objetivo. A declaração em voz alta fortalece e ajuda a memória a gravar a informação. Sempre que estiver almejando algum objetivo, diário, financeiro, de hábitos saudáveis, entre outros, medite, reflita e declare em voz alta, esta simples atitude fortalece seu foco na tarefa a desempenhar.",
    number: 1,
    score: 5,
    isExtra: false,
    challenge_level_id: 1,
  },
  {
    id: 2,
    title: "A carta",
    imperative_phrase: "Escreva uma Carta a mão para alguém que você ama.",
    description:
      "Você deve escolher um familiar, um amigo ou alguém que você preze. Esta carta é livre, ou seja, você deve escrever o que vier à sua mente. Fique à vontade para falar o que você sentir no momento em que estiver escrevendo. Elogie, conte alguma história do passado entre vocês, conte planos futuros, demonstre seus sentimentos, faça o que seu coração mandar. Apenas passe o DESAFIO 03 quando a carta for enviada ao destinatário, seja pelo correio, entregue pessoalmente ou por intermédio de terceiros.",
    completion_message:
      "Parabéns, mais um desafio concluído.  Enviar uma carta escrita para alguém demonstra carinho e afeto. Devemos dedicar mais tempo para quem amamos. Isto faz bem para você e para quem recebe este maravilhoso presente. Daqui alguns anos, se possível, encontre essa pessoa e releia esta carta.",
    number: 2,
    score: 5,
    isExtra: false,
    challenge_level_id: 1,
  },
  {
    id: 3,
    title: "Caminhada na Natureza",
    imperative_phrase:
      "Faça uma caminhada ao ar livre estando em contato direto com a natureza e a paisagem.",
    description:
      "Se desloque para um local que você possa observar ou sentir a natureza, seja por estar próximo a ela ou por poder observar a mesma à distância. Contemplar o cenário que envolve uma caminhada ao ar livre é o objetivo. Ao chegar ao local caminhe na velocidade que preferir por pelo menos meia hora e sinta a presença da natureza no ambiente e em você. Respire profundamente o ar sentindo ele entrar e sair do seu corpo. Faça isso algumas vezes. Passe para o DESAFIO 04 ao realizar a caminhada.",
    completion_message:
      "É isso aí, você completou mais um desafio. Estar próximo a natureza ajuda a controlar o emocional, colabora para o autoconhecimento, diminui o estresse, promove o contato com o sol.  O ar fresco e um ambiente mais agradável aumenta inclusive a criatividade e a concentração, entre muitas benfeitorias para a mente e o corpo. Permita-se fazer isso mais vezes durante todo esse processo de transformação e ao longo de sua vida.",
    number: 3,
    score: 5,
    isExtra: false,
    challenge_level_id: 1,
  },
  {
    id: 4,
    title: "A Leitura",
    imperative_phrase: "Comece a ler um livro de seu agrado.",
    description:
      "Escolha um livro interessante que você deverá ler até o final. O livro pode ser escolhido conforme seu desejo e gosto. Foque em temas que lhe interessam e chamem a sua atenção. Se o livro que escolheu não lhe interessar mais, troque por outro de seu agrado. Para seguir para o DESAFIO 05, leia no mínimo dez páginas com atenção e só assim você terá este direito. No entanto, o desafio continua. Para concluir todos os 101 Desafios da Onda você deverá ler este livro até o final. Não deixe para concluir depois: leia um pouco todos os dias.",
    completion_message:
      "Parabéns! Não pense que essa foi fácil, você deve finalizar este livro. A leitura é fundamental. O hábito de ler aumenta o aprendizado e aprimora o vocabulário e a escrita, processo fundamental no desenvolvimento intelectual, que corrobora para a construção de textos e frases. Ler é o caminho para o conhecimento.",
    number: 4,
    score: 5,
    isExtra: false,
    challenge_level_id: 1,
  },
  {
    id: 5,
    title: "O Abraço do Bem",
    imperative_phrase: "Comece a ler um livro de seu agrado.",
    description:
      "Escolha uma pessoa que você goste e dê um abraço forte e afetuoso. Para realizar essa tarefa você não deve mencionar que está participando dos desafios do bem, caso você queira avisar a pessoa, isto deve acontecer apenas depois do abraço. A ação deve durar no mínimo 30 segundos. Sinta toda a energia que emana deste momento e após executar esse abraço do bem passe para o DESAFIO 06.",
    completion_message:
      "Abraço concluído com sucesso. Parabéns! O abraço envolve carinho e afeto e traz benefícios diários, como reduzir o estresse, promover a empatia e muitas vezes pode substituir as palavras. Abraçar alguém que você gosta é uma excelente terapia, acalma, fortalece e pode inclusive estabelecer uma relação de confiança e honestidade.Fará um bem não apenas para você, para outro também. Esta interação é muito mais que meramente física; é um contato real que nos conforta e que deve ser praticado sempre. ",
    number: 5,
    score: 5,
    isExtra: false,
    challenge_level_id: 1,
  },
  {
    id: 6,
    title: "Desenho do Futuro",
    imperative_phrase: "Desenhe como Você se Imagina daqui 10 Anos",
    description:
      "Faça um desenho de como você se imagina daqui dez anos no cenário mais positivo possível. Seja otimista. Esse desenho pode ser um rabisco, conter apenas linhas ou tinta, ou uma mistura de formas de pinturas e desenhos diferentes, como você quiser. O objetivo é fazer uma representação da sua pessoa no futuro. Imagine e desenhe suas conquistas no trabalho, nos estudos, nas questões estéticas e de saúde, lazer, espiritualidade, relacionamento, entre outras. Use sua criatividade, o sonho de hoje pode se tornar a realidade amanhã. Após finalizar siga para o DESAFIO 07. Daqui 10 anos volte a este desenho e se surpreenda. ",
    completion_message:
      "Que ótimo, você tem um desenho do seu futuro e de onde quer chegar, corra atrás disso agora. Essa técnica de desenho te ajuda a mentalizar um futuro positivo e vencedor. Ela colabora para que você imagine um crescimento pessoal e profissional expressado visualmente através de um amanhã muito bom para sua vida. ",
    number: 6,
    score: 5,
    isExtra: false,
    challenge_level_id: 1,
  },
  {
    id: 7,
    title: "Mãos Limpas",
    imperative_phrase: "Lave as mãos para deixá-las bem limpas",
    description:
      "Lave as mãos massageando-as. Entre em contato com a água e faça uma boa lavagem das suas mãos, de forma a massagear as palmas e molhar os pulsos com água.  Execute a massagem com calma e sem pressa de acabar o processo. Utilize água em temperatura ambiente e sabão, esfregando bem. Pressione as mãos de forma a sentir a força de uma mão na outra. Se preferir, após o primeiro enxágue, passe sabão outra vez. Deixe cair bastante água de forma a limpar toda a espuma. Procure durante o processo massagear, além das palmas, os pulsos de forma sutil e as pontas dos dedos, conforme a água for caindo. Faça isso por pelo menos um minuto e vá para o DESAFIO 08. ",
    completion_message:
      "Parabéns, você está relaxado e com as mãos limpas. Lavar as mãos é muita mais que um ato simples do seu dia, a higienização delas tem grande importância e é uma medida de prevenção contra várias doenças, podendo inclusive, salvar vidas. A grande surpresa é quando as pessoas descobrem os prazeres de uma boa massagem nas mãos: ela acalma, traz sensação de prazer e alivia as tensões. O contato com a água também é fundamental durante o processo de lavagem e massagem, trazendo uma sensação de paz, relaxamento e limpeza. Procure lavar as mãos várias vezes ao dia, você estará fazendo um bem gigantesco para o seu corpo e ajudando diretamente o próximo na prevenção de doenças.",
    number: 7,
    score: 5,
    isExtra: false,
    challenge_level_id: 1,
  },
  {
    id: 8,
    title: "Além das Fronteiras",
    imperative_phrase: "Aprenda a falar palavras em cinco idiomas diferentes",
    description:
      "Aprenda a falar “OI”, “Obrigado” e “Tchau” em cinco (05) línguas diferentes. Procure descobrir a pronúncia correta e a escrita dessas palavras, depois declare em voz alta cada uma delas nos respectivos idiomas. Para a execução deste desafio você pode utilizar ferramentas da internet e sites de busca e tradução, dicionários, ou conversar com pessoas que falam outras línguas. Aprenda as palavras. Você só deverá passar para o DESAFIO 09 quando conseguir pronunciar as três palavras nas cinco línguas diferentes com os olhos fechados.",
    completion_message:
      "Oi, Obrigado e Tchau, são palavras importantíssimas de qualquer idioma. Com elas você começará, agradecerá e terminará qualquer tipo de interação humana. Aprender outro idioma leva tempo e deve ser algo contínuo, entretanto, ter o domínio de algumas palavras iniciais podem aguçar seu desejo por aprender algumas línguas. Estamos plantando a sementinha em você, busque aprender outros idiomas. Ter o conhecimento de outra língua e conseguir interagir com pessoas de outras nacionalidades e culturas é extremamente importante e colabora para o seu desenvolvimento pessoal e profissional. ",
    number: 8,
    score: 5,
    isExtra: false,
    challenge_level_id: 1,
  },
  {
    id: 9,
    title: "Prato Colorido",
    imperative_phrase: "Aprenda a falar palavras em cinco idiomas diferentes",
    description:
      "Faça um prato colorido no almoço com mais de (07) cores diferentes e se alimente de forma leve. Quanto mais colorido mais saudável. Faça uma alimentação com proteínas, carboidratos e gorduras de forma equilibrada. Para compor o prato e a variação de cores capriche nas verduras e nos legumes e, se possível, reduza o consuma de alimentos processados. A intenção desta tarefa é que você faça uma alimentação leve e saudável não só no almoço, mas durante todo o dia. Siga este conceito após a refeição do almoço para todo seu dia. Consciente de que se alimentou de forma saudável o desafio estará concluído e você já poderá seguir para o DESAFIO 10.",
    completion_message:
      "Parabéns, você fez uma alimentação saudável. As cores do seu prato têm muito a dizer sobre a qualidade da sua alimentação. Quanto mais variedades de cores mais nutrientes e mais composto fitoquímicos estarão presentes na sua refeição, evitando a monotonia alimentar e trazendo benefícios para o organismo. A partir deste desafio, comece a reparar às cores dos alimentos que você tem em seu prato. Essa pode ser uma das maneiras de saber se você está em dia com a sua alimentação e consequentemente a sua saúde. Tenha o habito também de consumir legumes, verduras e frutas. Você tem a obrigação de se alimentar bem sempre.",
    number: 9,
    score: 5,
    isExtra: false,
    challenge_level_id: 1,
  },
  {
    id: 10,
    title: "As Qualidades de um Amigo",
    imperative_phrase:
      "Escreva dois textos com adjetivos qualitativos destinados para dois amigos diferentes",
    description:
      "Escreva dez (10) adjetivos qualitativos, como legal, feliz, alegre, bonito, em papéis diferentes. Selecione cinco (05) destes adjetivos e formule um texto ou uma frase para algum amigo. Com os outros cinco (05) adjetivos restantes formule outro texto ou frase para outro amigo que você teve pouco contato nos últimos tempos. Os adjetivos devem estar inseridos ao longo do texto e nunca um seguido do outro, de forma que tenhamos mais de duas palavras separando-os. Os textos são livres e não possuem limite de tamanho, escreva o que quiser para as pessoas, desde que pelo menos 05 destes adjetivos estejam presentes. Você pode utilizar mais adjetivos qualitativos distinto dos dez (10) adjetivos iniciais se você quiser. Use a criatividade. Para finalizar entregue os textos para os amigos e vá para o DESAFIO 11. Guarde os papéis com os adjetivos escolhidos até o final dos 101 desafios.",
    completion_message:
      "Parabéns, você fortaleceu a amizade com um amigo próximo e se aproximou de alguém que você estava distante, mas considera importante. Apontar as qualidades do próximo traz benefícios de reflexão sobre como amigos possuem potencialidades e que, em muitos casos, elas podem contribuir para a sua vida emocional e a sua felicidade. Estar ao lado de pessoas com qualidades positivas e que vibram em uma energia boa traz para você e para o ambiente uma grande magia: os sorrisos são mais sinceros e a alegria contagia. Portanto, se aproxime de pessoas que te agreguem e te façam crescer como ser humano. Seja recíproco e agregue valor ao que puder. ",
    number: 10,
    score: 5,
    isExtra: false,
    challenge_level_id: 1,
  },
  {
    id: 11,
    title: "Risada Marcada",
    imperative_phrase: "Faça alguém rir em um horário inesperado.",
    description:
      "Coloque o despertador para tocar em um horário aleatório. Na hora que o despertador tocar seja criativo e faça alguém rir. O objetivo é ter momentos de risos em horários incomuns. Você pode contar uma piada, fazer uma careta ou o que vier na sua cabeça para tirar o riso de uma ou mais pessoas. Faça o desafio no lugar que estiver no momento que o despertador tocar. Portanto, se programe para colocar o alarme em um horário inesperado. Ao tocar faça alguém rir e siga para o DESAFIO 12.",
    completion_message:
      "Parabéns, você acaba de melhor o ambiente que está. Fazer um amigo ou algumas pessoas rirem é algo muito prazeroso. O bom humor é contagiante. Um riso compartilhado aumenta a felicidade, alegra o ambiente e une ainda mais as pessoas. Melhor ainda quando acontecer em um momento que nem você estivesse esperando por isso. O simples fato de você ter que fazer alguém rir é um desafio pessoal que pode te fazer dar risada sozinho da situação. O riso desencadeia mudanças físicas saudáveis ao organismo: fortalece o sistema imunológico, protegendo contra os efeitos do estresse, aumenta a energia e o estado de felicidade. Faça alguém rir: melhore o seu dia e os ambientes que frequenta e ao interagir com as pessoas. ",
    number: 11,
    score: 5,
    isExtra: false,
    challenge_level_id: 2,
  },
  {
    id: 12,
    title: "Eu te amo",
    imperative_phrase: 'Diga "Eu te Amo" par alguém que você ama.',
    description:
      "Diga pessoalmente para uma pessoa que você ame muito: “Eu te Amo”. Pode ser em qualquer momento do dia. Lembre-se, dizer “eu te amo” é fácil, difícil é amar de verdade. Só diga se for verdadeiro e profundo. A frase deve ser dita em voz alta e clara, demonstrando todo o sentimento que isto representa. Se possível reforce a frase mais de uma vez no momento, para ficar bem claro para pessoa o quanto ela é importante para você. Após este momento você estará pronto para ir ao DESAFIO 13.  ",
    completion_message:
      "A frase eu te amo tem muita força. Quem ouve essa frase na maioria das vezes vai lembrar deste momento e nunca mais esquecer. Quando você diz “Eu te amo” de maneira sincera, muita coisa está em jogo, muito sentimento, muita convivência, muitas atitudes e demonstrações de afeto e carinho. Não torne esta frase banal, diga sempre com sinceridade e verdade. Ao dizer isso para alguém que você ame você fortalece ainda mais o carinho que você tem por essa pessoa. Amar é reconhecer que as pessoas têm defeitos e virtudes, é aceitar as pessoas como elas são. Este desafio te coloca no mais alto valor de sentimento existente, dizer eu te amo é transformador, é libertador. ",
    number: 12,
    score: 5,
    isExtra: false,
    challenge_level_id: 2,
  },
  {
    id: 13,
    title: "Desapegue",
    imperative_phrase: "Faça uma doação de uma peça de roupa",
    description:
      "O DESAFIO 12 foi uma preparação para este desafio, pois só quem ama é capaz de ajudar o próximo.  Faça uma doação de uma peça de roupa, seja para uma instituição, para uma entidade, para algum morador de rua, para uma associação que auxilie no cuidado com pessoas carentes, ou alguém que você queira ajudar. Simplesmente desapegue-se e ajude os mais necessitados. Após realizar a doação siga para o DESAFIO 14.",
    completion_message:
      "Parabéns, ao praticar esse desafio você além de ajudar alguém, começa a perceber que bens materiais não são tão importantes assim na sua vida. O mais importante: você aprende a valorizar o outro e ter empatia. Doar traz renovação e aumenta o bem-estar; libera espaço útil, retirando coisas inúteis e não tão importantes para você e que na utilidade do próximo pode ser mais importante; traz sentimento de satisfação e realização, juntamente com a gratidão do outro; é ecologicamente correto e difunde a ideia do reuso; ou seja, doar beneficia você, os outros e o planeta. Seja um doador e ajude o mundo a se transformar em um local melhor.",
    number: 13,
    score: 5,
    isExtra: false,
    challenge_level_id: 2,
  },
  {
    id: 14,
    title: "A Foto",
    imperative_phrase:
      "Faça uma foto atual semelhante a uma foto que você possua",
    description:
      "Primeiramente, olhe algumas fotos de momentos que você estava feliz e se recorda muito. Após analisar as fotos selecione uma delas para fazer um novo registro. Faça uma meditação e uma reflexão sobre aquele momento. Tente lembrar os momentos que antecederam o registro da foto. Ela deve ser tomada em um local que você possa ir atualmente e fazer um novo retrato semelhante ao primeiro. Se o registro foi tomado com mais pessoas tente levá-los para o local. Posicione-se de forma parecida, sozinho ou com as pessoas que estavam na foto, tentando imitar aquele momento e tire uma nova foto. Após este ato compare a foto do passado e a foto atual. Siga para o DESAFIO 15.",
    completion_message:
      "Parabéns, mais um desafio concluído. Reviver um momento prazeroso da nossa vida é muito legal, principalmente quando podemos ir a um local grandes lembranças. Registrar esse momento novamente é uma coisa linda. Comparar a transformação das pessoas no tempo possibilita uma reflexão de que muita coisa mudou até o momento atual. Daqui 10 anos tente voltar ao mesmo local e fazer um novo registro como este.",
    number: 14,
    score: 5,
    isExtra: false,
    challenge_level_id: 2,
  },
  {
    id: 15,
    title: "Nostalgia",
    imperative_phrase: "Ouça uma música que te marcou no passado",
    description:
      "Ouça uma música que te marcou e que você gostava muito na sua infância. Lembre-se de um som que você adorava escutar. Esta música pode ser de um desenho animado que você assistia ou uma melodia que você ouvia alguém cantar, uma canção que tocava nas rádios, na sua casa ou na sua escola. Escolha uma música que te traga toda a nostalgia do momento e te faça relembrar o quanto você gostava daquele som. Após ouvir a música siga para o DESAFIO 16.",
    completion_message:
      "Parabéns! Você escutou uma música que te marcou e voltou a ser criança, pelo menos por alguns minutos. Relembrar os nossos momentos de infância faz com que voltemos anos da nossa vida para uma época de muito divertimento e pouca preocupação, em que o brincar era a principal atividade. Algumas músicas tem o poder de nos jogar lá no passado e trazer toda a alegria e felicidade que a sua criança possuía. A música além da capacidade de nos fazer voltar atrás no tempo, ela também pode afetar positivamente sua vida: aliviando sua ansiedade, promovendo o bom humor, motivar e impulsiona o desempenho, emociona com sua beleza e melodia, entre muitas outras qualidades. Escute música.",
    number: 15,
    score: 5,
    isExtra: false,
    challenge_level_id: 2,
  },
  {
    id: 16,
    title: "Valorização do Turismo e Cultura Local",
    imperative_phrase: "Visite um ponto turístico de sua cidade.",
    description:
      "Vá a um ponto turístico ou um local muito bonito da cidade que você mora. O local não precisa ser necessariamente algo que tenha muitos visitantes, mas sim, um local que tenha cultura, história ou simplesmente uma bela paisagem. Busque um espaço que tenha representação e beleza para você, não para os outros. Portanto, a escolha é livre e particular. Vá até o local e faça o registro de uma fotografia e publique. Após a publicação você está pronto para o DESAFIO 17",
    completion_message:
      "Parabéns, você teve a experiência de visitar um local que por estar diante de você, provavelmente você deva ter ido poucas vezes, ou nunca. Isto mostra que em muitos casos as coisas boas estão próximas da gente e não percebemos. Existem lugares belos e incríveis diante de você. Busque sempre ir a estes locais. Não há a necessidade de viajar sempre para que possas visitar um ponto que te traga uma bela vista para observar, ou história para aprender, ou simplesmente paz para sentir. Este espaço pode estar diante de seus olhos. Dê valor ao que está próximo de você também.",
    number: 16,
    score: 5,
    isExtra: false,
    challenge_level_id: 2,
  },
  {
    id: 17,
    title: "Experiência Vivida",
    imperative_phrase: "Relembre uma experiência que te marcou muito",
    description:
      "Procure lembrar-se de uma experiência vivida por você que te fez aprender muito ao longo de sua vida e evoluir. Esta experiência pode ter sido algo bom ou ruim, mas que te fez aprender uma lição a respeito de algo importante. Lembre-se bem do fato e reflita sobre o que aconteceu. Para finalizar o desafio, e seguir para o DESAFIO 18, conte essa experiência para alguém, por ligação, pessoalmente, ou como preferir, e relate principalmente como esta experiência que você viveu te ajudou, ela também pode ser uma lição e ter um aprendizado para o próximo.",
    completion_message:
      "Parabéns, você acaba de refletir sobre um momento importante de sua vida que te fez aprender e evoluir. Compartilhar esse momento com alguém pode contribuir para que o outro também tire uma lição. A vida é um aprendizado e sempre estamos aprendendo algo novo. Estamos em constante transformação. Quem você foi, quem você é, e quem você será fazem parte de um processo da qual as experiências vividas são fundamentais e contribuem diretamente para a sua evolução. A vida te ensina algo hoje e na maioria das vezes você aprende amanhã.",
    number: 17,
    score: 5,
    isExtra: false,
    challenge_level_id: 2,
  },
  {
    id: 18,
    title: "Treino e Energia",
    imperative_phrase: "Exercite-se e tome um suco saudável",
    description:
      "Faça um exercício físico aeróbico e tome um suco verde, detox ou uma batida de frutas. Exercite-se e alimente-se para ter um grande dia e uma boa noite de sono. Com relação ao exercício você pode optar por caminhar, correr, andar de bicicleta, entre outros. Já com relação ao suco ou batida, você pode escolher as misturas desde que contenha pelo menos duas (02) composições, como por exemplo, mamão com laranja, cenoura com maça, ou mais composições como água de coco com abacaxi, couve, laranja e gengibre. Siga para o DESAFIO 19.",
    completion_message:
      "Parabéns, você se alimentou e se exercitou. Exercitar-se é uma das coisas mais importantes para o organismo e para isso você deve se alimentar de forma correta. O exercício aeróbico, principalmente, trabalha o coração, libera endorfina, e é responsável por colaborar no combate ao estresse, a depressão e a ansiedade. Os sucos são fundamentais para ajudar a uma alimentação saudável e estimular suas atividades diárias pelo poder energético que possuem. São responsáveis por aumentar a concentração, aumentar o nível de energia para atividades, melhorar a digestão e o sistema imunológico, inclusive, melhora o sono. Tenha o hábito de se alimentar bem e praticar atividades físicas. ",
    number: 18,
    score: 5,
    isExtra: false,
    challenge_level_id: 2,
  },
  {
    id: 19,
    title: "Se presenteie",
    imperative_phrase: "Dê um presente para você mesmo",
    description:
      "Faça-se bem e encontre algo que você deseje e seja inovador para presentear-se. Descanse, ofereça-se momentos de serenidade, dê a você a oportunidade de receber algo que você anseie, dê tempo para fazer algo que você queira. Não espere que outros te deem aquilo que você mesmo pode se oferecer. O presente muitas vezes não envolve bens materiais, muitas vezes são imateriais, mas se estiver precisando de algo material ao seu alcance se presenteie. Um abraço pode ser um presente dado e recebido. Uma memória pode ser um presente para a alma. Seja criativo e receba algo que goste. O importante do desafio é sentir-se presenteado. Amar-se antes de amar alguém. Após se presentear parta para o DESAFIO 20.",
    completion_message:
      "Todos precisamos receber carinho. Muitas vezes o carinho pode vir de nós mesmos. Este desafio mostra que presentear-se é uma forma de cuidar de si mesmo. Dar-se um presente é algo muito importante e que traz alegria e felicidade. Durante sua vida presenteie-se de amor, de carinho, de memórias, de divertimentos, de tempo, cuida de você.",
    number: 19,
    score: 5,
    isExtra: false,
    challenge_level_id: 2,
  },
  {
    id: 20,
    title: "Pese e Separe seu Lixo",
    imperative_phrase: "Pese e Separe o lixo que consumir durante 24 horas",
    description:
      "Você deve separar os resíduos de maneira adequada. É preciso ficar atento, não misture recicláveis com orgânicos. Lave as embalagens do tipo longa vida, latas, garrafas e frascos de vidro e plástico. Após separar o lixo que você produziu durante o dia, pese cada quantidade e some o montante total de lixo produzido. Anote ou marque o peso total de forma que você não o esqueça, você utilizará esse valor futuramente em outro desafio. Agora que você já sabe o tanto de lixo aproximado que produz diariamente, pense em como diminui-lo a partir de hoje e siga para o DESAFIO 21.",
    completion_message:
      "Parabéns, você acaba de realizar uma das tarefas mais importantes. O lixo é um dos principais problemas para o nosso planeta. A consciência da quantidade de lixo que produzimos e a separação para reciclagem são fundamentais para que possamos diminuir a quantidade de resíduos que produzimos e facilitarmos a reciclagem dos mesmos. A reciclagem do lixo é fundamental para reduzir o impacto sobre o meio ambiente, diminuindo a retirada de matéria prima da natureza, gerando economia de água e energia, reduzindo as áreas de armazenagem e disposição inadequadas do lixo, além de outros problemas relacionados a ele. Você é responsável pelo que consome, busque separar e diminuir a quantidade de resíduos que você produz",
    number: 20,
    score: 5,
    isExtra: false,
    challenge_level_id: 2,
  },
  {
    id: 111,
    title: "Lorem ipsum dolor sit amet",
    imperative_phrase:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tempor pulvinar enim, et vulputate lacus hendrerit quis. Duis rhoncus aliquet lorem, ac tincidunt mi iaculis vitae. Vivamus vel malesuada odio. Donec pharetra lobortis libero, eu placerat magna consectetur id. Aenean lacinia vulputate diam. Sed rhoncus diam nisl, at posuere est accumsan non. Integer tellus dolor, condimentum nec ullamcorper eu, efficitur sit amet dolor. Sed iaculis molestie mollis. Morbi id purus eu eros viverra porta vitae sed arcu. Aliquam aliquam non quam eu placerat. Etiam ut odio dui. .",
    completion_message:
      "Nam ornare ullamcorper sagittis. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec et augue id leo finibus placerat nec eget augue. Phasellus quis auctor neque. Etiam volutpat libero ut tortor pretium tempus. Donec non volutpat est, sed aliquet quam. Nam nec scelerisque urna. Aenean venenatis dignissim finibus. Vestibulum tincidunt vestibulum velit, at euismod lorem posuere bibendum. Proin ut orci vitae lorem pharetra rhoncus. Quisque blandit malesuada neque pharetra sagittis. Maecenas magna nisi, porttitor eu turpis eget, congue iaculis ante. Suspendisse id vulputate velit. ",
    number: 111,
    score: 15,
    isExtra: true,
    challenge_level_id: 1,
  },
  {
    id: 112,
    title: "Lorem ipsum dolor sit amet",
    imperative_phrase:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tempor pulvinar enim, et vulputate lacus hendrerit quis. Duis rhoncus aliquet lorem, ac tincidunt mi iaculis vitae. Vivamus vel malesuada odio. Donec pharetra lobortis libero, eu placerat magna consectetur id. Aenean lacinia vulputate diam. Sed rhoncus diam nisl, at posuere est accumsan non. Integer tellus dolor, condimentum nec ullamcorper eu, efficitur sit amet dolor. Sed iaculis molestie mollis. Morbi id purus eu eros viverra porta vitae sed arcu. Aliquam aliquam non quam eu placerat. Etiam ut odio dui. .",
    completion_message:
      "Nam ornare ullamcorper sagittis. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec et augue id leo finibus placerat nec eget augue. Phasellus quis auctor neque. Etiam volutpat libero ut tortor pretium tempus. Donec non volutpat est, sed aliquet quam. Nam nec scelerisque urna. Aenean venenatis dignissim finibus. Vestibulum tincidunt vestibulum velit, at euismod lorem posuere bibendum. Proin ut orci vitae lorem pharetra rhoncus. Quisque blandit malesuada neque pharetra sagittis. Maecenas magna nisi, porttitor eu turpis eget, congue iaculis ante. Suspendisse id vulputate velit. ",
    number: 112,
    score: 15,
    isExtra: true,
    challenge_level_id: 1,
  },
];
