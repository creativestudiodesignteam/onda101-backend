import * as Knex from "knex";

export async function seed(knex: Knex): Promise<void> {
  await knex("access_code_status").del();

  await knex("access_code_status").insert([
    {id: 1, description: "pending"},
    {id: 2, description: "active"},
    {id: 3, description: "invalid"},
  ]);
}
