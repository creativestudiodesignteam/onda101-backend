import * as Knex from "knex";

export async function seed(knex: Knex): Promise<void> {
  // Deletes ALL existing entries
  await knex("groups").del();

  // Inserts seed entries
  await knex("groups").insert([
    {
      id: 1,
      name: "Marola",
      wave_number: 1,
      created_at: knex.fn.now(),
      //   updated_at: async () => await knex.fn.now(),
    },
    {
      id: 2,
      name: "Ondinha",
      wave_number: 2,
      created_at: knex.fn.now(),
    },
    {
      id: 3,
      name: "Big Wave",
      wave_number: 3,
      //   updated_at: async () => await knex.fn.now(),
    },
  ]);
}
