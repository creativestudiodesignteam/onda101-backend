import * as Knex from "knex";

export async function seed(knex: Knex): Promise<void> {
  await knex("challenge_status").del();

  await knex("challenge_status").insert([
    {id: 1, description: "incomplete"},
    {id: 2, description: "started"},
    {id: 3, description: "done"},
  ]);
}
