import * as Knex from "knex";
import {challenges} from "./resources/challenges";

export async function seed(knex: Knex): Promise<void> {
  await knex("challenges").del();

  await knex("challenges").insert(challenges);
}
