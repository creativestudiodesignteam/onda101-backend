import * as Knex from "knex";

export async function seed(knex: Knex): Promise<void> {
  await knex("challenge_levels").del();

  await knex("challenge_levels").insert([
    {id: 1, description: "Nível 1", level_number: 1},
    {id: 2, description: "Nível 2", level_number: 2},
    {id: 3, description: "Nível 3", level_number: 3},
  ]);
}
