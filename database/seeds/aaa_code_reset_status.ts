import * as Knex from "knex";

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex("code_reset_status").del();

    // Inserts seed entries
    await knex("code_reset_status").insert([
        { id: 1, description: "active" },
        { id: 2, description: "used" },
        { id: 3, description: "inative" }
    ]);
};
