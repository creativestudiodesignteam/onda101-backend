import * as Knex from "knex";

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable("code_receivers_data", table => {
    table.increments("id").primary();
    table.string("known_receiver_name");
    table.string("code_requester_email", 100).notNullable();
    table
      .integer("app_access_code_id")
      .references("id")
      .inTable("app_access_codes")
      .notNullable();
    table
      .boolean("has_welcome_message")
      .notNullable()
      .defaultTo(false);
  });
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable("code_receivers_data");
}
