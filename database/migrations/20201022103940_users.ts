import * as Knex from "knex";

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable("users", table => {
    table.increments("id").primary();
    table.string("username", 80).notNullable();
    table.string("email", 80).notNullable();
    table.string("password", 80).notNullable();
    table.timestamps(undefined, true);
  });
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable("users");
}
