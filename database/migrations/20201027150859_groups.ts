import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable("groups", table => {
        table.increments("id").primary();
        table.string("name", 250).notNullable();
        table.integer("wave_number").notNullable();
        table.timestamps(undefined, true);
    });
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable("groups");
}

