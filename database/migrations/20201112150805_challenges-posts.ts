import * as Knex from "knex";

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable("challenges_posts", table => {
    table.increments("id").primary();
    table.string("name_file").notNullable();
    table
      .integer("challenge_id")
      .references("id")
      .inTable("challenges")
      .onDelete("cascade")
      .notNullable();
    table
      .integer("profile_id ")
      .references("id")
      .inTable("profiles")
      .onDelete("cascade")
      .notNullable();
    table
      .dateTime("created_at")
      .notNullable()
      .defaultTo(knex.raw("CURRENT_TIMESTAMP"));
  });
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable("challenges_posts");
}
