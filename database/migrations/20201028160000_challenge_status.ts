import * as Knex from "knex";

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable("challenge_status", table => {
    table.increments().primary();
    table.string("description", 120);
  });
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable("challenge_status");
}
