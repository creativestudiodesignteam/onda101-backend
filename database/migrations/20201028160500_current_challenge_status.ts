import * as Knex from "knex";

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable("current_challenge_status", table => {
    table.increments().primary();
    table
      .integer("challenge_id")
      .references("id")
      .inTable("challenges")
      .notNullable();
    table
      .integer("profile_id")
      .references("id")
      .inTable("profiles")
      .notNullable();
    table
      .integer("challenge_status_id")
      .references("id")
      .inTable("challenge_status")
      .notNullable();
    table.timestamps(undefined, true);
  });
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable("current_challenge_status");
}
