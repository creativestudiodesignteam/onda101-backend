import * as Knex from "knex";

export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable("code_reset_status", table => {
        table.increments("id").primary();
        table.string("description", 45).notNullable();
        table.timestamps(undefined, true);
    });
}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable("code_reset_status");
}
