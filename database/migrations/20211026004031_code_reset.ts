import * as Knex from "knex";

export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable("code_reset", table => {
        table.increments("id").primary();
        table.string("code").notNullable();
        table
            .dateTime("created_at")
            .notNullable()
            .defaultTo(knex.fn.now());
        table.dateTime("update_at")
            .notNullable()
            .defaultTo(knex.fn.now());
        table.dateTime("due_date")
            .notNullable()
            .defaultTo(knex.fn.now());
        table
            .integer("code_reset_status_id")
            .references("id")
            .inTable("code_reset_status");
        table
            .integer("user_id")
            .references("id")
            .inTable("users");
        //table.timestamps(undefined, true);
    });
}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable("code_reset");
}
