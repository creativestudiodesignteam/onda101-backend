import * as Knex from "knex";

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable("app_access_codes", table => {
    table.increments("id").primary();
    table.string("value").notNullable();
    table
      .dateTime("created_at")
      .notNullable()
      .defaultTo(knex.fn.now());
    table.dateTime("due_date").notNullable();
    table
      .integer("access_code_status_id")
      .references("id")
      .inTable("access_code_status");
  });
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable("app_access_codes");
}
