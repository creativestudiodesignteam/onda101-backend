import * as Knex from "knex";

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable("access_code_status", table => {
    table.increments("id").primary();
    table.string("description", 45).notNullable();
  });
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable("access_code_status");
}
