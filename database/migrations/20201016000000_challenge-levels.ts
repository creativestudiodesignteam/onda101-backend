import * as Knex from "knex";

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable("challenge_levels", table => {
    table.increments().primary();
    table.string("description", 80).notNullable();
    table.integer("level_number").notNullable();
  });
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable("challenge_levels");
}
