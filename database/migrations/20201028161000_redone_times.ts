import * as Knex from "knex";

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable("redone_times", table => {
    table.increments().primary();
    table
      .integer("profile_id")
      .references("id")
      .inTable("profiles")
      .notNullable();
    table
      .integer("challenge_id")
      .references("id")
      .inTable("challenges")
      .notNullable();
    table
      .dateTime("done_at")
      .notNullable()
      .defaultTo(knex.fn.now());
  });
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable("redone_times");
}
