import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable("completed_challenges", table => {
        table.increments("id").primary();
        table.dateTime("first_time_done_at").notNullable();
        table.dateTime("last_time_done_at");
        table.integer("redone_times").defaultTo(0).notNullable();
        table.integer('challenge_id').references('id').inTable('challenges').onDelete("cascade").notNullable();
        table.integer('profile_id ').references('id').inTable('profiles').onDelete("cascade").notNullable();
    });
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable("completed_challenges");
}

