import * as Knex from "knex";

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable("challenge_ratings", table => {
    table.increments("id").primary();
    table
      .integer("profile_id")
      .references("id")
      .inTable("profiles")
      .notNullable();
    table
      .integer("challenge_id")
      .references("id")
      .inTable("challenges")
      .notNullable();
    table.integer("rating").notNullable();
    table.timestamps(undefined, true);
  });
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable("challenge_ratings");
}
