import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable("challenge_attachments", table => {
        table.increments("id").primary();
        table.string("description", 250);
        table.string("value", 250).notNullable();
        table.integer('challenge_id').references('id').inTable('challenges').onDelete("cascade").notNullable();
        table.timestamps(undefined, true);
    });
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable("challenge_attachments");
}

