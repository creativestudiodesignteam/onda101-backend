import * as Knex from "knex";

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable("profiles", table => {
    table.increments("id").primary();
    table.string("first_name", 250).notNullable();
    table.string("last_name", 250).notNullable();
    table.date("birth_date").notNullable();
    table.string("avatar");
    table
      .integer("user_id")
      .references("id")
      .inTable("users")
      .onDelete("cascade")
      .notNullable();
    table
      .integer("group_id")
      .references("id")
      .inTable("groups")
      .onDelete("set null")
      .notNullable();
    table
      .integer("total_score")
      .notNullable()
      .defaultTo(0);
    table
      .integer("challenge_level_id")
      .references("id")
      .inTable("challenge_levels")
      .notNullable();
    table.timestamps(undefined, true);
  });
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable("profiles");
}
