import * as Knex from "knex";

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable("challenges", table => {
    table.increments("id").primary();
    table.string("title", 45).notNullable();
    table.string("imperative_phrase", 250).notNullable();
    table.text("description").notNullable();
    table.text("completion_message").notNullable();
    table.integer("number").notNullable();
    table.integer("score").notNullable();
    table.boolean("isExtra").notNullable();
    table
      .integer("challenge_level_id")
      .references("id")
      .inTable("challenge_levels")
      .notNullable();
    table.timestamps(undefined, true);
  });
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable("challenges");
}
